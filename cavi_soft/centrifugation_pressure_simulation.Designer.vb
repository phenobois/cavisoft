﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class centrifugation_pressure_simulation
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.RichTextBox_centri_rpm = New System.Windows.Forms.RichTextBox()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.RichTextBox_centri_tension = New System.Windows.Forms.RichTextBox()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.Label_opticcalibration = New System.Windows.Forms.Label()
        Me.TextBox_opticcalibration = New System.Windows.Forms.TextBox()
        Me.Button_compute = New System.Windows.Forms.Button()
        Me.Label_arrow = New System.Windows.Forms.Label()
        Me.ComboBox2 = New System.Windows.Forms.ComboBox()
        Me.SuspendLayout()
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label7.Location = New System.Drawing.Point(61, 50)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(111, 13)
        Me.Label7.TabIndex = 72
        Me.Label7.Text = "Nb rotation per minute"
        '
        'RichTextBox_centri_rpm
        '
        Me.RichTextBox_centri_rpm.BackColor = System.Drawing.Color.Cornsilk
        Me.RichTextBox_centri_rpm.Font = New System.Drawing.Font("Microsoft Sans Serif", 40.0!)
        Me.RichTextBox_centri_rpm.ForeColor = System.Drawing.SystemColors.WindowText
        Me.RichTextBox_centri_rpm.Location = New System.Drawing.Point(64, 72)
        Me.RichTextBox_centri_rpm.Name = "RichTextBox_centri_rpm"
        Me.RichTextBox_centri_rpm.Size = New System.Drawing.Size(171, 73)
        Me.RichTextBox_centri_rpm.TabIndex = 71
        Me.RichTextBox_centri_rpm.TabStop = False
        Me.RichTextBox_centri_rpm.Text = "0"
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label19.Location = New System.Drawing.Point(295, 50)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(146, 13)
        Me.Label19.TabIndex = 73
        Me.Label19.Text = "Centrifugation pressure (MPa)"
        '
        'RichTextBox_centri_tension
        '
        Me.RichTextBox_centri_tension.BackColor = System.Drawing.Color.PaleGoldenrod
        Me.RichTextBox_centri_tension.Font = New System.Drawing.Font("Microsoft Sans Serif", 40.0!)
        Me.RichTextBox_centri_tension.ForeColor = System.Drawing.SystemColors.WindowText
        Me.RichTextBox_centri_tension.Location = New System.Drawing.Point(311, 72)
        Me.RichTextBox_centri_tension.Name = "RichTextBox_centri_tension"
        Me.RichTextBox_centri_tension.ReadOnly = True
        Me.RichTextBox_centri_tension.Size = New System.Drawing.Size(200, 73)
        Me.RichTextBox_centri_tension.TabIndex = 74
        Me.RichTextBox_centri_tension.TabStop = False
        Me.RichTextBox_centri_tension.Text = "--"
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(64, 185)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(100, 20)
        Me.TextBox1.TabIndex = 75
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(61, 169)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(99, 13)
        Me.Label1.TabIndex = 76
        Me.Label1.Text = "Rotor diameter (2R)"
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(434, 328)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(77, 34)
        Me.Button1.TabIndex = 77
        Me.Button1.Text = "Exit"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label2.Location = New System.Drawing.Point(295, 169)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(158, 13)
        Me.Label2.TabIndex = 79
        Me.Label2.Text = "Minimum meniscus position pixel"
        '
        'TextBox2
        '
        Me.TextBox2.Location = New System.Drawing.Point(298, 185)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(100, 20)
        Me.TextBox2.TabIndex = 78
        '
        'Label_opticcalibration
        '
        Me.Label_opticcalibration.AutoSize = True
        Me.Label_opticcalibration.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label_opticcalibration.Location = New System.Drawing.Point(295, 231)
        Me.Label_opticcalibration.Name = "Label_opticcalibration"
        Me.Label_opticcalibration.Size = New System.Drawing.Size(50, 13)
        Me.Label_opticcalibration.TabIndex = 81
        Me.Label_opticcalibration.Text = "Pixel size"
        '
        'TextBox_opticcalibration
        '
        Me.TextBox_opticcalibration.Location = New System.Drawing.Point(298, 247)
        Me.TextBox_opticcalibration.Name = "TextBox_opticcalibration"
        Me.TextBox_opticcalibration.Size = New System.Drawing.Size(100, 20)
        Me.TextBox_opticcalibration.TabIndex = 80
        '
        'Button_compute
        '
        Me.Button_compute.Location = New System.Drawing.Point(64, 214)
        Me.Button_compute.Name = "Button_compute"
        Me.Button_compute.Size = New System.Drawing.Size(171, 46)
        Me.Button_compute.TabIndex = 84
        Me.Button_compute.Text = "COMPUTE"
        Me.Button_compute.UseVisualStyleBackColor = True
        '
        'Label_arrow
        '
        Me.Label_arrow.AutoSize = True
        Me.Label_arrow.Font = New System.Drawing.Font("Microsoft Sans Serif", 26.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label_arrow.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label_arrow.Location = New System.Drawing.Point(239, 92)
        Me.Label_arrow.Name = "Label_arrow"
        Me.Label_arrow.Size = New System.Drawing.Size(59, 39)
        Me.Label_arrow.TabIndex = 85
        Me.Label_arrow.Text = "=>"
        '
        'ComboBox2
        '
        Me.ComboBox2.FormattingEnabled = True
        Me.ComboBox2.Items.AddRange(New Object() {"Rotation to Pressure", "Pressure to Rotation"})
        Me.ComboBox2.Location = New System.Drawing.Point(64, 12)
        Me.ComboBox2.Name = "ComboBox2"
        Me.ComboBox2.Size = New System.Drawing.Size(121, 21)
        Me.ComboBox2.TabIndex = 86
        Me.ComboBox2.Text = "Rotation to Pressure"
        '
        'centrifugation_pressure_simulation
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(534, 390)
        Me.Controls.Add(Me.ComboBox2)
        Me.Controls.Add(Me.Label_arrow)
        Me.Controls.Add(Me.Button_compute)
        Me.Controls.Add(Me.Label_opticcalibration)
        Me.Controls.Add(Me.TextBox_opticcalibration)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.TextBox2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.TextBox1)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.RichTextBox_centri_rpm)
        Me.Controls.Add(Me.Label19)
        Me.Controls.Add(Me.RichTextBox_centri_tension)
        Me.Name = "centrifugation_pressure_simulation"
        Me.Text = "Centrifugation pressure simulation"
        Me.TopMost = True
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents RichTextBox_centri_rpm As System.Windows.Forms.RichTextBox
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents RichTextBox_centri_tension As System.Windows.Forms.RichTextBox
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents Label_opticcalibration As System.Windows.Forms.Label
    Friend WithEvents TextBox_opticcalibration As System.Windows.Forms.TextBox
    Friend WithEvents Button_compute As System.Windows.Forms.Button
    Friend WithEvents Label_arrow As System.Windows.Forms.Label
    Friend WithEvents ComboBox2 As System.Windows.Forms.ComboBox
End Class
