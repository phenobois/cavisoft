﻿Imports System.Windows.Forms



Public Class Dialog_meniscus


    Private Sub OK_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK_Button.Click

        If MaskedTextBox1.Text = "" Then
            MaskedTextBox1.Text = 0
        End If

        MAIN_Form.min_meniscus_position_pixel = MaskedTextBox1.Text
        Me.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.Close()

        MAIN_Form.TextBox_minmeniscusposition.Text = MAIN_Form.min_meniscus_position_pixel

    End Sub

    Private Sub Cancel_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel_Button.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub Dialog6_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If MAIN_Form.TextBox_minmeniscusposition.Text <> "" Then
            MaskedTextBox1.Text = MAIN_Form.TextBox_minmeniscusposition.Text
        End If
        MaskedTextBox1.Select()

    End Sub
End Class
