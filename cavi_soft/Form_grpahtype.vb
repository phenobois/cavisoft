﻿Public Class Form_graphtype

    Private Sub Form_graphtype_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load

        Select Case MAIN_Form.graphetype_string

            Case "PLC_vs_pres"
                RectangleShape1.Visible = True
                RectangleShape2.Visible = False
                RectangleShape3.Visible = False

            Case "cond_vs_pres"
                RectangleShape1.Visible = False
                RectangleShape2.Visible = True
                RectangleShape3.Visible = False
            Case "cond_vs_time"
                RectangleShape1.Visible = False
                RectangleShape2.Visible = False
                RectangleShape3.Visible = False


        End Select


    End Sub

    Private Sub PLC_vs_pres()
        RectangleShape1.Visible = True
        RectangleShape2.Visible = False
        RectangleShape3.Visible = False
        MAIN_Form.graphetype_string = "PLC_vs_pres"
        MAIN_Form.Label_graphtype.Text = "PLC vs pressure "
    End Sub

    Private Sub cond_vs_pres()
        RectangleShape1.Visible = False
        RectangleShape2.Visible = True
        RectangleShape3.Visible = False
        MAIN_Form.graphetype_string = "cond_vs_pres"
        MAIN_Form.Label_graphtype.Text = "conductance vs pressure "

    End Sub

    Private Sub cond_vs_time()
        RectangleShape1.Visible = False
        RectangleShape2.Visible = False
        RectangleShape3.Visible = False
        '  MAIN_Form.graphetype_string = "cond_vs_time"
        '    MAIN_Form.Label_graphtype.Text = "conductance vs time "
        MsgBox("Sorry, not available !")
        MAIN_Form.graphetype_string = "PLC_vs_pres"

    End Sub

    Private Sub PictureBox1_Click(sender As System.Object, e As System.EventArgs) Handles PictureBox1.Click
        PLC_vs_pres()
    End Sub


    Private Sub Label1_Click(sender As System.Object, e As System.EventArgs) Handles Label1.Click
        PLC_vs_pres()
    End Sub

    Private Sub PictureBox2_Click(sender As System.Object, e As System.EventArgs) Handles PictureBox2.Click
        cond_vs_pres()
    End Sub

    Private Sub Label2_Click(sender As System.Object, e As System.EventArgs) Handles Label2.Click
        cond_vs_pres()
    End Sub

    Private Sub PictureBox3_Click(sender As System.Object, e As System.EventArgs) Handles PictureBox3.Click
        cond_vs_time()

    End Sub

    Private Sub Label4_Click(sender As System.Object, e As System.EventArgs) Handles Label4.Click
        cond_vs_time()
    End Sub

    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles Button1.Click
        MAIN_Form.update_graph()
        Me.Close()
    End Sub

  
End Class