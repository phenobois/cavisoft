﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Dialog_compute_PLC
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.OK_Button = New System.Windows.Forms.Button()
        Me.RadioButton_definedvalue = New System.Windows.Forms.RadioButton()
        Me.RadioButton_minimumLPmax_value = New System.Windows.Forms.RadioButton()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.RadioButton_meanLPmax_speedclass = New System.Windows.Forms.RadioButton()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.TextBox_PLC_speed_class = New System.Windows.Forms.TextBox()
        Me.TextBox_Lpmax_integral = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.TextBox_meanLPmax_integral = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.TextBox_PLC_zero_integral = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'OK_Button
        '
        Me.OK_Button.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.OK_Button.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.OK_Button.Location = New System.Drawing.Point(255, 436)
        Me.OK_Button.Name = "OK_Button"
        Me.OK_Button.Size = New System.Drawing.Size(67, 96)
        Me.OK_Button.TabIndex = 3
        Me.OK_Button.Text = "Exit"
        '
        'RadioButton_definedvalue
        '
        Me.RadioButton_definedvalue.AutoSize = True
        Me.RadioButton_definedvalue.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton_definedvalue.Location = New System.Drawing.Point(36, 185)
        Me.RadioButton_definedvalue.Name = "RadioButton_definedvalue"
        Me.RadioButton_definedvalue.Size = New System.Drawing.Size(141, 28)
        Me.RadioButton_definedvalue.TabIndex = 2
        Me.RadioButton_definedvalue.Text = "defined value"
        Me.RadioButton_definedvalue.UseVisualStyleBackColor = True
        '
        'RadioButton_minimumLPmax_value
        '
        Me.RadioButton_minimumLPmax_value.AutoSize = True
        Me.RadioButton_minimumLPmax_value.Checked = True
        Me.RadioButton_minimumLPmax_value.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton_minimumLPmax_value.Location = New System.Drawing.Point(36, 300)
        Me.RadioButton_minimumLPmax_value.Name = "RadioButton_minimumLPmax_value"
        Me.RadioButton_minimumLPmax_value.Size = New System.Drawing.Size(156, 28)
        Me.RadioButton_minimumLPmax_value.TabIndex = 3
        Me.RadioButton_minimumLPmax_value.TabStop = True
        Me.RadioButton_minimumLPmax_value.Text = "minimum value"
        Me.RadioButton_minimumLPmax_value.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(62, 344)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(273, 13)
        Me.Label2.TabIndex = 5
        Me.Label2.Text = "Use the minimum value of Lp in the datagrid for PLC_O%"
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.SystemColors.GrayText
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(51, 435)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(181, 96)
        Me.Button1.TabIndex = 1
        Me.Button1.Text = "COMPUTE"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'RadioButton_meanLPmax_speedclass
        '
        Me.RadioButton_meanLPmax_speedclass.AutoSize = True
        Me.RadioButton_meanLPmax_speedclass.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton_meanLPmax_speedclass.Location = New System.Drawing.Point(51, 28)
        Me.RadioButton_meanLPmax_speedclass.Name = "RadioButton_meanLPmax_speedclass"
        Me.RadioButton_meanLPmax_speedclass.Size = New System.Drawing.Size(126, 28)
        Me.RadioButton_meanLPmax_speedclass.TabIndex = 1
        Me.RadioButton_meanLPmax_speedclass.Text = "mean value"
        Me.RadioButton_meanLPmax_speedclass.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(48, 81)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(232, 13)
        Me.Label3.TabIndex = 10
        Me.Label3.Text = "Use mean value for PLC_O% from speed class :"
        '
        'TextBox_PLC_speed_class
        '
        Me.TextBox_PLC_speed_class.Location = New System.Drawing.Point(286, 78)
        Me.TextBox_PLC_speed_class.Name = "TextBox_PLC_speed_class"
        Me.TextBox_PLC_speed_class.Size = New System.Drawing.Size(35, 20)
        Me.TextBox_PLC_speed_class.TabIndex = 2
        '
        'TextBox_Lpmax_integral
        '
        Me.TextBox_Lpmax_integral.Location = New System.Drawing.Point(223, 383)
        Me.TextBox_Lpmax_integral.Name = "TextBox_Lpmax_integral"
        Me.TextBox_Lpmax_integral.Size = New System.Drawing.Size(98, 20)
        Me.TextBox_Lpmax_integral.TabIndex = 5
        Me.TextBox_Lpmax_integral.TabStop = False
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(115, 384)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(90, 13)
        Me.Label6.TabIndex = 16
        Me.Label6.Text = "min value Integral"
        '
        'TextBox_meanLPmax_integral
        '
        Me.TextBox_meanLPmax_integral.Enabled = False
        Me.TextBox_meanLPmax_integral.Location = New System.Drawing.Point(222, 130)
        Me.TextBox_meanLPmax_integral.Name = "TextBox_meanLPmax_integral"
        Me.TextBox_meanLPmax_integral.Size = New System.Drawing.Size(99, 20)
        Me.TextBox_meanLPmax_integral.TabIndex = 17
        Me.TextBox_meanLPmax_integral.TabStop = False
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(104, 131)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(99, 13)
        Me.Label7.TabIndex = 18
        Me.Label7.Text = "mean value integral"
        '
        'TextBox_PLC_zero_integral
        '
        Me.TextBox_PLC_zero_integral.Location = New System.Drawing.Point(222, 224)
        Me.TextBox_PLC_zero_integral.Name = "TextBox_PLC_zero_integral"
        Me.TextBox_PLC_zero_integral.Size = New System.Drawing.Size(99, 20)
        Me.TextBox_PLC_zero_integral.TabIndex = 20
        Me.TextBox_PLC_zero_integral.TabStop = False
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(62, 227)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(137, 13)
        Me.Label4.TabIndex = 21
        Me.Label4.Text = "Use this value for PLC_O% "
        '
        'Dialog_compute_PLC
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(347, 593)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.TextBox_PLC_zero_integral)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.TextBox_meanLPmax_integral)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.TextBox_Lpmax_integral)
        Me.Controls.Add(Me.TextBox_PLC_speed_class)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.RadioButton_meanLPmax_speedclass)
        Me.Controls.Add(Me.OK_Button)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.RadioButton_minimumLPmax_value)
        Me.Controls.Add(Me.RadioButton_definedvalue)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.Name = "Dialog_compute_PLC"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Recompute PLC"
        Me.TopMost = True
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents OK_Button As System.Windows.Forms.Button
    Friend WithEvents RadioButton_definedvalue As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton_minimumLPmax_value As System.Windows.Forms.RadioButton
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents RadioButton_meanLPmax_speedclass As System.Windows.Forms.RadioButton
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents TextBox_PLC_speed_class As System.Windows.Forms.TextBox
    Friend WithEvents TextBox_Lpmax_integral As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents TextBox_meanLPmax_integral As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents TextBox_PLC_zero_integral As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label

End Class
