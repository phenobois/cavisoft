﻿Public Class dialog_branchdiameter

    Dim sum_big_res As Double
    Dim sum_small_res As Double
    Public branch_diameter_rawdata_upstream As String
    Public branch_diameter_rawdata_downstream As String
    Dim fPreviousValue As Int16

    Dim i As Int16
    Dim j As Int16

    Private Sub OK_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK_Button.Click
        Dim j As Int16

        compute_sum_and_mean()
        compute_crosssectionarea()

        branch_diameter_rawdata_upstream = ""
        branch_diameter_rawdata_downstream = ""
        For i = 0 To DataGridView1.RowCount - 1
            branch_diameter_rawdata_upstream = branch_diameter_rawdata_upstream & Convert.ToString(DataGridView1.Rows(i).Cells(1).Value) & ";"
            branch_diameter_rawdata_downstream = branch_diameter_rawdata_downstream & Convert.ToString(DataGridView1.Rows(i).Cells(2).Value) & ";"
        Next



        '  MAIN_Form.TextBox_diam_std_dev.Text = RichTextBox_diam_std_dev.Text

        'RECOMPUTE Lp in datagrid from Lp_0 values.
        If MAIN_Form.DataGridView_MAINDATA.RowCount > 2 Then

            For j = 0 To MAIN_Form.DataGridView_MAINDATA.RowCount - 2
                '   MAIN_Form.DataGridView_MAINDATA.Rows(j).Cells("branch_diameter_big_reservoir").Value = MAIN_Form.branch_diameter_bigreservoir
                '     MAIN_Form.DataGridView_MAINDATA.Rows(j).Cells("branch_diameter_small_reservoir").Value = MAIN_Form.branch_diameter_smallreservoir
                MAIN_Form.DataGridView_MAINDATA.Rows(j).Cells("branch_diameter_rawdata_upstream").Value = branch_diameter_rawdata_upstream
                MAIN_Form.DataGridView_MAINDATA.Rows(j).Cells("branch_diameter_rawdata_downstream").Value = branch_diameter_rawdata_downstream
                MAIN_Form.DataGridView_MAINDATA.Rows(j).Cells("Column_mean_diameter_big_res_mm").Value = MAIN_Form.mean_diameter_big_res
                MAIN_Form.DataGridView_MAINDATA.Rows(j).Cells("Column_mean_diameter_small_res_mm").Value = MAIN_Form.mean_diameter_small_res

            Next
        End If
        '   For j = 0 To MAIN_Form.DataGridView1.RowCount - 2
        'MAIN_Form.DataGridView1.Rows(j).Cells("branch_diameter").Value = Format(MAIN_Form.TextBox_branch_diam.Text, "0.##")


        ''Next
        If MAIN_Form.cross_section_area < MAIN_Form.reservoir_area Then
            'send data to main form & close

            MAIN_Form.TextBox_numberofstem.Text = NumericUpDown_numberofstem.Value
            If NumericUpDown_numberofstem.Value > 0 And MAIN_Form.mean_diameter_big_res > 0 Then
                Me.Close()
            Else
                MsgBox("You did not input any value. Please try again", MsgBoxStyle.Critical)
            End If

        Else
            MsgBox("The cross section area of your samples is higher than the reservoir area ! ,  Please check your diameter", MsgBoxStyle.Critical)
        End If

    End Sub

    Private Sub Cancel_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel_Button.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub




    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        fPreviousValue = NumericUpDown_numberofstem.Value


    End Sub

    Private Sub NumericUpDown1_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NumericUpDown_numberofstem.ValueChanged
        'Determine which way the value of the control has changed
        If NumericUpDown_numberofstem.Value > fPreviousValue Then

            DataGridView1.Rows.Add()

        ElseIf NumericUpDown_numberofstem.Value < fPreviousValue Then

            DataGridView1.Rows.Remove(DataGridView1.Rows(DataGridView1.Rows.Count - 1))

        Else

        End If
        fPreviousValue = NumericUpDown_numberofstem.Value

        For i = 0 To DataGridView1.Rows.Count - 1
            DataGridView1.Rows(i).Cells(0).Value = i + 1
        Next

        ' DataGridView1.Rows.Clear()
        '    For i = 0 To NumericUpDown1.Value - 1

        '   DataGridView1.Rows.Add()
        compute_sum_and_mean()
        compute_crosssectionarea()

        '   Next
    End Sub

    Private Sub DataGridView1_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellValidated


        compute_sum_and_mean()
        compute_crosssectionarea()

    End Sub
    Public Sub compute_sum_and_mean()



        '     If DataGridView1.CurrentCell.ColumnIndex = 1 Then
        sum_big_res = 0

        For j = 0 To (DataGridView1.RowCount - 1)
            If IsNumeric(DataGridView1.Rows(j).Cells("big_reservoir_diameter").Value) = True Then
                sum_big_res = sum_big_res + DataGridView1.Rows(j).Cells("big_reservoir_diameter").Value


            End If
        Next
        MAIN_Form.mean_diameter_big_res = sum_big_res / (j)
        MAIN_Form.TextBox_meanupstreamdiameter.Text = MAIN_Form.mean_diameter_big_res
        '  End If

        '      If DataGridView1.CurrentCell.ColumnIndex = 2 Then
        sum_small_res = 0
        For j = 0 To (DataGridView1.RowCount - 1)
            If IsNumeric(DataGridView1.Rows(j).Cells("small_reservoir_diameter").Value) = True Then
                sum_small_res = sum_small_res + DataGridView1.Rows(j).Cells("small_reservoir_diameter").Value

            End If
        Next
        MAIN_Form.mean_diameter_small_res = sum_small_res / (j)
        MAIN_Form.TextBox_meandownstreamdiameter.Text = MAIN_Form.mean_diameter_small_res
        '    End If
    End Sub

    Public Sub compute_crosssectionarea()

        If sum_big_res <> 0 Then
            sum_big_res = 0
            For j = 0 To (DataGridView1.RowCount - 1)
                If IsNumeric(DataGridView1.Rows(j).Cells("big_reservoir_diameter").Value) = True Then
                    sum_big_res = sum_big_res + (Math.PI * (DataGridView1.Rows(j).Cells("big_reservoir_diameter").Value / 2000) ^ 2)
                    RichTextBox_TOTAL_UPSTREAMcrosssectionarea.Text = Format(sum_big_res, "Scientific")
                    MAIN_Form.cross_section_area = RichTextBox_TOTAL_UPSTREAMcrosssectionarea.Text
                End If
            Next

            ' MAIN_Form.cross_section_area = branch_diameter_bigreservoir ^ 2 / 4 * Math.PI / 1000000 'branch diam in mm & s in m²
            MAIN_Form.TextBox_branch_crosssection.Text = Format(MAIN_Form.cross_section_area, "Scientific")
        Else
            MAIN_Form.TextBox_branch_crosssection.Text = ""
            MAIN_Form.cross_section_area = 10 ^ 2 / 4 * Math.PI / 1000000 'value normalised for a branch of diameter 10mm & s in m²
            '    TextBox_crosssectionarea.Text = Format(s, "Scientific")
        End If


    End Sub

End Class