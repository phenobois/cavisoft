﻿Public Class Stats
    'This class was created to hold various mathimatical functions

    Public Class RegressInfo

        Public samplecount As Integer = 0
        Public sigmaX As Double
        Public sigmaY As Double
        Public sigmaXX As Double
        Public sigmaXY As Double
        Public sigmaYY As Double
        Public regSlope As Double
        Public regYInt As Double
        Public regValue As Double
        Public ResidualErr As Double
        Public PearsonsR As Double
        Public PearsonsRsquare As Double


        Public Overrides Function ToString() As String
            'This function builds the RegressInfo class into a string
            'Dim ret As String = Me.regSlope
            'MAIN_Form.slope = Me.regSlope
            'MAIN_Form.intercept = Me.regYInt
            'MAIN_Form.residue = Me.ResidualErr
            'MAIN_Form.equ_pearsonR = Me.PearsonsR

            Dim ret As String = _
           "SampleSize       = " & Me.samplecount & vbCrLf & _
           "Formula used     =   y=" & Me.regSlope & "x + " & Me.regYInt & vbCrLf & _
           "Residual Error   = " & Me.ResidualErr & vbCrLf & _
           "Pearsons R² value = " & Me.PearsonsRsquare
            Return ret
        End Function

   

    End Class

    Public Shared Function _
    Regression(ByVal predictor As Double, ByVal xvalues() As Double, ByVal yvalues() As Double)
        'Caution: This function is dependent on three things
        '   1. A valid input value for the prediction
        '   2. Both input arrays MUST be the same length
        '   3. Both input arrays MUST contain numeric data
        '   These dependencies could be verified in this function
        '   with a couple of error traps but my application includes
        '   the traps in the calling procedures.
        '   Once computations have finished the function will return
        '   the "data" object

        Dim i As Integer
        Dim reserrX As Double
        Dim reserrY As Double
        Dim resultY As Double
        Dim resErr As Double
        Dim sumsigX As Double
        Dim sumsigY As Double
        Dim sumsigXY As Double

        Dim REGRESSION_data As RegressInfo = New RegressInfo

        'First we need to find the sums of X, Y, X*Y, and X^2 for the input arrays
        For i = LBound(xvalues) To UBound(xvalues)
            REGRESSION_data.sigmaXY += xvalues(i) * yvalues(i)
            REGRESSION_data.sigmaXX += xvalues(i) ^ 2
            REGRESSION_data.sigmaYY += yvalues(i) ^ 2
            REGRESSION_data.sigmaX += xvalues(i)
            REGRESSION_data.sigmaY += yvalues(i)
        Next

        'Now we need to determine what the slope of the regression line will be
        'Slope(b) = NÓXY - (ÓX)(ÓY) / (NÓX2 - (ÓX)2)
        REGRESSION_data.regSlope = ((xvalues.Length * REGRESSION_data.sigmaXY) - (REGRESSION_data.sigmaX * REGRESSION_data.sigmaY)) / _
                   ((xvalues.Length * REGRESSION_data.sigmaXX) - (REGRESSION_data.sigmaX ^ 2))

        'Next we need to determine what the point of Y intercept is
        'Intercept(a) = (ÓY - b(ÓX)) / N 
        REGRESSION_data.regYInt = (REGRESSION_data.sigmaY - (REGRESSION_data.regSlope * REGRESSION_data.sigmaX)) / xvalues.Length

        'Now use slope and intercept and predictor value in regression equation
        'Regression Equation(y) = a + bx
        REGRESSION_data.regValue = REGRESSION_data.regYInt + (REGRESSION_data.regSlope * predictor)

        'Now we will use the regression formula to find the residual error
        For i = LBound(xvalues) To UBound(xvalues)
            reserrX = xvalues(i)
            reserrY = yvalues(i)
            'Use regression formula
            resultY = REGRESSION_data.regYInt + (REGRESSION_data.regSlope * xvalues(i))
            'Now figure error value
            resErr = yvalues(i) - resultY
            REGRESSION_data.ResidualErr += resErr ^ 2
        Next

        'Now we will calculate Pearsons R value
        sumsigX = REGRESSION_data.sigmaXX - ((REGRESSION_data.sigmaX * REGRESSION_data.sigmaX) / xvalues.Length)
        sumsigY = REGRESSION_data.sigmaYY - ((REGRESSION_data.sigmaY * REGRESSION_data.sigmaY) / xvalues.Length)
        sumsigXY = REGRESSION_data.sigmaXY - ((REGRESSION_data.sigmaX * REGRESSION_data.sigmaY) / xvalues.Length)
        REGRESSION_data.PearsonsR = sumsigXY / Math.Sqrt(sumsigX * sumsigY)
        REGRESSION_data.PearsonsRsquare = REGRESSION_data.PearsonsR * REGRESSION_data.PearsonsR


        Return REGRESSION_data

    End Function

    Public Shared Function _
    StandardDeviation(ByVal Values() As Double) As Double

        Dim Mean As Double = 0
        Dim Sum As Double = 0
        Dim SquaredDeviations(Values.Length - 1) As Double
        Dim i As Integer

        'Sum the values in the array
        For i = 0 To Values.Length - 2
            Sum += Values(i)
        Next

        'Calculate the mean of the array
        Mean = Sum / (Values.Length - 1)
        Sum = 0

        'Find the squared deviation of each array value
        '   and calculate the sum
        For i = 0 To Values.Length - 2
            SquaredDeviations(i) = (Values(i) - Mean) ^ 2
            Sum += SquaredDeviations(i)
        Next

        'Return the standard deviation value for the array
        Return _
        System.Math.Sqrt(Sum / (SquaredDeviations.Length - 1))

    End Function
End Class