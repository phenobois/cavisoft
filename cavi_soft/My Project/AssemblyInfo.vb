﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' Les informations générales relatives à un assembly dépendent de 
' l'ensemble d'attributs suivant. Changez les valeurs de ces attributs pour modifier les informations
' associées à un assembly.

' Vérifiez les valeurs des attributs de l'assembly

<Assembly: AssemblyTitle("CaviSoft")> 
<Assembly: AssemblyDescription("Please send any comment or bug report to regis.burlett@u-bordeaux.fr")> 
<Assembly: AssemblyCompany("Université de BORDEAUX")> 
<Assembly: AssemblyProduct("CaviSoft")> 
<Assembly: AssemblyCopyright("Copyright ©  2018")> 
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(True)> 

'Le GUID suivant est pour l'ID de la typelib si ce projet est exposé à COM
<Assembly: Guid("03b5bb0d-3b4f-40ae-8a70-d78ff993a467")> 

' Les informations de version pour un assembly se composent des quatre valeurs suivantes :
'
'      Version principale
'      Version secondaire 
'      Numéro de build
'      Révision
'
' Vous pouvez spécifier toutes les valeurs ou indiquer les numéros de build et de révision par défaut 
' en utilisant '*', comme indiqué ci-dessous :
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("5.2.1.0")> 
<Assembly: AssemblyFileVersion("5.2.1.0")> 
