﻿Imports NationalInstruments.DAQmx

Public Class CAVITRON_SETUP




    Private Sub CAVITRON_SETUP_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load






        ListBox1.Items.Add("########## DIGITAL OUTPUT CHANNELS ############ ")
        ListBox1.Items.Add(Chr(13))
        ListBox1.Items.AddRange(DaqSystem.Local.GetPhysicalChannels(PhysicalChannelTypes.DOPort, PhysicalChannelAccess.All))
        ListBox1.Items.Add(Chr(13))
        ListBox1.Items.Add("########## ANALOG INPUT CHANNELS ############ ")
        ListBox1.Items.Add(Chr(13))
        ListBox1.Items.AddRange(DaqSystem.Local.GetPhysicalChannels(PhysicalChannelTypes.AI, PhysicalChannelAccess.All))

        LedArray_pushwheel_port0.SetValues(MAIN_Form.dataArray_port0)
        LedArray_pushwheel_port1.SetValues(MAIN_Form.dataArray_port1)
    End Sub



    Private Sub LinkLabel1_LinkClicked_1(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles LinkLabel1.LinkClicked
        MAIN_Form.max__possible_cavispeed_rpm = TextBox_maxcentri_rpm.Text
    End Sub


    'Private Sub ComboBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox_fileextension.SelectedIndexChanged
    '    If MAIN_Form.is_MAINFORM_INIT Then
    '        MAIN_Form.define_fileextension()
    '    End If

    'End Sub




    Private Sub OK_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK_Button.Click

        SAVE_MY_SETTINGS()
        SYNC_MY_SETTINGS()

        Me.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.Close()
        '     MAIN_Form.init_datalogger()
    End Sub

    Private Sub Cancel_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel_Button.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Close()

    End Sub

    Public Sub set_to_default()

        TextBox_offset_ai0.Text = "0"
        TextBox_offset_ai1.Text = "-40"

        TextBox_offset_counter.Text = "0"


        TextBox_gain_ai0.Text = "1"
        TextBox_gain_ai1.Text = "12"

        TextBox_gain_counter.Text = "3.75"


    End Sub

    Private Sub CheckBox2_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox_useSIunit.CheckedChanged

        'If MAIN_Form.is_MAINFORM_INIT = True Then
        '    'Define display format, as a function of whether or not values are scaled for cross_section area.
        '    If CheckBox2.Checked = False Then

        '        MAIN_Form.displayformat = "0.#####"
        '    Else
        '        MAIN_Form.displayformat = "scientific"
        '    End If
        'End If

    End Sub
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_settodefault.Click

        set_to_default()
    End Sub


    Private Sub ComboBox2_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox_ai0range.SelectedIndexChanged

        '   MAIN_Form.ai0_minimumValue_volt = 0

        Select Case ComboBox_ai0range.SelectedIndex

            Case 0
                MAIN_Form.ai0_maximumValue_volt = 10
            Case 1
                MAIN_Form.ai0_maximumValue_volt = 5
            Case 2
                MAIN_Form.ai0_maximumValue_volt = 4
            Case 3
                MAIN_Form.ai0_maximumValue_volt = 2.5
            Case 4
                MAIN_Form.ai0_maximumValue_volt = 2
            Case 5
                MAIN_Form.ai0_maximumValue_volt = 1.25
            Case 6
                MAIN_Form.ai0_maximumValue_volt = 1


        End Select
        MAIN_Form.init_datalogger()

    End Sub





    Private Sub Button1_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        MAIN_Form.Digital_output_cavispeed_pushwheel_control()

    End Sub



    Public Sub ComboBox_set_cavispeed_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox_cavispeed_setup.SelectedIndexChanged


        MAIN_Form.ComboBox_set_cavispeed_boolean = False

        Select Case ComboBox_cavispeed_setup.SelectedIndex
            Case 0
                MAIN_Form.RichTextBox_centri_rpm.Text = 0
                MAIN_Form.Label_cavispeedcontrolsetvalue.Visible = True
                MAIN_Form.Label_cavispeedcontrolsetvalue.Text = "set value (rpm) :"
                MAIN_Form.TextBox_cavispeed_setvalue.Visible = True
                MAIN_Form.RichTextBox_centri_rpm.BackColor = Color.Orange
                MAIN_Form.RichTextBox_centri_tension.BackColor = Color.PaleGoldenrod
                MAIN_Form.ComboBox_set_cavispeed_boolean = True

                MAIN_Form.Bt_setcavispeed.Location = New Point(16, 144)
                MAIN_Form.Bt_setcavispeed.Text = "set cavispeed (rpm)"

                MAIN_Form.CheckBox_tachymeter_connected.Location = New Point(250, 151)
                MAIN_Form.GroupBox_centrifugecontrol.Visible = True

            Case 1
                MAIN_Form.read_Cavispeed_rpm = 0
                ' MAIN_Form.centri_tension = 0
                MAIN_Form.RichTextBox_centri_rpm.Text = 0
                MAIN_Form.RichTextBox_centri_tension.Text = 0
                MAIN_Form.Label_cavispeedcontrolsetvalue.Visible = True
                MAIN_Form.Label_cavispeedcontrolsetvalue.Text = "set value (MPa) :"
                MAIN_Form.TextBox_cavispeed_setvalue.Visible = True
                MAIN_Form.RichTextBox_centri_rpm.BackColor = Color.PaleGoldenrod
                MAIN_Form.RichTextBox_centri_tension.BackColor = Color.Orange
                MAIN_Form.ComboBox_set_cavispeed_boolean = True

                MAIN_Form.Bt_setcavispeed.Location = New Point(250, 144)

                MAIN_Form.CheckBox_tachymeter_connected.Location = New Point(16, 151)

                MAIN_Form.Bt_setcavispeed.Text = "set cavitron pressure (MPa)"
                MAIN_Form.GroupBox_centrifugecontrol.Visible = True
            Case 2
                MAIN_Form.RichTextBox_centri_rpm.Text = 0
                MAIN_Form.Label_cavispeedcontrolsetvalue.Visible = False
                MAIN_Form.Label_cavispeedcontrolsetvalue.Text = ""
                MAIN_Form.TextBox_cavispeed_setvalue.Visible = False
                MAIN_Form.RichTextBox_centri_rpm.BackColor = Color.PaleGoldenrod
                MAIN_Form.RichTextBox_centri_tension.BackColor = Color.PaleGoldenrod
                MAIN_Form.ComboBox_set_cavispeed_boolean = True

                MAIN_Form.Bt_setcavispeed.Location = New Point(16, 144)

                MAIN_Form.CheckBox_tachymeter_connected.Location = New Point(250, 151)
                MAIN_Form.Bt_setcavispeed.Text = "set cavispeed (rpm)"
                MAIN_Form.GroupBox_centrifugecontrol.Visible = False


                MAIN_Form.centri_Start = False
                MAIN_Form.centri_Stop = True

                '       MAIN_Form.write_digital_START_STOP_BRAKE()

        End Select

    End Sub

    Public Sub LOAD_MY_SETTINGS()


        '---''FROM TAB 1''''''''''

        TextBox_CAVITRON_NAME.Text = My.Settings.cavitron_name
        ComboBox_rotor_name.SelectedIndex = My.Settings.rotor_name
        TextBox_rotor_diameter_cm.Text = My.Settings.rotor_diameter
        ComboBox_optical_source.SelectedIndex = My.Settings.optical_source
        TextBox_pixelsize.Text = My.Settings.optical_calibration_pixel
        ComboBox_cavispeed_acquisiton.SelectedIndex = My.Settings.cavispeed_acquisiton
        ComboBox_cavispeed_setup.SelectedIndex = My.Settings.cavispeed_setup
        ComboBox_temperature_measurement.SelectedIndex = My.Settings.temperature_measurement
        ComboBox_reservoir_innersurface.SelectedIndex = My.Settings.reservoir_surface



        '---''FROM TAB 2''''''''''
        TextBox_offset_ai0.Text = My.Settings.offset_ai0
        TextBox_offset_ai1.Text = My.Settings.offset_ai1

        TextBox_gain_ai0.Text = My.Settings.gain_ai0
        TextBox_gain_ai1.Text = My.Settings.gain_ai1

        TextBox_lowlevel_ao0.Text = My.Settings.lowlevel_ao0
        TextBox_highlevel_ao0.Text = My.Settings.highlevel_ao0

        TextBox_lowlevel_ao1.Text = My.Settings.lowlevel_ao1
        TextBox_highlevel_ao1.Text = My.Settings.highlevel_ao1

        TextBox_gain_counter.Text = My.Settings.gain_counter
        TextBox_offset_counter.Text = My.Settings.offset_counter

        ComboBox_centrifugemodel.SelectedIndex = My.Settings.centrifuge_model
        RichTextBox_operator_list.Text = My.Settings.operator_list


    End Sub

    Public Sub SYNC_MY_SETTINGS()

        MAIN_Form.max__possible_cavispeed_rpm = TextBox_maxcentri_rpm.Text

        MAIN_Form.cavitron_name = TextBox_CAVITRON_NAME.Text
        MAIN_Form.Label_cavitron_name.Text = TextBox_CAVITRON_NAME.Text

        MAIN_Form.rotor_name = ComboBox_rotor_name.Text
        MAIN_Form.Label_rotor_number.Text = ComboBox_rotor_name.Text

        If TextBox_rotor_diameter_cm.Text <> "" Then
            MAIN_Form.rotor_diameter = Convert.ToDouble(TextBox_rotor_diameter_cm.Text)
            MAIN_Form.Label_rotor_diameter.Text = TextBox_rotor_diameter_cm.Text
        End If

        MAIN_Form.optical_source = ComboBox_optical_source.Text
        MAIN_Form.Label_optical_source.Text = ComboBox_optical_source.Text

        MAIN_Form.PIXEL_SIZE_mmperpixel = Convert.ToDouble(TextBox_pixelsize.Text)
        MAIN_Form.Label_optical_calibration.Text = TextBox_pixelsize.Text

        MAIN_Form.Label_cavispeed_acq.Text = ComboBox_cavispeed_acquisiton.Text

        MAIN_Form.Label_temperature_source.Text = ComboBox_temperature_measurement.Text
        MAIN_Form.Label_SIunit.Text = ComboBox_Lp_SIunit_select.Text

        MAIN_Form.Label_reservoir_area.Text = ComboBox_reservoir_innersurface.Text
        MAIN_Form.reservoir_area = ComboBox_reservoir_innersurface.Text

        If ComboBox_reservoir_innersurface.SelectedIndex = 1 Then
            MAIN_Form.ComboBox_cavi1000_reservoirsize.SelectedIndex = 1  ' correspond to SMALL RESERVOIR
        Else
            MAIN_Form.ComboBox_cavi1000_reservoirsize.Text = ""
        End If

        If ComboBox_reservoir_innersurface.SelectedIndex = 2 Then
            MAIN_Form.ComboBox_cavi1000_reservoirsize.SelectedIndex = 0  ' correspond to BIG RESERVOIR
        Else
            MAIN_Form.ComboBox_cavi1000_reservoirsize.Text = ""
        End If


        MAIN_Form.fill_combobox_operator()


        '---''FROM TAB 2''''''''''
        MAIN_Form.DAQ_offset_ai0 = TextBox_offset_ai0.Text
        MAIN_Form.DAQ_offset_ai1 = TextBox_offset_ai1.Text


        MAIN_Form.DAQ_gain_ai0 = TextBox_gain_ai0.Text
        MAIN_Form.DAQ_gain_ai1 = TextBox_gain_ai1.Text


        MAIN_Form.max_Cavispeed_rpm = 0

        MAIN_Form.RESERVOIR_FILLING_lowlevel_mV = TextBox_lowlevel_ao0.Text
        MAIN_Form.RESERVOIR_FILLING_highlevel_mV = TextBox_highlevel_ao0.Text

        MAIN_Form.bt_START_lowlevel_mV = TextBox_lowlevel_ao1.Text
        MAIN_Form.bt_START_highlevel_mV = TextBox_highlevel_ao1.Text



        MAIN_Form.Sorvallfrequencyoutput_gain_counter = TextBox_gain_counter.Text
        MAIN_Form.Sorvallfrequencyoutput_offset_counter = TextBox_offset_counter.Text

        MAIN_Form.centrifuge_model = ComboBox_centrifugemodel.Text

    End Sub

    Public Sub SAVE_MY_SETTINGS()


        '---''FROM TAB 1''''''''''

        My.Settings.cavitron_name = TextBox_CAVITRON_NAME.Text
        My.Settings.rotor_name = ComboBox_rotor_name.SelectedIndex
        My.Settings.rotor_diameter = TextBox_rotor_diameter_cm.Text
        My.Settings.optical_source = ComboBox_optical_source.SelectedIndex
        My.Settings.optical_calibration_pixel = TextBox_pixelsize.Text
        My.Settings.cavispeed_acquisiton = ComboBox_cavispeed_acquisiton.SelectedIndex
        My.Settings.cavispeed_setup = ComboBox_cavispeed_setup.SelectedIndex
        My.Settings.temperature_measurement = ComboBox_temperature_measurement.SelectedIndex
        My.Settings.reservoir_surface = ComboBox_reservoir_innersurface.SelectedIndex



        '---''FROM TAB 2''''''''''
        My.Settings.offset_ai0 = TextBox_offset_ai0.Text
        My.Settings.offset_ai1 = TextBox_offset_ai1.Text

        My.Settings.gain_ai0 = TextBox_gain_ai0.Text
        My.Settings.gain_ai1 = TextBox_gain_ai1.Text

        My.Settings.lowlevel_ao0 = TextBox_lowlevel_ao0.Text
        My.Settings.highlevel_ao0 = TextBox_highlevel_ao0.Text

        My.Settings.lowlevel_ao1 = TextBox_lowlevel_ao1.Text
        My.Settings.highlevel_ao1 = TextBox_highlevel_ao1.Text

        My.Settings.gain_counter = TextBox_gain_counter.Text
        My.Settings.offset_counter = TextBox_offset_counter.Text

        My.Settings.centrifuge_model = ComboBox_centrifugemodel.SelectedIndex
        My.Settings.operator_list = RichTextBox_operator_list.Text

        My.Settings.Save()


    End Sub

    Private Sub Button2_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        SYNC_MY_SETTINGS()
        SAVE_MY_SETTINGS()
    End Sub



End Class