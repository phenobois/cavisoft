﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class dialog_branchdiameter
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.branch_number = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.big_reservoir_diameter = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.small_reservoir_diameter = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.NumericUpDown_numberofstem = New System.Windows.Forms.NumericUpDown()
        Me.TextBox12 = New System.Windows.Forms.TextBox()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Cancel_Button = New System.Windows.Forms.Button()
        Me.OK_Button = New System.Windows.Forms.Button()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.RichTextBox_TOTAL_UPSTREAMcrosssectionarea = New System.Windows.Forms.RichTextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.RichTextBox_TOTAL_DOWNSTREAMcrosssectionarea = New System.Windows.Forms.RichTextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDown_numberofstem, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AllowUserToDeleteRows = False
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.615385!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridView1.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.branch_number, Me.big_reservoir_diameter, Me.small_reservoir_diameter})
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.615385!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridView1.DefaultCellStyle = DataGridViewCellStyle2
        Me.DataGridView1.Location = New System.Drawing.Point(143, 121)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.Size = New System.Drawing.Size(444, 408)
        Me.DataGridView1.TabIndex = 68
        '
        'branch_number
        '
        Me.branch_number.HeaderText = "branch number"
        Me.branch_number.Name = "branch_number"
        '
        'big_reservoir_diameter
        '
        Me.big_reservoir_diameter.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.big_reservoir_diameter.HeaderText = "upstream reservoir diameter (mm)"
        Me.big_reservoir_diameter.Name = "big_reservoir_diameter"
        Me.big_reservoir_diameter.Width = 150
        '
        'small_reservoir_diameter
        '
        Me.small_reservoir_diameter.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.small_reservoir_diameter.HeaderText = "downstream reservoir diameter (mm)"
        Me.small_reservoir_diameter.Name = "small_reservoir_diameter"
        Me.small_reservoir_diameter.Width = 150
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(53, 54)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(200, 29)
        Me.Label1.TabIndex = 67
        Me.Label1.Text = "number of branch"
        '
        'NumericUpDown_numberofstem
        '
        Me.NumericUpDown_numberofstem.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NumericUpDown_numberofstem.Location = New System.Drawing.Point(272, 52)
        Me.NumericUpDown_numberofstem.Name = "NumericUpDown_numberofstem"
        Me.NumericUpDown_numberofstem.ReadOnly = True
        Me.NumericUpDown_numberofstem.Size = New System.Drawing.Size(70, 35)
        Me.NumericUpDown_numberofstem.TabIndex = 66
        '
        'TextBox12
        '
        Me.TextBox12.BackColor = System.Drawing.Color.Khaki
        Me.TextBox12.Location = New System.Drawing.Point(35, 669)
        Me.TextBox12.Multiline = True
        Me.TextBox12.Name = "TextBox12"
        Me.TextBox12.ReadOnly = True
        Me.TextBox12.Size = New System.Drawing.Size(398, 41)
        Me.TextBox12.TabIndex = 65
        Me.TextBox12.Text = "Please note that only the first measurement will be used for maximum conductivity" & _
            " computation. Measure should be done at 15mm from the branch extremity"
        Me.TextBox12.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(54, 29)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(229, 13)
        Me.Label19.TabIndex = 64
        Me.Label19.Text = "Please measure at about 15 mm from each end"
        '
        'Cancel_Button
        '
        Me.Cancel_Button.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Cancel_Button.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Cancel_Button.Location = New System.Drawing.Point(470, 671)
        Me.Cancel_Button.Name = "Cancel_Button"
        Me.Cancel_Button.Size = New System.Drawing.Size(136, 39)
        Me.Cancel_Button.TabIndex = 63
        Me.Cancel_Button.Text = "EXIT WITHOUT SAVING"
        '
        'OK_Button
        '
        Me.OK_Button.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.OK_Button.Location = New System.Drawing.Point(612, 671)
        Me.OK_Button.Name = "OK_Button"
        Me.OK_Button.Size = New System.Drawing.Size(103, 39)
        Me.OK_Button.TabIndex = 62
        Me.OK_Button.Text = "OK"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.Location = New System.Drawing.Point(52, 4)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(402, 24)
        Me.Label14.TabIndex = 61
        Me.Label14.Text = "Enter diameters at the extremities of the branch"
        '
        'RichTextBox_TOTAL_UPSTREAMcrosssectionarea
        '
        Me.RichTextBox_TOTAL_UPSTREAMcrosssectionarea.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RichTextBox_TOTAL_UPSTREAMcrosssectionarea.Location = New System.Drawing.Point(169, 578)
        Me.RichTextBox_TOTAL_UPSTREAMcrosssectionarea.Name = "RichTextBox_TOTAL_UPSTREAMcrosssectionarea"
        Me.RichTextBox_TOTAL_UPSTREAMcrosssectionarea.ReadOnly = True
        Me.RichTextBox_TOTAL_UPSTREAMcrosssectionarea.Size = New System.Drawing.Size(159, 39)
        Me.RichTextBox_TOTAL_UPSTREAMcrosssectionarea.TabIndex = 71
        Me.RichTextBox_TOTAL_UPSTREAMcrosssectionarea.Text = ""
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(146, 560)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(214, 13)
        Me.Label4.TabIndex = 74
        Me.Label4.Text = "TOTAL cross section area UPSTREAM (m²)"
        '
        'RichTextBox_TOTAL_DOWNSTREAMcrosssectionarea
        '
        Me.RichTextBox_TOTAL_DOWNSTREAMcrosssectionarea.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RichTextBox_TOTAL_DOWNSTREAMcrosssectionarea.Location = New System.Drawing.Point(440, 578)
        Me.RichTextBox_TOTAL_DOWNSTREAMcrosssectionarea.Name = "RichTextBox_TOTAL_DOWNSTREAMcrosssectionarea"
        Me.RichTextBox_TOTAL_DOWNSTREAMcrosssectionarea.ReadOnly = True
        Me.RichTextBox_TOTAL_DOWNSTREAMcrosssectionarea.Size = New System.Drawing.Size(159, 39)
        Me.RichTextBox_TOTAL_DOWNSTREAMcrosssectionarea.TabIndex = 75
        Me.RichTextBox_TOTAL_DOWNSTREAMcrosssectionarea.Text = ""
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(409, 560)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(231, 13)
        Me.Label5.TabIndex = 76
        Me.Label5.Text = "TOTAL cross section areaDOWNSTREAM (m²)"
        '
        'dialog_branchdiameter
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(769, 722)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.RichTextBox_TOTAL_DOWNSTREAMcrosssectionarea)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.RichTextBox_TOTAL_UPSTREAMcrosssectionarea)
        Me.Controls.Add(Me.DataGridView1)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.NumericUpDown_numberofstem)
        Me.Controls.Add(Me.TextBox12)
        Me.Controls.Add(Me.Label19)
        Me.Controls.Add(Me.Cancel_Button)
        Me.Controls.Add(Me.OK_Button)
        Me.Controls.Add(Me.Label14)
        Me.Name = "dialog_branchdiameter"
        Me.Text = "Branch diameter"
        Me.TopMost = True
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDown_numberofstem, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents NumericUpDown_numberofstem As System.Windows.Forms.NumericUpDown
    Friend WithEvents TextBox12 As System.Windows.Forms.TextBox
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents Cancel_Button As System.Windows.Forms.Button
    Friend WithEvents OK_Button As System.Windows.Forms.Button
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents RichTextBox_TOTAL_UPSTREAMcrosssectionarea As System.Windows.Forms.RichTextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents branch_number As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents big_reservoir_diameter As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents small_reservoir_diameter As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents RichTextBox_TOTAL_DOWNSTREAMcrosssectionarea As System.Windows.Forms.RichTextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
End Class
