﻿Imports System.Windows.Forms

Public Class Dialog_compute_PLC

    Dim Lp_0_integral As Double
    Dim Lp_0_AUTOMODE As Double

    Public Lp_max_integral As Double
    Public Lp_max_automode As Double

    Private Sub RadioButton1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton_definedvalue.GotFocus
        RadioButton_definedvalue.Checked = True
        RadioButton_minimumLPmax_value.Checked = False
        RadioButton_meanLPmax_speedclass.Checked = False
    End Sub
    Private Sub RadioButton2_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton_minimumLPmax_value.GotFocus
        RadioButton_definedvalue.Checked = False
        RadioButton_minimumLPmax_value.Checked = True
        RadioButton_meanLPmax_speedclass.Checked = False
    End Sub


    Private Sub RadioButton3_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton_meanLPmax_speedclass.GotFocus
        RadioButton_definedvalue.Checked = False
        RadioButton_minimumLPmax_value.Checked = False
        RadioButton_meanLPmax_speedclass.Checked = True
    End Sub
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim i As Integer  'loop variable
        Dim j As Integer = 0  'loop variable

        Dim temporary_Lp_integral As Double
        '----------------------------------------------
        'user-defined value
        If RadioButton_definedvalue.Checked Then
            '    Lp_0_cochard = TextBox_PLC_zero_cochard.Text
            If TextBox_PLC_zero_integral.Text <> "" Then
                Lp_0_integral = TextBox_PLC_zero_integral.Text
                Lp_0_AUTOMODE = TextBox_PLC_zero_integral.Text
            Else
                MsgBox("please enter a value", MsgBoxStyle.Exclamation)

            End If


        End If
        '----------------------------------------------
        'recompute with minimum value 
        If RadioButton_minimumLPmax_value.Checked Then

            'For i = 0 To MAIN_Form.DataGridView_MAINDATA.RowCount - 2
            '    temporary_Lp_cochard = MAIN_Form.DataGridView_MAINDATA.Rows(i).Cells("conductance_LP").Value

            '    If Lp_max_cochard < temporary_Lp_cochard Then
            '        Lp_max_cochard = temporary_Lp_cochard
            '    End If
            'Next

            'TextBox_LPmax_cochard.Text = Lp_max_cochard
            'Lp_0_cochard = Lp_max_cochard

            For i = 0 To MAIN_Form.DataGridView_MAINDATA.RowCount - 2
                temporary_Lp_integral = MAIN_Form.DataGridView_MAINDATA.Rows(i).Cells("Conductivity_SI_corrT").Value

                If Lp_max_integral < temporary_Lp_integral Then
                    Lp_max_integral = temporary_Lp_integral
                End If
            Next

            TextBox_Lpmax_integral.Text = Lp_max_integral
            Lp_0_integral = Lp_max_integral
            Lp_0_AUTOMODE = Lp_max_integral
        End If

        '----------------------------------------------
        'recompute with mean value from a given speed class (first speed class in the datagrid is set as default)
        If RadioButton_meanLPmax_speedclass.Checked Then


            For i = 0 To MAIN_Form.DataGridView_MAINDATA.RowCount - 2
                If MAIN_Form.DataGridView_MAINDATA.Rows(i).Cells("speedclass").Value = TextBox_PLC_speed_class.Text Then
                    j = j + 1
                    '  temporary_Lp_cochard = temporary_Lp_cochard + MAIN_Form.DataGridView_MAINDATA.Rows(i).Cells("conductance_LP_cochard").Value
                    temporary_Lp_integral = temporary_Lp_integral + MAIN_Form.DataGridView_MAINDATA.Rows(i).Cells("Conductivity_SI_corrT").Value
                End If

            Next

            '   TextBox_meanLPmax_cochard.Text = Format(temporary_Lp_cochard / j, "#.000")
            '   Lp_0_cochard = TextBox_meanLPmax_cochard.Text


            TextBox_meanLPmax_integral.Text = Format(temporary_Lp_integral / j, "#.000")
            Lp_0_integral = TextBox_meanLPmax_integral.Text
            Lp_0_AUTOMODE = TextBox_meanLPmax_integral.Text

        End If

        '-----------------------------------------------

        'RECOMPUTE Lp in datagrid from Lp_0 values.

        For j = 0 To MAIN_Form.DataGridView_MAINDATA.RowCount - 2
            'MAIN_Form.DataGridView_MAINDATA.Rows(j).Cells("column_PLC_cochard").Value = Format(100 * (1 - (MAIN_Form.DataGridView_MAINDATA.Rows(j).Cells("conductance_LP_cochard").Value / Lp_0_cochard)), "0.##")
            MAIN_Form.DataGridView_MAINDATA.Rows(j).Cells("column_PLC_integral").Value = Format(100 * (1 - (MAIN_Form.DataGridView_MAINDATA.Rows(j).Cells("Conductivity_SI_corrT").Value / Lp_0_integral)), "0.##")

        Next
        '   MAIN_Form.TextBox_LP0_Cochard.Text = Lp_0_cochard
        MAIN_Form.TextBox_LP0_integral.Text = Lp_0_integral
        MAIN_Form.RichTextBox_Lp0_automode.Text = Lp_0_AUTOMODE



        ' MAIN_Form.RichTextBox_PLC_Cochard.Text = MAIN_Form.DataGridView_MAINDATA.Rows(MAIN_Form.DataGridView_MAINDATA.Rows.Count - 1).Cells("column_PLC_cochard").Value
        MAIN_Form.RichTextBox_PLC_integral.Text = MAIN_Form.DataGridView_MAINDATA.Rows(MAIN_Form.DataGridView_MAINDATA.Rows.Count - 1).Cells("column_PLC_integral").Value


        '   MAIN_Form.compute_MAX_Lp_cochard_SIunit()


        MAIN_Form.update_graph()
    End Sub



    '############################################################################
    ' EVENT HANDLING
    '############################################################################




   

    Private Sub TextBox_PLC_zero_integral_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox_PLC_zero_integral.KeyPress
        e.Handled = MAIN_Form.NumbersOnly(e.KeyChar, TextBox_PLC_zero_integral)
    End Sub

    Private Sub Dialog7_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        RadioButton_meanLPmax_speedclass.Checked = True
        '  Lp_max_cochard = 0
        Lp_max_integral = 0

        If (MAIN_Form.DataGridView_MAINDATA.RowCount > 1) Then

            TextBox_PLC_speed_class.Text = MAIN_Form.DataGridView_MAINDATA.Rows(1).Cells("speedclass").Value
        End If
        Button1.Select()
    End Sub

   

    
    Private Sub OK_Button_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK_Button.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.Close()
    End Sub

    
   
End Class
