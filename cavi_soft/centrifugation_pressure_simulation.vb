﻿Public Class centrifugation_pressure_simulation

    Dim centri_tension
    Dim rotor_diameter
    Dim min_meniscus_position_mm
    Dim calib_optic_grid
    Dim cavispeed_rad_s_1
    Dim cavispeed_rpm


    Private Sub compute_pressure()

        Try

            rotor_diameter = TextBox1.Text
            cavispeed_rpm = RichTextBox_centri_rpm.Text
            cavispeed_rad_s_1 = 2 * Math.PI * cavispeed_rpm / 60

            'Cochard 2002, équation (3)
            ' P en MPa



            min_meniscus_position_mm = Convert.ToDouble(TextBox2.Text) * Convert.ToDouble(CAVITRON_SETUP.TextBox_pixelsize.Text)
            centri_tension = -0.25 * 1000 * cavispeed_rad_s_1 ^ 2 * ((rotor_diameter / 2 / 100) ^ 2 + ((rotor_diameter / 2 / 100) - (min_meniscus_position_mm / 1000)) ^ 2) / 1000000




            RichTextBox_centri_tension.Text = Format(centri_tension, "##0.000")

            '   max_centri_tension = -0.25 * 1000 * max_cavispeed_rad_s_1 ^ 2 * ((rotor_diameter / 2 / 100) ^ 2 + ((rotor_diameter / 2 / 100) - (min_meniscus_position * (calib_optic_grid / 50) / 10000)) ^ 2) / 1000000


        Catch ex As Exception

            MsgBox(ex.Message & "... Please try again with numeric values only", MsgBoxStyle.Exclamation)

            TextBox_opticcalibration.Text = "0"
            TextBox1.Text = "0"
            TextBox2.Text = "0"
            RichTextBox_centri_rpm.Text = "0"
            RichTextBox_centri_tension.Text = "0"
        End Try

    End Sub

    Private Sub compute_rotation_speed()

        Try

            rotor_diameter = TextBox1.Text
            centri_tension = RichTextBox_centri_tension.Text

            'Cochard 2002, équation (3)
            ' P en MPa
            ' centri_tension = -0.25 * 1000 * cavispeed_rad_s_1 ^ 2 * ((rotor_diameter / 2 / 100) ^ 2 + ((rotor_diameter / 2 / 100) - (min_meniscus_position_grad * (calib_optic_grid / 50) / 10000)) ^ 2) / 1000000



            
                    min_meniscus_position_mm = TextBox2.Text

                    cavispeed_rad_s_1 = (centri_tension / (-0.25 * 1000 * ((rotor_diameter / 2 / 100) ^ 2 + ((rotor_diameter / 2 / 100) - (min_meniscus_position_mm / 1000)) ^ 2) / 1000000)) ^ 0.5

                    cavispeed_rpm = cavispeed_rad_s_1 * 60 / 2 / Math.PI



            RichTextBox_centri_rpm.Text = Format(cavispeed_rpm, "##0")

            '   max_centri_tension = -0.25 * 1000 * max_cavispeed_rad_s_1 ^ 2 * ((rotor_diameter / 2 / 100) ^ 2 + ((rotor_diameter / 2 / 100) - (min_meniscus_position * (calib_optic_grid / 50) / 10000)) ^ 2) / 1000000

        Catch ex As Exception

            MsgBox(ex.Message & "... Please try again with numeric values only", MsgBoxStyle.Exclamation)

            TextBox_opticcalibration.Text = "0"
            TextBox1.Text = "0"
            TextBox2.Text = "0"
            RichTextBox_centri_rpm.Text = "0"
            RichTextBox_centri_tension.Text = "0"
        End Try

    End Sub



    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles Button1.Click
        Close()
    End Sub

    Private Sub centrifugation_pressure_simulation_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        RichTextBox_centri_rpm.Text = MAIN_Form.RichTextBox_centri_rpm.Text
        rotor_diameter = CAVITRON_SETUP.TextBox_rotor_diameter_cm.Text
        TextBox_opticcalibration.Text = CAVITRON_SETUP.TextBox_pixelsize.Text
        TextBox1.Text = rotor_diameter

        If MAIN_Form.TextBox_minmeniscusposition.Text = "" Then
            TextBox2.Text = "0"
        Else
            TextBox2.Text = MAIN_Form.TextBox_minmeniscusposition.Text

        End If

    End Sub

   

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_compute.Click


        Select Case ComboBox2.SelectedItem
            Case "Rotation to Pressure"
                compute_pressure()
            Case "Pressure to Rotation"
                compute_rotation_speed()
        End Select

    End Sub

    Private Sub ComboBox2_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles ComboBox2.SelectedIndexChanged

        Select Case ComboBox2.SelectedItem


            Case "Rotation to Pressure"
                RichTextBox_centri_rpm.BackColor = Color.Cornsilk
                RichTextBox_centri_rpm.Text = "0"
                RichTextBox_centri_rpm.ReadOnly = False

                RichTextBox_centri_tension.BackColor = Color.PaleGoldenrod
                RichTextBox_centri_tension.Text = "--"
                RichTextBox_centri_tension.ReadOnly = True

                Label_arrow.Text = "=>"


            Case "Pressure to Rotation"
                RichTextBox_centri_rpm.BackColor = Color.PaleGoldenrod
                RichTextBox_centri_rpm.Text = "--"
                RichTextBox_centri_rpm.ReadOnly = True

                RichTextBox_centri_tension.BackColor = Color.Cornsilk
                RichTextBox_centri_tension.Text = "-0.0"
                RichTextBox_centri_tension.ReadOnly = False

                Label_arrow.Text = "<="

        End Select

    End Sub




End Class