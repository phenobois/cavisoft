﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class CAVITRON_SETUP
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(CAVITRON_SETUP))
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.TextBox_rotor_diameter_cm = New System.Windows.Forms.TextBox()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.LinkLabel1 = New System.Windows.Forms.LinkLabel()
        Me.TextBox_maxcentri_rpm = New System.Windows.Forms.TextBox()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.RichTextBox_operator_list = New System.Windows.Forms.RichTextBox()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.ComboBox_centrifugemodel = New System.Windows.Forms.ComboBox()
        Me.Label30 = New System.Windows.Forms.Label()
        Me.ComboBox_fileextension = New System.Windows.Forms.ComboBox()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.NumericUpDown1 = New System.Windows.Forms.NumericUpDown()
        Me.Label38 = New System.Windows.Forms.Label()
        Me.NumericUpDown_beep_freq = New System.Windows.Forms.NumericUpDown()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label36 = New System.Windows.Forms.Label()
        Me.TextBox3 = New System.Windows.Forms.TextBox()
        Me.Label35 = New System.Windows.Forms.Label()
        Me.TextBox_beep_duration = New System.Windows.Forms.TextBox()
        Me.CheckBox_play_a_sound = New System.Windows.Forms.CheckBox()
        Me.Label79 = New System.Windows.Forms.Label()
        Me.Label45 = New System.Windows.Forms.Label()
        Me.NumericUpDown_autodetect_threshold = New System.Windows.Forms.NumericUpDown()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.ComboBox_Lp_SIunit_select = New System.Windows.Forms.ComboBox()
        Me.Label58 = New System.Windows.Forms.Label()
        Me.CheckBox_useSIunit = New System.Windows.Forms.CheckBox()
        Me.GroupBox_cavproperties = New System.Windows.Forms.GroupBox()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.ComboBox_rotor_name = New System.Windows.Forms.ComboBox()
        Me.ComboBox_cavispeed_setup = New System.Windows.Forms.ComboBox()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.ComboBox_cavispeed_acquisiton = New System.Windows.Forms.ComboBox()
        Me.Label94 = New System.Windows.Forms.Label()
        Me.ComboBox_temperature_measurement = New System.Windows.Forms.ComboBox()
        Me.Label60 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label59 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.ComboBox_reservoir_innersurface = New System.Windows.Forms.ComboBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.LinkLabel2 = New System.Windows.Forms.LinkLabel()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.TextBox_pixelsize = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.ComboBox_optical_source = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.TextBox_CAVITRON_NAME = New System.Windows.Forms.TextBox()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.TabControl2 = New System.Windows.Forms.TabControl()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.RichTextBox2 = New System.Windows.Forms.RichTextBox()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.TextBox_highlevel_ao1 = New System.Windows.Forms.TextBox()
        Me.TextBox_lowlevel_ao1 = New System.Windows.Forms.TextBox()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.TextBox_highlevel_ao0 = New System.Windows.Forms.TextBox()
        Me.TextBox_lowlevel_ao0 = New System.Windows.Forms.TextBox()
        Me.GroupBox_counter = New System.Windows.Forms.GroupBox()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.TextBox_offset_counter = New System.Windows.Forms.TextBox()
        Me.TextBox_gain_counter = New System.Windows.Forms.TextBox()
        Me.GroupBox_analog_IN = New System.Windows.Forms.GroupBox()
        Me.RichTextBox4 = New System.Windows.Forms.RichTextBox()
        Me.RichTextBox1 = New System.Windows.Forms.RichTextBox()
        Me.ComboBox_ai0range = New System.Windows.Forms.ComboBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Button_settodefault = New System.Windows.Forms.Button()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.TextBox_offset_ai1 = New System.Windows.Forms.TextBox()
        Me.TextBox_offset_ai0 = New System.Windows.Forms.TextBox()
        Me.TextBox_gain_ai1 = New System.Windows.Forms.TextBox()
        Me.TextBox_gain_ai0 = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.TabPage3 = New System.Windows.Forms.TabPage()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.RichTextBox3 = New System.Windows.Forms.RichTextBox()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.GroupBox_pushwheel = New System.Windows.Forms.GroupBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.TextBox_bin_pushwheel = New System.Windows.Forms.TextBox()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.LedArray_pushwheel_port1 = New NationalInstruments.UI.WindowsForms.LedArray()
        Me.LedArray_pushwheel_port0 = New NationalInstruments.UI.WindowsForms.LedArray()
        Me.ShapeContainer1 = New Microsoft.VisualBasic.PowerPacks.ShapeContainer()
        Me.RectangleShape2 = New Microsoft.VisualBasic.PowerPacks.RectangleShape()
        Me.RectangleShape1 = New Microsoft.VisualBasic.PowerPacks.RectangleShape()
        Me.GroupBox6 = New System.Windows.Forms.GroupBox()
        Me.ListBox1 = New System.Windows.Forms.ListBox()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.OK_Button = New System.Windows.Forms.Button()
        Me.Cancel_Button = New System.Windows.Forms.Button()
        Me.TabPage1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.Panel3.SuspendLayout()
        CType(Me.NumericUpDown1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDown_beep_freq, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDown_autodetect_threshold, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox4.SuspendLayout()
        Me.GroupBox_cavproperties.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.TabControl2.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        Me.GroupBox_counter.SuspendLayout()
        Me.GroupBox_analog_IN.SuspendLayout()
        Me.TabPage3.SuspendLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox_pushwheel.SuspendLayout()
        CType(Me.LedArray_pushwheel_port1.ItemTemplate, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LedArray_pushwheel_port0.ItemTemplate, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox6.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'TextBox_rotor_diameter_cm
        '
        Me.TextBox_rotor_diameter_cm.AutoCompleteCustomSource.AddRange(New String() {"26", "100", "16", "38.5"})
        Me.TextBox_rotor_diameter_cm.Location = New System.Drawing.Point(336, 38)
        Me.TextBox_rotor_diameter_cm.Name = "TextBox_rotor_diameter_cm"
        Me.TextBox_rotor_diameter_cm.Size = New System.Drawing.Size(111, 23)
        Me.TextBox_rotor_diameter_cm.TabIndex = 24
        Me.TextBox_rotor_diameter_cm.Text = "26"
        Me.TextBox_rotor_diameter_cm.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.ToolTip1.SetToolTip(Me.TextBox_rotor_diameter_cm, "For 30cm rotor => 2R = 26cm" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "For 16cm rotor => 2R = 14cm" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "For 40cm rotor => 2R = " & _
                "38.5" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "For CAVI1000 => 2R = 100cm")
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.GroupBox2)
        Me.TabPage1.Controls.Add(Me.GroupBox3)
        Me.TabPage1.Controls.Add(Me.GroupBox4)
        Me.TabPage1.Controls.Add(Me.GroupBox_cavproperties)
        Me.TabPage1.Controls.Add(Me.GroupBox1)
        Me.TabPage1.Controls.Add(Me.Label1)
        Me.TabPage1.Controls.Add(Me.TextBox_CAVITRON_NAME)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(1176, 568)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "MAIN PARAMETERS"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.Label6)
        Me.GroupBox2.Controls.Add(Me.LinkLabel1)
        Me.GroupBox2.Controls.Add(Me.TextBox_maxcentri_rpm)
        Me.GroupBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(137, 384)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(561, 67)
        Me.GroupBox2.TabIndex = 129
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "CHANGE THIS VALUE ONLY IF YOU KNOW WHAT YOU'RE DOING !!"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(307, 30)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(32, 17)
        Me.Label6.TabIndex = 2
        Me.Label6.Text = "rpm"
        '
        'LinkLabel1
        '
        Me.LinkLabel1.AutoSize = True
        Me.LinkLabel1.Location = New System.Drawing.Point(6, 27)
        Me.LinkLabel1.Name = "LinkLabel1"
        Me.LinkLabel1.Size = New System.Drawing.Size(215, 17)
        Me.LinkLabel1.TabIndex = 1
        Me.LinkLabel1.TabStop = True
        Me.LinkLabel1.Text = "Set maximum possible cavispeed"
        '
        'TextBox_maxcentri_rpm
        '
        Me.TextBox_maxcentri_rpm.Location = New System.Drawing.Point(225, 27)
        Me.TextBox_maxcentri_rpm.Name = "TextBox_maxcentri_rpm"
        Me.TextBox_maxcentri_rpm.Size = New System.Drawing.Size(76, 23)
        Me.TextBox_maxcentri_rpm.TabIndex = 0
        Me.TextBox_maxcentri_rpm.Text = "10000"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.RichTextBox_operator_list)
        Me.GroupBox3.Controls.Add(Me.Label31)
        Me.GroupBox3.Controls.Add(Me.ComboBox_centrifugemodel)
        Me.GroupBox3.Controls.Add(Me.Label30)
        Me.GroupBox3.Controls.Add(Me.ComboBox_fileextension)
        Me.GroupBox3.Controls.Add(Me.Panel3)
        Me.GroupBox3.Controls.Add(Me.Label79)
        Me.GroupBox3.Controls.Add(Me.Label45)
        Me.GroupBox3.Controls.Add(Me.NumericUpDown_autodetect_threshold)
        Me.GroupBox3.Location = New System.Drawing.Point(785, 254)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(346, 278)
        Me.GroupBox3.TabIndex = 131
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "MISC"
        '
        'RichTextBox_operator_list
        '
        Me.RichTextBox_operator_list.Location = New System.Drawing.Point(10, 182)
        Me.RichTextBox_operator_list.Name = "RichTextBox_operator_list"
        Me.RichTextBox_operator_list.Size = New System.Drawing.Size(329, 42)
        Me.RichTextBox_operator_list.TabIndex = 127
        Me.RichTextBox_operator_list.Text = ""
        '
        'Label31
        '
        Me.Label31.AutoSize = True
        Me.Label31.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label31.Location = New System.Drawing.Point(7, 163)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(202, 13)
        Me.Label31.TabIndex = 126
        Me.Label31.Text = "Operator list   !! list separator is COMMA !!"
        '
        'ComboBox_centrifugemodel
        '
        Me.ComboBox_centrifugemodel.AutoCompleteCustomSource.AddRange(New String() {"plate 1", "plate 2", "plate 3", "plate 4", "plate 5", "plate 6", "plate 7"})
        Me.ComboBox_centrifugemodel.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.ComboBox_centrifugemodel.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.ComboBox_centrifugemodel.FormattingEnabled = True
        Me.ComboBox_centrifugemodel.Items.AddRange(New Object() {"Sorvall RC serie", "Beckman coulter", "CAVI 1000", "Custom"})
        Me.ComboBox_centrifugemodel.Location = New System.Drawing.Point(160, 71)
        Me.ComboBox_centrifugemodel.Name = "ComboBox_centrifugemodel"
        Me.ComboBox_centrifugemodel.Size = New System.Drawing.Size(141, 21)
        Me.ComboBox_centrifugemodel.TabIndex = 125
        '
        'Label30
        '
        Me.Label30.AutoSize = True
        Me.Label30.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label30.Location = New System.Drawing.Point(45, 74)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(86, 13)
        Me.Label30.TabIndex = 124
        Me.Label30.Text = "Centrifuge model"
        '
        'ComboBox_fileextension
        '
        Me.ComboBox_fileextension.AutoCompleteCustomSource.AddRange(New String() {"plate 1", "plate 2", "plate 3", "plate 4", "plate 5", "plate 6", "plate 7"})
        Me.ComboBox_fileextension.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.ComboBox_fileextension.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.ComboBox_fileextension.FormattingEnabled = True
        Me.ComboBox_fileextension.Items.AddRange(New Object() {".dat", ".txt", ".csv", ".xls"})
        Me.ComboBox_fileextension.Location = New System.Drawing.Point(161, 45)
        Me.ComboBox_fileextension.Name = "ComboBox_fileextension"
        Me.ComboBox_fileextension.Size = New System.Drawing.Size(57, 21)
        Me.ComboBox_fileextension.TabIndex = 120
        Me.ComboBox_fileextension.Text = ".csv"
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.NumericUpDown1)
        Me.Panel3.Controls.Add(Me.Label38)
        Me.Panel3.Controls.Add(Me.NumericUpDown_beep_freq)
        Me.Panel3.Controls.Add(Me.Label9)
        Me.Panel3.Controls.Add(Me.Label36)
        Me.Panel3.Controls.Add(Me.TextBox3)
        Me.Panel3.Controls.Add(Me.Label35)
        Me.Panel3.Controls.Add(Me.TextBox_beep_duration)
        Me.Panel3.Controls.Add(Me.CheckBox_play_a_sound)
        Me.Panel3.Location = New System.Drawing.Point(48, 98)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(291, 58)
        Me.Panel3.TabIndex = 121
        '
        'NumericUpDown1
        '
        Me.NumericUpDown1.Increment = New Decimal(New Integer() {10, 0, 0, 0})
        Me.NumericUpDown1.Location = New System.Drawing.Point(116, 10)
        Me.NumericUpDown1.Maximum = New Decimal(New Integer() {10000, 0, 0, 0})
        Me.NumericUpDown1.Minimum = New Decimal(New Integer() {10, 0, 0, 0})
        Me.NumericUpDown1.Name = "NumericUpDown1"
        Me.NumericUpDown1.Size = New System.Drawing.Size(49, 20)
        Me.NumericUpDown1.TabIndex = 0
        Me.NumericUpDown1.Value = New Decimal(New Integer() {250, 0, 0, 0})
        '
        'Label38
        '
        Me.Label38.AutoSize = True
        Me.Label38.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label38.Location = New System.Drawing.Point(20, 36)
        Me.Label38.Name = "Label38"
        Me.Label38.Size = New System.Drawing.Size(73, 13)
        Me.Label38.TabIndex = 78
        Me.Label38.Text = "Beep duration"
        '
        'NumericUpDown_beep_freq
        '
        Me.NumericUpDown_beep_freq.Increment = New Decimal(New Integer() {10, 0, 0, 0})
        Me.NumericUpDown_beep_freq.Location = New System.Drawing.Point(114, 10)
        Me.NumericUpDown_beep_freq.Maximum = New Decimal(New Integer() {10000, 0, 0, 0})
        Me.NumericUpDown_beep_freq.Minimum = New Decimal(New Integer() {10, 0, 0, 0})
        Me.NumericUpDown_beep_freq.Name = "NumericUpDown_beep_freq"
        Me.NumericUpDown_beep_freq.Size = New System.Drawing.Size(49, 20)
        Me.NumericUpDown_beep_freq.TabIndex = 80
        Me.NumericUpDown_beep_freq.Value = New Decimal(New Integer() {250, 0, 0, 0})
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label9.Location = New System.Drawing.Point(20, 12)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(82, 13)
        Me.Label9.TabIndex = 77
        Me.Label9.Text = "Beep frequency"
        '
        'Label36
        '
        Me.Label36.AutoSize = True
        Me.Label36.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label36.Location = New System.Drawing.Point(18, 36)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(72, 13)
        Me.Label36.TabIndex = 78
        Me.Label36.Text = "beep duration"
        '
        'TextBox3
        '
        Me.TextBox3.Location = New System.Drawing.Point(116, 33)
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.Size = New System.Drawing.Size(49, 20)
        Me.TextBox3.TabIndex = 1
        Me.TextBox3.Text = "80"
        '
        'Label35
        '
        Me.Label35.AutoSize = True
        Me.Label35.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label35.Location = New System.Drawing.Point(18, 12)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(81, 13)
        Me.Label35.TabIndex = 77
        Me.Label35.Text = "beep frequency"
        '
        'TextBox_beep_duration
        '
        Me.TextBox_beep_duration.Location = New System.Drawing.Point(114, 33)
        Me.TextBox_beep_duration.Name = "TextBox_beep_duration"
        Me.TextBox_beep_duration.Size = New System.Drawing.Size(49, 20)
        Me.TextBox_beep_duration.TabIndex = 76
        Me.TextBox_beep_duration.Text = "80"
        '
        'CheckBox_play_a_sound
        '
        Me.CheckBox_play_a_sound.AutoSize = True
        Me.CheckBox_play_a_sound.Checked = True
        Me.CheckBox_play_a_sound.CheckState = System.Windows.Forms.CheckState.Checked
        Me.CheckBox_play_a_sound.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.CheckBox_play_a_sound.Location = New System.Drawing.Point(170, 11)
        Me.CheckBox_play_a_sound.Name = "CheckBox_play_a_sound"
        Me.CheckBox_play_a_sound.Size = New System.Drawing.Size(122, 17)
        Me.CheckBox_play_a_sound.TabIndex = 46
        Me.CheckBox_play_a_sound.Text = "Play a sound on tick"
        Me.CheckBox_play_a_sound.UseVisualStyleBackColor = True
        '
        'Label79
        '
        Me.Label79.AutoSize = True
        Me.Label79.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label79.Location = New System.Drawing.Point(7, 21)
        Me.Label79.Name = "Label79"
        Me.Label79.Size = New System.Drawing.Size(146, 13)
        Me.Label79.TabIndex = 123
        Me.Label79.Text = "ROI autodetect threshold (bit)"
        '
        'Label45
        '
        Me.Label45.AutoSize = True
        Me.Label45.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label45.Location = New System.Drawing.Point(46, 48)
        Me.Label45.Name = "Label45"
        Me.Label45.Size = New System.Drawing.Size(108, 13)
        Me.Label45.TabIndex = 119
        Me.Label45.Text = "Extension for data file"
        '
        'NumericUpDown_autodetect_threshold
        '
        Me.NumericUpDown_autodetect_threshold.Location = New System.Drawing.Point(161, 19)
        Me.NumericUpDown_autodetect_threshold.Minimum = New Decimal(New Integer() {2, 0, 0, 0})
        Me.NumericUpDown_autodetect_threshold.Name = "NumericUpDown_autodetect_threshold"
        Me.NumericUpDown_autodetect_threshold.Size = New System.Drawing.Size(59, 20)
        Me.NumericUpDown_autodetect_threshold.TabIndex = 122
        Me.NumericUpDown_autodetect_threshold.Value = New Decimal(New Integer() {40, 0, 0, 0})
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.ComboBox_Lp_SIunit_select)
        Me.GroupBox4.Controls.Add(Me.Label58)
        Me.GroupBox4.Controls.Add(Me.CheckBox_useSIunit)
        Me.GroupBox4.Location = New System.Drawing.Point(137, 465)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(561, 67)
        Me.GroupBox4.TabIndex = 132
        Me.GroupBox4.TabStop = False
        '
        'ComboBox_Lp_SIunit_select
        '
        Me.ComboBox_Lp_SIunit_select.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox_Lp_SIunit_select.FormattingEnabled = True
        Me.ComboBox_Lp_SIunit_select.Items.AddRange(New Object() {"kg.m-1.Mpa-1.s-1", "mol.Mpa-1.s-1", "m².Mpa-1.s-1"})
        Me.ComboBox_Lp_SIunit_select.Location = New System.Drawing.Point(182, 15)
        Me.ComboBox_Lp_SIunit_select.Name = "ComboBox_Lp_SIunit_select"
        Me.ComboBox_Lp_SIunit_select.Size = New System.Drawing.Size(155, 24)
        Me.ComboBox_Lp_SIunit_select.TabIndex = 118
        Me.ComboBox_Lp_SIunit_select.Text = "kg.m-1.Mpa-1.s-1"
        '
        'Label58
        '
        Me.Label58.AutoSize = True
        Me.Label58.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold)
        Me.Label58.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label58.Location = New System.Drawing.Point(15, 16)
        Me.Label58.Name = "Label58"
        Me.Label58.Size = New System.Drawing.Size(132, 18)
        Me.Label58.TabIndex = 117
        Me.Label58.Text = "Conductivity unit"
        '
        'CheckBox_useSIunit
        '
        Me.CheckBox_useSIunit.AutoSize = True
        Me.CheckBox_useSIunit.Checked = True
        Me.CheckBox_useSIunit.CheckState = System.Windows.Forms.CheckState.Checked
        Me.CheckBox_useSIunit.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.CheckBox_useSIunit.Location = New System.Drawing.Point(182, 45)
        Me.CheckBox_useSIunit.Name = "CheckBox_useSIunit"
        Me.CheckBox_useSIunit.Size = New System.Drawing.Size(149, 17)
        Me.CheckBox_useSIunit.TabIndex = 116
        Me.CheckBox_useSIunit.Text = "Use conductivity in SI unit"
        Me.CheckBox_useSIunit.UseVisualStyleBackColor = True
        '
        'GroupBox_cavproperties
        '
        Me.GroupBox_cavproperties.Controls.Add(Me.Label29)
        Me.GroupBox_cavproperties.Controls.Add(Me.Label28)
        Me.GroupBox_cavproperties.Controls.Add(Me.ComboBox_rotor_name)
        Me.GroupBox_cavproperties.Controls.Add(Me.ComboBox_cavispeed_setup)
        Me.GroupBox_cavproperties.Controls.Add(Me.Label27)
        Me.GroupBox_cavproperties.Controls.Add(Me.ComboBox_cavispeed_acquisiton)
        Me.GroupBox_cavproperties.Controls.Add(Me.Label94)
        Me.GroupBox_cavproperties.Controls.Add(Me.TextBox_rotor_diameter_cm)
        Me.GroupBox_cavproperties.Controls.Add(Me.ComboBox_temperature_measurement)
        Me.GroupBox_cavproperties.Controls.Add(Me.Label60)
        Me.GroupBox_cavproperties.Controls.Add(Me.Label5)
        Me.GroupBox_cavproperties.Controls.Add(Me.Label59)
        Me.GroupBox_cavproperties.Controls.Add(Me.Label7)
        Me.GroupBox_cavproperties.Controls.Add(Me.ComboBox_reservoir_innersurface)
        Me.GroupBox_cavproperties.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox_cavproperties.Location = New System.Drawing.Point(136, 87)
        Me.GroupBox_cavproperties.Name = "GroupBox_cavproperties"
        Me.GroupBox_cavproperties.Size = New System.Drawing.Size(562, 291)
        Me.GroupBox_cavproperties.TabIndex = 130
        Me.GroupBox_cavproperties.TabStop = False
        Me.GroupBox_cavproperties.Text = "CAVITRON PROPERTIES"
        '
        'Label29
        '
        Me.Label29.AutoSize = True
        Me.Label29.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label29.Location = New System.Drawing.Point(234, 77)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(82, 17)
        Me.Label29.TabIndex = 23
        Me.Label29.Text = "Rotor name"
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label28.Location = New System.Drawing.Point(6, 252)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(152, 17)
        Me.Label28.TabIndex = 129
        Me.Label28.Text = "Cavispeed setup mode"
        '
        'ComboBox_rotor_name
        '
        Me.ComboBox_rotor_name.FormattingEnabled = True
        Me.ComboBox_rotor_name.Items.AddRange(New Object() {"CAVI1000", "rotor 1", "rotor 2", "rotor 3", "rotor 4", "rotor 5", "rotor 6", "rotor 7", "rotor 8", "rotor 9", "rotor 10", "rotor 11", "rotor 12", "rotor 13", "rotor 14", "rotor 15", "rotor 16", "rotor 17", "rotor 18", "rotor 20"})
        Me.ComboBox_rotor_name.Location = New System.Drawing.Point(335, 74)
        Me.ComboBox_rotor_name.Name = "ComboBox_rotor_name"
        Me.ComboBox_rotor_name.Size = New System.Drawing.Size(112, 24)
        Me.ComboBox_rotor_name.TabIndex = 0
        '
        'ComboBox_cavispeed_setup
        '
        Me.ComboBox_cavispeed_setup.BackColor = System.Drawing.SystemColors.Window
        Me.ComboBox_cavispeed_setup.FormattingEnabled = True
        Me.ComboBox_cavispeed_setup.Items.AddRange(New Object() {"Set cavispeed (rpm)", "Set centrifugation pressure (Mpa)", "Use centrifuge setting (MANUAL MODE)"})
        Me.ComboBox_cavispeed_setup.Location = New System.Drawing.Point(164, 252)
        Me.ComboBox_cavispeed_setup.Name = "ComboBox_cavispeed_setup"
        Me.ComboBox_cavispeed_setup.Size = New System.Drawing.Size(280, 24)
        Me.ComboBox_cavispeed_setup.TabIndex = 128
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label27.Location = New System.Drawing.Point(6, 208)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(185, 17)
        Me.Label27.TabIndex = 127
        Me.Label27.Text = "Cavispeed Acquisition mode"
        '
        'ComboBox_cavispeed_acquisiton
        '
        Me.ComboBox_cavispeed_acquisiton.FormattingEnabled = True
        Me.ComboBox_cavispeed_acquisiton.Items.AddRange(New Object() {"NI_USB_6008 channel ai0", "Sorvall RC5C+ frequency read", "CAVI 1000", "NONE"})
        Me.ComboBox_cavispeed_acquisiton.Location = New System.Drawing.Point(245, 205)
        Me.ComboBox_cavispeed_acquisiton.Name = "ComboBox_cavispeed_acquisiton"
        Me.ComboBox_cavispeed_acquisiton.Size = New System.Drawing.Size(199, 24)
        Me.ComboBox_cavispeed_acquisiton.TabIndex = 126
        '
        'Label94
        '
        Me.Label94.AutoSize = True
        Me.Label94.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label94.Location = New System.Drawing.Point(6, 144)
        Me.Label94.Name = "Label94"
        Me.Label94.Size = New System.Drawing.Size(295, 17)
        Me.Label94.TabIndex = 87
        Me.Label94.Text = "Source used for temperature MESUREMENT:"
        '
        'ComboBox_temperature_measurement
        '
        Me.ComboBox_temperature_measurement.AllowDrop = True
        Me.ComboBox_temperature_measurement.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append
        Me.ComboBox_temperature_measurement.FormattingEnabled = True
        Me.ComboBox_temperature_measurement.Items.AddRange(New Object() {"NONE", "dev1/ai0", "dev1/ai1", "CAVI 1000"})
        Me.ComboBox_temperature_measurement.Location = New System.Drawing.Point(39, 164)
        Me.ComboBox_temperature_measurement.Name = "ComboBox_temperature_measurement"
        Me.ComboBox_temperature_measurement.Size = New System.Drawing.Size(405, 24)
        Me.ComboBox_temperature_measurement.TabIndex = 86
        '
        'Label60
        '
        Me.Label60.AutoSize = True
        Me.Label60.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label60.Location = New System.Drawing.Point(160, 112)
        Me.Label60.Name = "Label60"
        Me.Label60.Size = New System.Drawing.Size(156, 17)
        Me.Label60.TabIndex = 120
        Me.Label60.Text = "Reservoir inner surface"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label5.Location = New System.Drawing.Point(448, 41)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(26, 17)
        Me.Label5.TabIndex = 23
        Me.Label5.Text = "cm"
        '
        'Label59
        '
        Me.Label59.AutoSize = True
        Me.Label59.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label59.Location = New System.Drawing.Point(448, 120)
        Me.Label59.Name = "Label59"
        Me.Label59.Size = New System.Drawing.Size(24, 17)
        Me.Label59.TabIndex = 119
        Me.Label59.Text = "m²"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label7.Location = New System.Drawing.Point(121, 41)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(195, 17)
        Me.Label7.TabIndex = 22
        Me.Label7.Text = "Maximum rotor diameter ( 2R)"
        '
        'ComboBox_reservoir_innersurface
        '
        Me.ComboBox_reservoir_innersurface.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox_reservoir_innersurface.FormattingEnabled = True
        Me.ComboBox_reservoir_innersurface.Items.AddRange(New Object() {"0.0001", "0.00049087", "0.00138544", "0.00038013"})
        Me.ComboBox_reservoir_innersurface.Location = New System.Drawing.Point(335, 109)
        Me.ComboBox_reservoir_innersurface.Name = "ComboBox_reservoir_innersurface"
        Me.ComboBox_reservoir_innersurface.Size = New System.Drawing.Size(109, 24)
        Me.ComboBox_reservoir_innersurface.TabIndex = 118
        Me.ComboBox_reservoir_innersurface.Text = "0.0001"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.LinkLabel2)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.TextBox_pixelsize)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.ComboBox_optical_source)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(782, 87)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(349, 161)
        Me.GroupBox1.TabIndex = 128
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Optical parameter"
        '
        'LinkLabel2
        '
        Me.LinkLabel2.AutoSize = True
        Me.LinkLabel2.Location = New System.Drawing.Point(15, 95)
        Me.LinkLabel2.Name = "LinkLabel2"
        Me.LinkLabel2.Size = New System.Drawing.Size(80, 17)
        Me.LinkLabel2.TabIndex = 97
        Me.LinkLabel2.TabStop = True
        Me.LinkLabel2.Text = "PIXEL SIZE"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label4.Location = New System.Drawing.Point(227, 97)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(75, 17)
        Me.Label4.TabIndex = 96
        Me.Label4.Text = "mm.pixel-1"
        '
        'TextBox_pixelsize
        '
        Me.TextBox_pixelsize.AutoCompleteCustomSource.AddRange(New String() {"Cavitron 1", "Cavitron 2", "Cavitron 3", "Cavitron 4", "Cavitron 5", "Cavitron 6", "Cavitron 7", "Cavitron 8", "CAVI1000"})
        Me.TextBox_pixelsize.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox_pixelsize.Location = New System.Drawing.Point(128, 86)
        Me.TextBox_pixelsize.Name = "TextBox_pixelsize"
        Me.TextBox_pixelsize.Size = New System.Drawing.Size(93, 29)
        Me.TextBox_pixelsize.TabIndex = 20
        Me.TextBox_pixelsize.Text = "0.01"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label2.Location = New System.Drawing.Point(15, 41)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(99, 17)
        Me.Label2.TabIndex = 94
        Me.Label2.Text = "Optical source"
        '
        'ComboBox_optical_source
        '
        Me.ComboBox_optical_source.FormattingEnabled = True
        Me.ComboBox_optical_source.Items.AddRange(New Object() {"", "BASLER sca640", "stereo microscope"})
        Me.ComboBox_optical_source.Location = New System.Drawing.Point(128, 42)
        Me.ComboBox_optical_source.Name = "ComboBox_optical_source"
        Me.ComboBox_optical_source.Size = New System.Drawing.Size(147, 24)
        Me.ComboBox_optical_source.TabIndex = 18
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(141, 32)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(173, 24)
        Me.Label1.TabIndex = 127
        Me.Label1.Text = "CAVITRON NAME:"
        '
        'TextBox_CAVITRON_NAME
        '
        Me.TextBox_CAVITRON_NAME.AutoCompleteCustomSource.AddRange(New String() {"Cavitron 1", "Cavitron 2", "Cavitron 3", "Cavitron 4", "Cavitron 5", "Cavitron 6", "Cavitron 7", "Cavitron 8", "CAVI1000"})
        Me.TextBox_CAVITRON_NAME.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox_CAVITRON_NAME.Location = New System.Drawing.Point(328, 29)
        Me.TextBox_CAVITRON_NAME.Name = "TextBox_CAVITRON_NAME"
        Me.TextBox_CAVITRON_NAME.Size = New System.Drawing.Size(289, 29)
        Me.TextBox_CAVITRON_NAME.TabIndex = 126
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(1075, 614)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(117, 30)
        Me.Button2.TabIndex = 128
        Me.Button2.Text = "update My.settings"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'TabControl2
        '
        Me.TabControl2.Controls.Add(Me.TabPage1)
        Me.TabControl2.Controls.Add(Me.TabPage2)
        Me.TabControl2.Controls.Add(Me.TabPage3)
        Me.TabControl2.Location = New System.Drawing.Point(12, 12)
        Me.TabControl2.Name = "TabControl2"
        Me.TabControl2.Padding = New System.Drawing.Point(99, 3)
        Me.TabControl2.SelectedIndex = 0
        Me.TabControl2.Size = New System.Drawing.Size(1184, 594)
        Me.TabControl2.TabIndex = 127
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.RichTextBox2)
        Me.TabPage2.Controls.Add(Me.GroupBox5)
        Me.TabPage2.Controls.Add(Me.GroupBox_counter)
        Me.TabPage2.Controls.Add(Me.GroupBox_analog_IN)
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(1176, 568)
        Me.TabPage2.TabIndex = 0
        Me.TabPage2.Text = "NI_USB_6008"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'RichTextBox2
        '
        Me.RichTextBox2.BackColor = System.Drawing.Color.LightGray
        Me.RichTextBox2.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.RichTextBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RichTextBox2.ForeColor = System.Drawing.Color.DarkRed
        Me.RichTextBox2.Location = New System.Drawing.Point(17, 16)
        Me.RichTextBox2.Name = "RichTextBox2"
        Me.RichTextBox2.ReadOnly = True
        Me.RichTextBox2.Size = New System.Drawing.Size(1068, 59)
        Me.RichTextBox2.TabIndex = 32
        Me.RichTextBox2.Text = "IMPORTANT: THIS DEVICE MUST BE CONFIGURED AS "" dev 1"" !! If it's not the case, pl" & _
            "ease rename it using National Instrument software : MAX"
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.Label26)
        Me.GroupBox5.Controls.Add(Me.Label24)
        Me.GroupBox5.Controls.Add(Me.TextBox_highlevel_ao1)
        Me.GroupBox5.Controls.Add(Me.TextBox_lowlevel_ao1)
        Me.GroupBox5.Controls.Add(Me.Label21)
        Me.GroupBox5.Controls.Add(Me.Label22)
        Me.GroupBox5.Controls.Add(Me.Label23)
        Me.GroupBox5.Controls.Add(Me.TextBox_highlevel_ao0)
        Me.GroupBox5.Controls.Add(Me.TextBox_lowlevel_ao0)
        Me.GroupBox5.Location = New System.Drawing.Point(587, 329)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(489, 177)
        Me.GroupBox5.TabIndex = 35
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "analog OUT"
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label26.Location = New System.Drawing.Point(128, 143)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(214, 13)
        Me.Label26.TabIndex = 42
        Me.Label26.Text = "value required to trigger solenoid valve relay"
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Location = New System.Drawing.Point(128, 96)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(54, 13)
        Me.Label24.TabIndex = 40
        Me.Label24.Text = "dev1/ao1"
        '
        'TextBox_highlevel_ao1
        '
        Me.TextBox_highlevel_ao1.Enabled = False
        Me.TextBox_highlevel_ao1.Location = New System.Drawing.Point(346, 93)
        Me.TextBox_highlevel_ao1.Name = "TextBox_highlevel_ao1"
        Me.TextBox_highlevel_ao1.Size = New System.Drawing.Size(75, 20)
        Me.TextBox_highlevel_ao1.TabIndex = 39
        Me.TextBox_highlevel_ao1.TabStop = False
        Me.TextBox_highlevel_ao1.Text = "5000"
        Me.TextBox_highlevel_ao1.UseWaitCursor = True
        '
        'TextBox_lowlevel_ao1
        '
        Me.TextBox_lowlevel_ao1.Enabled = False
        Me.TextBox_lowlevel_ao1.Location = New System.Drawing.Point(195, 93)
        Me.TextBox_lowlevel_ao1.Name = "TextBox_lowlevel_ao1"
        Me.TextBox_lowlevel_ao1.Size = New System.Drawing.Size(75, 20)
        Me.TextBox_lowlevel_ao1.TabIndex = 38
        Me.TextBox_lowlevel_ao1.Text = "0"
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label21.Location = New System.Drawing.Point(342, 34)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(127, 20)
        Me.Label21.TabIndex = 37
        Me.Label21.Text = "high level (mV)"
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label22.Location = New System.Drawing.Point(191, 34)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(125, 20)
        Me.Label22.TabIndex = 36
        Me.Label22.Text = "Low level (mV)"
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Location = New System.Drawing.Point(61, 67)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(126, 13)
        Me.Label23.TabIndex = 35
        Me.Label23.Text = "reservoir filling :dev1/ao0"
        '
        'TextBox_highlevel_ao0
        '
        Me.TextBox_highlevel_ao0.Location = New System.Drawing.Point(346, 64)
        Me.TextBox_highlevel_ao0.Name = "TextBox_highlevel_ao0"
        Me.TextBox_highlevel_ao0.Size = New System.Drawing.Size(75, 20)
        Me.TextBox_highlevel_ao0.TabIndex = 34
        Me.TextBox_highlevel_ao0.Text = "5000"
        '
        'TextBox_lowlevel_ao0
        '
        Me.TextBox_lowlevel_ao0.Location = New System.Drawing.Point(195, 64)
        Me.TextBox_lowlevel_ao0.Name = "TextBox_lowlevel_ao0"
        Me.TextBox_lowlevel_ao0.Size = New System.Drawing.Size(75, 20)
        Me.TextBox_lowlevel_ao0.TabIndex = 33
        Me.TextBox_lowlevel_ao0.Text = "0"
        '
        'GroupBox_counter
        '
        Me.GroupBox_counter.Controls.Add(Me.Label25)
        Me.GroupBox_counter.Controls.Add(Me.Label19)
        Me.GroupBox_counter.Controls.Add(Me.Label20)
        Me.GroupBox_counter.Controls.Add(Me.Label17)
        Me.GroupBox_counter.Controls.Add(Me.TextBox_offset_counter)
        Me.GroupBox_counter.Controls.Add(Me.TextBox_gain_counter)
        Me.GroupBox_counter.Location = New System.Drawing.Point(596, 92)
        Me.GroupBox_counter.Name = "GroupBox_counter"
        Me.GroupBox_counter.Size = New System.Drawing.Size(489, 173)
        Me.GroupBox_counter.TabIndex = 34
        Me.GroupBox_counter.TabStop = False
        Me.GroupBox_counter.Text = "COUNTER"
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label25.Location = New System.Drawing.Point(121, 113)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(288, 13)
        Me.Label25.TabIndex = 41
        Me.Label25.Text = "Frequency output calibration for Sorvall RC5+ at 1000 msec"
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.Location = New System.Drawing.Point(342, 35)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(56, 20)
        Me.Label19.TabIndex = 32
        Me.Label19.Text = "offset"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.Location = New System.Drawing.Point(191, 35)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(43, 20)
        Me.Label20.TabIndex = 31
        Me.Label20.Text = "gain"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(121, 65)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(53, 13)
        Me.Label17.TabIndex = 30
        Me.Label17.Text = "dev1/pfi0"
        '
        'TextBox_offset_counter
        '
        Me.TextBox_offset_counter.Location = New System.Drawing.Point(346, 62)
        Me.TextBox_offset_counter.Name = "TextBox_offset_counter"
        Me.TextBox_offset_counter.Size = New System.Drawing.Size(75, 20)
        Me.TextBox_offset_counter.TabIndex = 29
        Me.TextBox_offset_counter.Text = "0"
        '
        'TextBox_gain_counter
        '
        Me.TextBox_gain_counter.Location = New System.Drawing.Point(195, 62)
        Me.TextBox_gain_counter.Name = "TextBox_gain_counter"
        Me.TextBox_gain_counter.Size = New System.Drawing.Size(75, 20)
        Me.TextBox_gain_counter.TabIndex = 28
        Me.TextBox_gain_counter.Text = "3.75310810645"
        '
        'GroupBox_analog_IN
        '
        Me.GroupBox_analog_IN.Controls.Add(Me.RichTextBox4)
        Me.GroupBox_analog_IN.Controls.Add(Me.RichTextBox1)
        Me.GroupBox_analog_IN.Controls.Add(Me.ComboBox_ai0range)
        Me.GroupBox_analog_IN.Controls.Add(Me.Label3)
        Me.GroupBox_analog_IN.Controls.Add(Me.Button_settodefault)
        Me.GroupBox_analog_IN.Controls.Add(Me.Label8)
        Me.GroupBox_analog_IN.Controls.Add(Me.Label12)
        Me.GroupBox_analog_IN.Controls.Add(Me.TextBox_offset_ai1)
        Me.GroupBox_analog_IN.Controls.Add(Me.TextBox_offset_ai0)
        Me.GroupBox_analog_IN.Controls.Add(Me.TextBox_gain_ai1)
        Me.GroupBox_analog_IN.Controls.Add(Me.TextBox_gain_ai0)
        Me.GroupBox_analog_IN.Controls.Add(Me.Label13)
        Me.GroupBox_analog_IN.Controls.Add(Me.Label14)
        Me.GroupBox_analog_IN.Location = New System.Drawing.Point(17, 92)
        Me.GroupBox_analog_IN.Name = "GroupBox_analog_IN"
        Me.GroupBox_analog_IN.Size = New System.Drawing.Size(481, 414)
        Me.GroupBox_analog_IN.TabIndex = 33
        Me.GroupBox_analog_IN.TabStop = False
        Me.GroupBox_analog_IN.Text = "analog IN"
        '
        'RichTextBox4
        '
        Me.RichTextBox4.BackColor = System.Drawing.Color.Khaki
        Me.RichTextBox4.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.RichTextBox4.Location = New System.Drawing.Point(172, 271)
        Me.RichTextBox4.Name = "RichTextBox4"
        Me.RichTextBox4.ReadOnly = True
        Me.RichTextBox4.Size = New System.Drawing.Size(278, 75)
        Me.RichTextBox4.TabIndex = 32
        Me.RichTextBox4.Text = "If you use a EE08 probe, please use differential channel 1. " & Global.Microsoft.VisualBasic.ChrW(10) & "Connect as follow:" & Global.Microsoft.VisualBasic.ChrW(10) & "A" & _
            "I1 (+) et AI5 (-) " & Global.Microsoft.VisualBasic.ChrW(10) & "0V => -40°C" & Global.Microsoft.VisualBasic.ChrW(10) & "10V=> 80°C"
        '
        'RichTextBox1
        '
        Me.RichTextBox1.BackColor = System.Drawing.Color.Khaki
        Me.RichTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.RichTextBox1.Location = New System.Drawing.Point(172, 103)
        Me.RichTextBox1.Name = "RichTextBox1"
        Me.RichTextBox1.ReadOnly = True
        Me.RichTextBox1.Size = New System.Drawing.Size(278, 59)
        Me.RichTextBox1.TabIndex = 31
        Me.RichTextBox1.Text = "If you use an analog tachymeter, please use differential channel 0. " & Global.Microsoft.VisualBasic.ChrW(10) & "Connect as f" & _
            "ollow:" & Global.Microsoft.VisualBasic.ChrW(10) & "AI0 (+) et AI4 (-) "
        '
        'ComboBox_ai0range
        '
        Me.ComboBox_ai0range.FormattingEnabled = True
        Me.ComboBox_ai0range.Items.AddRange(New Object() {"±10 V", "±5 V", "±4 V", "±2.5 V", "±2 V", "±1.25 V", "±1 V"})
        Me.ComboBox_ai0range.Location = New System.Drawing.Point(345, 62)
        Me.ComboBox_ai0range.Name = "ComboBox_ai0range"
        Me.ComboBox_ai0range.Size = New System.Drawing.Size(98, 21)
        Me.ComboBox_ai0range.TabIndex = 30
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(341, 35)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(84, 20)
        Me.Label3.TabIndex = 28
        Me.Label3.Text = "range (V)"
        '
        'Button_settodefault
        '
        Me.Button_settodefault.Location = New System.Drawing.Point(226, 353)
        Me.Button_settodefault.Name = "Button_settodefault"
        Me.Button_settodefault.Size = New System.Drawing.Size(111, 21)
        Me.Button_settodefault.TabIndex = 27
        Me.Button_settodefault.Text = "set to default"
        Me.Button_settodefault.UseVisualStyleBackColor = True
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(251, 35)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(56, 20)
        Me.Label8.TabIndex = 26
        Me.Label8.Text = "offset"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(161, 35)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(43, 20)
        Me.Label12.TabIndex = 24
        Me.Label12.Text = "gain"
        '
        'TextBox_offset_ai1
        '
        Me.TextBox_offset_ai1.Location = New System.Drawing.Point(262, 244)
        Me.TextBox_offset_ai1.Name = "TextBox_offset_ai1"
        Me.TextBox_offset_ai1.Size = New System.Drawing.Size(75, 20)
        Me.TextBox_offset_ai1.TabIndex = 17
        Me.TextBox_offset_ai1.Text = "-40"
        '
        'TextBox_offset_ai0
        '
        Me.TextBox_offset_ai0.Location = New System.Drawing.Point(255, 62)
        Me.TextBox_offset_ai0.Name = "TextBox_offset_ai0"
        Me.TextBox_offset_ai0.Size = New System.Drawing.Size(75, 20)
        Me.TextBox_offset_ai0.TabIndex = 16
        Me.TextBox_offset_ai0.Text = "0"
        '
        'TextBox_gain_ai1
        '
        Me.TextBox_gain_ai1.Location = New System.Drawing.Point(172, 244)
        Me.TextBox_gain_ai1.Name = "TextBox_gain_ai1"
        Me.TextBox_gain_ai1.Size = New System.Drawing.Size(75, 20)
        Me.TextBox_gain_ai1.TabIndex = 9
        Me.TextBox_gain_ai1.Text = "12"
        '
        'TextBox_gain_ai0
        '
        Me.TextBox_gain_ai0.Location = New System.Drawing.Point(165, 62)
        Me.TextBox_gain_ai0.Name = "TextBox_gain_ai0"
        Me.TextBox_gain_ai0.Size = New System.Drawing.Size(75, 20)
        Me.TextBox_gain_ai0.TabIndex = 8
        Me.TextBox_gain_ai0.Text = "1"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(20, 247)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(146, 13)
        Me.Label13.TabIndex = 1
        Me.Label13.Text = "analog thermometer dev1/ai1"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(20, 65)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(133, 13)
        Me.Label14.TabIndex = 0
        Me.Label14.Text = "TACHYMETER : dev1/ai0"
        '
        'TabPage3
        '
        Me.TabPage3.Controls.Add(Me.PictureBox2)
        Me.TabPage3.Controls.Add(Me.RichTextBox3)
        Me.TabPage3.Controls.Add(Me.PictureBox1)
        Me.TabPage3.Controls.Add(Me.Button1)
        Me.TabPage3.Location = New System.Drawing.Point(4, 22)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage3.Size = New System.Drawing.Size(1176, 568)
        Me.TabPage3.TabIndex = 1
        Me.TabPage3.Text = "Cavispeed_control (NI_USB6501)"
        Me.TabPage3.UseVisualStyleBackColor = True
        '
        'PictureBox2
        '
        Me.PictureBox2.Image = Global.cavi_soft.My.Resources.Resources.pushwheel_diagram1
        Me.PictureBox2.Location = New System.Drawing.Point(668, 13)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(502, 530)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox2.TabIndex = 34
        Me.PictureBox2.TabStop = False
        '
        'RichTextBox3
        '
        Me.RichTextBox3.BackColor = System.Drawing.Color.LightGray
        Me.RichTextBox3.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.RichTextBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RichTextBox3.ForeColor = System.Drawing.Color.DarkRed
        Me.RichTextBox3.Location = New System.Drawing.Point(14, 14)
        Me.RichTextBox3.Name = "RichTextBox3"
        Me.RichTextBox3.ReadOnly = True
        Me.RichTextBox3.Size = New System.Drawing.Size(631, 59)
        Me.RichTextBox3.TabIndex = 33
        Me.RichTextBox3.Text = "IMPORTANT: THIS DEVICE MUST BE CONFIGURED AS "" dev 2"" !! If it's not the case, pl" & _
            "ease rename it using National Instrument software : MAX"
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = Global.cavi_soft.My.Resources.Resources.pushwheel_connection
        Me.PictureBox1.Location = New System.Drawing.Point(16, 158)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(631, 386)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 4
        Me.PictureBox1.TabStop = False
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(497, 85)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(150, 67)
        Me.Button1.TabIndex = 3
        Me.Button1.Text = "Reset Digital output task"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'GroupBox_pushwheel
        '
        Me.GroupBox_pushwheel.Controls.Add(Me.Label15)
        Me.GroupBox_pushwheel.Controls.Add(Me.Label16)
        Me.GroupBox_pushwheel.Controls.Add(Me.TextBox_bin_pushwheel)
        Me.GroupBox_pushwheel.Controls.Add(Me.Label18)
        Me.GroupBox_pushwheel.Controls.Add(Me.LedArray_pushwheel_port1)
        Me.GroupBox_pushwheel.Controls.Add(Me.LedArray_pushwheel_port0)
        Me.GroupBox_pushwheel.Controls.Add(Me.ShapeContainer1)
        Me.GroupBox_pushwheel.Location = New System.Drawing.Point(390, 632)
        Me.GroupBox_pushwheel.Name = "GroupBox_pushwheel"
        Me.GroupBox_pushwheel.Size = New System.Drawing.Size(425, 169)
        Me.GroupBox_pushwheel.TabIndex = 139
        Me.GroupBox_pushwheel.TabStop = False
        Me.GroupBox_pushwheel.Text = "Pushwheel relay command"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.615385!, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label15.Location = New System.Drawing.Point(188, 32)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(46, 13)
        Me.Label15.TabIndex = 139
        Me.Label15.Text = "PORT 1"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.615385!, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label16.Location = New System.Drawing.Point(14, 32)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(46, 13)
        Me.Label16.TabIndex = 138
        Me.Label16.Text = "PORT 0"
        '
        'TextBox_bin_pushwheel
        '
        Me.TextBox_bin_pushwheel.Font = New System.Drawing.Font("Microsoft JhengHei", 18.0!)
        Me.TextBox_bin_pushwheel.Location = New System.Drawing.Point(17, 123)
        Me.TextBox_bin_pushwheel.Multiline = True
        Me.TextBox_bin_pushwheel.Name = "TextBox_bin_pushwheel"
        Me.TextBox_bin_pushwheel.Size = New System.Drawing.Size(295, 40)
        Me.TextBox_bin_pushwheel.TabIndex = 136
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(9, 54)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(297, 13)
        Me.Label18.TabIndex = 135
        Me.Label18.Text = "B4   A4  D3   C3   B3   A3  D2   C2   B2  A2  D1   C1   B1   A1"
        '
        'LedArray_pushwheel_port1
        '
        '
        '
        '
        Me.LedArray_pushwheel_port1.ItemTemplate.LedStyle = NationalInstruments.UI.LedStyle.Round3D
        Me.LedArray_pushwheel_port1.ItemTemplate.Location = New System.Drawing.Point(0, 0)
        Me.LedArray_pushwheel_port1.ItemTemplate.Name = ""
        Me.LedArray_pushwheel_port1.ItemTemplate.Size = New System.Drawing.Size(20, 20)
        Me.LedArray_pushwheel_port1.ItemTemplate.TabIndex = 0
        Me.LedArray_pushwheel_port1.ItemTemplate.TabStop = False
        Me.LedArray_pushwheel_port1.LayoutMode = NationalInstruments.UI.ControlArrayLayoutMode.Horizontal
        Me.LedArray_pushwheel_port1.Location = New System.Drawing.Point(177, 61)
        Me.LedArray_pushwheel_port1.Name = "LedArray_pushwheel_port1"
        Me.LedArray_pushwheel_port1.ScaleMode = NationalInstruments.UI.ControlArrayScaleMode.CreateFixedMode(8)
        Me.LedArray_pushwheel_port1.Size = New System.Drawing.Size(180, 31)
        Me.LedArray_pushwheel_port1.TabIndex = 134
        '
        'LedArray_pushwheel_port0
        '
        '
        '
        '
        Me.LedArray_pushwheel_port0.ItemTemplate.LedStyle = NationalInstruments.UI.LedStyle.Round3D
        Me.LedArray_pushwheel_port0.ItemTemplate.Location = New System.Drawing.Point(0, 0)
        Me.LedArray_pushwheel_port0.ItemTemplate.Name = ""
        Me.LedArray_pushwheel_port0.ItemTemplate.Size = New System.Drawing.Size(20, 20)
        Me.LedArray_pushwheel_port0.ItemTemplate.TabIndex = 0
        Me.LedArray_pushwheel_port0.ItemTemplate.TabStop = False
        Me.LedArray_pushwheel_port0.LayoutMode = NationalInstruments.UI.ControlArrayLayoutMode.Horizontal
        Me.LedArray_pushwheel_port0.Location = New System.Drawing.Point(9, 61)
        Me.LedArray_pushwheel_port0.Name = "LedArray_pushwheel_port0"
        Me.LedArray_pushwheel_port0.ScaleMode = NationalInstruments.UI.ControlArrayScaleMode.CreateFixedMode(8)
        Me.LedArray_pushwheel_port0.Size = New System.Drawing.Size(180, 31)
        Me.LedArray_pushwheel_port0.TabIndex = 133
        '
        'ShapeContainer1
        '
        Me.ShapeContainer1.Location = New System.Drawing.Point(3, 16)
        Me.ShapeContainer1.Margin = New System.Windows.Forms.Padding(0)
        Me.ShapeContainer1.Name = "ShapeContainer1"
        Me.ShapeContainer1.Shapes.AddRange(New Microsoft.VisualBasic.PowerPacks.Shape() {Me.RectangleShape2, Me.RectangleShape1})
        Me.ShapeContainer1.Size = New System.Drawing.Size(419, 150)
        Me.ShapeContainer1.TabIndex = 137
        Me.ShapeContainer1.TabStop = False
        '
        'RectangleShape2
        '
        Me.RectangleShape2.Location = New System.Drawing.Point(180, 15)
        Me.RectangleShape2.Name = "RectangleShape2"
        Me.RectangleShape2.Size = New System.Drawing.Size(178, 74)
        '
        'RectangleShape1
        '
        Me.RectangleShape1.Location = New System.Drawing.Point(1, 14)
        Me.RectangleShape1.Name = "RectangleShape1"
        Me.RectangleShape1.Size = New System.Drawing.Size(173, 74)
        '
        'GroupBox6
        '
        Me.GroupBox6.Controls.Add(Me.ListBox1)
        Me.GroupBox6.Location = New System.Drawing.Point(12, 614)
        Me.GroupBox6.Name = "GroupBox6"
        Me.GroupBox6.Size = New System.Drawing.Size(348, 198)
        Me.GroupBox6.TabIndex = 138
        Me.GroupBox6.TabStop = False
        Me.GroupBox6.Text = "NI Device connected"
        '
        'ListBox1
        '
        Me.ListBox1.FormattingEnabled = True
        Me.ListBox1.Location = New System.Drawing.Point(11, 19)
        Me.ListBox1.Name = "ListBox1"
        Me.ListBox1.Size = New System.Drawing.Size(322, 173)
        Me.ListBox1.TabIndex = 36
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TableLayoutPanel1.ColumnCount = 2
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.OK_Button, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.Cancel_Button, 1, 0)
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(998, 664)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 1
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(194, 63)
        Me.TableLayoutPanel1.TabIndex = 137
        '
        'OK_Button
        '
        Me.OK_Button.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.OK_Button.Location = New System.Drawing.Point(7, 3)
        Me.OK_Button.Name = "OK_Button"
        Me.OK_Button.Size = New System.Drawing.Size(82, 57)
        Me.OK_Button.TabIndex = 0
        Me.OK_Button.Text = "OK"
        '
        'Cancel_Button
        '
        Me.Cancel_Button.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Cancel_Button.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Cancel_Button.Location = New System.Drawing.Point(103, 3)
        Me.Cancel_Button.Name = "Cancel_Button"
        Me.Cancel_Button.Size = New System.Drawing.Size(85, 56)
        Me.Cancel_Button.TabIndex = 1
        Me.Cancel_Button.Text = "Annuler"
        '
        'CAVITRON_SETUP
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1208, 813)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.GroupBox_pushwheel)
        Me.Controls.Add(Me.GroupBox6)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Controls.Add(Me.TabControl2)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "CAVITRON_SETUP"
        Me.Text = "CAVITRON_SETUP"
        Me.TopMost = True
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        CType(Me.NumericUpDown1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDown_beep_freq, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDown_autodetect_threshold, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.GroupBox_cavproperties.ResumeLayout(False)
        Me.GroupBox_cavproperties.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.TabControl2.ResumeLayout(False)
        Me.TabPage2.ResumeLayout(False)
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        Me.GroupBox_counter.ResumeLayout(False)
        Me.GroupBox_counter.PerformLayout()
        Me.GroupBox_analog_IN.ResumeLayout(False)
        Me.GroupBox_analog_IN.PerformLayout()
        Me.TabPage3.ResumeLayout(False)
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox_pushwheel.ResumeLayout(False)
        Me.GroupBox_pushwheel.PerformLayout()
        CType(Me.LedArray_pushwheel_port1.ItemTemplate, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LedArray_pushwheel_port0.ItemTemplate, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox6.ResumeLayout(False)
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents ComboBox_Lp_SIunit_select As System.Windows.Forms.ComboBox
    Friend WithEvents Label58 As System.Windows.Forms.Label
    Friend WithEvents CheckBox_useSIunit As System.Windows.Forms.CheckBox
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents ComboBox_fileextension As System.Windows.Forms.ComboBox
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents NumericUpDown1 As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label38 As System.Windows.Forms.Label
    Friend WithEvents NumericUpDown_beep_freq As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label36 As System.Windows.Forms.Label
    Friend WithEvents TextBox3 As System.Windows.Forms.TextBox
    Friend WithEvents Label35 As System.Windows.Forms.Label
    Friend WithEvents TextBox_beep_duration As System.Windows.Forms.TextBox
    Friend WithEvents CheckBox_play_a_sound As System.Windows.Forms.CheckBox
    Friend WithEvents Label79 As System.Windows.Forms.Label
    Friend WithEvents Label45 As System.Windows.Forms.Label
    Friend WithEvents NumericUpDown_autodetect_threshold As System.Windows.Forms.NumericUpDown
    Friend WithEvents GroupBox_cavproperties As System.Windows.Forms.GroupBox
    Friend WithEvents ComboBox_cavispeed_acquisiton As System.Windows.Forms.ComboBox
    Friend WithEvents Label94 As System.Windows.Forms.Label
    Friend WithEvents TextBox_rotor_diameter_cm As System.Windows.Forms.TextBox
    Friend WithEvents ComboBox_temperature_measurement As System.Windows.Forms.ComboBox
    Friend WithEvents Label60 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label59 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents ComboBox_reservoir_innersurface As System.Windows.Forms.ComboBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents LinkLabel1 As System.Windows.Forms.LinkLabel
    Friend WithEvents TextBox_maxcentri_rpm As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents LinkLabel2 As System.Windows.Forms.LinkLabel
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents TextBox_pixelsize As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents ComboBox_optical_source As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents TextBox_CAVITRON_NAME As System.Windows.Forms.TextBox
    Friend WithEvents TabControl2 As System.Windows.Forms.TabControl
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents RichTextBox2 As System.Windows.Forms.RichTextBox
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents TextBox_highlevel_ao1 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox_lowlevel_ao1 As System.Windows.Forms.TextBox
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents TextBox_highlevel_ao0 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox_lowlevel_ao0 As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox_counter As System.Windows.Forms.GroupBox
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents TextBox_offset_counter As System.Windows.Forms.TextBox
    Friend WithEvents TextBox_gain_counter As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox_analog_IN As System.Windows.Forms.GroupBox
    Friend WithEvents RichTextBox4 As System.Windows.Forms.RichTextBox
    Friend WithEvents RichTextBox1 As System.Windows.Forms.RichTextBox
    Friend WithEvents ComboBox_ai0range As System.Windows.Forms.ComboBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Button_settodefault As System.Windows.Forms.Button
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents TextBox_offset_ai1 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox_offset_ai0 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox_gain_ai1 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox_gain_ai0 As System.Windows.Forms.TextBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents TabPage3 As System.Windows.Forms.TabPage
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents RichTextBox3 As System.Windows.Forms.RichTextBox
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents GroupBox_pushwheel As System.Windows.Forms.GroupBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents TextBox_bin_pushwheel As System.Windows.Forms.TextBox
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents LedArray_pushwheel_port1 As NationalInstruments.UI.WindowsForms.LedArray
    Friend WithEvents LedArray_pushwheel_port0 As NationalInstruments.UI.WindowsForms.LedArray
    Friend WithEvents ShapeContainer1 As Microsoft.VisualBasic.PowerPacks.ShapeContainer
    Friend WithEvents RectangleShape2 As Microsoft.VisualBasic.PowerPacks.RectangleShape
    Friend WithEvents RectangleShape1 As Microsoft.VisualBasic.PowerPacks.RectangleShape
    Friend WithEvents GroupBox6 As System.Windows.Forms.GroupBox
    Friend WithEvents ListBox1 As System.Windows.Forms.ListBox
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents OK_Button As System.Windows.Forms.Button
    Friend WithEvents Cancel_Button As System.Windows.Forms.Button
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents ComboBox_cavispeed_setup As System.Windows.Forms.ComboBox
    Friend WithEvents Label29 As System.Windows.Forms.Label
    Friend WithEvents ComboBox_rotor_name As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBox_centrifugemodel As System.Windows.Forms.ComboBox
    Friend WithEvents Label30 As System.Windows.Forms.Label
    Friend WithEvents RichTextBox_operator_list As System.Windows.Forms.RichTextBox
    Friend WithEvents Label31 As System.Windows.Forms.Label
    Friend WithEvents Button2 As System.Windows.Forms.Button
End Class
