<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MAIN_Form
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(MAIN_Form))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim ChartArea1 As System.Windows.Forms.DataVisualization.Charting.ChartArea = New System.Windows.Forms.DataVisualization.Charting.ChartArea()
        Dim Series1 As System.Windows.Forms.DataVisualization.Charting.Series = New System.Windows.Forms.DataVisualization.Charting.Series()
        Dim DataPoint1 As System.Windows.Forms.DataVisualization.Charting.DataPoint = New System.Windows.Forms.DataVisualization.Charting.DataPoint(0.0R, "0,0")
        Dim Series2 As System.Windows.Forms.DataVisualization.Charting.Series = New System.Windows.Forms.DataVisualization.Charting.Series()
        Dim Series3 As System.Windows.Forms.DataVisualization.Charting.Series = New System.Windows.Forms.DataVisualization.Charting.Series()
        Dim Series4 As System.Windows.Forms.DataVisualization.Charting.Series = New System.Windows.Forms.DataVisualization.Charting.Series()
        Dim Series5 As System.Windows.Forms.DataVisualization.Charting.Series = New System.Windows.Forms.DataVisualization.Charting.Series()
        Dim Series6 As System.Windows.Forms.DataVisualization.Charting.Series = New System.Windows.Forms.DataVisualization.Charting.Series()
        Dim Series7 As System.Windows.Forms.DataVisualization.Charting.Series = New System.Windows.Forms.DataVisualization.Charting.Series()
        Dim Series8 As System.Windows.Forms.DataVisualization.Charting.Series = New System.Windows.Forms.DataVisualization.Charting.Series()
        Dim Title1 As System.Windows.Forms.DataVisualization.Charting.Title = New System.Windows.Forms.DataVisualization.Charting.Title()
        Dim ChartArea2 As System.Windows.Forms.DataVisualization.Charting.ChartArea = New System.Windows.Forms.DataVisualization.Charting.ChartArea()
        Dim Legend1 As System.Windows.Forms.DataVisualization.Charting.Legend = New System.Windows.Forms.DataVisualization.Charting.Legend()
        Dim Series9 As System.Windows.Forms.DataVisualization.Charting.Series = New System.Windows.Forms.DataVisualization.Charting.Series()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.ToolStripContainer1 = New System.Windows.Forms.ToolStripContainer()
        Me.TabControl_main = New System.Windows.Forms.TabControl()
        Me.TabPage3 = New System.Windows.Forms.TabPage()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.GroupBox9 = New System.Windows.Forms.GroupBox()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.Label45 = New System.Windows.Forms.Label()
        Me.TextBox_meandownstreamdiameter = New System.Windows.Forms.TextBox()
        Me.Label53 = New System.Windows.Forms.Label()
        Me.Label41 = New System.Windows.Forms.Label()
        Me.Label38 = New System.Windows.Forms.Label()
        Me.TextBox_meanupstreamdiameter = New System.Windows.Forms.TextBox()
        Me.Label36 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.TextBox_numberofstem = New System.Windows.Forms.TextBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.LinkLabel1 = New System.Windows.Forms.LinkLabel()
        Me.TextBox_crosssectionarea_ofWATER = New System.Windows.Forms.TextBox()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.TextBox_branch_crosssection = New System.Windows.Forms.TextBox()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Panel_MANUAL_markset = New System.Windows.Forms.Panel()
        Me.Label_interval_between_read = New System.Windows.Forms.Label()
        Me.TextBox_Max_meniscus_sep = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.TextBox_interval_between_read = New System.Windows.Forms.TextBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label49 = New System.Windows.Forms.Label()
        Me.Label47 = New System.Windows.Forms.Label()
        Me.CheckBox_APPEND_DATA_FILES = New System.Windows.Forms.CheckBox()
        Me.CheckBox_saverawcavispeed = New System.Windows.Forms.CheckBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.TextBox_species = New System.Windows.Forms.TextBox()
        Me.TextBox_sampleref1 = New System.Windows.Forms.TextBox()
        Me.TextBox_sampleref2 = New System.Windows.Forms.TextBox()
        Me.Label33 = New System.Windows.Forms.Label()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.TextBox_campaign_name = New System.Windows.Forms.TextBox()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.TextBox_location = New System.Windows.Forms.TextBox()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.ComboBox_operator = New System.Windows.Forms.ComboBox()
        Me.Label32 = New System.Windows.Forms.Label()
        Me.TextBox_comment = New System.Windows.Forms.TextBox()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.ComboBox_sampletype = New System.Windows.Forms.ComboBox()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.TextBox_sampleref3 = New System.Windows.Forms.TextBox()
        Me.Label34 = New System.Windows.Forms.Label()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.ComboBox_cavi1000_reservoirsize = New System.Windows.Forms.ComboBox()
        Me.Label_temperature_source = New System.Windows.Forms.Label()
        Me.Label_reservoir_area = New System.Windows.Forms.Label()
        Me.Label_SIunit = New System.Windows.Forms.Label()
        Me.Label_cavispeed_acq = New System.Windows.Forms.Label()
        Me.Label_optical_calibration = New System.Windows.Forms.Label()
        Me.Label_optical_source = New System.Windows.Forms.Label()
        Me.Label_rotor_diameter = New System.Windows.Forms.Label()
        Me.Label_rotor_number = New System.Windows.Forms.Label()
        Me.Label_cavitron_name = New System.Windows.Forms.Label()
        Me.Label94 = New System.Windows.Forms.Label()
        Me.Button_EDIT_ADVANCED_PARAMETERS = New System.Windows.Forms.Button()
        Me.Label58 = New System.Windows.Forms.Label()
        Me.Label61 = New System.Windows.Forms.Label()
        Me.Label55 = New System.Windows.Forms.Label()
        Me.Label57 = New System.Windows.Forms.Label()
        Me.Label54 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label60 = New System.Windows.Forms.Label()
        Me.Label59 = New System.Windows.Forms.Label()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.TextBox_temperature_correction = New System.Windows.Forms.TextBox()
        Me.ComboBox_correction_temp_source = New System.Windows.Forms.ComboBox()
        Me.Label39 = New System.Windows.Forms.Label()
        Me.Button_centrifugation_pressure_simulation = New System.Windows.Forms.Button()
        Me.CheckBox_autobrake = New System.Windows.Forms.CheckBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.Button_set_min_max_Yaxisrange = New System.Windows.Forms.Button()
        Me.Button_set_Yaxisrange2 = New System.Windows.Forms.Button()
        Me.TextBox_chart_Yaxis_min = New System.Windows.Forms.TextBox()
        Me.Label44 = New System.Windows.Forms.Label()
        Me.TextBox_chart_Yaxis_max = New System.Windows.Forms.TextBox()
        Me.Label37 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.NumericUpDown_detect_speedclass_step = New System.Windows.Forms.NumericUpDown()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.Button_recomputespeedclass = New System.Windows.Forms.Button()
        Me.Button_update_datagrid = New System.Windows.Forms.Button()
        Me.DataGridView_MAINDATA = New System.Windows.Forms.DataGridView()
        Me.RichTextBox3 = New System.Windows.Forms.RichTextBox()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.Label_meas_voltage = New System.Windows.Forms.Label()
        Me.RichTextBox_measured_voltage = New System.Windows.Forms.RichTextBox()
        Me.Button_RAWDATA_graph_axis_default = New System.Windows.Forms.Button()
        Me.GroupBox7 = New System.Windows.Forms.GroupBox()
        Me.Label90 = New System.Windows.Forms.Label()
        Me.Label89 = New System.Windows.Forms.Label()
        Me.NumericUpDown_outlier_LOW_threshold = New System.Windows.Forms.NumericUpDown()
        Me.Label88 = New System.Windows.Forms.Label()
        Me.NumericUpDown_outlier_threshold = New System.Windows.Forms.NumericUpDown()
        Me.CheckBox_outlier_reject = New System.Windows.Forms.CheckBox()
        Me.Button8 = New System.Windows.Forms.Button()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.Label87 = New System.Windows.Forms.Label()
        Me.RichTextBox_PLC_automode = New System.Windows.Forms.RichTextBox()
        Me.Label81 = New System.Windows.Forms.Label()
        Me.Label86 = New System.Windows.Forms.Label()
        Me.Label75 = New System.Windows.Forms.Label()
        Me.RichTextBox_Lp0_automode = New System.Windows.Forms.RichTextBox()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.ScatterGraph1 = New NationalInstruments.UI.WindowsForms.ScatterGraph()
        Me.dataplot1 = New NationalInstruments.UI.ScatterPlot()
        Me.XAxis1 = New NationalInstruments.UI.XAxis()
        Me.YAxis1 = New NationalInstruments.UI.YAxis()
        Me.fittedplot1 = New NationalInstruments.UI.ScatterPlot()
        Me.dataplot2 = New NationalInstruments.UI.ScatterPlot()
        Me.fittedplot2 = New NationalInstruments.UI.ScatterPlot()
        Me.dataplot3 = New NationalInstruments.UI.ScatterPlot()
        Me.fittedplot3 = New NationalInstruments.UI.ScatterPlot()
        Me.dataplot4 = New NationalInstruments.UI.ScatterPlot()
        Me.fittedplot4 = New NationalInstruments.UI.ScatterPlot()
        Me.dataplot5 = New NationalInstruments.UI.ScatterPlot()
        Me.fittedplot5 = New NationalInstruments.UI.ScatterPlot()
        Me.dataplot6 = New NationalInstruments.UI.ScatterPlot()
        Me.fittedplot6 = New NationalInstruments.UI.ScatterPlot()
        Me.dataplot7 = New NationalInstruments.UI.ScatterPlot()
        Me.fittedplot7 = New NationalInstruments.UI.ScatterPlot()
        Me.dataplot8 = New NationalInstruments.UI.ScatterPlot()
        Me.fittedplot8 = New NationalInstruments.UI.ScatterPlot()
        Me.RichTextBox_pos = New System.Windows.Forms.RichTextBox()
        Me.Label82 = New System.Windows.Forms.Label()
        Me.Label80 = New System.Windows.Forms.Label()
        Me.RichTextBox_AUTOMODE_meanLp_integral = New System.Windows.Forms.RichTextBox()
        Me.RichTextBox_AUTOMODE_stderror_Lp_integral = New System.Windows.Forms.RichTextBox()
        Me.Label68 = New System.Windows.Forms.Label()
        Me.RichTextBox4 = New System.Windows.Forms.RichTextBox()
        Me.Button_COMPUTE = New System.Windows.Forms.Button()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.DataGridView_RAWDATA = New System.Windows.Forms.DataGridView()
        Me.Column_time = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column_distance = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.distance_mm = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Chart3 = New System.Windows.Forms.DataVisualization.Charting.Chart()
        Me.CheckBox_validateboolean = New System.Windows.Forms.CheckBox()
        Me.Button_validate_meas_AUTOMODE = New System.Windows.Forms.Button()
        Me.Button_Startmeas_AUTOMODE = New System.Windows.Forms.Button()
        Me.Button_intensity_profile = New System.Windows.Forms.Button()
        Me.Timer_1sec = New System.Windows.Forms.Timer(Me.components)
        Me.RichTextBox_centri_tension = New System.Windows.Forms.RichTextBox()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.SaveFileDialog_main_data = New System.Windows.Forms.SaveFileDialog()
        Me.toolStripSeparator5 = New System.Windows.Forms.ToolStripSeparator()
        Me.AideToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TextBox_time = New System.Windows.Forms.TextBox()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.TextBox_max_cavispeed = New System.Windows.Forms.TextBox()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.Bt_setcavispeed = New System.Windows.Forms.Button()
        Me.Label_cavispeedcontrolsetvalue = New System.Windows.Forms.Label()
        Me.TextBox_cavispeed_setvalue = New System.Windows.Forms.TextBox()
        Me.LinkLabel_minmeniscusposition = New System.Windows.Forms.LinkLabel()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.RichTextBox_centri_rpm = New System.Windows.Forms.RichTextBox()
        Me.CheckBox_tachymeter_connected = New System.Windows.Forms.CheckBox()
        Me.TextBox_minmeniscusposition = New System.Windows.Forms.TextBox()
        Me.RichTextBox_MEASMODE_STATUS = New System.Windows.Forms.RichTextBox()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.DefineSavedImageFolderToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator()
        Me.InputSampleDiameterToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RECOMPUTEPLCToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator4 = New System.Windows.Forms.ToolStripSeparator()
        Me.OpenFileDialog_parameter = New System.Windows.Forms.OpenFileDialog()
        Me.SaveFileDialog_parameter = New System.Windows.Forms.SaveFileDialog()
        Me.SaveFileDialog_cavispeed = New System.Windows.Forms.SaveFileDialog()
        Me.OpenFileDialog_APPENDED = New System.Windows.Forms.OpenFileDialog()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.Label_graphtype = New System.Windows.Forms.Label()
        Me.Button6 = New System.Windows.Forms.Button()
        Me.GroupBox_pushwheel = New System.Windows.Forms.GroupBox()
        Me.Label66 = New System.Windows.Forms.Label()
        Me.Label65 = New System.Windows.Forms.Label()
        Me.Label64 = New System.Windows.Forms.Label()
        Me.TextBox_set_pressure_fixed = New System.Windows.Forms.TextBox()
        Me.TextBox_set_cavispeed_fixed = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label40 = New System.Windows.Forms.Label()
        Me.TextBox_bin_pushwheel = New System.Windows.Forms.TextBox()
        Me.Label51 = New System.Windows.Forms.Label()
        Me.LedArray_pushwheel_port1 = New NationalInstruments.UI.WindowsForms.LedArray()
        Me.LedArray_pushwheel_port0 = New NationalInstruments.UI.WindowsForms.LedArray()
        Me.ShapeContainer1 = New Microsoft.VisualBasic.PowerPacks.ShapeContainer()
        Me.RectangleShape2 = New Microsoft.VisualBasic.PowerPacks.RectangleShape()
        Me.RectangleShape1 = New Microsoft.VisualBasic.PowerPacks.RectangleShape()
        Me.Button_default_axisrange = New System.Windows.Forms.Button()
        Me.Button_setYaxisrange1 = New System.Windows.Forms.Button()
        Me.Chart_PLC = New System.Windows.Forms.DataVisualization.Charting.Chart()
        Me.Label_liveimage = New System.Windows.Forms.Label()
        Me.GroupBox_integral = New System.Windows.Forms.GroupBox()
        Me.Label35 = New System.Windows.Forms.Label()
        Me.RichTextBox_PLC_integral = New System.Windows.Forms.RichTextBox()
        Me.TextBox_LP0_integral = New System.Windows.Forms.TextBox()
        Me.RichTextBox_mean_inst_LP_integral = New System.Windows.Forms.RichTextBox()
        Me.Button_set_LP0_integral = New System.Windows.Forms.Button()
        Me.richtextbox_std_dev_LP_integral = New System.Windows.Forms.RichTextBox()
        Me.Label46 = New System.Windows.Forms.Label()
        Me.Label48 = New System.Windows.Forms.Label()
        Me.Label_int2 = New System.Windows.Forms.Label()
        Me.Label50 = New System.Windows.Forms.Label()
        Me.Label_int1 = New System.Windows.Forms.Label()
        Me.Label52 = New System.Windows.Forms.Label()
        Me.TextBox_Lp1_integral = New System.Windows.Forms.TextBox()
        Me.TextBox_Lp2_integral = New System.Windows.Forms.TextBox()
        Me.Timer_camera = New System.Windows.Forms.Timer(Me.components)
        Me.CheckBox_save_camera_image = New System.Windows.Forms.CheckBox()
        Me.Button_save_camera_images = New System.Windows.Forms.Button()
        Me.OpenFileDialog_read_data = New System.Windows.Forms.OpenFileDialog()
        Me.CheckBox_CAM_connected = New System.Windows.Forms.CheckBox()
        Me.Button_set_grid_origin = New System.Windows.Forms.Button()
        Me.Label_grid_origin = New System.Windows.Forms.Label()
        Me.NumericUpDown_exposureTimeAbs = New System.Windows.Forms.NumericUpDown()
        Me.Label_gridgraduation50 = New System.Windows.Forms.Label()
        Me.Label_gridgraduation40 = New System.Windows.Forms.Label()
        Me.Label_gridgraduation30 = New System.Windows.Forms.Label()
        Me.Label_gridgraduation20 = New System.Windows.Forms.Label()
        Me.Label_gridgraduation10 = New System.Windows.Forms.Label()
        Me.Label_gridgraduation60 = New System.Windows.Forms.Label()
        Me.Label_gridgraduation70 = New System.Windows.Forms.Label()
        Me.Label56 = New System.Windows.Forms.Label()
        Me.FolderBrowserDialog_save_image = New System.Windows.Forms.FolderBrowserDialog()
        Me.GroupBox_stopwatch = New System.Windows.Forms.GroupBox()
        Me.Label43 = New System.Windows.Forms.Label()
        Me.Label42 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.NumericUpDown_countdown_sec = New System.Windows.Forms.NumericUpDown()
        Me.NumericUpDown_countdown_min = New System.Windows.Forms.NumericUpDown()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.TextBox_countdown = New System.Windows.Forms.TextBox()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.Panel6 = New System.Windows.Forms.Panel()
        Me.PictureBox4 = New System.Windows.Forms.PictureBox()
        Me.RichTextBox1 = New System.Windows.Forms.RichTextBox()
        Me.toolStripSeparator7 = New System.Windows.Forms.ToolStripSeparator()
        Me.toolStripSeparator8 = New System.Windows.Forms.ToolStripSeparator()
        Me.toolStripSeparator9 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolStripSeparator6 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolStripButton_activate_MEASUREMENT_mode = New System.Windows.Forms.ToolStripButton()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.Button_downstream_ROI = New System.Windows.Forms.Button()
        Me.PictureBox_main_image = New System.Windows.Forms.PictureBox()
        Me.PictureBox_RESERVOIR_Fill = New System.Windows.Forms.PictureBox()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.PictureBox_rec = New System.Windows.Forms.PictureBox()
        Me.PictureBox_grid = New System.Windows.Forms.PictureBox()
        Me.Label_gridgraduation80 = New System.Windows.Forms.Label()
        Me.Label_gridgraduation90 = New System.Windows.Forms.Label()
        Me.Label_gridgraduation100 = New System.Windows.Forms.Label()
        Me.Label_gridgraduation110 = New System.Windows.Forms.Label()
        Me.Label_gridgraduation120 = New System.Windows.Forms.Label()
        Me.Label_gridgraduation130 = New System.Windows.Forms.Label()
        Me.Label_gridgraduation140 = New System.Windows.Forms.Label()
        Me.Label_gridgraduation150 = New System.Windows.Forms.Label()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.ToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem2 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem_sameparameter = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem_newparameter = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem_openfile = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator10 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolStripMenuItem_savedata = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem_savedata_as = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem_appenddata = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem_appendfile = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator11 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolStripMenuItem_imagefolder = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator13 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolStripMenuItem_exit = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItemexit_withoutsaving = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem15 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem_branchdiameter = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItemmin_meniscusposition = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem_reinit = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem_reinitchart = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem_reinitgrid = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem_reinitdatalogger = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem_reinitparameter = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem_reinitstddev = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem25 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem_shortcut = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem_about = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip()
        Me.ToolStripButton_recompute_PLC = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton_newmeas_sameparameter = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton_open = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton_save = New System.Windows.Forms.ToolStripButton()
        Me.GroupBox_centrifugecontrol = New System.Windows.Forms.GroupBox()
        Me.Panel7 = New System.Windows.Forms.Panel()
        Me.Led_brake = New NationalInstruments.UI.WindowsForms.Led()
        Me.Bt_centri_BRAKE = New System.Windows.Forms.Button()
        Me.Label63 = New System.Windows.Forms.Label()
        Me.Label62 = New System.Windows.Forms.Label()
        Me.Bt_centri_START_STOP = New System.Windows.Forms.Button()
        Me.Led_stop = New NationalInstruments.UI.WindowsForms.Led()
        Me.Led_start = New NationalInstruments.UI.WindowsForms.Led()
        Me.Label_centri_running = New System.Windows.Forms.Label()
        Me.Label_mouseposition = New System.Windows.Forms.Label()
        Me.Button_AUTO_MANUAL = New System.Windows.Forms.Button()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage_MANUAL = New System.Windows.Forms.TabPage()
        Me.TabPage_AUTO = New System.Windows.Forms.TabPage()
        Me.CheckBox5 = New System.Windows.Forms.CheckBox()
        Me.GroupBox6 = New System.Windows.Forms.GroupBox()
        Me.Label84 = New System.Windows.Forms.Label()
        Me.NumericUpDown_reservoir_filling_time = New System.Windows.Forms.NumericUpDown()
        Me.Label83 = New System.Windows.Forms.Label()
        Me.NumericUpDown_numb_of_replicate = New System.Windows.Forms.NumericUpDown()
        Me.CheckBox4 = New System.Windows.Forms.CheckBox()
        Me.CheckBox_display_AUTOMODE = New System.Windows.Forms.CheckBox()
        Me.GroupBox_ROI = New System.Windows.Forms.GroupBox()
        Me.Label92 = New System.Windows.Forms.Label()
        Me.Label73 = New System.Windows.Forms.Label()
        Me.Label91 = New System.Windows.Forms.Label()
        Me.Button_upstream_ROI = New System.Windows.Forms.Button()
        Me.Label85 = New System.Windows.Forms.Label()
        Me.NumericUpDown_measuredline_pixel = New System.Windows.Forms.NumericUpDown()
        Me.Button_automatic_ROI = New System.Windows.Forms.Button()
        Me.Label71 = New System.Windows.Forms.Label()
        Me.NumericUpDown_downstream_ROI_width_pixel = New System.Windows.Forms.NumericUpDown()
        Me.NumericUpDown_upstream_ROI_width_pixel = New System.Windows.Forms.NumericUpDown()
        Me.Label72 = New System.Windows.Forms.Label()
        Me.Label74 = New System.Windows.Forms.Label()
        Me.TextBox_downstream_ROI_pixel_origin_value = New System.Windows.Forms.TextBox()
        Me.TextBox_upstream_ROI_pixel_origin_value = New System.Windows.Forms.TextBox()
        Me.GroupBox_Integralmethod_AUTOMODE = New System.Windows.Forms.GroupBox()
        Me.Label70 = New System.Windows.Forms.Label()
        Me.NumericUpDown_AUTOMODE_integral_stabilisation_time = New System.Windows.Forms.NumericUpDown()
        Me.Label69 = New System.Windows.Forms.Label()
        Me.NumericUpDown_AUTOMODE_integral_integration_interval_ms = New System.Windows.Forms.NumericUpDown()
        Me.NumericUpDown_AUTOMODE_number_of_rep_integral = New System.Windows.Forms.NumericUpDown()
        Me.Label67 = New System.Windows.Forms.Label()
        Me.Timer_getdistance = New System.Windows.Forms.Timer(Me.components)
        Me.ScatterGraph_intensityprofile = New NationalInstruments.UI.WindowsForms.ScatterGraph()
        Me.ScatterPlot1 = New NationalInstruments.UI.ScatterPlot()
        Me.XAxis2 = New NationalInstruments.UI.XAxis()
        Me.YAxis2 = New NationalInstruments.UI.YAxis()
        Me.ScatterPlot_background = New NationalInstruments.UI.ScatterPlot()
        Me.ScatterPlot_threshold = New NationalInstruments.UI.ScatterPlot()
        Me.Timer_intensityprofile = New System.Windows.Forms.Timer(Me.components)
        Me.Label14 = New System.Windows.Forms.Label()
        Me.GroupBox8 = New System.Windows.Forms.GroupBox()
        Me.TextBox_temperature = New System.Windows.Forms.TextBox()
        Me.Label93 = New System.Windows.Forms.Label()
        Me.NumericUpDown_grid_origin = New System.Windows.Forms.NumericUpDown()
        Me.Column1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column11 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column10 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column18 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column19 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.meas_cavispeed = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.max_cavispeed = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column_pressure = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.raw_conductance_kgMPas = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Conductivity_SI_corrT = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.std_error_LP_integral = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column_PLC_integral = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Note = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.speedclass = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column8 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.roomtemperature2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.correctiontemp = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column_temp_factor = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column16 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.column9 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column14 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.column_deltaP = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column_calib_optique = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column_meas_MODE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.rotordiameter = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Lp_unit = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Equation_slope = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Equation_intercept = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column_reservoir_crosssection = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.branch_area_big_reservoir = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column_mean_diameter_big_res_mm = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column_mean_diameter_small_res_mm = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.number_of_stem = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.AUTOMODE_rawdata_time = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.AUTOMODE_rawdata_distance_pixel = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.AUTOMODE_rawdata_distance_mm = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.branch_diameter_rawdata_upstream = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.branch_diameter_rawdata_downstream = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ToolStripContainer1.SuspendLayout()
        Me.TabControl_main.SuspendLayout()
        Me.TabPage3.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox9.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.Panel_MANUAL_markset.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.Panel4.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.NumericUpDown_detect_speedclass_step, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage1.SuspendLayout()
        CType(Me.DataGridView_MAINDATA, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage2.SuspendLayout()
        Me.GroupBox7.SuspendLayout()
        CType(Me.NumericUpDown_outlier_LOW_threshold, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDown_outlier_threshold, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox5.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ScatterGraph1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox4.SuspendLayout()
        CType(Me.DataGridView_RAWDATA, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Chart3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel2.SuspendLayout()
        Me.Panel5.SuspendLayout()
        Me.GroupBox_pushwheel.SuspendLayout()
        CType(Me.LedArray_pushwheel_port1.ItemTemplate, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LedArray_pushwheel_port0.ItemTemplate, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Chart_PLC, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox_integral.SuspendLayout()
        CType(Me.NumericUpDown_exposureTimeAbs, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox_stopwatch.SuspendLayout()
        CType(Me.NumericUpDown_countdown_sec, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDown_countdown_min, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel6.SuspendLayout()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox_main_image, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox_RESERVOIR_Fill, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox_rec, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox_grid, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.MenuStrip1.SuspendLayout()
        Me.ToolStrip1.SuspendLayout()
        Me.GroupBox_centrifugecontrol.SuspendLayout()
        Me.Panel7.SuspendLayout()
        CType(Me.Led_brake, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Led_stop, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Led_start, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabControl1.SuspendLayout()
        Me.TabPage_MANUAL.SuspendLayout()
        Me.TabPage_AUTO.SuspendLayout()
        Me.GroupBox6.SuspendLayout()
        CType(Me.NumericUpDown_reservoir_filling_time, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDown_numb_of_replicate, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox_ROI.SuspendLayout()
        CType(Me.NumericUpDown_measuredline_pixel, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDown_downstream_ROI_width_pixel, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDown_upstream_ROI_width_pixel, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox_Integralmethod_AUTOMODE.SuspendLayout()
        CType(Me.NumericUpDown_AUTOMODE_integral_stabilisation_time, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDown_AUTOMODE_integral_integration_interval_ms, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDown_AUTOMODE_number_of_rep_integral, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ScatterGraph_intensityprofile, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox8.SuspendLayout()
        CType(Me.NumericUpDown_grid_origin, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ToolStripContainer1
        '
        '
        'ToolStripContainer1.ContentPanel
        '
        resources.ApplyResources(Me.ToolStripContainer1.ContentPanel, "ToolStripContainer1.ContentPanel")
        resources.ApplyResources(Me.ToolStripContainer1, "ToolStripContainer1")
        Me.ToolStripContainer1.Name = "ToolStripContainer1"
        '
        'TabControl_main
        '
        Me.TabControl_main.AllowDrop = True
        Me.TabControl_main.Controls.Add(Me.TabPage3)
        Me.TabControl_main.Controls.Add(Me.TabPage1)
        Me.TabControl_main.Controls.Add(Me.TabPage2)
        Me.TabControl_main.HotTrack = True
        resources.ApplyResources(Me.TabControl_main, "TabControl_main")
        Me.TabControl_main.Name = "TabControl_main"
        Me.TabControl_main.SelectedIndex = 0
        '
        'TabPage3
        '
        Me.TabPage3.Controls.Add(Me.GroupBox1)
        resources.ApplyResources(Me.TabPage3, "TabPage3")
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.DarkSeaGreen
        Me.GroupBox1.Controls.Add(Me.GroupBox9)
        Me.GroupBox1.Controls.Add(Me.GroupBox3)
        Me.GroupBox1.Controls.Add(Me.Button_centrifugation_pressure_simulation)
        Me.GroupBox1.Controls.Add(Me.CheckBox_autobrake)
        Me.GroupBox1.Controls.Add(Me.GroupBox2)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.NumericUpDown_detect_speedclass_step)
        resources.ApplyResources(Me.GroupBox1, "GroupBox1")
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.TabStop = False
        '
        'GroupBox9
        '
        Me.GroupBox9.Controls.Add(Me.Panel3)
        Me.GroupBox9.Controls.Add(Me.Panel_MANUAL_markset)
        Me.GroupBox9.Controls.Add(Me.Label49)
        Me.GroupBox9.Controls.Add(Me.Label47)
        Me.GroupBox9.Controls.Add(Me.CheckBox_APPEND_DATA_FILES)
        Me.GroupBox9.Controls.Add(Me.CheckBox_saverawcavispeed)
        Me.GroupBox9.Controls.Add(Me.Label12)
        Me.GroupBox9.Controls.Add(Me.TextBox_species)
        Me.GroupBox9.Controls.Add(Me.TextBox_sampleref1)
        Me.GroupBox9.Controls.Add(Me.TextBox_sampleref2)
        Me.GroupBox9.Controls.Add(Me.Label33)
        Me.GroupBox9.Controls.Add(Me.Label23)
        Me.GroupBox9.Controls.Add(Me.TextBox_campaign_name)
        Me.GroupBox9.Controls.Add(Me.Label17)
        Me.GroupBox9.Controls.Add(Me.Label13)
        Me.GroupBox9.Controls.Add(Me.TextBox_location)
        Me.GroupBox9.Controls.Add(Me.Panel1)
        resources.ApplyResources(Me.GroupBox9, "GroupBox9")
        Me.GroupBox9.Name = "GroupBox9"
        Me.GroupBox9.TabStop = False
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.Label45)
        Me.Panel3.Controls.Add(Me.TextBox_meandownstreamdiameter)
        Me.Panel3.Controls.Add(Me.Label53)
        Me.Panel3.Controls.Add(Me.Label41)
        Me.Panel3.Controls.Add(Me.Label38)
        Me.Panel3.Controls.Add(Me.TextBox_meanupstreamdiameter)
        Me.Panel3.Controls.Add(Me.Label36)
        Me.Panel3.Controls.Add(Me.Label9)
        Me.Panel3.Controls.Add(Me.TextBox_numberofstem)
        Me.Panel3.Controls.Add(Me.Label15)
        Me.Panel3.Controls.Add(Me.LinkLabel1)
        Me.Panel3.Controls.Add(Me.TextBox_crosssectionarea_ofWATER)
        Me.Panel3.Controls.Add(Me.Label20)
        Me.Panel3.Controls.Add(Me.TextBox_branch_crosssection)
        Me.Panel3.Controls.Add(Me.Label22)
        Me.Panel3.Controls.Add(Me.Label21)
        resources.ApplyResources(Me.Panel3, "Panel3")
        Me.Panel3.Name = "Panel3"
        '
        'Label45
        '
        resources.ApplyResources(Me.Label45, "Label45")
        Me.Label45.Name = "Label45"
        '
        'TextBox_meandownstreamdiameter
        '
        Me.TextBox_meandownstreamdiameter.BackColor = System.Drawing.SystemColors.Control
        resources.ApplyResources(Me.TextBox_meandownstreamdiameter, "TextBox_meandownstreamdiameter")
        Me.TextBox_meandownstreamdiameter.Name = "TextBox_meandownstreamdiameter"
        Me.TextBox_meandownstreamdiameter.ReadOnly = True
        '
        'Label53
        '
        resources.ApplyResources(Me.Label53, "Label53")
        Me.Label53.Name = "Label53"
        '
        'Label41
        '
        resources.ApplyResources(Me.Label41, "Label41")
        Me.Label41.Name = "Label41"
        '
        'Label38
        '
        resources.ApplyResources(Me.Label38, "Label38")
        Me.Label38.Name = "Label38"
        '
        'TextBox_meanupstreamdiameter
        '
        Me.TextBox_meanupstreamdiameter.BackColor = System.Drawing.SystemColors.Control
        resources.ApplyResources(Me.TextBox_meanupstreamdiameter, "TextBox_meanupstreamdiameter")
        Me.TextBox_meanupstreamdiameter.Name = "TextBox_meanupstreamdiameter"
        Me.TextBox_meanupstreamdiameter.ReadOnly = True
        '
        'Label36
        '
        resources.ApplyResources(Me.Label36, "Label36")
        Me.Label36.Name = "Label36"
        '
        'Label9
        '
        resources.ApplyResources(Me.Label9, "Label9")
        Me.Label9.Name = "Label9"
        '
        'TextBox_numberofstem
        '
        resources.ApplyResources(Me.TextBox_numberofstem, "TextBox_numberofstem")
        Me.TextBox_numberofstem.Name = "TextBox_numberofstem"
        Me.TextBox_numberofstem.ReadOnly = True
        '
        'Label15
        '
        resources.ApplyResources(Me.Label15, "Label15")
        Me.Label15.Name = "Label15"
        '
        'LinkLabel1
        '
        resources.ApplyResources(Me.LinkLabel1, "LinkLabel1")
        Me.LinkLabel1.Name = "LinkLabel1"
        Me.LinkLabel1.TabStop = True
        '
        'TextBox_crosssectionarea_ofWATER
        '
        Me.TextBox_crosssectionarea_ofWATER.BackColor = System.Drawing.SystemColors.Control
        resources.ApplyResources(Me.TextBox_crosssectionarea_ofWATER, "TextBox_crosssectionarea_ofWATER")
        Me.TextBox_crosssectionarea_ofWATER.Name = "TextBox_crosssectionarea_ofWATER"
        Me.TextBox_crosssectionarea_ofWATER.ReadOnly = True
        '
        'Label20
        '
        resources.ApplyResources(Me.Label20, "Label20")
        Me.Label20.Name = "Label20"
        '
        'TextBox_branch_crosssection
        '
        Me.TextBox_branch_crosssection.BackColor = System.Drawing.SystemColors.Control
        resources.ApplyResources(Me.TextBox_branch_crosssection, "TextBox_branch_crosssection")
        Me.TextBox_branch_crosssection.Name = "TextBox_branch_crosssection"
        '
        'Label22
        '
        resources.ApplyResources(Me.Label22, "Label22")
        Me.Label22.Name = "Label22"
        '
        'Label21
        '
        resources.ApplyResources(Me.Label21, "Label21")
        Me.Label21.Name = "Label21"
        '
        'Panel_MANUAL_markset
        '
        Me.Panel_MANUAL_markset.Controls.Add(Me.Label_interval_between_read)
        Me.Panel_MANUAL_markset.Controls.Add(Me.TextBox_Max_meniscus_sep)
        Me.Panel_MANUAL_markset.Controls.Add(Me.Label3)
        Me.Panel_MANUAL_markset.Controls.Add(Me.Label4)
        Me.Panel_MANUAL_markset.Controls.Add(Me.TextBox_interval_between_read)
        Me.Panel_MANUAL_markset.Controls.Add(Me.Label16)
        resources.ApplyResources(Me.Panel_MANUAL_markset, "Panel_MANUAL_markset")
        Me.Panel_MANUAL_markset.Name = "Panel_MANUAL_markset"
        '
        'Label_interval_between_read
        '
        resources.ApplyResources(Me.Label_interval_between_read, "Label_interval_between_read")
        Me.Label_interval_between_read.Name = "Label_interval_between_read"
        '
        'TextBox_Max_meniscus_sep
        '
        resources.ApplyResources(Me.TextBox_Max_meniscus_sep, "TextBox_Max_meniscus_sep")
        Me.TextBox_Max_meniscus_sep.Name = "TextBox_Max_meniscus_sep"
        '
        'Label3
        '
        resources.ApplyResources(Me.Label3, "Label3")
        Me.Label3.Name = "Label3"
        '
        'Label4
        '
        resources.ApplyResources(Me.Label4, "Label4")
        Me.Label4.Name = "Label4"
        '
        'TextBox_interval_between_read
        '
        resources.ApplyResources(Me.TextBox_interval_between_read, "TextBox_interval_between_read")
        Me.TextBox_interval_between_read.Name = "TextBox_interval_between_read"
        '
        'Label16
        '
        resources.ApplyResources(Me.Label16, "Label16")
        Me.Label16.Name = "Label16"
        '
        'Label49
        '
        resources.ApplyResources(Me.Label49, "Label49")
        Me.Label49.Name = "Label49"
        '
        'Label47
        '
        resources.ApplyResources(Me.Label47, "Label47")
        Me.Label47.Name = "Label47"
        '
        'CheckBox_APPEND_DATA_FILES
        '
        resources.ApplyResources(Me.CheckBox_APPEND_DATA_FILES, "CheckBox_APPEND_DATA_FILES")
        Me.CheckBox_APPEND_DATA_FILES.Checked = True
        Me.CheckBox_APPEND_DATA_FILES.CheckState = System.Windows.Forms.CheckState.Checked
        Me.CheckBox_APPEND_DATA_FILES.Name = "CheckBox_APPEND_DATA_FILES"
        Me.CheckBox_APPEND_DATA_FILES.UseVisualStyleBackColor = True
        '
        'CheckBox_saverawcavispeed
        '
        resources.ApplyResources(Me.CheckBox_saverawcavispeed, "CheckBox_saverawcavispeed")
        Me.CheckBox_saverawcavispeed.Name = "CheckBox_saverawcavispeed"
        Me.CheckBox_saverawcavispeed.UseVisualStyleBackColor = True
        '
        'Label12
        '
        resources.ApplyResources(Me.Label12, "Label12")
        Me.Label12.Name = "Label12"
        '
        'TextBox_species
        '
        Me.TextBox_species.AutoCompleteCustomSource.AddRange(New String() {resources.GetString("TextBox_species.AutoCompleteCustomSource"), resources.GetString("TextBox_species.AutoCompleteCustomSource1"), resources.GetString("TextBox_species.AutoCompleteCustomSource2"), resources.GetString("TextBox_species.AutoCompleteCustomSource3"), resources.GetString("TextBox_species.AutoCompleteCustomSource4"), resources.GetString("TextBox_species.AutoCompleteCustomSource5"), resources.GetString("TextBox_species.AutoCompleteCustomSource6"), resources.GetString("TextBox_species.AutoCompleteCustomSource7"), resources.GetString("TextBox_species.AutoCompleteCustomSource8"), resources.GetString("TextBox_species.AutoCompleteCustomSource9"), resources.GetString("TextBox_species.AutoCompleteCustomSource10"), resources.GetString("TextBox_species.AutoCompleteCustomSource11"), resources.GetString("TextBox_species.AutoCompleteCustomSource12"), resources.GetString("TextBox_species.AutoCompleteCustomSource13"), resources.GetString("TextBox_species.AutoCompleteCustomSource14"), resources.GetString("TextBox_species.AutoCompleteCustomSource15"), resources.GetString("TextBox_species.AutoCompleteCustomSource16"), resources.GetString("TextBox_species.AutoCompleteCustomSource17"), resources.GetString("TextBox_species.AutoCompleteCustomSource18"), resources.GetString("TextBox_species.AutoCompleteCustomSource19"), resources.GetString("TextBox_species.AutoCompleteCustomSource20"), resources.GetString("TextBox_species.AutoCompleteCustomSource21"), resources.GetString("TextBox_species.AutoCompleteCustomSource22"), resources.GetString("TextBox_species.AutoCompleteCustomSource23"), resources.GetString("TextBox_species.AutoCompleteCustomSource24"), resources.GetString("TextBox_species.AutoCompleteCustomSource25"), resources.GetString("TextBox_species.AutoCompleteCustomSource26"), resources.GetString("TextBox_species.AutoCompleteCustomSource27"), resources.GetString("TextBox_species.AutoCompleteCustomSource28"), resources.GetString("TextBox_species.AutoCompleteCustomSource29"), resources.GetString("TextBox_species.AutoCompleteCustomSource30"), resources.GetString("TextBox_species.AutoCompleteCustomSource31"), resources.GetString("TextBox_species.AutoCompleteCustomSource32"), resources.GetString("TextBox_species.AutoCompleteCustomSource33"), resources.GetString("TextBox_species.AutoCompleteCustomSource34"), resources.GetString("TextBox_species.AutoCompleteCustomSource35"), resources.GetString("TextBox_species.AutoCompleteCustomSource36"), resources.GetString("TextBox_species.AutoCompleteCustomSource37"), resources.GetString("TextBox_species.AutoCompleteCustomSource38"), resources.GetString("TextBox_species.AutoCompleteCustomSource39"), resources.GetString("TextBox_species.AutoCompleteCustomSource40"), resources.GetString("TextBox_species.AutoCompleteCustomSource41"), resources.GetString("TextBox_species.AutoCompleteCustomSource42"), resources.GetString("TextBox_species.AutoCompleteCustomSource43"), resources.GetString("TextBox_species.AutoCompleteCustomSource44"), resources.GetString("TextBox_species.AutoCompleteCustomSource45"), resources.GetString("TextBox_species.AutoCompleteCustomSource46"), resources.GetString("TextBox_species.AutoCompleteCustomSource47"), resources.GetString("TextBox_species.AutoCompleteCustomSource48"), resources.GetString("TextBox_species.AutoCompleteCustomSource49"), resources.GetString("TextBox_species.AutoCompleteCustomSource50"), resources.GetString("TextBox_species.AutoCompleteCustomSource51"), resources.GetString("TextBox_species.AutoCompleteCustomSource52"), resources.GetString("TextBox_species.AutoCompleteCustomSource53"), resources.GetString("TextBox_species.AutoCompleteCustomSource54"), resources.GetString("TextBox_species.AutoCompleteCustomSource55"), resources.GetString("TextBox_species.AutoCompleteCustomSource56"), resources.GetString("TextBox_species.AutoCompleteCustomSource57"), resources.GetString("TextBox_species.AutoCompleteCustomSource58"), resources.GetString("TextBox_species.AutoCompleteCustomSource59"), resources.GetString("TextBox_species.AutoCompleteCustomSource60"), resources.GetString("TextBox_species.AutoCompleteCustomSource61"), resources.GetString("TextBox_species.AutoCompleteCustomSource62"), resources.GetString("TextBox_species.AutoCompleteCustomSource63"), resources.GetString("TextBox_species.AutoCompleteCustomSource64"), resources.GetString("TextBox_species.AutoCompleteCustomSource65"), resources.GetString("TextBox_species.AutoCompleteCustomSource66"), resources.GetString("TextBox_species.AutoCompleteCustomSource67"), resources.GetString("TextBox_species.AutoCompleteCustomSource68"), resources.GetString("TextBox_species.AutoCompleteCustomSource69"), resources.GetString("TextBox_species.AutoCompleteCustomSource70"), resources.GetString("TextBox_species.AutoCompleteCustomSource71"), resources.GetString("TextBox_species.AutoCompleteCustomSource72"), resources.GetString("TextBox_species.AutoCompleteCustomSource73"), resources.GetString("TextBox_species.AutoCompleteCustomSource74"), resources.GetString("TextBox_species.AutoCompleteCustomSource75"), resources.GetString("TextBox_species.AutoCompleteCustomSource76"), resources.GetString("TextBox_species.AutoCompleteCustomSource77"), resources.GetString("TextBox_species.AutoCompleteCustomSource78"), resources.GetString("TextBox_species.AutoCompleteCustomSource79"), resources.GetString("TextBox_species.AutoCompleteCustomSource80"), resources.GetString("TextBox_species.AutoCompleteCustomSource81"), resources.GetString("TextBox_species.AutoCompleteCustomSource82"), resources.GetString("TextBox_species.AutoCompleteCustomSource83"), resources.GetString("TextBox_species.AutoCompleteCustomSource84"), resources.GetString("TextBox_species.AutoCompleteCustomSource85"), resources.GetString("TextBox_species.AutoCompleteCustomSource86"), resources.GetString("TextBox_species.AutoCompleteCustomSource87"), resources.GetString("TextBox_species.AutoCompleteCustomSource88"), resources.GetString("TextBox_species.AutoCompleteCustomSource89"), resources.GetString("TextBox_species.AutoCompleteCustomSource90"), resources.GetString("TextBox_species.AutoCompleteCustomSource91"), resources.GetString("TextBox_species.AutoCompleteCustomSource92"), resources.GetString("TextBox_species.AutoCompleteCustomSource93"), resources.GetString("TextBox_species.AutoCompleteCustomSource94"), resources.GetString("TextBox_species.AutoCompleteCustomSource95"), resources.GetString("TextBox_species.AutoCompleteCustomSource96"), resources.GetString("TextBox_species.AutoCompleteCustomSource97"), resources.GetString("TextBox_species.AutoCompleteCustomSource98"), resources.GetString("TextBox_species.AutoCompleteCustomSource99"), resources.GetString("TextBox_species.AutoCompleteCustomSource100"), resources.GetString("TextBox_species.AutoCompleteCustomSource101"), resources.GetString("TextBox_species.AutoCompleteCustomSource102"), resources.GetString("TextBox_species.AutoCompleteCustomSource103"), resources.GetString("TextBox_species.AutoCompleteCustomSource104"), resources.GetString("TextBox_species.AutoCompleteCustomSource105"), resources.GetString("TextBox_species.AutoCompleteCustomSource106"), resources.GetString("TextBox_species.AutoCompleteCustomSource107"), resources.GetString("TextBox_species.AutoCompleteCustomSource108"), resources.GetString("TextBox_species.AutoCompleteCustomSource109"), resources.GetString("TextBox_species.AutoCompleteCustomSource110"), resources.GetString("TextBox_species.AutoCompleteCustomSource111"), resources.GetString("TextBox_species.AutoCompleteCustomSource112"), resources.GetString("TextBox_species.AutoCompleteCustomSource113"), resources.GetString("TextBox_species.AutoCompleteCustomSource114"), resources.GetString("TextBox_species.AutoCompleteCustomSource115"), resources.GetString("TextBox_species.AutoCompleteCustomSource116"), resources.GetString("TextBox_species.AutoCompleteCustomSource117"), resources.GetString("TextBox_species.AutoCompleteCustomSource118"), resources.GetString("TextBox_species.AutoCompleteCustomSource119"), resources.GetString("TextBox_species.AutoCompleteCustomSource120"), resources.GetString("TextBox_species.AutoCompleteCustomSource121"), resources.GetString("TextBox_species.AutoCompleteCustomSource122"), resources.GetString("TextBox_species.AutoCompleteCustomSource123"), resources.GetString("TextBox_species.AutoCompleteCustomSource124"), resources.GetString("TextBox_species.AutoCompleteCustomSource125"), resources.GetString("TextBox_species.AutoCompleteCustomSource126"), resources.GetString("TextBox_species.AutoCompleteCustomSource127"), resources.GetString("TextBox_species.AutoCompleteCustomSource128"), resources.GetString("TextBox_species.AutoCompleteCustomSource129"), resources.GetString("TextBox_species.AutoCompleteCustomSource130"), resources.GetString("TextBox_species.AutoCompleteCustomSource131"), resources.GetString("TextBox_species.AutoCompleteCustomSource132"), resources.GetString("TextBox_species.AutoCompleteCustomSource133"), resources.GetString("TextBox_species.AutoCompleteCustomSource134"), resources.GetString("TextBox_species.AutoCompleteCustomSource135"), resources.GetString("TextBox_species.AutoCompleteCustomSource136"), resources.GetString("TextBox_species.AutoCompleteCustomSource137"), resources.GetString("TextBox_species.AutoCompleteCustomSource138"), resources.GetString("TextBox_species.AutoCompleteCustomSource139"), resources.GetString("TextBox_species.AutoCompleteCustomSource140"), resources.GetString("TextBox_species.AutoCompleteCustomSource141"), resources.GetString("TextBox_species.AutoCompleteCustomSource142"), resources.GetString("TextBox_species.AutoCompleteCustomSource143"), resources.GetString("TextBox_species.AutoCompleteCustomSource144"), resources.GetString("TextBox_species.AutoCompleteCustomSource145"), resources.GetString("TextBox_species.AutoCompleteCustomSource146"), resources.GetString("TextBox_species.AutoCompleteCustomSource147"), resources.GetString("TextBox_species.AutoCompleteCustomSource148"), resources.GetString("TextBox_species.AutoCompleteCustomSource149"), resources.GetString("TextBox_species.AutoCompleteCustomSource150"), resources.GetString("TextBox_species.AutoCompleteCustomSource151"), resources.GetString("TextBox_species.AutoCompleteCustomSource152"), resources.GetString("TextBox_species.AutoCompleteCustomSource153"), resources.GetString("TextBox_species.AutoCompleteCustomSource154"), resources.GetString("TextBox_species.AutoCompleteCustomSource155"), resources.GetString("TextBox_species.AutoCompleteCustomSource156"), resources.GetString("TextBox_species.AutoCompleteCustomSource157"), resources.GetString("TextBox_species.AutoCompleteCustomSource158"), resources.GetString("TextBox_species.AutoCompleteCustomSource159"), resources.GetString("TextBox_species.AutoCompleteCustomSource160"), resources.GetString("TextBox_species.AutoCompleteCustomSource161"), resources.GetString("TextBox_species.AutoCompleteCustomSource162"), resources.GetString("TextBox_species.AutoCompleteCustomSource163"), resources.GetString("TextBox_species.AutoCompleteCustomSource164"), resources.GetString("TextBox_species.AutoCompleteCustomSource165"), resources.GetString("TextBox_species.AutoCompleteCustomSource166"), resources.GetString("TextBox_species.AutoCompleteCustomSource167"), resources.GetString("TextBox_species.AutoCompleteCustomSource168"), resources.GetString("TextBox_species.AutoCompleteCustomSource169"), resources.GetString("TextBox_species.AutoCompleteCustomSource170"), resources.GetString("TextBox_species.AutoCompleteCustomSource171"), resources.GetString("TextBox_species.AutoCompleteCustomSource172"), resources.GetString("TextBox_species.AutoCompleteCustomSource173"), resources.GetString("TextBox_species.AutoCompleteCustomSource174"), resources.GetString("TextBox_species.AutoCompleteCustomSource175"), resources.GetString("TextBox_species.AutoCompleteCustomSource176"), resources.GetString("TextBox_species.AutoCompleteCustomSource177"), resources.GetString("TextBox_species.AutoCompleteCustomSource178"), resources.GetString("TextBox_species.AutoCompleteCustomSource179"), resources.GetString("TextBox_species.AutoCompleteCustomSource180"), resources.GetString("TextBox_species.AutoCompleteCustomSource181"), resources.GetString("TextBox_species.AutoCompleteCustomSource182"), resources.GetString("TextBox_species.AutoCompleteCustomSource183"), resources.GetString("TextBox_species.AutoCompleteCustomSource184"), resources.GetString("TextBox_species.AutoCompleteCustomSource185"), resources.GetString("TextBox_species.AutoCompleteCustomSource186"), resources.GetString("TextBox_species.AutoCompleteCustomSource187"), resources.GetString("TextBox_species.AutoCompleteCustomSource188"), resources.GetString("TextBox_species.AutoCompleteCustomSource189"), resources.GetString("TextBox_species.AutoCompleteCustomSource190"), resources.GetString("TextBox_species.AutoCompleteCustomSource191"), resources.GetString("TextBox_species.AutoCompleteCustomSource192"), resources.GetString("TextBox_species.AutoCompleteCustomSource193"), resources.GetString("TextBox_species.AutoCompleteCustomSource194"), resources.GetString("TextBox_species.AutoCompleteCustomSource195"), resources.GetString("TextBox_species.AutoCompleteCustomSource196"), resources.GetString("TextBox_species.AutoCompleteCustomSource197"), resources.GetString("TextBox_species.AutoCompleteCustomSource198"), resources.GetString("TextBox_species.AutoCompleteCustomSource199"), resources.GetString("TextBox_species.AutoCompleteCustomSource200"), resources.GetString("TextBox_species.AutoCompleteCustomSource201"), resources.GetString("TextBox_species.AutoCompleteCustomSource202"), resources.GetString("TextBox_species.AutoCompleteCustomSource203"), resources.GetString("TextBox_species.AutoCompleteCustomSource204"), resources.GetString("TextBox_species.AutoCompleteCustomSource205"), resources.GetString("TextBox_species.AutoCompleteCustomSource206"), resources.GetString("TextBox_species.AutoCompleteCustomSource207"), resources.GetString("TextBox_species.AutoCompleteCustomSource208"), resources.GetString("TextBox_species.AutoCompleteCustomSource209"), resources.GetString("TextBox_species.AutoCompleteCustomSource210"), resources.GetString("TextBox_species.AutoCompleteCustomSource211"), resources.GetString("TextBox_species.AutoCompleteCustomSource212"), resources.GetString("TextBox_species.AutoCompleteCustomSource213"), resources.GetString("TextBox_species.AutoCompleteCustomSource214"), resources.GetString("TextBox_species.AutoCompleteCustomSource215"), resources.GetString("TextBox_species.AutoCompleteCustomSource216"), resources.GetString("TextBox_species.AutoCompleteCustomSource217"), resources.GetString("TextBox_species.AutoCompleteCustomSource218"), resources.GetString("TextBox_species.AutoCompleteCustomSource219"), resources.GetString("TextBox_species.AutoCompleteCustomSource220"), resources.GetString("TextBox_species.AutoCompleteCustomSource221"), resources.GetString("TextBox_species.AutoCompleteCustomSource222"), resources.GetString("TextBox_species.AutoCompleteCustomSource223"), resources.GetString("TextBox_species.AutoCompleteCustomSource224"), resources.GetString("TextBox_species.AutoCompleteCustomSource225"), resources.GetString("TextBox_species.AutoCompleteCustomSource226"), resources.GetString("TextBox_species.AutoCompleteCustomSource227"), resources.GetString("TextBox_species.AutoCompleteCustomSource228"), resources.GetString("TextBox_species.AutoCompleteCustomSource229"), resources.GetString("TextBox_species.AutoCompleteCustomSource230"), resources.GetString("TextBox_species.AutoCompleteCustomSource231"), resources.GetString("TextBox_species.AutoCompleteCustomSource232"), resources.GetString("TextBox_species.AutoCompleteCustomSource233"), resources.GetString("TextBox_species.AutoCompleteCustomSource234"), resources.GetString("TextBox_species.AutoCompleteCustomSource235"), resources.GetString("TextBox_species.AutoCompleteCustomSource236"), resources.GetString("TextBox_species.AutoCompleteCustomSource237"), resources.GetString("TextBox_species.AutoCompleteCustomSource238"), resources.GetString("TextBox_species.AutoCompleteCustomSource239"), resources.GetString("TextBox_species.AutoCompleteCustomSource240"), resources.GetString("TextBox_species.AutoCompleteCustomSource241"), resources.GetString("TextBox_species.AutoCompleteCustomSource242"), resources.GetString("TextBox_species.AutoCompleteCustomSource243"), resources.GetString("TextBox_species.AutoCompleteCustomSource244"), resources.GetString("TextBox_species.AutoCompleteCustomSource245"), resources.GetString("TextBox_species.AutoCompleteCustomSource246"), resources.GetString("TextBox_species.AutoCompleteCustomSource247"), resources.GetString("TextBox_species.AutoCompleteCustomSource248"), resources.GetString("TextBox_species.AutoCompleteCustomSource249"), resources.GetString("TextBox_species.AutoCompleteCustomSource250"), resources.GetString("TextBox_species.AutoCompleteCustomSource251"), resources.GetString("TextBox_species.AutoCompleteCustomSource252"), resources.GetString("TextBox_species.AutoCompleteCustomSource253"), resources.GetString("TextBox_species.AutoCompleteCustomSource254"), resources.GetString("TextBox_species.AutoCompleteCustomSource255"), resources.GetString("TextBox_species.AutoCompleteCustomSource256"), resources.GetString("TextBox_species.AutoCompleteCustomSource257"), resources.GetString("TextBox_species.AutoCompleteCustomSource258"), resources.GetString("TextBox_species.AutoCompleteCustomSource259"), resources.GetString("TextBox_species.AutoCompleteCustomSource260"), resources.GetString("TextBox_species.AutoCompleteCustomSource261"), resources.GetString("TextBox_species.AutoCompleteCustomSource262"), resources.GetString("TextBox_species.AutoCompleteCustomSource263"), resources.GetString("TextBox_species.AutoCompleteCustomSource264"), resources.GetString("TextBox_species.AutoCompleteCustomSource265"), resources.GetString("TextBox_species.AutoCompleteCustomSource266"), resources.GetString("TextBox_species.AutoCompleteCustomSource267"), resources.GetString("TextBox_species.AutoCompleteCustomSource268"), resources.GetString("TextBox_species.AutoCompleteCustomSource269"), resources.GetString("TextBox_species.AutoCompleteCustomSource270"), resources.GetString("TextBox_species.AutoCompleteCustomSource271"), resources.GetString("TextBox_species.AutoCompleteCustomSource272"), resources.GetString("TextBox_species.AutoCompleteCustomSource273"), resources.GetString("TextBox_species.AutoCompleteCustomSource274"), resources.GetString("TextBox_species.AutoCompleteCustomSource275"), resources.GetString("TextBox_species.AutoCompleteCustomSource276"), resources.GetString("TextBox_species.AutoCompleteCustomSource277"), resources.GetString("TextBox_species.AutoCompleteCustomSource278"), resources.GetString("TextBox_species.AutoCompleteCustomSource279"), resources.GetString("TextBox_species.AutoCompleteCustomSource280"), resources.GetString("TextBox_species.AutoCompleteCustomSource281"), resources.GetString("TextBox_species.AutoCompleteCustomSource282"), resources.GetString("TextBox_species.AutoCompleteCustomSource283"), resources.GetString("TextBox_species.AutoCompleteCustomSource284"), resources.GetString("TextBox_species.AutoCompleteCustomSource285"), resources.GetString("TextBox_species.AutoCompleteCustomSource286"), resources.GetString("TextBox_species.AutoCompleteCustomSource287"), resources.GetString("TextBox_species.AutoCompleteCustomSource288"), resources.GetString("TextBox_species.AutoCompleteCustomSource289"), resources.GetString("TextBox_species.AutoCompleteCustomSource290"), resources.GetString("TextBox_species.AutoCompleteCustomSource291"), resources.GetString("TextBox_species.AutoCompleteCustomSource292"), resources.GetString("TextBox_species.AutoCompleteCustomSource293"), resources.GetString("TextBox_species.AutoCompleteCustomSource294"), resources.GetString("TextBox_species.AutoCompleteCustomSource295"), resources.GetString("TextBox_species.AutoCompleteCustomSource296"), resources.GetString("TextBox_species.AutoCompleteCustomSource297"), resources.GetString("TextBox_species.AutoCompleteCustomSource298"), resources.GetString("TextBox_species.AutoCompleteCustomSource299"), resources.GetString("TextBox_species.AutoCompleteCustomSource300"), resources.GetString("TextBox_species.AutoCompleteCustomSource301"), resources.GetString("TextBox_species.AutoCompleteCustomSource302"), resources.GetString("TextBox_species.AutoCompleteCustomSource303"), resources.GetString("TextBox_species.AutoCompleteCustomSource304"), resources.GetString("TextBox_species.AutoCompleteCustomSource305"), resources.GetString("TextBox_species.AutoCompleteCustomSource306"), resources.GetString("TextBox_species.AutoCompleteCustomSource307"), resources.GetString("TextBox_species.AutoCompleteCustomSource308"), resources.GetString("TextBox_species.AutoCompleteCustomSource309"), resources.GetString("TextBox_species.AutoCompleteCustomSource310"), resources.GetString("TextBox_species.AutoCompleteCustomSource311"), resources.GetString("TextBox_species.AutoCompleteCustomSource312"), resources.GetString("TextBox_species.AutoCompleteCustomSource313"), resources.GetString("TextBox_species.AutoCompleteCustomSource314"), resources.GetString("TextBox_species.AutoCompleteCustomSource315"), resources.GetString("TextBox_species.AutoCompleteCustomSource316"), resources.GetString("TextBox_species.AutoCompleteCustomSource317"), resources.GetString("TextBox_species.AutoCompleteCustomSource318"), resources.GetString("TextBox_species.AutoCompleteCustomSource319"), resources.GetString("TextBox_species.AutoCompleteCustomSource320"), resources.GetString("TextBox_species.AutoCompleteCustomSource321"), resources.GetString("TextBox_species.AutoCompleteCustomSource322"), resources.GetString("TextBox_species.AutoCompleteCustomSource323"), resources.GetString("TextBox_species.AutoCompleteCustomSource324"), resources.GetString("TextBox_species.AutoCompleteCustomSource325"), resources.GetString("TextBox_species.AutoCompleteCustomSource326"), resources.GetString("TextBox_species.AutoCompleteCustomSource327"), resources.GetString("TextBox_species.AutoCompleteCustomSource328"), resources.GetString("TextBox_species.AutoCompleteCustomSource329"), resources.GetString("TextBox_species.AutoCompleteCustomSource330"), resources.GetString("TextBox_species.AutoCompleteCustomSource331"), resources.GetString("TextBox_species.AutoCompleteCustomSource332"), resources.GetString("TextBox_species.AutoCompleteCustomSource333"), resources.GetString("TextBox_species.AutoCompleteCustomSource334"), resources.GetString("TextBox_species.AutoCompleteCustomSource335"), resources.GetString("TextBox_species.AutoCompleteCustomSource336"), resources.GetString("TextBox_species.AutoCompleteCustomSource337"), resources.GetString("TextBox_species.AutoCompleteCustomSource338"), resources.GetString("TextBox_species.AutoCompleteCustomSource339"), resources.GetString("TextBox_species.AutoCompleteCustomSource340"), resources.GetString("TextBox_species.AutoCompleteCustomSource341"), resources.GetString("TextBox_species.AutoCompleteCustomSource342"), resources.GetString("TextBox_species.AutoCompleteCustomSource343"), resources.GetString("TextBox_species.AutoCompleteCustomSource344"), resources.GetString("TextBox_species.AutoCompleteCustomSource345"), resources.GetString("TextBox_species.AutoCompleteCustomSource346"), resources.GetString("TextBox_species.AutoCompleteCustomSource347"), resources.GetString("TextBox_species.AutoCompleteCustomSource348"), resources.GetString("TextBox_species.AutoCompleteCustomSource349"), resources.GetString("TextBox_species.AutoCompleteCustomSource350"), resources.GetString("TextBox_species.AutoCompleteCustomSource351"), resources.GetString("TextBox_species.AutoCompleteCustomSource352"), resources.GetString("TextBox_species.AutoCompleteCustomSource353"), resources.GetString("TextBox_species.AutoCompleteCustomSource354"), resources.GetString("TextBox_species.AutoCompleteCustomSource355"), resources.GetString("TextBox_species.AutoCompleteCustomSource356"), resources.GetString("TextBox_species.AutoCompleteCustomSource357"), resources.GetString("TextBox_species.AutoCompleteCustomSource358"), resources.GetString("TextBox_species.AutoCompleteCustomSource359"), resources.GetString("TextBox_species.AutoCompleteCustomSource360"), resources.GetString("TextBox_species.AutoCompleteCustomSource361"), resources.GetString("TextBox_species.AutoCompleteCustomSource362"), resources.GetString("TextBox_species.AutoCompleteCustomSource363"), resources.GetString("TextBox_species.AutoCompleteCustomSource364"), resources.GetString("TextBox_species.AutoCompleteCustomSource365"), resources.GetString("TextBox_species.AutoCompleteCustomSource366"), resources.GetString("TextBox_species.AutoCompleteCustomSource367"), resources.GetString("TextBox_species.AutoCompleteCustomSource368"), resources.GetString("TextBox_species.AutoCompleteCustomSource369"), resources.GetString("TextBox_species.AutoCompleteCustomSource370"), resources.GetString("TextBox_species.AutoCompleteCustomSource371"), resources.GetString("TextBox_species.AutoCompleteCustomSource372"), resources.GetString("TextBox_species.AutoCompleteCustomSource373"), resources.GetString("TextBox_species.AutoCompleteCustomSource374"), resources.GetString("TextBox_species.AutoCompleteCustomSource375"), resources.GetString("TextBox_species.AutoCompleteCustomSource376"), resources.GetString("TextBox_species.AutoCompleteCustomSource377"), resources.GetString("TextBox_species.AutoCompleteCustomSource378"), resources.GetString("TextBox_species.AutoCompleteCustomSource379"), resources.GetString("TextBox_species.AutoCompleteCustomSource380"), resources.GetString("TextBox_species.AutoCompleteCustomSource381"), resources.GetString("TextBox_species.AutoCompleteCustomSource382"), resources.GetString("TextBox_species.AutoCompleteCustomSource383"), resources.GetString("TextBox_species.AutoCompleteCustomSource384"), resources.GetString("TextBox_species.AutoCompleteCustomSource385"), resources.GetString("TextBox_species.AutoCompleteCustomSource386"), resources.GetString("TextBox_species.AutoCompleteCustomSource387"), resources.GetString("TextBox_species.AutoCompleteCustomSource388"), resources.GetString("TextBox_species.AutoCompleteCustomSource389"), resources.GetString("TextBox_species.AutoCompleteCustomSource390"), resources.GetString("TextBox_species.AutoCompleteCustomSource391"), resources.GetString("TextBox_species.AutoCompleteCustomSource392"), resources.GetString("TextBox_species.AutoCompleteCustomSource393"), resources.GetString("TextBox_species.AutoCompleteCustomSource394"), resources.GetString("TextBox_species.AutoCompleteCustomSource395"), resources.GetString("TextBox_species.AutoCompleteCustomSource396"), resources.GetString("TextBox_species.AutoCompleteCustomSource397"), resources.GetString("TextBox_species.AutoCompleteCustomSource398"), resources.GetString("TextBox_species.AutoCompleteCustomSource399"), resources.GetString("TextBox_species.AutoCompleteCustomSource400"), resources.GetString("TextBox_species.AutoCompleteCustomSource401"), resources.GetString("TextBox_species.AutoCompleteCustomSource402"), resources.GetString("TextBox_species.AutoCompleteCustomSource403"), resources.GetString("TextBox_species.AutoCompleteCustomSource404"), resources.GetString("TextBox_species.AutoCompleteCustomSource405"), resources.GetString("TextBox_species.AutoCompleteCustomSource406"), resources.GetString("TextBox_species.AutoCompleteCustomSource407"), resources.GetString("TextBox_species.AutoCompleteCustomSource408"), resources.GetString("TextBox_species.AutoCompleteCustomSource409"), resources.GetString("TextBox_species.AutoCompleteCustomSource410"), resources.GetString("TextBox_species.AutoCompleteCustomSource411"), resources.GetString("TextBox_species.AutoCompleteCustomSource412"), resources.GetString("TextBox_species.AutoCompleteCustomSource413"), resources.GetString("TextBox_species.AutoCompleteCustomSource414"), resources.GetString("TextBox_species.AutoCompleteCustomSource415"), resources.GetString("TextBox_species.AutoCompleteCustomSource416"), resources.GetString("TextBox_species.AutoCompleteCustomSource417"), resources.GetString("TextBox_species.AutoCompleteCustomSource418"), resources.GetString("TextBox_species.AutoCompleteCustomSource419"), resources.GetString("TextBox_species.AutoCompleteCustomSource420"), resources.GetString("TextBox_species.AutoCompleteCustomSource421"), resources.GetString("TextBox_species.AutoCompleteCustomSource422"), resources.GetString("TextBox_species.AutoCompleteCustomSource423"), resources.GetString("TextBox_species.AutoCompleteCustomSource424"), resources.GetString("TextBox_species.AutoCompleteCustomSource425"), resources.GetString("TextBox_species.AutoCompleteCustomSource426"), resources.GetString("TextBox_species.AutoCompleteCustomSource427"), resources.GetString("TextBox_species.AutoCompleteCustomSource428"), resources.GetString("TextBox_species.AutoCompleteCustomSource429"), resources.GetString("TextBox_species.AutoCompleteCustomSource430"), resources.GetString("TextBox_species.AutoCompleteCustomSource431"), resources.GetString("TextBox_species.AutoCompleteCustomSource432"), resources.GetString("TextBox_species.AutoCompleteCustomSource433"), resources.GetString("TextBox_species.AutoCompleteCustomSource434"), resources.GetString("TextBox_species.AutoCompleteCustomSource435"), resources.GetString("TextBox_species.AutoCompleteCustomSource436"), resources.GetString("TextBox_species.AutoCompleteCustomSource437"), resources.GetString("TextBox_species.AutoCompleteCustomSource438"), resources.GetString("TextBox_species.AutoCompleteCustomSource439"), resources.GetString("TextBox_species.AutoCompleteCustomSource440"), resources.GetString("TextBox_species.AutoCompleteCustomSource441"), resources.GetString("TextBox_species.AutoCompleteCustomSource442"), resources.GetString("TextBox_species.AutoCompleteCustomSource443"), resources.GetString("TextBox_species.AutoCompleteCustomSource444"), resources.GetString("TextBox_species.AutoCompleteCustomSource445"), resources.GetString("TextBox_species.AutoCompleteCustomSource446"), resources.GetString("TextBox_species.AutoCompleteCustomSource447"), resources.GetString("TextBox_species.AutoCompleteCustomSource448"), resources.GetString("TextBox_species.AutoCompleteCustomSource449"), resources.GetString("TextBox_species.AutoCompleteCustomSource450"), resources.GetString("TextBox_species.AutoCompleteCustomSource451"), resources.GetString("TextBox_species.AutoCompleteCustomSource452"), resources.GetString("TextBox_species.AutoCompleteCustomSource453"), resources.GetString("TextBox_species.AutoCompleteCustomSource454"), resources.GetString("TextBox_species.AutoCompleteCustomSource455"), resources.GetString("TextBox_species.AutoCompleteCustomSource456"), resources.GetString("TextBox_species.AutoCompleteCustomSource457"), resources.GetString("TextBox_species.AutoCompleteCustomSource458"), resources.GetString("TextBox_species.AutoCompleteCustomSource459"), resources.GetString("TextBox_species.AutoCompleteCustomSource460"), resources.GetString("TextBox_species.AutoCompleteCustomSource461"), resources.GetString("TextBox_species.AutoCompleteCustomSource462"), resources.GetString("TextBox_species.AutoCompleteCustomSource463"), resources.GetString("TextBox_species.AutoCompleteCustomSource464"), resources.GetString("TextBox_species.AutoCompleteCustomSource465"), resources.GetString("TextBox_species.AutoCompleteCustomSource466"), resources.GetString("TextBox_species.AutoCompleteCustomSource467"), resources.GetString("TextBox_species.AutoCompleteCustomSource468"), resources.GetString("TextBox_species.AutoCompleteCustomSource469"), resources.GetString("TextBox_species.AutoCompleteCustomSource470"), resources.GetString("TextBox_species.AutoCompleteCustomSource471"), resources.GetString("TextBox_species.AutoCompleteCustomSource472"), resources.GetString("TextBox_species.AutoCompleteCustomSource473"), resources.GetString("TextBox_species.AutoCompleteCustomSource474"), resources.GetString("TextBox_species.AutoCompleteCustomSource475"), resources.GetString("TextBox_species.AutoCompleteCustomSource476"), resources.GetString("TextBox_species.AutoCompleteCustomSource477"), resources.GetString("TextBox_species.AutoCompleteCustomSource478"), resources.GetString("TextBox_species.AutoCompleteCustomSource479"), resources.GetString("TextBox_species.AutoCompleteCustomSource480"), resources.GetString("TextBox_species.AutoCompleteCustomSource481"), resources.GetString("TextBox_species.AutoCompleteCustomSource482"), resources.GetString("TextBox_species.AutoCompleteCustomSource483"), resources.GetString("TextBox_species.AutoCompleteCustomSource484"), resources.GetString("TextBox_species.AutoCompleteCustomSource485"), resources.GetString("TextBox_species.AutoCompleteCustomSource486"), resources.GetString("TextBox_species.AutoCompleteCustomSource487"), resources.GetString("TextBox_species.AutoCompleteCustomSource488"), resources.GetString("TextBox_species.AutoCompleteCustomSource489"), resources.GetString("TextBox_species.AutoCompleteCustomSource490"), resources.GetString("TextBox_species.AutoCompleteCustomSource491"), resources.GetString("TextBox_species.AutoCompleteCustomSource492"), resources.GetString("TextBox_species.AutoCompleteCustomSource493"), resources.GetString("TextBox_species.AutoCompleteCustomSource494"), resources.GetString("TextBox_species.AutoCompleteCustomSource495"), resources.GetString("TextBox_species.AutoCompleteCustomSource496"), resources.GetString("TextBox_species.AutoCompleteCustomSource497"), resources.GetString("TextBox_species.AutoCompleteCustomSource498"), resources.GetString("TextBox_species.AutoCompleteCustomSource499"), resources.GetString("TextBox_species.AutoCompleteCustomSource500"), resources.GetString("TextBox_species.AutoCompleteCustomSource501"), resources.GetString("TextBox_species.AutoCompleteCustomSource502"), resources.GetString("TextBox_species.AutoCompleteCustomSource503"), resources.GetString("TextBox_species.AutoCompleteCustomSource504"), resources.GetString("TextBox_species.AutoCompleteCustomSource505"), resources.GetString("TextBox_species.AutoCompleteCustomSource506"), resources.GetString("TextBox_species.AutoCompleteCustomSource507"), resources.GetString("TextBox_species.AutoCompleteCustomSource508"), resources.GetString("TextBox_species.AutoCompleteCustomSource509"), resources.GetString("TextBox_species.AutoCompleteCustomSource510"), resources.GetString("TextBox_species.AutoCompleteCustomSource511"), resources.GetString("TextBox_species.AutoCompleteCustomSource512"), resources.GetString("TextBox_species.AutoCompleteCustomSource513"), resources.GetString("TextBox_species.AutoCompleteCustomSource514"), resources.GetString("TextBox_species.AutoCompleteCustomSource515"), resources.GetString("TextBox_species.AutoCompleteCustomSource516"), resources.GetString("TextBox_species.AutoCompleteCustomSource517"), resources.GetString("TextBox_species.AutoCompleteCustomSource518"), resources.GetString("TextBox_species.AutoCompleteCustomSource519"), resources.GetString("TextBox_species.AutoCompleteCustomSource520"), resources.GetString("TextBox_species.AutoCompleteCustomSource521"), resources.GetString("TextBox_species.AutoCompleteCustomSource522"), resources.GetString("TextBox_species.AutoCompleteCustomSource523"), resources.GetString("TextBox_species.AutoCompleteCustomSource524"), resources.GetString("TextBox_species.AutoCompleteCustomSource525"), resources.GetString("TextBox_species.AutoCompleteCustomSource526"), resources.GetString("TextBox_species.AutoCompleteCustomSource527"), resources.GetString("TextBox_species.AutoCompleteCustomSource528"), resources.GetString("TextBox_species.AutoCompleteCustomSource529"), resources.GetString("TextBox_species.AutoCompleteCustomSource530"), resources.GetString("TextBox_species.AutoCompleteCustomSource531"), resources.GetString("TextBox_species.AutoCompleteCustomSource532"), resources.GetString("TextBox_species.AutoCompleteCustomSource533"), resources.GetString("TextBox_species.AutoCompleteCustomSource534"), resources.GetString("TextBox_species.AutoCompleteCustomSource535"), resources.GetString("TextBox_species.AutoCompleteCustomSource536"), resources.GetString("TextBox_species.AutoCompleteCustomSource537"), resources.GetString("TextBox_species.AutoCompleteCustomSource538"), resources.GetString("TextBox_species.AutoCompleteCustomSource539"), resources.GetString("TextBox_species.AutoCompleteCustomSource540"), resources.GetString("TextBox_species.AutoCompleteCustomSource541"), resources.GetString("TextBox_species.AutoCompleteCustomSource542"), resources.GetString("TextBox_species.AutoCompleteCustomSource543"), resources.GetString("TextBox_species.AutoCompleteCustomSource544"), resources.GetString("TextBox_species.AutoCompleteCustomSource545"), resources.GetString("TextBox_species.AutoCompleteCustomSource546"), resources.GetString("TextBox_species.AutoCompleteCustomSource547"), resources.GetString("TextBox_species.AutoCompleteCustomSource548"), resources.GetString("TextBox_species.AutoCompleteCustomSource549"), resources.GetString("TextBox_species.AutoCompleteCustomSource550"), resources.GetString("TextBox_species.AutoCompleteCustomSource551"), resources.GetString("TextBox_species.AutoCompleteCustomSource552"), resources.GetString("TextBox_species.AutoCompleteCustomSource553"), resources.GetString("TextBox_species.AutoCompleteCustomSource554"), resources.GetString("TextBox_species.AutoCompleteCustomSource555"), resources.GetString("TextBox_species.AutoCompleteCustomSource556"), resources.GetString("TextBox_species.AutoCompleteCustomSource557"), resources.GetString("TextBox_species.AutoCompleteCustomSource558"), resources.GetString("TextBox_species.AutoCompleteCustomSource559"), resources.GetString("TextBox_species.AutoCompleteCustomSource560"), resources.GetString("TextBox_species.AutoCompleteCustomSource561"), resources.GetString("TextBox_species.AutoCompleteCustomSource562"), resources.GetString("TextBox_species.AutoCompleteCustomSource563"), resources.GetString("TextBox_species.AutoCompleteCustomSource564"), resources.GetString("TextBox_species.AutoCompleteCustomSource565"), resources.GetString("TextBox_species.AutoCompleteCustomSource566"), resources.GetString("TextBox_species.AutoCompleteCustomSource567"), resources.GetString("TextBox_species.AutoCompleteCustomSource568"), resources.GetString("TextBox_species.AutoCompleteCustomSource569"), resources.GetString("TextBox_species.AutoCompleteCustomSource570"), resources.GetString("TextBox_species.AutoCompleteCustomSource571"), resources.GetString("TextBox_species.AutoCompleteCustomSource572"), resources.GetString("TextBox_species.AutoCompleteCustomSource573"), resources.GetString("TextBox_species.AutoCompleteCustomSource574"), resources.GetString("TextBox_species.AutoCompleteCustomSource575"), resources.GetString("TextBox_species.AutoCompleteCustomSource576"), resources.GetString("TextBox_species.AutoCompleteCustomSource577"), resources.GetString("TextBox_species.AutoCompleteCustomSource578"), resources.GetString("TextBox_species.AutoCompleteCustomSource579"), resources.GetString("TextBox_species.AutoCompleteCustomSource580"), resources.GetString("TextBox_species.AutoCompleteCustomSource581"), resources.GetString("TextBox_species.AutoCompleteCustomSource582"), resources.GetString("TextBox_species.AutoCompleteCustomSource583"), resources.GetString("TextBox_species.AutoCompleteCustomSource584"), resources.GetString("TextBox_species.AutoCompleteCustomSource585"), resources.GetString("TextBox_species.AutoCompleteCustomSource586"), resources.GetString("TextBox_species.AutoCompleteCustomSource587"), resources.GetString("TextBox_species.AutoCompleteCustomSource588"), resources.GetString("TextBox_species.AutoCompleteCustomSource589"), resources.GetString("TextBox_species.AutoCompleteCustomSource590"), resources.GetString("TextBox_species.AutoCompleteCustomSource591"), resources.GetString("TextBox_species.AutoCompleteCustomSource592"), resources.GetString("TextBox_species.AutoCompleteCustomSource593"), resources.GetString("TextBox_species.AutoCompleteCustomSource594"), resources.GetString("TextBox_species.AutoCompleteCustomSource595"), resources.GetString("TextBox_species.AutoCompleteCustomSource596"), resources.GetString("TextBox_species.AutoCompleteCustomSource597"), resources.GetString("TextBox_species.AutoCompleteCustomSource598"), resources.GetString("TextBox_species.AutoCompleteCustomSource599"), resources.GetString("TextBox_species.AutoCompleteCustomSource600"), resources.GetString("TextBox_species.AutoCompleteCustomSource601"), resources.GetString("TextBox_species.AutoCompleteCustomSource602"), resources.GetString("TextBox_species.AutoCompleteCustomSource603"), resources.GetString("TextBox_species.AutoCompleteCustomSource604"), resources.GetString("TextBox_species.AutoCompleteCustomSource605"), resources.GetString("TextBox_species.AutoCompleteCustomSource606"), resources.GetString("TextBox_species.AutoCompleteCustomSource607"), resources.GetString("TextBox_species.AutoCompleteCustomSource608"), resources.GetString("TextBox_species.AutoCompleteCustomSource609"), resources.GetString("TextBox_species.AutoCompleteCustomSource610"), resources.GetString("TextBox_species.AutoCompleteCustomSource611"), resources.GetString("TextBox_species.AutoCompleteCustomSource612"), resources.GetString("TextBox_species.AutoCompleteCustomSource613"), resources.GetString("TextBox_species.AutoCompleteCustomSource614"), resources.GetString("TextBox_species.AutoCompleteCustomSource615"), resources.GetString("TextBox_species.AutoCompleteCustomSource616"), resources.GetString("TextBox_species.AutoCompleteCustomSource617"), resources.GetString("TextBox_species.AutoCompleteCustomSource618"), resources.GetString("TextBox_species.AutoCompleteCustomSource619"), resources.GetString("TextBox_species.AutoCompleteCustomSource620"), resources.GetString("TextBox_species.AutoCompleteCustomSource621"), resources.GetString("TextBox_species.AutoCompleteCustomSource622"), resources.GetString("TextBox_species.AutoCompleteCustomSource623"), resources.GetString("TextBox_species.AutoCompleteCustomSource624"), resources.GetString("TextBox_species.AutoCompleteCustomSource625"), resources.GetString("TextBox_species.AutoCompleteCustomSource626"), resources.GetString("TextBox_species.AutoCompleteCustomSource627"), resources.GetString("TextBox_species.AutoCompleteCustomSource628"), resources.GetString("TextBox_species.AutoCompleteCustomSource629"), resources.GetString("TextBox_species.AutoCompleteCustomSource630"), resources.GetString("TextBox_species.AutoCompleteCustomSource631"), resources.GetString("TextBox_species.AutoCompleteCustomSource632"), resources.GetString("TextBox_species.AutoCompleteCustomSource633"), resources.GetString("TextBox_species.AutoCompleteCustomSource634"), resources.GetString("TextBox_species.AutoCompleteCustomSource635"), resources.GetString("TextBox_species.AutoCompleteCustomSource636"), resources.GetString("TextBox_species.AutoCompleteCustomSource637"), resources.GetString("TextBox_species.AutoCompleteCustomSource638"), resources.GetString("TextBox_species.AutoCompleteCustomSource639"), resources.GetString("TextBox_species.AutoCompleteCustomSource640"), resources.GetString("TextBox_species.AutoCompleteCustomSource641"), resources.GetString("TextBox_species.AutoCompleteCustomSource642"), resources.GetString("TextBox_species.AutoCompleteCustomSource643"), resources.GetString("TextBox_species.AutoCompleteCustomSource644"), resources.GetString("TextBox_species.AutoCompleteCustomSource645"), resources.GetString("TextBox_species.AutoCompleteCustomSource646"), resources.GetString("TextBox_species.AutoCompleteCustomSource647"), resources.GetString("TextBox_species.AutoCompleteCustomSource648"), resources.GetString("TextBox_species.AutoCompleteCustomSource649"), resources.GetString("TextBox_species.AutoCompleteCustomSource650"), resources.GetString("TextBox_species.AutoCompleteCustomSource651"), resources.GetString("TextBox_species.AutoCompleteCustomSource652"), resources.GetString("TextBox_species.AutoCompleteCustomSource653"), resources.GetString("TextBox_species.AutoCompleteCustomSource654"), resources.GetString("TextBox_species.AutoCompleteCustomSource655"), resources.GetString("TextBox_species.AutoCompleteCustomSource656"), resources.GetString("TextBox_species.AutoCompleteCustomSource657"), resources.GetString("TextBox_species.AutoCompleteCustomSource658"), resources.GetString("TextBox_species.AutoCompleteCustomSource659"), resources.GetString("TextBox_species.AutoCompleteCustomSource660"), resources.GetString("TextBox_species.AutoCompleteCustomSource661"), resources.GetString("TextBox_species.AutoCompleteCustomSource662"), resources.GetString("TextBox_species.AutoCompleteCustomSource663"), resources.GetString("TextBox_species.AutoCompleteCustomSource664"), resources.GetString("TextBox_species.AutoCompleteCustomSource665"), resources.GetString("TextBox_species.AutoCompleteCustomSource666"), resources.GetString("TextBox_species.AutoCompleteCustomSource667"), resources.GetString("TextBox_species.AutoCompleteCustomSource668"), resources.GetString("TextBox_species.AutoCompleteCustomSource669"), resources.GetString("TextBox_species.AutoCompleteCustomSource670"), resources.GetString("TextBox_species.AutoCompleteCustomSource671"), resources.GetString("TextBox_species.AutoCompleteCustomSource672"), resources.GetString("TextBox_species.AutoCompleteCustomSource673"), resources.GetString("TextBox_species.AutoCompleteCustomSource674"), resources.GetString("TextBox_species.AutoCompleteCustomSource675"), resources.GetString("TextBox_species.AutoCompleteCustomSource676"), resources.GetString("TextBox_species.AutoCompleteCustomSource677"), resources.GetString("TextBox_species.AutoCompleteCustomSource678"), resources.GetString("TextBox_species.AutoCompleteCustomSource679"), resources.GetString("TextBox_species.AutoCompleteCustomSource680"), resources.GetString("TextBox_species.AutoCompleteCustomSource681"), resources.GetString("TextBox_species.AutoCompleteCustomSource682"), resources.GetString("TextBox_species.AutoCompleteCustomSource683"), resources.GetString("TextBox_species.AutoCompleteCustomSource684"), resources.GetString("TextBox_species.AutoCompleteCustomSource685"), resources.GetString("TextBox_species.AutoCompleteCustomSource686"), resources.GetString("TextBox_species.AutoCompleteCustomSource687"), resources.GetString("TextBox_species.AutoCompleteCustomSource688"), resources.GetString("TextBox_species.AutoCompleteCustomSource689"), resources.GetString("TextBox_species.AutoCompleteCustomSource690"), resources.GetString("TextBox_species.AutoCompleteCustomSource691"), resources.GetString("TextBox_species.AutoCompleteCustomSource692"), resources.GetString("TextBox_species.AutoCompleteCustomSource693"), resources.GetString("TextBox_species.AutoCompleteCustomSource694"), resources.GetString("TextBox_species.AutoCompleteCustomSource695"), resources.GetString("TextBox_species.AutoCompleteCustomSource696"), resources.GetString("TextBox_species.AutoCompleteCustomSource697"), resources.GetString("TextBox_species.AutoCompleteCustomSource698"), resources.GetString("TextBox_species.AutoCompleteCustomSource699"), resources.GetString("TextBox_species.AutoCompleteCustomSource700"), resources.GetString("TextBox_species.AutoCompleteCustomSource701"), resources.GetString("TextBox_species.AutoCompleteCustomSource702"), resources.GetString("TextBox_species.AutoCompleteCustomSource703"), resources.GetString("TextBox_species.AutoCompleteCustomSource704"), resources.GetString("TextBox_species.AutoCompleteCustomSource705"), resources.GetString("TextBox_species.AutoCompleteCustomSource706"), resources.GetString("TextBox_species.AutoCompleteCustomSource707"), resources.GetString("TextBox_species.AutoCompleteCustomSource708"), resources.GetString("TextBox_species.AutoCompleteCustomSource709"), resources.GetString("TextBox_species.AutoCompleteCustomSource710"), resources.GetString("TextBox_species.AutoCompleteCustomSource711"), resources.GetString("TextBox_species.AutoCompleteCustomSource712"), resources.GetString("TextBox_species.AutoCompleteCustomSource713"), resources.GetString("TextBox_species.AutoCompleteCustomSource714"), resources.GetString("TextBox_species.AutoCompleteCustomSource715"), resources.GetString("TextBox_species.AutoCompleteCustomSource716"), resources.GetString("TextBox_species.AutoCompleteCustomSource717"), resources.GetString("TextBox_species.AutoCompleteCustomSource718"), resources.GetString("TextBox_species.AutoCompleteCustomSource719"), resources.GetString("TextBox_species.AutoCompleteCustomSource720"), resources.GetString("TextBox_species.AutoCompleteCustomSource721"), resources.GetString("TextBox_species.AutoCompleteCustomSource722"), resources.GetString("TextBox_species.AutoCompleteCustomSource723"), resources.GetString("TextBox_species.AutoCompleteCustomSource724"), resources.GetString("TextBox_species.AutoCompleteCustomSource725"), resources.GetString("TextBox_species.AutoCompleteCustomSource726"), resources.GetString("TextBox_species.AutoCompleteCustomSource727"), resources.GetString("TextBox_species.AutoCompleteCustomSource728"), resources.GetString("TextBox_species.AutoCompleteCustomSource729"), resources.GetString("TextBox_species.AutoCompleteCustomSource730"), resources.GetString("TextBox_species.AutoCompleteCustomSource731"), resources.GetString("TextBox_species.AutoCompleteCustomSource732"), resources.GetString("TextBox_species.AutoCompleteCustomSource733"), resources.GetString("TextBox_species.AutoCompleteCustomSource734"), resources.GetString("TextBox_species.AutoCompleteCustomSource735"), resources.GetString("TextBox_species.AutoCompleteCustomSource736"), resources.GetString("TextBox_species.AutoCompleteCustomSource737"), resources.GetString("TextBox_species.AutoCompleteCustomSource738"), resources.GetString("TextBox_species.AutoCompleteCustomSource739"), resources.GetString("TextBox_species.AutoCompleteCustomSource740"), resources.GetString("TextBox_species.AutoCompleteCustomSource741"), resources.GetString("TextBox_species.AutoCompleteCustomSource742"), resources.GetString("TextBox_species.AutoCompleteCustomSource743"), resources.GetString("TextBox_species.AutoCompleteCustomSource744"), resources.GetString("TextBox_species.AutoCompleteCustomSource745"), resources.GetString("TextBox_species.AutoCompleteCustomSource746"), resources.GetString("TextBox_species.AutoCompleteCustomSource747"), resources.GetString("TextBox_species.AutoCompleteCustomSource748"), resources.GetString("TextBox_species.AutoCompleteCustomSource749"), resources.GetString("TextBox_species.AutoCompleteCustomSource750"), resources.GetString("TextBox_species.AutoCompleteCustomSource751"), resources.GetString("TextBox_species.AutoCompleteCustomSource752"), resources.GetString("TextBox_species.AutoCompleteCustomSource753"), resources.GetString("TextBox_species.AutoCompleteCustomSource754"), resources.GetString("TextBox_species.AutoCompleteCustomSource755"), resources.GetString("TextBox_species.AutoCompleteCustomSource756"), resources.GetString("TextBox_species.AutoCompleteCustomSource757"), resources.GetString("TextBox_species.AutoCompleteCustomSource758"), resources.GetString("TextBox_species.AutoCompleteCustomSource759"), resources.GetString("TextBox_species.AutoCompleteCustomSource760"), resources.GetString("TextBox_species.AutoCompleteCustomSource761"), resources.GetString("TextBox_species.AutoCompleteCustomSource762"), resources.GetString("TextBox_species.AutoCompleteCustomSource763"), resources.GetString("TextBox_species.AutoCompleteCustomSource764"), resources.GetString("TextBox_species.AutoCompleteCustomSource765"), resources.GetString("TextBox_species.AutoCompleteCustomSource766"), resources.GetString("TextBox_species.AutoCompleteCustomSource767"), resources.GetString("TextBox_species.AutoCompleteCustomSource768"), resources.GetString("TextBox_species.AutoCompleteCustomSource769"), resources.GetString("TextBox_species.AutoCompleteCustomSource770"), resources.GetString("TextBox_species.AutoCompleteCustomSource771"), resources.GetString("TextBox_species.AutoCompleteCustomSource772"), resources.GetString("TextBox_species.AutoCompleteCustomSource773"), resources.GetString("TextBox_species.AutoCompleteCustomSource774"), resources.GetString("TextBox_species.AutoCompleteCustomSource775"), resources.GetString("TextBox_species.AutoCompleteCustomSource776")})
        Me.TextBox_species.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.TextBox_species.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource
        resources.ApplyResources(Me.TextBox_species, "TextBox_species")
        Me.TextBox_species.Name = "TextBox_species"
        '
        'TextBox_sampleref1
        '
        resources.ApplyResources(Me.TextBox_sampleref1, "TextBox_sampleref1")
        Me.TextBox_sampleref1.Name = "TextBox_sampleref1"
        '
        'TextBox_sampleref2
        '
        resources.ApplyResources(Me.TextBox_sampleref2, "TextBox_sampleref2")
        Me.TextBox_sampleref2.Name = "TextBox_sampleref2"
        '
        'Label33
        '
        resources.ApplyResources(Me.Label33, "Label33")
        Me.Label33.Name = "Label33"
        '
        'Label23
        '
        resources.ApplyResources(Me.Label23, "Label23")
        Me.Label23.Name = "Label23"
        '
        'TextBox_campaign_name
        '
        Me.TextBox_campaign_name.BackColor = System.Drawing.Color.PaleGoldenrod
        resources.ApplyResources(Me.TextBox_campaign_name, "TextBox_campaign_name")
        Me.TextBox_campaign_name.Name = "TextBox_campaign_name"
        '
        'Label17
        '
        resources.ApplyResources(Me.Label17, "Label17")
        Me.Label17.Name = "Label17"
        '
        'Label13
        '
        resources.ApplyResources(Me.Label13, "Label13")
        Me.Label13.Name = "Label13"
        '
        'TextBox_location
        '
        Me.TextBox_location.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        resources.ApplyResources(Me.TextBox_location, "TextBox_location")
        Me.TextBox_location.Name = "TextBox_location"
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.Transparent
        Me.Panel1.Controls.Add(Me.ComboBox_operator)
        Me.Panel1.Controls.Add(Me.Label32)
        Me.Panel1.Controls.Add(Me.TextBox_comment)
        Me.Panel1.Controls.Add(Me.Label18)
        Me.Panel1.Controls.Add(Me.ComboBox_sampletype)
        Me.Panel1.Controls.Add(Me.Label31)
        Me.Panel1.Controls.Add(Me.TextBox_sampleref3)
        Me.Panel1.Controls.Add(Me.Label34)
        resources.ApplyResources(Me.Panel1, "Panel1")
        Me.Panel1.Name = "Panel1"
        '
        'ComboBox_operator
        '
        Me.ComboBox_operator.AllowDrop = True
        Me.ComboBox_operator.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append
        Me.ComboBox_operator.FormattingEnabled = True
        resources.ApplyResources(Me.ComboBox_operator, "ComboBox_operator")
        Me.ComboBox_operator.Name = "ComboBox_operator"
        '
        'Label32
        '
        resources.ApplyResources(Me.Label32, "Label32")
        Me.Label32.Name = "Label32"
        '
        'TextBox_comment
        '
        resources.ApplyResources(Me.TextBox_comment, "TextBox_comment")
        Me.TextBox_comment.Name = "TextBox_comment"
        '
        'Label18
        '
        resources.ApplyResources(Me.Label18, "Label18")
        Me.Label18.Name = "Label18"
        '
        'ComboBox_sampletype
        '
        Me.ComboBox_sampletype.FormattingEnabled = True
        Me.ComboBox_sampletype.Items.AddRange(New Object() {resources.GetString("ComboBox_sampletype.Items"), resources.GetString("ComboBox_sampletype.Items1"), resources.GetString("ComboBox_sampletype.Items2"), resources.GetString("ComboBox_sampletype.Items3")})
        resources.ApplyResources(Me.ComboBox_sampletype, "ComboBox_sampletype")
        Me.ComboBox_sampletype.Name = "ComboBox_sampletype"
        '
        'Label31
        '
        resources.ApplyResources(Me.Label31, "Label31")
        Me.Label31.Name = "Label31"
        '
        'TextBox_sampleref3
        '
        resources.ApplyResources(Me.TextBox_sampleref3, "TextBox_sampleref3")
        Me.TextBox_sampleref3.Name = "TextBox_sampleref3"
        '
        'Label34
        '
        resources.ApplyResources(Me.Label34, "Label34")
        Me.Label34.Name = "Label34"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.ComboBox_cavi1000_reservoirsize)
        Me.GroupBox3.Controls.Add(Me.Label_temperature_source)
        Me.GroupBox3.Controls.Add(Me.Label_reservoir_area)
        Me.GroupBox3.Controls.Add(Me.Label_SIunit)
        Me.GroupBox3.Controls.Add(Me.Label_cavispeed_acq)
        Me.GroupBox3.Controls.Add(Me.Label_optical_calibration)
        Me.GroupBox3.Controls.Add(Me.Label_optical_source)
        Me.GroupBox3.Controls.Add(Me.Label_rotor_diameter)
        Me.GroupBox3.Controls.Add(Me.Label_rotor_number)
        Me.GroupBox3.Controls.Add(Me.Label_cavitron_name)
        Me.GroupBox3.Controls.Add(Me.Label94)
        Me.GroupBox3.Controls.Add(Me.Button_EDIT_ADVANCED_PARAMETERS)
        Me.GroupBox3.Controls.Add(Me.Label58)
        Me.GroupBox3.Controls.Add(Me.Label61)
        Me.GroupBox3.Controls.Add(Me.Label55)
        Me.GroupBox3.Controls.Add(Me.Label57)
        Me.GroupBox3.Controls.Add(Me.Label54)
        Me.GroupBox3.Controls.Add(Me.Label2)
        Me.GroupBox3.Controls.Add(Me.Label1)
        Me.GroupBox3.Controls.Add(Me.Label10)
        Me.GroupBox3.Controls.Add(Me.Label11)
        Me.GroupBox3.Controls.Add(Me.Label60)
        Me.GroupBox3.Controls.Add(Me.Label59)
        Me.GroupBox3.Controls.Add(Me.Panel4)
        resources.ApplyResources(Me.GroupBox3, "GroupBox3")
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.TabStop = False
        '
        'ComboBox_cavi1000_reservoirsize
        '
        Me.ComboBox_cavi1000_reservoirsize.FormattingEnabled = True
        Me.ComboBox_cavi1000_reservoirsize.Items.AddRange(New Object() {resources.GetString("ComboBox_cavi1000_reservoirsize.Items"), resources.GetString("ComboBox_cavi1000_reservoirsize.Items1")})
        resources.ApplyResources(Me.ComboBox_cavi1000_reservoirsize, "ComboBox_cavi1000_reservoirsize")
        Me.ComboBox_cavi1000_reservoirsize.Name = "ComboBox_cavi1000_reservoirsize"
        '
        'Label_temperature_source
        '
        resources.ApplyResources(Me.Label_temperature_source, "Label_temperature_source")
        Me.Label_temperature_source.Name = "Label_temperature_source"
        '
        'Label_reservoir_area
        '
        resources.ApplyResources(Me.Label_reservoir_area, "Label_reservoir_area")
        Me.Label_reservoir_area.Name = "Label_reservoir_area"
        '
        'Label_SIunit
        '
        resources.ApplyResources(Me.Label_SIunit, "Label_SIunit")
        Me.Label_SIunit.Name = "Label_SIunit"
        '
        'Label_cavispeed_acq
        '
        resources.ApplyResources(Me.Label_cavispeed_acq, "Label_cavispeed_acq")
        Me.Label_cavispeed_acq.Name = "Label_cavispeed_acq"
        '
        'Label_optical_calibration
        '
        resources.ApplyResources(Me.Label_optical_calibration, "Label_optical_calibration")
        Me.Label_optical_calibration.Name = "Label_optical_calibration"
        '
        'Label_optical_source
        '
        resources.ApplyResources(Me.Label_optical_source, "Label_optical_source")
        Me.Label_optical_source.Name = "Label_optical_source"
        '
        'Label_rotor_diameter
        '
        resources.ApplyResources(Me.Label_rotor_diameter, "Label_rotor_diameter")
        Me.Label_rotor_diameter.Name = "Label_rotor_diameter"
        '
        'Label_rotor_number
        '
        resources.ApplyResources(Me.Label_rotor_number, "Label_rotor_number")
        Me.Label_rotor_number.Name = "Label_rotor_number"
        '
        'Label_cavitron_name
        '
        resources.ApplyResources(Me.Label_cavitron_name, "Label_cavitron_name")
        Me.Label_cavitron_name.Name = "Label_cavitron_name"
        '
        'Label94
        '
        resources.ApplyResources(Me.Label94, "Label94")
        Me.Label94.Name = "Label94"
        '
        'Button_EDIT_ADVANCED_PARAMETERS
        '
        Me.Button_EDIT_ADVANCED_PARAMETERS.BackColor = System.Drawing.SystemColors.Control
        resources.ApplyResources(Me.Button_EDIT_ADVANCED_PARAMETERS, "Button_EDIT_ADVANCED_PARAMETERS")
        Me.Button_EDIT_ADVANCED_PARAMETERS.Name = "Button_EDIT_ADVANCED_PARAMETERS"
        Me.Button_EDIT_ADVANCED_PARAMETERS.UseVisualStyleBackColor = False
        '
        'Label58
        '
        resources.ApplyResources(Me.Label58, "Label58")
        Me.Label58.Name = "Label58"
        '
        'Label61
        '
        resources.ApplyResources(Me.Label61, "Label61")
        Me.Label61.Name = "Label61"
        '
        'Label55
        '
        resources.ApplyResources(Me.Label55, "Label55")
        Me.Label55.Name = "Label55"
        '
        'Label57
        '
        resources.ApplyResources(Me.Label57, "Label57")
        Me.Label57.Name = "Label57"
        '
        'Label54
        '
        resources.ApplyResources(Me.Label54, "Label54")
        Me.Label54.Name = "Label54"
        '
        'Label2
        '
        resources.ApplyResources(Me.Label2, "Label2")
        Me.Label2.Name = "Label2"
        '
        'Label1
        '
        resources.ApplyResources(Me.Label1, "Label1")
        Me.Label1.Name = "Label1"
        '
        'Label10
        '
        resources.ApplyResources(Me.Label10, "Label10")
        Me.Label10.Name = "Label10"
        '
        'Label11
        '
        resources.ApplyResources(Me.Label11, "Label11")
        Me.Label11.Name = "Label11"
        '
        'Label60
        '
        resources.ApplyResources(Me.Label60, "Label60")
        Me.Label60.Name = "Label60"
        '
        'Label59
        '
        resources.ApplyResources(Me.Label59, "Label59")
        Me.Label59.Name = "Label59"
        '
        'Panel4
        '
        Me.Panel4.Controls.Add(Me.TextBox_temperature_correction)
        Me.Panel4.Controls.Add(Me.ComboBox_correction_temp_source)
        Me.Panel4.Controls.Add(Me.Label39)
        resources.ApplyResources(Me.Panel4, "Panel4")
        Me.Panel4.Name = "Panel4"
        '
        'TextBox_temperature_correction
        '
        Me.TextBox_temperature_correction.BackColor = System.Drawing.Color.White
        resources.ApplyResources(Me.TextBox_temperature_correction, "TextBox_temperature_correction")
        Me.TextBox_temperature_correction.Name = "TextBox_temperature_correction"
        '
        'ComboBox_correction_temp_source
        '
        Me.ComboBox_correction_temp_source.AllowDrop = True
        Me.ComboBox_correction_temp_source.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append
        Me.ComboBox_correction_temp_source.FormattingEnabled = True
        Me.ComboBox_correction_temp_source.Items.AddRange(New Object() {resources.GetString("ComboBox_correction_temp_source.Items"), resources.GetString("ComboBox_correction_temp_source.Items1")})
        resources.ApplyResources(Me.ComboBox_correction_temp_source, "ComboBox_correction_temp_source")
        Me.ComboBox_correction_temp_source.Name = "ComboBox_correction_temp_source"
        '
        'Label39
        '
        resources.ApplyResources(Me.Label39, "Label39")
        Me.Label39.Name = "Label39"
        '
        'Button_centrifugation_pressure_simulation
        '
        Me.Button_centrifugation_pressure_simulation.BackColor = System.Drawing.SystemColors.ButtonFace
        resources.ApplyResources(Me.Button_centrifugation_pressure_simulation, "Button_centrifugation_pressure_simulation")
        Me.Button_centrifugation_pressure_simulation.Name = "Button_centrifugation_pressure_simulation"
        Me.Button_centrifugation_pressure_simulation.UseVisualStyleBackColor = False
        '
        'CheckBox_autobrake
        '
        resources.ApplyResources(Me.CheckBox_autobrake, "CheckBox_autobrake")
        Me.CheckBox_autobrake.Checked = True
        Me.CheckBox_autobrake.CheckState = System.Windows.Forms.CheckState.Checked
        Me.CheckBox_autobrake.Name = "CheckBox_autobrake"
        Me.CheckBox_autobrake.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.Button_set_min_max_Yaxisrange)
        Me.GroupBox2.Controls.Add(Me.Button_set_Yaxisrange2)
        Me.GroupBox2.Controls.Add(Me.TextBox_chart_Yaxis_min)
        Me.GroupBox2.Controls.Add(Me.Label44)
        Me.GroupBox2.Controls.Add(Me.TextBox_chart_Yaxis_max)
        Me.GroupBox2.Controls.Add(Me.Label37)
        resources.ApplyResources(Me.GroupBox2, "GroupBox2")
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.TabStop = False
        '
        'Button_set_min_max_Yaxisrange
        '
        Me.Button_set_min_max_Yaxisrange.BackColor = System.Drawing.SystemColors.ButtonFace
        resources.ApplyResources(Me.Button_set_min_max_Yaxisrange, "Button_set_min_max_Yaxisrange")
        Me.Button_set_min_max_Yaxisrange.Name = "Button_set_min_max_Yaxisrange"
        Me.Button_set_min_max_Yaxisrange.UseVisualStyleBackColor = False
        '
        'Button_set_Yaxisrange2
        '
        Me.Button_set_Yaxisrange2.BackColor = System.Drawing.SystemColors.ButtonFace
        resources.ApplyResources(Me.Button_set_Yaxisrange2, "Button_set_Yaxisrange2")
        Me.Button_set_Yaxisrange2.Name = "Button_set_Yaxisrange2"
        Me.Button_set_Yaxisrange2.UseVisualStyleBackColor = False
        '
        'TextBox_chart_Yaxis_min
        '
        resources.ApplyResources(Me.TextBox_chart_Yaxis_min, "TextBox_chart_Yaxis_min")
        Me.TextBox_chart_Yaxis_min.Name = "TextBox_chart_Yaxis_min"
        '
        'Label44
        '
        resources.ApplyResources(Me.Label44, "Label44")
        Me.Label44.Name = "Label44"
        '
        'TextBox_chart_Yaxis_max
        '
        resources.ApplyResources(Me.TextBox_chart_Yaxis_max, "TextBox_chart_Yaxis_max")
        Me.TextBox_chart_Yaxis_max.Name = "TextBox_chart_Yaxis_max"
        '
        'Label37
        '
        resources.ApplyResources(Me.Label37, "Label37")
        Me.Label37.Name = "Label37"
        '
        'Label6
        '
        resources.ApplyResources(Me.Label6, "Label6")
        Me.Label6.Name = "Label6"
        '
        'NumericUpDown_detect_speedclass_step
        '
        Me.NumericUpDown_detect_speedclass_step.Increment = New Decimal(New Integer() {25, 0, 0, 0})
        resources.ApplyResources(Me.NumericUpDown_detect_speedclass_step, "NumericUpDown_detect_speedclass_step")
        Me.NumericUpDown_detect_speedclass_step.Maximum = New Decimal(New Integer() {1000, 0, 0, 0})
        Me.NumericUpDown_detect_speedclass_step.Minimum = New Decimal(New Integer() {75, 0, 0, 0})
        Me.NumericUpDown_detect_speedclass_step.Name = "NumericUpDown_detect_speedclass_step"
        Me.NumericUpDown_detect_speedclass_step.Value = New Decimal(New Integer() {75, 0, 0, 0})
        '
        'TabPage1
        '
        Me.TabPage1.BackColor = System.Drawing.Color.Silver
        Me.TabPage1.Controls.Add(Me.Button_recomputespeedclass)
        Me.TabPage1.Controls.Add(Me.Button_update_datagrid)
        Me.TabPage1.Controls.Add(Me.DataGridView_MAINDATA)
        Me.TabPage1.Controls.Add(Me.RichTextBox3)
        resources.ApplyResources(Me.TabPage1, "TabPage1")
        Me.TabPage1.Name = "TabPage1"
        '
        'Button_recomputespeedclass
        '
        resources.ApplyResources(Me.Button_recomputespeedclass, "Button_recomputespeedclass")
        Me.Button_recomputespeedclass.Name = "Button_recomputespeedclass"
        Me.ToolTip1.SetToolTip(Me.Button_recomputespeedclass, resources.GetString("Button_recomputespeedclass.ToolTip"))
        Me.Button_recomputespeedclass.UseVisualStyleBackColor = True
        '
        'Button_update_datagrid
        '
        resources.ApplyResources(Me.Button_update_datagrid, "Button_update_datagrid")
        Me.Button_update_datagrid.Name = "Button_update_datagrid"
        Me.ToolTip1.SetToolTip(Me.Button_update_datagrid, resources.GetString("Button_update_datagrid.ToolTip"))
        Me.Button_update_datagrid.UseVisualStyleBackColor = True
        '
        'DataGridView_MAINDATA
        '
        DataGridViewCellStyle1.NullValue = Nothing
        Me.DataGridView_MAINDATA.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.DataGridView_MAINDATA.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.ColumnHeader
        Me.DataGridView_MAINDATA.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells
        Me.DataGridView_MAINDATA.BackgroundColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.DataGridView_MAINDATA.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.DataGridView_MAINDATA.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.Disable
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridView_MAINDATA.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle2
        Me.DataGridView_MAINDATA.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView_MAINDATA.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Column1, Me.Column11, Me.Column10, Me.Column18, Me.Column19, Me.meas_cavispeed, Me.max_cavispeed, Me.Column_pressure, Me.raw_conductance_kgMPas, Me.Conductivity_SI_corrT, Me.std_error_LP_integral, Me.Column_PLC_integral, Me.Note, Me.speedclass, Me.Column8, Me.roomtemperature2, Me.correctiontemp, Me.Column_temp_factor, Me.Column16, Me.column9, Me.Column2, Me.Column3, Me.Column4, Me.Column14, Me.column_deltaP, Me.Column_calib_optique, Me.Column_meas_MODE, Me.rotordiameter, Me.Lp_unit, Me.Equation_slope, Me.Equation_intercept, Me.Column6, Me.Column5, Me.Column_reservoir_crosssection, Me.branch_area_big_reservoir, Me.Column_mean_diameter_big_res_mm, Me.Column_mean_diameter_small_res_mm, Me.number_of_stem, Me.AUTOMODE_rawdata_time, Me.AUTOMODE_rawdata_distance_pixel, Me.AUTOMODE_rawdata_distance_mm, Me.branch_diameter_rawdata_upstream, Me.branch_diameter_rawdata_downstream})
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridView_MAINDATA.DefaultCellStyle = DataGridViewCellStyle8
        Me.DataGridView_MAINDATA.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnF2
        Me.DataGridView_MAINDATA.GridColor = System.Drawing.Color.Silver
        resources.ApplyResources(Me.DataGridView_MAINDATA, "DataGridView_MAINDATA")
        Me.DataGridView_MAINDATA.Name = "DataGridView_MAINDATA"
        Me.DataGridView_MAINDATA.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle9.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridView_MAINDATA.RowHeadersDefaultCellStyle = DataGridViewCellStyle9
        Me.DataGridView_MAINDATA.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders
        '
        'RichTextBox3
        '
        Me.RichTextBox3.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        resources.ApplyResources(Me.RichTextBox3, "RichTextBox3")
        Me.RichTextBox3.Name = "RichTextBox3"
        Me.RichTextBox3.ReadOnly = True
        Me.RichTextBox3.Tag = ""
        '
        'TabPage2
        '
        Me.TabPage2.BackColor = System.Drawing.Color.SteelBlue
        Me.TabPage2.Controls.Add(Me.Label_meas_voltage)
        Me.TabPage2.Controls.Add(Me.RichTextBox_measured_voltage)
        Me.TabPage2.Controls.Add(Me.Button_RAWDATA_graph_axis_default)
        Me.TabPage2.Controls.Add(Me.GroupBox7)
        Me.TabPage2.Controls.Add(Me.Button8)
        Me.TabPage2.Controls.Add(Me.GroupBox5)
        Me.TabPage2.Controls.Add(Me.Button_COMPUTE)
        Me.TabPage2.Controls.Add(Me.GroupBox4)
        Me.TabPage2.Controls.Add(Me.CheckBox_validateboolean)
        Me.TabPage2.Controls.Add(Me.Button_validate_meas_AUTOMODE)
        Me.TabPage2.Controls.Add(Me.Button_Startmeas_AUTOMODE)
        resources.ApplyResources(Me.TabPage2, "TabPage2")
        Me.TabPage2.Name = "TabPage2"
        '
        'Label_meas_voltage
        '
        resources.ApplyResources(Me.Label_meas_voltage, "Label_meas_voltage")
        Me.Label_meas_voltage.Name = "Label_meas_voltage"
        '
        'RichTextBox_measured_voltage
        '
        resources.ApplyResources(Me.RichTextBox_measured_voltage, "RichTextBox_measured_voltage")
        Me.RichTextBox_measured_voltage.Name = "RichTextBox_measured_voltage"
        '
        'Button_RAWDATA_graph_axis_default
        '
        resources.ApplyResources(Me.Button_RAWDATA_graph_axis_default, "Button_RAWDATA_graph_axis_default")
        Me.Button_RAWDATA_graph_axis_default.Name = "Button_RAWDATA_graph_axis_default"
        Me.Button_RAWDATA_graph_axis_default.UseVisualStyleBackColor = True
        '
        'GroupBox7
        '
        Me.GroupBox7.Controls.Add(Me.Label90)
        Me.GroupBox7.Controls.Add(Me.Label89)
        Me.GroupBox7.Controls.Add(Me.NumericUpDown_outlier_LOW_threshold)
        Me.GroupBox7.Controls.Add(Me.Label88)
        Me.GroupBox7.Controls.Add(Me.NumericUpDown_outlier_threshold)
        Me.GroupBox7.Controls.Add(Me.CheckBox_outlier_reject)
        resources.ApplyResources(Me.GroupBox7, "GroupBox7")
        Me.GroupBox7.Name = "GroupBox7"
        Me.GroupBox7.TabStop = False
        '
        'Label90
        '
        resources.ApplyResources(Me.Label90, "Label90")
        Me.Label90.Name = "Label90"
        '
        'Label89
        '
        resources.ApplyResources(Me.Label89, "Label89")
        Me.Label89.Name = "Label89"
        '
        'NumericUpDown_outlier_LOW_threshold
        '
        resources.ApplyResources(Me.NumericUpDown_outlier_LOW_threshold, "NumericUpDown_outlier_LOW_threshold")
        Me.NumericUpDown_outlier_LOW_threshold.Maximum = New Decimal(New Integer() {655, 0, 0, 0})
        Me.NumericUpDown_outlier_LOW_threshold.Name = "NumericUpDown_outlier_LOW_threshold"
        '
        'Label88
        '
        resources.ApplyResources(Me.Label88, "Label88")
        Me.Label88.Name = "Label88"
        '
        'NumericUpDown_outlier_threshold
        '
        resources.ApplyResources(Me.NumericUpDown_outlier_threshold, "NumericUpDown_outlier_threshold")
        Me.NumericUpDown_outlier_threshold.Maximum = New Decimal(New Integer() {656, 0, 0, 0})
        Me.NumericUpDown_outlier_threshold.Name = "NumericUpDown_outlier_threshold"
        Me.NumericUpDown_outlier_threshold.Value = New Decimal(New Integer() {656, 0, 0, 0})
        '
        'CheckBox_outlier_reject
        '
        resources.ApplyResources(Me.CheckBox_outlier_reject, "CheckBox_outlier_reject")
        Me.CheckBox_outlier_reject.Checked = True
        Me.CheckBox_outlier_reject.CheckState = System.Windows.Forms.CheckState.Checked
        Me.CheckBox_outlier_reject.Name = "CheckBox_outlier_reject"
        Me.CheckBox_outlier_reject.UseVisualStyleBackColor = True
        '
        'Button8
        '
        resources.ApplyResources(Me.Button8, "Button8")
        Me.Button8.Name = "Button8"
        Me.Button8.UseVisualStyleBackColor = True
        '
        'GroupBox5
        '
        Me.GroupBox5.BackColor = System.Drawing.Color.Silver
        Me.GroupBox5.Controls.Add(Me.Label87)
        Me.GroupBox5.Controls.Add(Me.RichTextBox_PLC_automode)
        Me.GroupBox5.Controls.Add(Me.Label81)
        Me.GroupBox5.Controls.Add(Me.Label86)
        Me.GroupBox5.Controls.Add(Me.Label75)
        Me.GroupBox5.Controls.Add(Me.RichTextBox_Lp0_automode)
        Me.GroupBox5.Controls.Add(Me.PictureBox1)
        Me.GroupBox5.Controls.Add(Me.ScatterGraph1)
        Me.GroupBox5.Controls.Add(Me.RichTextBox_pos)
        Me.GroupBox5.Controls.Add(Me.Label82)
        Me.GroupBox5.Controls.Add(Me.Label80)
        Me.GroupBox5.Controls.Add(Me.RichTextBox_AUTOMODE_meanLp_integral)
        Me.GroupBox5.Controls.Add(Me.RichTextBox_AUTOMODE_stderror_Lp_integral)
        Me.GroupBox5.Controls.Add(Me.Label68)
        Me.GroupBox5.Controls.Add(Me.RichTextBox4)
        resources.ApplyResources(Me.GroupBox5, "GroupBox5")
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.TabStop = False
        '
        'Label87
        '
        resources.ApplyResources(Me.Label87, "Label87")
        Me.Label87.Name = "Label87"
        '
        'RichTextBox_PLC_automode
        '
        Me.RichTextBox_PLC_automode.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        resources.ApplyResources(Me.RichTextBox_PLC_automode, "RichTextBox_PLC_automode")
        Me.RichTextBox_PLC_automode.ForeColor = System.Drawing.SystemColors.WindowText
        Me.RichTextBox_PLC_automode.Name = "RichTextBox_PLC_automode"
        Me.RichTextBox_PLC_automode.ReadOnly = True
        '
        'Label81
        '
        resources.ApplyResources(Me.Label81, "Label81")
        Me.Label81.Name = "Label81"
        '
        'Label86
        '
        resources.ApplyResources(Me.Label86, "Label86")
        Me.Label86.Name = "Label86"
        '
        'Label75
        '
        resources.ApplyResources(Me.Label75, "Label75")
        Me.Label75.Name = "Label75"
        '
        'RichTextBox_Lp0_automode
        '
        resources.ApplyResources(Me.RichTextBox_Lp0_automode, "RichTextBox_Lp0_automode")
        Me.RichTextBox_Lp0_automode.Name = "RichTextBox_Lp0_automode"
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = Global.cavi_soft.My.Resources.Resources.equation
        resources.ApplyResources(Me.PictureBox1, "PictureBox1")
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.TabStop = False
        '
        'ScatterGraph1
        '
        Me.ScatterGraph1.BackColor = System.Drawing.Color.Transparent
        resources.ApplyResources(Me.ScatterGraph1, "ScatterGraph1")
        Me.ScatterGraph1.Name = "ScatterGraph1"
        Me.ScatterGraph1.PlotAreaColor = System.Drawing.Color.White
        Me.ScatterGraph1.Plots.AddRange(New NationalInstruments.UI.ScatterPlot() {Me.dataplot1, Me.fittedplot1, Me.dataplot2, Me.fittedplot2, Me.dataplot3, Me.fittedplot3, Me.dataplot4, Me.fittedplot4, Me.dataplot5, Me.fittedplot5, Me.dataplot6, Me.fittedplot6, Me.dataplot7, Me.fittedplot7, Me.dataplot8, Me.fittedplot8})
        Me.ScatterGraph1.UseColorGenerator = True
        Me.ScatterGraph1.XAxes.AddRange(New NationalInstruments.UI.XAxis() {Me.XAxis1})
        Me.ScatterGraph1.YAxes.AddRange(New NationalInstruments.UI.YAxis() {Me.YAxis1})
        '
        'dataplot1
        '
        Me.dataplot1.LineStyle = NationalInstruments.UI.LineStyle.None
        Me.dataplot1.PointColor = System.Drawing.Color.SteelBlue
        Me.dataplot1.PointStyle = NationalInstruments.UI.PointStyle.SolidDiamond
        Me.dataplot1.XAxis = Me.XAxis1
        Me.dataplot1.YAxis = Me.YAxis1
        '
        'XAxis1
        '
        Me.XAxis1.BaseLineVisible = True
        Me.XAxis1.MajorDivisions.GridColor = System.Drawing.Color.DarkGray
        Me.XAxis1.MajorDivisions.GridVisible = True
        '
        'YAxis1
        '
        Me.YAxis1.MajorDivisions.GridColor = System.Drawing.Color.DarkGray
        Me.YAxis1.MajorDivisions.GridVisible = True
        '
        'fittedplot1
        '
        Me.fittedplot1.LineColor = System.Drawing.Color.SteelBlue
        Me.fittedplot1.LineColorPrecedence = NationalInstruments.UI.ColorPrecedence.UserDefinedColor
        Me.fittedplot1.XAxis = Me.XAxis1
        Me.fittedplot1.YAxis = Me.YAxis1
        '
        'dataplot2
        '
        Me.dataplot2.LineStyle = NationalInstruments.UI.LineStyle.None
        Me.dataplot2.PointColor = System.Drawing.Color.Orange
        Me.dataplot2.PointStyle = NationalInstruments.UI.PointStyle.SolidDiamond
        Me.dataplot2.XAxis = Me.XAxis1
        Me.dataplot2.YAxis = Me.YAxis1
        '
        'fittedplot2
        '
        Me.fittedplot2.LineColor = System.Drawing.Color.Orange
        Me.fittedplot2.LineColorPrecedence = NationalInstruments.UI.ColorPrecedence.UserDefinedColor
        Me.fittedplot2.XAxis = Me.XAxis1
        Me.fittedplot2.YAxis = Me.YAxis1
        '
        'dataplot3
        '
        Me.dataplot3.LineStyle = NationalInstruments.UI.LineStyle.None
        Me.dataplot3.PointColor = System.Drawing.Color.SlateGray
        Me.dataplot3.PointStyle = NationalInstruments.UI.PointStyle.SolidTriangleDown
        Me.dataplot3.XAxis = Me.XAxis1
        Me.dataplot3.YAxis = Me.YAxis1
        '
        'fittedplot3
        '
        Me.fittedplot3.LineColor = System.Drawing.Color.SlateGray
        Me.fittedplot3.LineColorPrecedence = NationalInstruments.UI.ColorPrecedence.UserDefinedColor
        Me.fittedplot3.XAxis = Me.XAxis1
        Me.fittedplot3.YAxis = Me.YAxis1
        '
        'dataplot4
        '
        Me.dataplot4.LineStyle = NationalInstruments.UI.LineStyle.None
        Me.dataplot4.PointColor = System.Drawing.Color.OliveDrab
        Me.dataplot4.PointStyle = NationalInstruments.UI.PointStyle.SolidDiamond
        Me.dataplot4.XAxis = Me.XAxis1
        Me.dataplot4.YAxis = Me.YAxis1
        '
        'fittedplot4
        '
        Me.fittedplot4.LineColor = System.Drawing.Color.OliveDrab
        Me.fittedplot4.LineColorPrecedence = NationalInstruments.UI.ColorPrecedence.UserDefinedColor
        Me.fittedplot4.PointColor = System.Drawing.Color.OliveDrab
        Me.fittedplot4.XAxis = Me.XAxis1
        Me.fittedplot4.YAxis = Me.YAxis1
        '
        'dataplot5
        '
        Me.dataplot5.LineStyle = NationalInstruments.UI.LineStyle.None
        Me.dataplot5.PointColor = System.Drawing.Color.Firebrick
        Me.dataplot5.PointStyle = NationalInstruments.UI.PointStyle.SolidTriangleDown
        Me.dataplot5.XAxis = Me.XAxis1
        Me.dataplot5.YAxis = Me.YAxis1
        '
        'fittedplot5
        '
        Me.fittedplot5.LineColor = System.Drawing.Color.Firebrick
        Me.fittedplot5.LineColorPrecedence = NationalInstruments.UI.ColorPrecedence.UserDefinedColor
        Me.fittedplot5.XAxis = Me.XAxis1
        Me.fittedplot5.YAxis = Me.YAxis1
        '
        'dataplot6
        '
        Me.dataplot6.LineStyle = NationalInstruments.UI.LineStyle.None
        Me.dataplot6.PointColor = System.Drawing.Color.DarkSeaGreen
        Me.dataplot6.PointStyle = NationalInstruments.UI.PointStyle.EmptySquare
        Me.dataplot6.XAxis = Me.XAxis1
        Me.dataplot6.YAxis = Me.YAxis1
        '
        'fittedplot6
        '
        Me.fittedplot6.LineColor = System.Drawing.Color.DarkSeaGreen
        Me.fittedplot6.LineColorPrecedence = NationalInstruments.UI.ColorPrecedence.UserDefinedColor
        Me.fittedplot6.XAxis = Me.XAxis1
        Me.fittedplot6.YAxis = Me.YAxis1
        '
        'dataplot7
        '
        Me.dataplot7.LineStyle = NationalInstruments.UI.LineStyle.None
        Me.dataplot7.PointColor = System.Drawing.Color.Sienna
        Me.dataplot7.PointStyle = NationalInstruments.UI.PointStyle.SolidCircle
        Me.dataplot7.XAxis = Me.XAxis1
        Me.dataplot7.YAxis = Me.YAxis1
        '
        'fittedplot7
        '
        Me.fittedplot7.LineColor = System.Drawing.Color.Sienna
        Me.fittedplot7.LineColorPrecedence = NationalInstruments.UI.ColorPrecedence.UserDefinedColor
        Me.fittedplot7.XAxis = Me.XAxis1
        Me.fittedplot7.YAxis = Me.YAxis1
        '
        'dataplot8
        '
        Me.dataplot8.LineStyle = NationalInstruments.UI.LineStyle.None
        Me.dataplot8.PointColor = System.Drawing.Color.Black
        Me.dataplot8.PointStyle = NationalInstruments.UI.PointStyle.EmptySquare
        Me.dataplot8.XAxis = Me.XAxis1
        Me.dataplot8.YAxis = Me.YAxis1
        '
        'fittedplot8
        '
        Me.fittedplot8.LineColor = System.Drawing.Color.Black
        Me.fittedplot8.LineColorPrecedence = NationalInstruments.UI.ColorPrecedence.UserDefinedColor
        Me.fittedplot8.XAxis = Me.XAxis1
        Me.fittedplot8.YAxis = Me.YAxis1
        '
        'RichTextBox_pos
        '
        Me.RichTextBox_pos.BackColor = System.Drawing.SystemColors.Info
        resources.ApplyResources(Me.RichTextBox_pos, "RichTextBox_pos")
        Me.RichTextBox_pos.Name = "RichTextBox_pos"
        '
        'Label82
        '
        resources.ApplyResources(Me.Label82, "Label82")
        Me.Label82.Name = "Label82"
        '
        'Label80
        '
        resources.ApplyResources(Me.Label80, "Label80")
        Me.Label80.Name = "Label80"
        '
        'RichTextBox_AUTOMODE_meanLp_integral
        '
        Me.RichTextBox_AUTOMODE_meanLp_integral.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        resources.ApplyResources(Me.RichTextBox_AUTOMODE_meanLp_integral, "RichTextBox_AUTOMODE_meanLp_integral")
        Me.RichTextBox_AUTOMODE_meanLp_integral.ForeColor = System.Drawing.SystemColors.WindowText
        Me.RichTextBox_AUTOMODE_meanLp_integral.Name = "RichTextBox_AUTOMODE_meanLp_integral"
        Me.RichTextBox_AUTOMODE_meanLp_integral.ReadOnly = True
        Me.RichTextBox_AUTOMODE_meanLp_integral.TabStop = False
        '
        'RichTextBox_AUTOMODE_stderror_Lp_integral
        '
        Me.RichTextBox_AUTOMODE_stderror_Lp_integral.AcceptsTab = True
        Me.RichTextBox_AUTOMODE_stderror_Lp_integral.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        resources.ApplyResources(Me.RichTextBox_AUTOMODE_stderror_Lp_integral, "RichTextBox_AUTOMODE_stderror_Lp_integral")
        Me.RichTextBox_AUTOMODE_stderror_Lp_integral.ForeColor = System.Drawing.SystemColors.WindowText
        Me.RichTextBox_AUTOMODE_stderror_Lp_integral.Name = "RichTextBox_AUTOMODE_stderror_Lp_integral"
        Me.RichTextBox_AUTOMODE_stderror_Lp_integral.ReadOnly = True
        Me.RichTextBox_AUTOMODE_stderror_Lp_integral.TabStop = False
        '
        'Label68
        '
        resources.ApplyResources(Me.Label68, "Label68")
        Me.Label68.Name = "Label68"
        '
        'RichTextBox4
        '
        Me.RichTextBox4.BackColor = System.Drawing.Color.Silver
        resources.ApplyResources(Me.RichTextBox4, "RichTextBox4")
        Me.RichTextBox4.Name = "RichTextBox4"
        '
        'Button_COMPUTE
        '
        resources.ApplyResources(Me.Button_COMPUTE, "Button_COMPUTE")
        Me.Button_COMPUTE.Name = "Button_COMPUTE"
        Me.Button_COMPUTE.UseVisualStyleBackColor = True
        '
        'GroupBox4
        '
        Me.GroupBox4.BackColor = System.Drawing.Color.Silver
        Me.GroupBox4.Controls.Add(Me.DataGridView_RAWDATA)
        Me.GroupBox4.Controls.Add(Me.Chart3)
        resources.ApplyResources(Me.GroupBox4, "GroupBox4")
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.TabStop = False
        '
        'DataGridView_RAWDATA
        '
        Me.DataGridView_RAWDATA.AllowUserToAddRows = False
        Me.DataGridView_RAWDATA.AllowUserToResizeRows = False
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle10.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridView_RAWDATA.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle10
        resources.ApplyResources(Me.DataGridView_RAWDATA, "DataGridView_RAWDATA")
        Me.DataGridView_RAWDATA.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Column_time, Me.Column_distance, Me.distance_mm})
        DataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle11.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridView_RAWDATA.DefaultCellStyle = DataGridViewCellStyle11
        Me.DataGridView_RAWDATA.Name = "DataGridView_RAWDATA"
        DataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle12.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridView_RAWDATA.RowHeadersDefaultCellStyle = DataGridViewCellStyle12
        '
        'Column_time
        '
        Me.Column_time.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        resources.ApplyResources(Me.Column_time, "Column_time")
        Me.Column_time.Name = "Column_time"
        '
        'Column_distance
        '
        Me.Column_distance.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        resources.ApplyResources(Me.Column_distance, "Column_distance")
        Me.Column_distance.Name = "Column_distance"
        '
        'distance_mm
        '
        resources.ApplyResources(Me.distance_mm, "distance_mm")
        Me.distance_mm.Name = "distance_mm"
        '
        'Chart3
        '
        Me.Chart3.BackColor = System.Drawing.Color.Transparent
        ChartArea1.AxisX.MajorGrid.LineColor = System.Drawing.Color.DarkGray
        ChartArea1.AxisY.MajorGrid.LineColor = System.Drawing.Color.DarkGray
        ChartArea1.AxisY.Maximum = 650.0R
        ChartArea1.AxisY.Minimum = 0.0R
        ChartArea1.Name = "ChartArea1"
        Me.Chart3.ChartAreas.Add(ChartArea1)
        resources.ApplyResources(Me.Chart3, "Chart3")
        Me.Chart3.Name = "Chart3"
        Me.Chart3.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.None
        Series1.ChartArea = "ChartArea1"
        Series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Point
        Series1.MarkerColor = System.Drawing.Color.SteelBlue
        Series1.Name = "Series1"
        Series1.Points.Add(DataPoint1)
        Series1.YValuesPerPoint = 2
        Series2.ChartArea = "ChartArea1"
        Series2.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Point
        Series2.MarkerColor = System.Drawing.Color.Orange
        Series2.Name = "Series2"
        Series3.ChartArea = "ChartArea1"
        Series3.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Point
        Series3.MarkerColor = System.Drawing.Color.SlateGray
        Series3.Name = "Series3"
        Series4.ChartArea = "ChartArea1"
        Series4.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Point
        Series4.MarkerColor = System.Drawing.Color.OliveDrab
        Series4.Name = "Series4"
        Series5.ChartArea = "ChartArea1"
        Series5.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Point
        Series5.MarkerColor = System.Drawing.Color.Firebrick
        Series5.Name = "Series5"
        Series6.ChartArea = "ChartArea1"
        Series6.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Point
        Series6.MarkerColor = System.Drawing.Color.DarkSeaGreen
        Series6.Name = "Series6"
        Series7.ChartArea = "ChartArea1"
        Series7.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.FastPoint
        Series7.MarkerColor = System.Drawing.Color.Sienna
        Series7.Name = "Series7"
        Series8.ChartArea = "ChartArea1"
        Series8.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Point
        Series8.MarkerColor = System.Drawing.Color.Black
        Series8.Name = "Series8"
        Me.Chart3.Series.Add(Series1)
        Me.Chart3.Series.Add(Series2)
        Me.Chart3.Series.Add(Series3)
        Me.Chart3.Series.Add(Series4)
        Me.Chart3.Series.Add(Series5)
        Me.Chart3.Series.Add(Series6)
        Me.Chart3.Series.Add(Series7)
        Me.Chart3.Series.Add(Series8)
        Title1.Name = "distance vs time"
        Me.Chart3.Titles.Add(Title1)
        '
        'CheckBox_validateboolean
        '
        resources.ApplyResources(Me.CheckBox_validateboolean, "CheckBox_validateboolean")
        Me.CheckBox_validateboolean.Name = "CheckBox_validateboolean"
        Me.CheckBox_validateboolean.UseVisualStyleBackColor = True
        '
        'Button_validate_meas_AUTOMODE
        '
        Me.Button_validate_meas_AUTOMODE.BackColor = System.Drawing.SystemColors.ButtonFace
        resources.ApplyResources(Me.Button_validate_meas_AUTOMODE, "Button_validate_meas_AUTOMODE")
        Me.Button_validate_meas_AUTOMODE.Name = "Button_validate_meas_AUTOMODE"
        Me.Button_validate_meas_AUTOMODE.UseVisualStyleBackColor = False
        '
        'Button_Startmeas_AUTOMODE
        '
        resources.ApplyResources(Me.Button_Startmeas_AUTOMODE, "Button_Startmeas_AUTOMODE")
        Me.Button_Startmeas_AUTOMODE.Name = "Button_Startmeas_AUTOMODE"
        Me.Button_Startmeas_AUTOMODE.UseVisualStyleBackColor = True
        '
        'Button_intensity_profile
        '
        Me.Button_intensity_profile.BackColor = System.Drawing.SystemColors.ButtonFace
        resources.ApplyResources(Me.Button_intensity_profile, "Button_intensity_profile")
        Me.Button_intensity_profile.Name = "Button_intensity_profile"
        Me.Button_intensity_profile.UseVisualStyleBackColor = False
        '
        'Timer_1sec
        '
        Me.Timer_1sec.Enabled = True
        Me.Timer_1sec.Interval = 1000
        '
        'RichTextBox_centri_tension
        '
        Me.RichTextBox_centri_tension.BackColor = System.Drawing.Color.PaleGoldenrod
        Me.RichTextBox_centri_tension.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        resources.ApplyResources(Me.RichTextBox_centri_tension, "RichTextBox_centri_tension")
        Me.RichTextBox_centri_tension.ForeColor = System.Drawing.SystemColors.WindowText
        Me.RichTextBox_centri_tension.Name = "RichTextBox_centri_tension"
        Me.RichTextBox_centri_tension.ReadOnly = True
        Me.RichTextBox_centri_tension.TabStop = False
        '
        'Label19
        '
        resources.ApplyResources(Me.Label19, "Label19")
        Me.Label19.Name = "Label19"
        '
        'toolStripSeparator5
        '
        Me.toolStripSeparator5.Name = "toolStripSeparator5"
        resources.ApplyResources(Me.toolStripSeparator5, "toolStripSeparator5")
        '
        'AideToolStripMenuItem
        '
        Me.AideToolStripMenuItem.Name = "AideToolStripMenuItem"
        resources.ApplyResources(Me.AideToolStripMenuItem, "AideToolStripMenuItem")
        '
        'TextBox_time
        '
        Me.TextBox_time.BackColor = System.Drawing.Color.PaleGoldenrod
        resources.ApplyResources(Me.TextBox_time, "TextBox_time")
        Me.TextBox_time.Name = "TextBox_time"
        Me.TextBox_time.ReadOnly = True
        '
        'Label24
        '
        resources.ApplyResources(Me.Label24, "Label24")
        Me.Label24.Name = "Label24"
        '
        'TextBox_max_cavispeed
        '
        Me.TextBox_max_cavispeed.BackColor = System.Drawing.Color.PaleGoldenrod
        resources.ApplyResources(Me.TextBox_max_cavispeed, "TextBox_max_cavispeed")
        Me.TextBox_max_cavispeed.Name = "TextBox_max_cavispeed"
        Me.TextBox_max_cavispeed.TabStop = False
        '
        'Panel2
        '
        Me.Panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel2.Controls.Add(Me.Bt_setcavispeed)
        Me.Panel2.Controls.Add(Me.Label_cavispeedcontrolsetvalue)
        Me.Panel2.Controls.Add(Me.TextBox_cavispeed_setvalue)
        Me.Panel2.Controls.Add(Me.LinkLabel_minmeniscusposition)
        Me.Panel2.Controls.Add(Me.Label27)
        Me.Panel2.Controls.Add(Me.Label24)
        Me.Panel2.Controls.Add(Me.TextBox_max_cavispeed)
        Me.Panel2.Controls.Add(Me.Label7)
        Me.Panel2.Controls.Add(Me.RichTextBox_centri_rpm)
        Me.Panel2.Controls.Add(Me.CheckBox_tachymeter_connected)
        Me.Panel2.Controls.Add(Me.Label19)
        Me.Panel2.Controls.Add(Me.RichTextBox_centri_tension)
        Me.Panel2.Controls.Add(Me.TextBox_minmeniscusposition)
        resources.ApplyResources(Me.Panel2, "Panel2")
        Me.Panel2.Name = "Panel2"
        '
        'Bt_setcavispeed
        '
        Me.Bt_setcavispeed.BackColor = System.Drawing.SystemColors.ButtonFace
        resources.ApplyResources(Me.Bt_setcavispeed, "Bt_setcavispeed")
        Me.Bt_setcavispeed.Name = "Bt_setcavispeed"
        Me.Bt_setcavispeed.UseVisualStyleBackColor = False
        '
        'Label_cavispeedcontrolsetvalue
        '
        resources.ApplyResources(Me.Label_cavispeedcontrolsetvalue, "Label_cavispeedcontrolsetvalue")
        Me.Label_cavispeedcontrolsetvalue.Name = "Label_cavispeedcontrolsetvalue"
        '
        'TextBox_cavispeed_setvalue
        '
        Me.TextBox_cavispeed_setvalue.BackColor = System.Drawing.SystemColors.InactiveCaption
        resources.ApplyResources(Me.TextBox_cavispeed_setvalue, "TextBox_cavispeed_setvalue")
        Me.TextBox_cavispeed_setvalue.Name = "TextBox_cavispeed_setvalue"
        Me.TextBox_cavispeed_setvalue.ReadOnly = True
        '
        'LinkLabel_minmeniscusposition
        '
        resources.ApplyResources(Me.LinkLabel_minmeniscusposition, "LinkLabel_minmeniscusposition")
        Me.LinkLabel_minmeniscusposition.Name = "LinkLabel_minmeniscusposition"
        Me.LinkLabel_minmeniscusposition.TabStop = True
        '
        'Label27
        '
        resources.ApplyResources(Me.Label27, "Label27")
        Me.Label27.Name = "Label27"
        '
        'Label7
        '
        resources.ApplyResources(Me.Label7, "Label7")
        Me.Label7.Name = "Label7"
        '
        'RichTextBox_centri_rpm
        '
        Me.RichTextBox_centri_rpm.BackColor = System.Drawing.Color.Orange
        Me.RichTextBox_centri_rpm.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        resources.ApplyResources(Me.RichTextBox_centri_rpm, "RichTextBox_centri_rpm")
        Me.RichTextBox_centri_rpm.ForeColor = System.Drawing.SystemColors.WindowText
        Me.RichTextBox_centri_rpm.Name = "RichTextBox_centri_rpm"
        Me.RichTextBox_centri_rpm.ReadOnly = True
        Me.RichTextBox_centri_rpm.TabStop = False
        '
        'CheckBox_tachymeter_connected
        '
        resources.ApplyResources(Me.CheckBox_tachymeter_connected, "CheckBox_tachymeter_connected")
        Me.CheckBox_tachymeter_connected.Name = "CheckBox_tachymeter_connected"
        Me.CheckBox_tachymeter_connected.UseVisualStyleBackColor = True
        '
        'TextBox_minmeniscusposition
        '
        Me.TextBox_minmeniscusposition.BackColor = System.Drawing.Color.PaleGoldenrod
        resources.ApplyResources(Me.TextBox_minmeniscusposition, "TextBox_minmeniscusposition")
        Me.TextBox_minmeniscusposition.Name = "TextBox_minmeniscusposition"
        Me.TextBox_minmeniscusposition.ReadOnly = True
        Me.TextBox_minmeniscusposition.TabStop = False
        Me.ToolTip1.SetToolTip(Me.TextBox_minmeniscusposition, resources.GetString("TextBox_minmeniscusposition.ToolTip"))
        '
        'RichTextBox_MEASMODE_STATUS
        '
        Me.RichTextBox_MEASMODE_STATUS.BackColor = System.Drawing.Color.White
        resources.ApplyResources(Me.RichTextBox_MEASMODE_STATUS, "RichTextBox_MEASMODE_STATUS")
        Me.RichTextBox_MEASMODE_STATUS.ForeColor = System.Drawing.SystemColors.WindowText
        Me.RichTextBox_MEASMODE_STATUS.Name = "RichTextBox_MEASMODE_STATUS"
        Me.RichTextBox_MEASMODE_STATUS.ReadOnly = True
        Me.RichTextBox_MEASMODE_STATUS.TabStop = False
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        resources.ApplyResources(Me.ToolStripSeparator1, "ToolStripSeparator1")
        '
        'DefineSavedImageFolderToolStripMenuItem
        '
        Me.DefineSavedImageFolderToolStripMenuItem.Name = "DefineSavedImageFolderToolStripMenuItem"
        resources.ApplyResources(Me.DefineSavedImageFolderToolStripMenuItem, "DefineSavedImageFolderToolStripMenuItem")
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        resources.ApplyResources(Me.ToolStripSeparator2, "ToolStripSeparator2")
        '
        'ToolStripSeparator3
        '
        Me.ToolStripSeparator3.Name = "ToolStripSeparator3"
        resources.ApplyResources(Me.ToolStripSeparator3, "ToolStripSeparator3")
        '
        'InputSampleDiameterToolStripMenuItem
        '
        Me.InputSampleDiameterToolStripMenuItem.Name = "InputSampleDiameterToolStripMenuItem"
        resources.ApplyResources(Me.InputSampleDiameterToolStripMenuItem, "InputSampleDiameterToolStripMenuItem")
        '
        'RECOMPUTEPLCToolStripMenuItem
        '
        Me.RECOMPUTEPLCToolStripMenuItem.Name = "RECOMPUTEPLCToolStripMenuItem"
        resources.ApplyResources(Me.RECOMPUTEPLCToolStripMenuItem, "RECOMPUTEPLCToolStripMenuItem")
        '
        'ToolStripSeparator4
        '
        Me.ToolStripSeparator4.Name = "ToolStripSeparator4"
        resources.ApplyResources(Me.ToolStripSeparator4, "ToolStripSeparator4")
        '
        'OpenFileDialog_parameter
        '
        Me.OpenFileDialog_parameter.FileName = "c:\CAVISOFT_PARAMETER.par"
        resources.ApplyResources(Me.OpenFileDialog_parameter, "OpenFileDialog_parameter")
        Me.OpenFileDialog_parameter.InitialDirectory = "c:\"
        '
        'SaveFileDialog_parameter
        '
        Me.SaveFileDialog_parameter.FileName = "c:\CAVISOFT_PARAMETER.par"
        resources.ApplyResources(Me.SaveFileDialog_parameter, "SaveFileDialog_parameter")
        Me.SaveFileDialog_parameter.InitialDirectory = "c:\"
        '
        'OpenFileDialog_APPENDED
        '
        Me.OpenFileDialog_APPENDED.CheckFileExists = False
        Me.OpenFileDialog_APPENDED.DefaultExt = "csv"
        Me.OpenFileDialog_APPENDED.FileName = "OpenFileDialog_APPENDED"
        resources.ApplyResources(Me.OpenFileDialog_APPENDED, "OpenFileDialog_APPENDED")
        '
        'Panel5
        '
        Me.Panel5.Controls.Add(Me.Label_graphtype)
        Me.Panel5.Controls.Add(Me.Button6)
        Me.Panel5.Controls.Add(Me.GroupBox_pushwheel)
        Me.Panel5.Controls.Add(Me.Button_default_axisrange)
        Me.Panel5.Controls.Add(Me.Button_setYaxisrange1)
        Me.Panel5.Controls.Add(Me.Chart_PLC)
        resources.ApplyResources(Me.Panel5, "Panel5")
        Me.Panel5.Name = "Panel5"
        '
        'Label_graphtype
        '
        resources.ApplyResources(Me.Label_graphtype, "Label_graphtype")
        Me.Label_graphtype.Name = "Label_graphtype"
        '
        'Button6
        '
        Me.Button6.BackColor = System.Drawing.SystemColors.ActiveBorder
        resources.ApplyResources(Me.Button6, "Button6")
        Me.Button6.Name = "Button6"
        Me.ToolTip1.SetToolTip(Me.Button6, resources.GetString("Button6.ToolTip"))
        Me.Button6.UseVisualStyleBackColor = False
        '
        'GroupBox_pushwheel
        '
        Me.GroupBox_pushwheel.Controls.Add(Me.Label66)
        Me.GroupBox_pushwheel.Controls.Add(Me.Label65)
        Me.GroupBox_pushwheel.Controls.Add(Me.Label64)
        Me.GroupBox_pushwheel.Controls.Add(Me.TextBox_set_pressure_fixed)
        Me.GroupBox_pushwheel.Controls.Add(Me.TextBox_set_cavispeed_fixed)
        Me.GroupBox_pushwheel.Controls.Add(Me.Label5)
        Me.GroupBox_pushwheel.Controls.Add(Me.Label40)
        Me.GroupBox_pushwheel.Controls.Add(Me.TextBox_bin_pushwheel)
        Me.GroupBox_pushwheel.Controls.Add(Me.Label51)
        Me.GroupBox_pushwheel.Controls.Add(Me.LedArray_pushwheel_port1)
        Me.GroupBox_pushwheel.Controls.Add(Me.LedArray_pushwheel_port0)
        Me.GroupBox_pushwheel.Controls.Add(Me.ShapeContainer1)
        resources.ApplyResources(Me.GroupBox_pushwheel, "GroupBox_pushwheel")
        Me.GroupBox_pushwheel.Name = "GroupBox_pushwheel"
        Me.GroupBox_pushwheel.TabStop = False
        '
        'Label66
        '
        resources.ApplyResources(Me.Label66, "Label66")
        Me.Label66.Name = "Label66"
        '
        'Label65
        '
        resources.ApplyResources(Me.Label65, "Label65")
        Me.Label65.Name = "Label65"
        '
        'Label64
        '
        resources.ApplyResources(Me.Label64, "Label64")
        Me.Label64.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label64.Name = "Label64"
        '
        'TextBox_set_pressure_fixed
        '
        resources.ApplyResources(Me.TextBox_set_pressure_fixed, "TextBox_set_pressure_fixed")
        Me.TextBox_set_pressure_fixed.Name = "TextBox_set_pressure_fixed"
        '
        'TextBox_set_cavispeed_fixed
        '
        resources.ApplyResources(Me.TextBox_set_cavispeed_fixed, "TextBox_set_cavispeed_fixed")
        Me.TextBox_set_cavispeed_fixed.Name = "TextBox_set_cavispeed_fixed"
        '
        'Label5
        '
        resources.ApplyResources(Me.Label5, "Label5")
        Me.Label5.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label5.Name = "Label5"
        '
        'Label40
        '
        resources.ApplyResources(Me.Label40, "Label40")
        Me.Label40.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label40.Name = "Label40"
        '
        'TextBox_bin_pushwheel
        '
        resources.ApplyResources(Me.TextBox_bin_pushwheel, "TextBox_bin_pushwheel")
        Me.TextBox_bin_pushwheel.Name = "TextBox_bin_pushwheel"
        '
        'Label51
        '
        resources.ApplyResources(Me.Label51, "Label51")
        Me.Label51.Name = "Label51"
        '
        'LedArray_pushwheel_port1
        '
        '
        '
        '
        Me.LedArray_pushwheel_port1.ItemTemplate.LedStyle = NationalInstruments.UI.LedStyle.Round3D
        Me.LedArray_pushwheel_port1.ItemTemplate.Location = CType(resources.GetObject("LedArray_pushwheel_port1.ItemTemplate.Location"), System.Drawing.Point)
        Me.LedArray_pushwheel_port1.ItemTemplate.Name = ""
        Me.LedArray_pushwheel_port1.ItemTemplate.Size = CType(resources.GetObject("LedArray_pushwheel_port1.ItemTemplate.Size"), System.Drawing.Size)
        Me.LedArray_pushwheel_port1.ItemTemplate.TabIndex = CType(resources.GetObject("LedArray_pushwheel_port1.ItemTemplate.TabIndex"), Integer)
        Me.LedArray_pushwheel_port1.ItemTemplate.TabStop = False
        Me.LedArray_pushwheel_port1.LayoutMode = NationalInstruments.UI.ControlArrayLayoutMode.Horizontal
        resources.ApplyResources(Me.LedArray_pushwheel_port1, "LedArray_pushwheel_port1")
        Me.LedArray_pushwheel_port1.Name = "LedArray_pushwheel_port1"
        Me.LedArray_pushwheel_port1.ScaleMode = NationalInstruments.UI.ControlArrayScaleMode.CreateFixedMode(8)
        '
        'LedArray_pushwheel_port0
        '
        '
        '
        '
        Me.LedArray_pushwheel_port0.ItemTemplate.LedStyle = NationalInstruments.UI.LedStyle.Round3D
        Me.LedArray_pushwheel_port0.ItemTemplate.Location = CType(resources.GetObject("LedArray_pushwheel_port0.ItemTemplate.Location"), System.Drawing.Point)
        Me.LedArray_pushwheel_port0.ItemTemplate.Name = ""
        Me.LedArray_pushwheel_port0.ItemTemplate.Size = CType(resources.GetObject("LedArray_pushwheel_port0.ItemTemplate.Size"), System.Drawing.Size)
        Me.LedArray_pushwheel_port0.ItemTemplate.TabIndex = CType(resources.GetObject("LedArray_pushwheel_port0.ItemTemplate.TabIndex"), Integer)
        Me.LedArray_pushwheel_port0.ItemTemplate.TabStop = False
        Me.LedArray_pushwheel_port0.LayoutMode = NationalInstruments.UI.ControlArrayLayoutMode.Horizontal
        resources.ApplyResources(Me.LedArray_pushwheel_port0, "LedArray_pushwheel_port0")
        Me.LedArray_pushwheel_port0.Name = "LedArray_pushwheel_port0"
        Me.LedArray_pushwheel_port0.ScaleMode = NationalInstruments.UI.ControlArrayScaleMode.CreateFixedMode(8)
        '
        'ShapeContainer1
        '
        resources.ApplyResources(Me.ShapeContainer1, "ShapeContainer1")
        Me.ShapeContainer1.Name = "ShapeContainer1"
        Me.ShapeContainer1.Shapes.AddRange(New Microsoft.VisualBasic.PowerPacks.Shape() {Me.RectangleShape2, Me.RectangleShape1})
        Me.ShapeContainer1.TabStop = False
        '
        'RectangleShape2
        '
        resources.ApplyResources(Me.RectangleShape2, "RectangleShape2")
        Me.RectangleShape2.Name = "RectangleShape2"
        '
        'RectangleShape1
        '
        resources.ApplyResources(Me.RectangleShape1, "RectangleShape1")
        Me.RectangleShape1.Name = "RectangleShape1"
        '
        'Button_default_axisrange
        '
        Me.Button_default_axisrange.BackColor = System.Drawing.SystemColors.ActiveBorder
        resources.ApplyResources(Me.Button_default_axisrange, "Button_default_axisrange")
        Me.Button_default_axisrange.Name = "Button_default_axisrange"
        Me.Button_default_axisrange.UseVisualStyleBackColor = False
        '
        'Button_setYaxisrange1
        '
        Me.Button_setYaxisrange1.BackColor = System.Drawing.SystemColors.ActiveBorder
        resources.ApplyResources(Me.Button_setYaxisrange1, "Button_setYaxisrange1")
        Me.Button_setYaxisrange1.Name = "Button_setYaxisrange1"
        Me.Button_setYaxisrange1.UseVisualStyleBackColor = False
        '
        'Chart_PLC
        '
        Me.Chart_PLC.BackColor = System.Drawing.Color.Transparent
        ChartArea2.AxisX.Crossing = -1.7976931348623157E+308R
        ChartArea2.AxisX.Enabled = System.Windows.Forms.DataVisualization.Charting.AxisEnabled.[True]
        ChartArea2.AxisX.Interval = 1.0R
        ChartArea2.AxisX.MajorGrid.Interval = 0.0R
        ChartArea2.AxisX.MajorGrid.LineColor = System.Drawing.Color.DarkGray
        ChartArea2.AxisX.Maximum = 0.0R
        ChartArea2.AxisX.Minimum = -3.0R
        ChartArea2.AxisX.ScaleBreakStyle.CollapsibleSpaceThreshold = 20
        ChartArea2.AxisX.Title = "Pressure (MPa)"
        ChartArea2.AxisX.TitleFont = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold)
        ChartArea2.AxisY.Interval = 20.0R
        ChartArea2.AxisY.IsStartedFromZero = False
        ChartArea2.AxisY.LabelAutoFitMinFontSize = 7
        ChartArea2.AxisY.MajorGrid.Interval = 20.0R
        ChartArea2.AxisY.MajorGrid.IntervalType = System.Windows.Forms.DataVisualization.Charting.DateTimeIntervalType.Number
        ChartArea2.AxisY.MajorGrid.LineColor = System.Drawing.Color.DarkGray
        ChartArea2.AxisY.Maximum = 100.0R
        ChartArea2.AxisY.Minimum = -20.0R
        ChartArea2.AxisY.ScaleBreakStyle.CollapsibleSpaceThreshold = 20
        ChartArea2.AxisY.Title = "PLC (%)"
        ChartArea2.AxisY.TitleFont = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold)
        ChartArea2.InnerPlotPosition.Auto = False
        ChartArea2.InnerPlotPosition.Height = 96.0!
        ChartArea2.InnerPlotPosition.Width = 88.73241!
        ChartArea2.InnerPlotPosition.X = 8.0!
        ChartArea2.Name = "ChartArea1"
        ChartArea2.Position.Auto = False
        ChartArea2.Position.Height = 94.0!
        ChartArea2.Position.Width = 98.0!
        ChartArea2.Position.X = 2.0!
        ChartArea2.Position.Y = 2.0!
        Me.Chart_PLC.ChartAreas.Add(ChartArea2)
        Legend1.BackColor = System.Drawing.Color.PaleGoldenrod
        Legend1.BorderColor = System.Drawing.Color.Black
        Legend1.Name = "Legend1"
        Legend1.Position.Auto = False
        Legend1.Position.Height = 6.756757!
        Legend1.Position.Width = 20.97027!
        Legend1.Position.X = 74.0!
        Legend1.Position.Y = 3.0!
        Me.Chart_PLC.Legends.Add(Legend1)
        resources.ApplyResources(Me.Chart_PLC, "Chart_PLC")
        Me.Chart_PLC.Name = "Chart_PLC"
        Me.Chart_PLC.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.Excel
        Series9.ChartArea = "ChartArea1"
        Series9.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Point
        Series9.Legend = "Legend1"
        Series9.MarkerColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Series9.MarkerSize = 12
        Series9.MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.Circle
        Series9.Name = "Integral method"
        Me.Chart_PLC.Series.Add(Series9)
        Me.Chart_PLC.SuppressExceptions = True
        Me.Chart_PLC.TabStop = False
        '
        'Label_liveimage
        '
        resources.ApplyResources(Me.Label_liveimage, "Label_liveimage")
        Me.Label_liveimage.BackColor = System.Drawing.Color.Transparent
        Me.Label_liveimage.ForeColor = System.Drawing.Color.Red
        Me.Label_liveimage.Name = "Label_liveimage"
        '
        'GroupBox_integral
        '
        Me.GroupBox_integral.BackColor = System.Drawing.Color.SteelBlue
        Me.GroupBox_integral.Controls.Add(Me.Label35)
        Me.GroupBox_integral.Controls.Add(Me.RichTextBox_PLC_integral)
        Me.GroupBox_integral.Controls.Add(Me.TextBox_LP0_integral)
        Me.GroupBox_integral.Controls.Add(Me.RichTextBox_mean_inst_LP_integral)
        Me.GroupBox_integral.Controls.Add(Me.Button_set_LP0_integral)
        Me.GroupBox_integral.Controls.Add(Me.richtextbox_std_dev_LP_integral)
        Me.GroupBox_integral.Controls.Add(Me.Label46)
        Me.GroupBox_integral.Controls.Add(Me.Label48)
        Me.GroupBox_integral.Controls.Add(Me.Label_int2)
        Me.GroupBox_integral.Controls.Add(Me.Label50)
        Me.GroupBox_integral.Controls.Add(Me.Label_int1)
        Me.GroupBox_integral.Controls.Add(Me.Label52)
        Me.GroupBox_integral.Controls.Add(Me.TextBox_Lp1_integral)
        Me.GroupBox_integral.Controls.Add(Me.TextBox_Lp2_integral)
        resources.ApplyResources(Me.GroupBox_integral, "GroupBox_integral")
        Me.GroupBox_integral.Name = "GroupBox_integral"
        Me.GroupBox_integral.TabStop = False
        '
        'Label35
        '
        resources.ApplyResources(Me.Label35, "Label35")
        Me.Label35.Name = "Label35"
        '
        'RichTextBox_PLC_integral
        '
        Me.RichTextBox_PLC_integral.BackColor = System.Drawing.Color.DarkRed
        resources.ApplyResources(Me.RichTextBox_PLC_integral, "RichTextBox_PLC_integral")
        Me.RichTextBox_PLC_integral.ForeColor = System.Drawing.SystemColors.WindowText
        Me.RichTextBox_PLC_integral.Name = "RichTextBox_PLC_integral"
        Me.RichTextBox_PLC_integral.ReadOnly = True
        '
        'TextBox_LP0_integral
        '
        resources.ApplyResources(Me.TextBox_LP0_integral, "TextBox_LP0_integral")
        Me.TextBox_LP0_integral.Name = "TextBox_LP0_integral"
        '
        'RichTextBox_mean_inst_LP_integral
        '
        Me.RichTextBox_mean_inst_LP_integral.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        resources.ApplyResources(Me.RichTextBox_mean_inst_LP_integral, "RichTextBox_mean_inst_LP_integral")
        Me.RichTextBox_mean_inst_LP_integral.ForeColor = System.Drawing.SystemColors.WindowText
        Me.RichTextBox_mean_inst_LP_integral.Name = "RichTextBox_mean_inst_LP_integral"
        Me.RichTextBox_mean_inst_LP_integral.ReadOnly = True
        Me.RichTextBox_mean_inst_LP_integral.TabStop = False
        '
        'Button_set_LP0_integral
        '
        Me.Button_set_LP0_integral.BackColor = System.Drawing.SystemColors.ButtonFace
        resources.ApplyResources(Me.Button_set_LP0_integral, "Button_set_LP0_integral")
        Me.Button_set_LP0_integral.Name = "Button_set_LP0_integral"
        Me.Button_set_LP0_integral.UseVisualStyleBackColor = False
        '
        'richtextbox_std_dev_LP_integral
        '
        Me.richtextbox_std_dev_LP_integral.AcceptsTab = True
        Me.richtextbox_std_dev_LP_integral.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        resources.ApplyResources(Me.richtextbox_std_dev_LP_integral, "richtextbox_std_dev_LP_integral")
        Me.richtextbox_std_dev_LP_integral.ForeColor = System.Drawing.SystemColors.WindowText
        Me.richtextbox_std_dev_LP_integral.Name = "richtextbox_std_dev_LP_integral"
        Me.richtextbox_std_dev_LP_integral.ReadOnly = True
        Me.richtextbox_std_dev_LP_integral.TabStop = False
        '
        'Label46
        '
        resources.ApplyResources(Me.Label46, "Label46")
        Me.Label46.Name = "Label46"
        '
        'Label48
        '
        resources.ApplyResources(Me.Label48, "Label48")
        Me.Label48.Name = "Label48"
        '
        'Label_int2
        '
        resources.ApplyResources(Me.Label_int2, "Label_int2")
        Me.Label_int2.Name = "Label_int2"
        '
        'Label50
        '
        resources.ApplyResources(Me.Label50, "Label50")
        Me.Label50.Name = "Label50"
        '
        'Label_int1
        '
        resources.ApplyResources(Me.Label_int1, "Label_int1")
        Me.Label_int1.Name = "Label_int1"
        '
        'Label52
        '
        resources.ApplyResources(Me.Label52, "Label52")
        Me.Label52.Name = "Label52"
        '
        'TextBox_Lp1_integral
        '
        Me.TextBox_Lp1_integral.BackColor = System.Drawing.SystemColors.HighlightText
        resources.ApplyResources(Me.TextBox_Lp1_integral, "TextBox_Lp1_integral")
        Me.TextBox_Lp1_integral.Name = "TextBox_Lp1_integral"
        Me.TextBox_Lp1_integral.ReadOnly = True
        Me.TextBox_Lp1_integral.TabStop = False
        '
        'TextBox_Lp2_integral
        '
        Me.TextBox_Lp2_integral.BackColor = System.Drawing.SystemColors.HighlightText
        resources.ApplyResources(Me.TextBox_Lp2_integral, "TextBox_Lp2_integral")
        Me.TextBox_Lp2_integral.Name = "TextBox_Lp2_integral"
        Me.TextBox_Lp2_integral.ReadOnly = True
        Me.TextBox_Lp2_integral.TabStop = False
        '
        'Timer_camera
        '
        Me.Timer_camera.Interval = 10
        '
        'CheckBox_save_camera_image
        '
        resources.ApplyResources(Me.CheckBox_save_camera_image, "CheckBox_save_camera_image")
        Me.CheckBox_save_camera_image.Name = "CheckBox_save_camera_image"
        Me.CheckBox_save_camera_image.UseVisualStyleBackColor = True
        '
        'Button_save_camera_images
        '
        Me.Button_save_camera_images.BackColor = System.Drawing.Color.Silver
        resources.ApplyResources(Me.Button_save_camera_images, "Button_save_camera_images")
        Me.Button_save_camera_images.FlatAppearance.BorderColor = System.Drawing.Color.Silver
        Me.Button_save_camera_images.Name = "Button_save_camera_images"
        Me.Button_save_camera_images.UseVisualStyleBackColor = False
        '
        'CheckBox_CAM_connected
        '
        resources.ApplyResources(Me.CheckBox_CAM_connected, "CheckBox_CAM_connected")
        Me.CheckBox_CAM_connected.Name = "CheckBox_CAM_connected"
        Me.CheckBox_CAM_connected.UseVisualStyleBackColor = True
        '
        'Button_set_grid_origin
        '
        resources.ApplyResources(Me.Button_set_grid_origin, "Button_set_grid_origin")
        Me.Button_set_grid_origin.Name = "Button_set_grid_origin"
        Me.Button_set_grid_origin.UseVisualStyleBackColor = True
        '
        'Label_grid_origin
        '
        resources.ApplyResources(Me.Label_grid_origin, "Label_grid_origin")
        Me.Label_grid_origin.Name = "Label_grid_origin"
        '
        'NumericUpDown_exposureTimeAbs
        '
        resources.ApplyResources(Me.NumericUpDown_exposureTimeAbs, "NumericUpDown_exposureTimeAbs")
        Me.NumericUpDown_exposureTimeAbs.Increment = New Decimal(New Integer() {1000, 0, 0, 0})
        Me.NumericUpDown_exposureTimeAbs.Maximum = New Decimal(New Integer() {9999999, 0, 0, 0})
        Me.NumericUpDown_exposureTimeAbs.Name = "NumericUpDown_exposureTimeAbs"
        Me.ToolTip1.SetToolTip(Me.NumericUpDown_exposureTimeAbs, resources.GetString("NumericUpDown_exposureTimeAbs.ToolTip"))
        Me.NumericUpDown_exposureTimeAbs.Value = New Decimal(New Integer() {80000, 0, 0, 0})
        '
        'Label_gridgraduation50
        '
        resources.ApplyResources(Me.Label_gridgraduation50, "Label_gridgraduation50")
        Me.Label_gridgraduation50.BackColor = System.Drawing.Color.Transparent
        Me.Label_gridgraduation50.ForeColor = System.Drawing.Color.Red
        Me.Label_gridgraduation50.Name = "Label_gridgraduation50"
        '
        'Label_gridgraduation40
        '
        resources.ApplyResources(Me.Label_gridgraduation40, "Label_gridgraduation40")
        Me.Label_gridgraduation40.BackColor = System.Drawing.Color.Transparent
        Me.Label_gridgraduation40.Cursor = System.Windows.Forms.Cursors.No
        Me.Label_gridgraduation40.ForeColor = System.Drawing.Color.Red
        Me.Label_gridgraduation40.Name = "Label_gridgraduation40"
        '
        'Label_gridgraduation30
        '
        resources.ApplyResources(Me.Label_gridgraduation30, "Label_gridgraduation30")
        Me.Label_gridgraduation30.BackColor = System.Drawing.Color.Transparent
        Me.Label_gridgraduation30.Cursor = System.Windows.Forms.Cursors.No
        Me.Label_gridgraduation30.ForeColor = System.Drawing.Color.Red
        Me.Label_gridgraduation30.Name = "Label_gridgraduation30"
        '
        'Label_gridgraduation20
        '
        resources.ApplyResources(Me.Label_gridgraduation20, "Label_gridgraduation20")
        Me.Label_gridgraduation20.BackColor = System.Drawing.Color.Transparent
        Me.Label_gridgraduation20.Cursor = System.Windows.Forms.Cursors.No
        Me.Label_gridgraduation20.ForeColor = System.Drawing.Color.Red
        Me.Label_gridgraduation20.Name = "Label_gridgraduation20"
        '
        'Label_gridgraduation10
        '
        resources.ApplyResources(Me.Label_gridgraduation10, "Label_gridgraduation10")
        Me.Label_gridgraduation10.BackColor = System.Drawing.Color.Transparent
        Me.Label_gridgraduation10.Cursor = System.Windows.Forms.Cursors.No
        Me.Label_gridgraduation10.ForeColor = System.Drawing.Color.Red
        Me.Label_gridgraduation10.Name = "Label_gridgraduation10"
        '
        'Label_gridgraduation60
        '
        resources.ApplyResources(Me.Label_gridgraduation60, "Label_gridgraduation60")
        Me.Label_gridgraduation60.BackColor = System.Drawing.Color.Transparent
        Me.Label_gridgraduation60.ForeColor = System.Drawing.Color.Red
        Me.Label_gridgraduation60.Name = "Label_gridgraduation60"
        '
        'Label_gridgraduation70
        '
        resources.ApplyResources(Me.Label_gridgraduation70, "Label_gridgraduation70")
        Me.Label_gridgraduation70.BackColor = System.Drawing.Color.Transparent
        Me.Label_gridgraduation70.ForeColor = System.Drawing.Color.Red
        Me.Label_gridgraduation70.Name = "Label_gridgraduation70"
        '
        'Label56
        '
        resources.ApplyResources(Me.Label56, "Label56")
        Me.Label56.Name = "Label56"
        '
        'GroupBox_stopwatch
        '
        Me.GroupBox_stopwatch.Controls.Add(Me.Label43)
        Me.GroupBox_stopwatch.Controls.Add(Me.Label42)
        Me.GroupBox_stopwatch.Controls.Add(Me.Label8)
        Me.GroupBox_stopwatch.Controls.Add(Me.NumericUpDown_countdown_sec)
        Me.GroupBox_stopwatch.Controls.Add(Me.NumericUpDown_countdown_min)
        Me.GroupBox_stopwatch.Controls.Add(Me.Button2)
        Me.GroupBox_stopwatch.Controls.Add(Me.Button1)
        Me.GroupBox_stopwatch.Controls.Add(Me.TextBox_countdown)
        Me.GroupBox_stopwatch.Controls.Add(Me.Label28)
        resources.ApplyResources(Me.GroupBox_stopwatch, "GroupBox_stopwatch")
        Me.GroupBox_stopwatch.Name = "GroupBox_stopwatch"
        Me.GroupBox_stopwatch.TabStop = False
        '
        'Label43
        '
        resources.ApplyResources(Me.Label43, "Label43")
        Me.Label43.Name = "Label43"
        '
        'Label42
        '
        resources.ApplyResources(Me.Label42, "Label42")
        Me.Label42.Name = "Label42"
        '
        'Label8
        '
        resources.ApplyResources(Me.Label8, "Label8")
        Me.Label8.Name = "Label8"
        '
        'NumericUpDown_countdown_sec
        '
        resources.ApplyResources(Me.NumericUpDown_countdown_sec, "NumericUpDown_countdown_sec")
        Me.NumericUpDown_countdown_sec.Maximum = New Decimal(New Integer() {59, 0, 0, 0})
        Me.NumericUpDown_countdown_sec.Name = "NumericUpDown_countdown_sec"
        '
        'NumericUpDown_countdown_min
        '
        resources.ApplyResources(Me.NumericUpDown_countdown_min, "NumericUpDown_countdown_min")
        Me.NumericUpDown_countdown_min.Maximum = New Decimal(New Integer() {99, 0, 0, 0})
        Me.NumericUpDown_countdown_min.Name = "NumericUpDown_countdown_min"
        Me.NumericUpDown_countdown_min.Value = New Decimal(New Integer() {2, 0, 0, 0})
        '
        'Button2
        '
        resources.ApplyResources(Me.Button2, "Button2")
        Me.Button2.Name = "Button2"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Button1
        '
        resources.ApplyResources(Me.Button1, "Button1")
        Me.Button1.Name = "Button1"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'TextBox_countdown
        '
        Me.TextBox_countdown.BackColor = System.Drawing.Color.PaleGoldenrod
        resources.ApplyResources(Me.TextBox_countdown, "TextBox_countdown")
        Me.TextBox_countdown.Name = "TextBox_countdown"
        Me.TextBox_countdown.ReadOnly = True
        '
        'Label28
        '
        resources.ApplyResources(Me.Label28, "Label28")
        Me.Label28.Name = "Label28"
        '
        'Panel6
        '
        Me.Panel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel6.Controls.Add(Me.PictureBox4)
        Me.Panel6.Controls.Add(Me.RichTextBox1)
        resources.ApplyResources(Me.Panel6, "Panel6")
        Me.Panel6.Name = "Panel6"
        '
        'PictureBox4
        '
        resources.ApplyResources(Me.PictureBox4, "PictureBox4")
        Me.PictureBox4.Name = "PictureBox4"
        Me.PictureBox4.TabStop = False
        '
        'RichTextBox1
        '
        Me.RichTextBox1.BackColor = System.Drawing.Color.SteelBlue
        Me.RichTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None
        resources.ApplyResources(Me.RichTextBox1, "RichTextBox1")
        Me.RichTextBox1.Name = "RichTextBox1"
        '
        'toolStripSeparator7
        '
        Me.toolStripSeparator7.Name = "toolStripSeparator7"
        resources.ApplyResources(Me.toolStripSeparator7, "toolStripSeparator7")
        '
        'toolStripSeparator8
        '
        Me.toolStripSeparator8.Name = "toolStripSeparator8"
        resources.ApplyResources(Me.toolStripSeparator8, "toolStripSeparator8")
        '
        'toolStripSeparator9
        '
        Me.toolStripSeparator9.Name = "toolStripSeparator9"
        resources.ApplyResources(Me.toolStripSeparator9, "toolStripSeparator9")
        '
        'ToolStripSeparator6
        '
        Me.ToolStripSeparator6.Name = "ToolStripSeparator6"
        resources.ApplyResources(Me.ToolStripSeparator6, "ToolStripSeparator6")
        '
        'ToolStripButton_activate_MEASUREMENT_mode
        '
        Me.ToolStripButton_activate_MEASUREMENT_mode.CheckOnClick = True
        Me.ToolStripButton_activate_MEASUREMENT_mode.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        resources.ApplyResources(Me.ToolStripButton_activate_MEASUREMENT_mode, "ToolStripButton_activate_MEASUREMENT_mode")
        Me.ToolStripButton_activate_MEASUREMENT_mode.Name = "ToolStripButton_activate_MEASUREMENT_mode"
        '
        'ToolTip1
        '
        Me.ToolTip1.AutoPopDelay = 7000
        Me.ToolTip1.InitialDelay = 500
        Me.ToolTip1.IsBalloon = True
        Me.ToolTip1.ReshowDelay = 100
        Me.ToolTip1.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info
        '
        'Button_downstream_ROI
        '
        Me.Button_downstream_ROI.BackColor = System.Drawing.Color.PaleGreen
        resources.ApplyResources(Me.Button_downstream_ROI, "Button_downstream_ROI")
        Me.Button_downstream_ROI.Name = "Button_downstream_ROI"
        Me.ToolTip1.SetToolTip(Me.Button_downstream_ROI, resources.GetString("Button_downstream_ROI.ToolTip"))
        Me.Button_downstream_ROI.UseVisualStyleBackColor = False
        '
        'PictureBox_main_image
        '
        resources.ApplyResources(Me.PictureBox_main_image, "PictureBox_main_image")
        Me.PictureBox_main_image.Name = "PictureBox_main_image"
        Me.PictureBox_main_image.TabStop = False
        '
        'PictureBox_RESERVOIR_Fill
        '
        resources.ApplyResources(Me.PictureBox_RESERVOIR_Fill, "PictureBox_RESERVOIR_Fill")
        Me.PictureBox_RESERVOIR_Fill.Name = "PictureBox_RESERVOIR_Fill"
        Me.PictureBox_RESERVOIR_Fill.TabStop = False
        '
        'PictureBox2
        '
        Me.PictureBox2.Image = Global.cavi_soft.My.Resources.Resources.LOGO_Universite_Bordeaux_RVB_03
        resources.ApplyResources(Me.PictureBox2, "PictureBox2")
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.TabStop = False
        '
        'PictureBox_rec
        '
        resources.ApplyResources(Me.PictureBox_rec, "PictureBox_rec")
        Me.PictureBox_rec.Name = "PictureBox_rec"
        Me.PictureBox_rec.TabStop = False
        '
        'PictureBox_grid
        '
        resources.ApplyResources(Me.PictureBox_grid, "PictureBox_grid")
        Me.PictureBox_grid.Name = "PictureBox_grid"
        Me.PictureBox_grid.TabStop = False
        '
        'Label_gridgraduation80
        '
        resources.ApplyResources(Me.Label_gridgraduation80, "Label_gridgraduation80")
        Me.Label_gridgraduation80.BackColor = System.Drawing.Color.Transparent
        Me.Label_gridgraduation80.Cursor = System.Windows.Forms.Cursors.No
        Me.Label_gridgraduation80.ForeColor = System.Drawing.Color.Red
        Me.Label_gridgraduation80.Name = "Label_gridgraduation80"
        '
        'Label_gridgraduation90
        '
        resources.ApplyResources(Me.Label_gridgraduation90, "Label_gridgraduation90")
        Me.Label_gridgraduation90.BackColor = System.Drawing.Color.Transparent
        Me.Label_gridgraduation90.Cursor = System.Windows.Forms.Cursors.No
        Me.Label_gridgraduation90.ForeColor = System.Drawing.Color.Red
        Me.Label_gridgraduation90.Name = "Label_gridgraduation90"
        '
        'Label_gridgraduation100
        '
        resources.ApplyResources(Me.Label_gridgraduation100, "Label_gridgraduation100")
        Me.Label_gridgraduation100.BackColor = System.Drawing.Color.Transparent
        Me.Label_gridgraduation100.Cursor = System.Windows.Forms.Cursors.No
        Me.Label_gridgraduation100.ForeColor = System.Drawing.Color.Red
        Me.Label_gridgraduation100.Name = "Label_gridgraduation100"
        '
        'Label_gridgraduation110
        '
        resources.ApplyResources(Me.Label_gridgraduation110, "Label_gridgraduation110")
        Me.Label_gridgraduation110.BackColor = System.Drawing.Color.Transparent
        Me.Label_gridgraduation110.Cursor = System.Windows.Forms.Cursors.No
        Me.Label_gridgraduation110.ForeColor = System.Drawing.Color.Red
        Me.Label_gridgraduation110.Name = "Label_gridgraduation110"
        '
        'Label_gridgraduation120
        '
        resources.ApplyResources(Me.Label_gridgraduation120, "Label_gridgraduation120")
        Me.Label_gridgraduation120.BackColor = System.Drawing.Color.Transparent
        Me.Label_gridgraduation120.Cursor = System.Windows.Forms.Cursors.No
        Me.Label_gridgraduation120.ForeColor = System.Drawing.Color.Red
        Me.Label_gridgraduation120.Name = "Label_gridgraduation120"
        '
        'Label_gridgraduation130
        '
        resources.ApplyResources(Me.Label_gridgraduation130, "Label_gridgraduation130")
        Me.Label_gridgraduation130.BackColor = System.Drawing.Color.Transparent
        Me.Label_gridgraduation130.Cursor = System.Windows.Forms.Cursors.No
        Me.Label_gridgraduation130.ForeColor = System.Drawing.Color.Red
        Me.Label_gridgraduation130.Name = "Label_gridgraduation130"
        '
        'Label_gridgraduation140
        '
        resources.ApplyResources(Me.Label_gridgraduation140, "Label_gridgraduation140")
        Me.Label_gridgraduation140.BackColor = System.Drawing.Color.Transparent
        Me.Label_gridgraduation140.Cursor = System.Windows.Forms.Cursors.No
        Me.Label_gridgraduation140.ForeColor = System.Drawing.Color.Red
        Me.Label_gridgraduation140.Name = "Label_gridgraduation140"
        '
        'Label_gridgraduation150
        '
        resources.ApplyResources(Me.Label_gridgraduation150, "Label_gridgraduation150")
        Me.Label_gridgraduation150.BackColor = System.Drawing.Color.Transparent
        Me.Label_gridgraduation150.Cursor = System.Windows.Forms.Cursors.No
        Me.Label_gridgraduation150.ForeColor = System.Drawing.Color.Red
        Me.Label_gridgraduation150.Name = "Label_gridgraduation150"
        '
        'MenuStrip1
        '
        Me.MenuStrip1.BackColor = System.Drawing.Color.SteelBlue
        resources.ApplyResources(Me.MenuStrip1, "MenuStrip1")
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItem1, Me.ToolStripMenuItem15, Me.ToolStripMenuItem25, Me.ToolStripMenuItem_about})
        Me.MenuStrip1.Name = "MenuStrip1"
        '
        'ToolStripMenuItem1
        '
        Me.ToolStripMenuItem1.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItem2, Me.ToolStripMenuItem_openfile, Me.ToolStripSeparator10, Me.ToolStripMenuItem_savedata, Me.ToolStripMenuItem_savedata_as, Me.ToolStripMenuItem_appenddata, Me.ToolStripSeparator11, Me.ToolStripMenuItem_imagefolder, Me.ToolStripSeparator13, Me.ToolStripMenuItem_exit})
        Me.ToolStripMenuItem1.Name = "ToolStripMenuItem1"
        resources.ApplyResources(Me.ToolStripMenuItem1, "ToolStripMenuItem1")
        '
        'ToolStripMenuItem2
        '
        Me.ToolStripMenuItem2.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItem_sameparameter, Me.ToolStripMenuItem_newparameter})
        resources.ApplyResources(Me.ToolStripMenuItem2, "ToolStripMenuItem2")
        Me.ToolStripMenuItem2.Name = "ToolStripMenuItem2"
        '
        'ToolStripMenuItem_sameparameter
        '
        Me.ToolStripMenuItem_sameparameter.Name = "ToolStripMenuItem_sameparameter"
        resources.ApplyResources(Me.ToolStripMenuItem_sameparameter, "ToolStripMenuItem_sameparameter")
        '
        'ToolStripMenuItem_newparameter
        '
        Me.ToolStripMenuItem_newparameter.Name = "ToolStripMenuItem_newparameter"
        resources.ApplyResources(Me.ToolStripMenuItem_newparameter, "ToolStripMenuItem_newparameter")
        '
        'ToolStripMenuItem_openfile
        '
        resources.ApplyResources(Me.ToolStripMenuItem_openfile, "ToolStripMenuItem_openfile")
        Me.ToolStripMenuItem_openfile.Name = "ToolStripMenuItem_openfile"
        '
        'ToolStripSeparator10
        '
        Me.ToolStripSeparator10.Name = "ToolStripSeparator10"
        resources.ApplyResources(Me.ToolStripSeparator10, "ToolStripSeparator10")
        '
        'ToolStripMenuItem_savedata
        '
        resources.ApplyResources(Me.ToolStripMenuItem_savedata, "ToolStripMenuItem_savedata")
        Me.ToolStripMenuItem_savedata.Name = "ToolStripMenuItem_savedata"
        '
        'ToolStripMenuItem_savedata_as
        '
        Me.ToolStripMenuItem_savedata_as.Name = "ToolStripMenuItem_savedata_as"
        resources.ApplyResources(Me.ToolStripMenuItem_savedata_as, "ToolStripMenuItem_savedata_as")
        '
        'ToolStripMenuItem_appenddata
        '
        Me.ToolStripMenuItem_appenddata.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItem_appendfile})
        Me.ToolStripMenuItem_appenddata.Name = "ToolStripMenuItem_appenddata"
        resources.ApplyResources(Me.ToolStripMenuItem_appenddata, "ToolStripMenuItem_appenddata")
        '
        'ToolStripMenuItem_appendfile
        '
        Me.ToolStripMenuItem_appendfile.Name = "ToolStripMenuItem_appendfile"
        resources.ApplyResources(Me.ToolStripMenuItem_appendfile, "ToolStripMenuItem_appendfile")
        '
        'ToolStripSeparator11
        '
        Me.ToolStripSeparator11.Name = "ToolStripSeparator11"
        resources.ApplyResources(Me.ToolStripSeparator11, "ToolStripSeparator11")
        '
        'ToolStripMenuItem_imagefolder
        '
        Me.ToolStripMenuItem_imagefolder.Name = "ToolStripMenuItem_imagefolder"
        resources.ApplyResources(Me.ToolStripMenuItem_imagefolder, "ToolStripMenuItem_imagefolder")
        '
        'ToolStripSeparator13
        '
        Me.ToolStripSeparator13.Name = "ToolStripSeparator13"
        resources.ApplyResources(Me.ToolStripSeparator13, "ToolStripSeparator13")
        '
        'ToolStripMenuItem_exit
        '
        Me.ToolStripMenuItem_exit.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItemexit_withoutsaving})
        Me.ToolStripMenuItem_exit.Name = "ToolStripMenuItem_exit"
        resources.ApplyResources(Me.ToolStripMenuItem_exit, "ToolStripMenuItem_exit")
        '
        'ToolStripMenuItemexit_withoutsaving
        '
        Me.ToolStripMenuItemexit_withoutsaving.Name = "ToolStripMenuItemexit_withoutsaving"
        resources.ApplyResources(Me.ToolStripMenuItemexit_withoutsaving, "ToolStripMenuItemexit_withoutsaving")
        '
        'ToolStripMenuItem15
        '
        Me.ToolStripMenuItem15.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItem_branchdiameter, Me.ToolStripMenuItemmin_meniscusposition, Me.ToolStripMenuItem_reinit})
        Me.ToolStripMenuItem15.Name = "ToolStripMenuItem15"
        resources.ApplyResources(Me.ToolStripMenuItem15, "ToolStripMenuItem15")
        '
        'ToolStripMenuItem_branchdiameter
        '
        Me.ToolStripMenuItem_branchdiameter.Name = "ToolStripMenuItem_branchdiameter"
        resources.ApplyResources(Me.ToolStripMenuItem_branchdiameter, "ToolStripMenuItem_branchdiameter")
        '
        'ToolStripMenuItemmin_meniscusposition
        '
        Me.ToolStripMenuItemmin_meniscusposition.Name = "ToolStripMenuItemmin_meniscusposition"
        resources.ApplyResources(Me.ToolStripMenuItemmin_meniscusposition, "ToolStripMenuItemmin_meniscusposition")
        '
        'ToolStripMenuItem_reinit
        '
        Me.ToolStripMenuItem_reinit.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItem_reinitchart, Me.ToolStripMenuItem_reinitgrid, Me.ToolStripMenuItem_reinitdatalogger, Me.ToolStripMenuItem_reinitparameter, Me.ToolStripMenuItem_reinitstddev})
        Me.ToolStripMenuItem_reinit.Name = "ToolStripMenuItem_reinit"
        resources.ApplyResources(Me.ToolStripMenuItem_reinit, "ToolStripMenuItem_reinit")
        '
        'ToolStripMenuItem_reinitchart
        '
        Me.ToolStripMenuItem_reinitchart.Name = "ToolStripMenuItem_reinitchart"
        resources.ApplyResources(Me.ToolStripMenuItem_reinitchart, "ToolStripMenuItem_reinitchart")
        '
        'ToolStripMenuItem_reinitgrid
        '
        Me.ToolStripMenuItem_reinitgrid.Name = "ToolStripMenuItem_reinitgrid"
        resources.ApplyResources(Me.ToolStripMenuItem_reinitgrid, "ToolStripMenuItem_reinitgrid")
        '
        'ToolStripMenuItem_reinitdatalogger
        '
        Me.ToolStripMenuItem_reinitdatalogger.Name = "ToolStripMenuItem_reinitdatalogger"
        resources.ApplyResources(Me.ToolStripMenuItem_reinitdatalogger, "ToolStripMenuItem_reinitdatalogger")
        '
        'ToolStripMenuItem_reinitparameter
        '
        Me.ToolStripMenuItem_reinitparameter.Name = "ToolStripMenuItem_reinitparameter"
        resources.ApplyResources(Me.ToolStripMenuItem_reinitparameter, "ToolStripMenuItem_reinitparameter")
        '
        'ToolStripMenuItem_reinitstddev
        '
        Me.ToolStripMenuItem_reinitstddev.Name = "ToolStripMenuItem_reinitstddev"
        resources.ApplyResources(Me.ToolStripMenuItem_reinitstddev, "ToolStripMenuItem_reinitstddev")
        '
        'ToolStripMenuItem25
        '
        Me.ToolStripMenuItem25.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItem_shortcut})
        Me.ToolStripMenuItem25.Name = "ToolStripMenuItem25"
        resources.ApplyResources(Me.ToolStripMenuItem25, "ToolStripMenuItem25")
        '
        'ToolStripMenuItem_shortcut
        '
        Me.ToolStripMenuItem_shortcut.Name = "ToolStripMenuItem_shortcut"
        resources.ApplyResources(Me.ToolStripMenuItem_shortcut, "ToolStripMenuItem_shortcut")
        '
        'ToolStripMenuItem_about
        '
        Me.ToolStripMenuItem_about.BackColor = System.Drawing.Color.Transparent
        Me.ToolStripMenuItem_about.Name = "ToolStripMenuItem_about"
        resources.ApplyResources(Me.ToolStripMenuItem_about, "ToolStripMenuItem_about")
        '
        'ToolStrip1
        '
        Me.ToolStrip1.BackColor = System.Drawing.Color.Silver
        resources.ApplyResources(Me.ToolStrip1, "ToolStrip1")
        Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripButton_recompute_PLC, Me.ToolStripButton_newmeas_sameparameter, Me.ToolStripButton_open, Me.ToolStripButton_save, Me.ToolStripButton_activate_MEASUREMENT_mode})
        Me.ToolStrip1.Name = "ToolStrip1"
        '
        'ToolStripButton_recompute_PLC
        '
        Me.ToolStripButton_recompute_PLC.CheckOnClick = True
        Me.ToolStripButton_recompute_PLC.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        resources.ApplyResources(Me.ToolStripButton_recompute_PLC, "ToolStripButton_recompute_PLC")
        Me.ToolStripButton_recompute_PLC.Name = "ToolStripButton_recompute_PLC"
        '
        'ToolStripButton_newmeas_sameparameter
        '
        Me.ToolStripButton_newmeas_sameparameter.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        resources.ApplyResources(Me.ToolStripButton_newmeas_sameparameter, "ToolStripButton_newmeas_sameparameter")
        Me.ToolStripButton_newmeas_sameparameter.Name = "ToolStripButton_newmeas_sameparameter"
        '
        'ToolStripButton_open
        '
        Me.ToolStripButton_open.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        resources.ApplyResources(Me.ToolStripButton_open, "ToolStripButton_open")
        Me.ToolStripButton_open.Name = "ToolStripButton_open"
        '
        'ToolStripButton_save
        '
        Me.ToolStripButton_save.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        resources.ApplyResources(Me.ToolStripButton_save, "ToolStripButton_save")
        Me.ToolStripButton_save.Name = "ToolStripButton_save"
        '
        'GroupBox_centrifugecontrol
        '
        Me.GroupBox_centrifugecontrol.Controls.Add(Me.Panel7)
        Me.GroupBox_centrifugecontrol.Controls.Add(Me.Label63)
        Me.GroupBox_centrifugecontrol.Controls.Add(Me.Label62)
        Me.GroupBox_centrifugecontrol.Controls.Add(Me.Bt_centri_START_STOP)
        Me.GroupBox_centrifugecontrol.Controls.Add(Me.Led_stop)
        Me.GroupBox_centrifugecontrol.Controls.Add(Me.Led_start)
        resources.ApplyResources(Me.GroupBox_centrifugecontrol, "GroupBox_centrifugecontrol")
        Me.GroupBox_centrifugecontrol.Name = "GroupBox_centrifugecontrol"
        Me.GroupBox_centrifugecontrol.TabStop = False
        '
        'Panel7
        '
        Me.Panel7.Controls.Add(Me.Led_brake)
        Me.Panel7.Controls.Add(Me.Bt_centri_BRAKE)
        resources.ApplyResources(Me.Panel7, "Panel7")
        Me.Panel7.Name = "Panel7"
        '
        'Led_brake
        '
        Me.Led_brake.LedStyle = NationalInstruments.UI.LedStyle.Round3D
        resources.ApplyResources(Me.Led_brake, "Led_brake")
        Me.Led_brake.Name = "Led_brake"
        '
        'Bt_centri_BRAKE
        '
        resources.ApplyResources(Me.Bt_centri_BRAKE, "Bt_centri_BRAKE")
        Me.Bt_centri_BRAKE.Name = "Bt_centri_BRAKE"
        Me.Bt_centri_BRAKE.UseVisualStyleBackColor = True
        '
        'Label63
        '
        resources.ApplyResources(Me.Label63, "Label63")
        Me.Label63.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label63.Name = "Label63"
        '
        'Label62
        '
        resources.ApplyResources(Me.Label62, "Label62")
        Me.Label62.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label62.Name = "Label62"
        '
        'Bt_centri_START_STOP
        '
        Me.Bt_centri_START_STOP.BackColor = System.Drawing.Color.Silver
        resources.ApplyResources(Me.Bt_centri_START_STOP, "Bt_centri_START_STOP")
        Me.Bt_centri_START_STOP.Name = "Bt_centri_START_STOP"
        Me.Bt_centri_START_STOP.UseVisualStyleBackColor = False
        '
        'Led_stop
        '
        Me.Led_stop.LedStyle = NationalInstruments.UI.LedStyle.Round3D
        resources.ApplyResources(Me.Led_stop, "Led_stop")
        Me.Led_stop.Name = "Led_stop"
        '
        'Led_start
        '
        Me.Led_start.LedStyle = NationalInstruments.UI.LedStyle.Round3D
        resources.ApplyResources(Me.Led_start, "Led_start")
        Me.Led_start.Name = "Led_start"
        '
        'Label_centri_running
        '
        resources.ApplyResources(Me.Label_centri_running, "Label_centri_running")
        Me.Label_centri_running.ForeColor = System.Drawing.Color.DarkRed
        Me.Label_centri_running.Name = "Label_centri_running"
        '
        'Label_mouseposition
        '
        resources.ApplyResources(Me.Label_mouseposition, "Label_mouseposition")
        Me.Label_mouseposition.Name = "Label_mouseposition"
        '
        'Button_AUTO_MANUAL
        '
        Me.Button_AUTO_MANUAL.BackgroundImage = Global.cavi_soft.My.Resources.Resources.manual_mode_image
        resources.ApplyResources(Me.Button_AUTO_MANUAL, "Button_AUTO_MANUAL")
        Me.Button_AUTO_MANUAL.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.Button_AUTO_MANUAL.Name = "Button_AUTO_MANUAL"
        Me.Button_AUTO_MANUAL.UseVisualStyleBackColor = True
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage_MANUAL)
        Me.TabControl1.Controls.Add(Me.TabPage_AUTO)
        resources.ApplyResources(Me.TabControl1, "TabControl1")
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        '
        'TabPage_MANUAL
        '
        Me.TabPage_MANUAL.BackColor = System.Drawing.Color.SteelBlue
        Me.TabPage_MANUAL.Controls.Add(Me.GroupBox_integral)
        resources.ApplyResources(Me.TabPage_MANUAL, "TabPage_MANUAL")
        Me.TabPage_MANUAL.Name = "TabPage_MANUAL"
        '
        'TabPage_AUTO
        '
        Me.TabPage_AUTO.BackColor = System.Drawing.Color.SlateGray
        Me.TabPage_AUTO.Controls.Add(Me.Button_intensity_profile)
        Me.TabPage_AUTO.Controls.Add(Me.CheckBox5)
        Me.TabPage_AUTO.Controls.Add(Me.GroupBox6)
        Me.TabPage_AUTO.Controls.Add(Me.CheckBox4)
        Me.TabPage_AUTO.Controls.Add(Me.CheckBox_display_AUTOMODE)
        Me.TabPage_AUTO.Controls.Add(Me.GroupBox_ROI)
        Me.TabPage_AUTO.Controls.Add(Me.GroupBox_Integralmethod_AUTOMODE)
        resources.ApplyResources(Me.TabPage_AUTO, "TabPage_AUTO")
        Me.TabPage_AUTO.Name = "TabPage_AUTO"
        '
        'CheckBox5
        '
        resources.ApplyResources(Me.CheckBox5, "CheckBox5")
        Me.CheckBox5.Name = "CheckBox5"
        Me.CheckBox5.UseVisualStyleBackColor = True
        '
        'GroupBox6
        '
        Me.GroupBox6.Controls.Add(Me.Label84)
        Me.GroupBox6.Controls.Add(Me.NumericUpDown_reservoir_filling_time)
        Me.GroupBox6.Controls.Add(Me.Label83)
        Me.GroupBox6.Controls.Add(Me.NumericUpDown_numb_of_replicate)
        resources.ApplyResources(Me.GroupBox6, "GroupBox6")
        Me.GroupBox6.Name = "GroupBox6"
        Me.GroupBox6.TabStop = False
        '
        'Label84
        '
        resources.ApplyResources(Me.Label84, "Label84")
        Me.Label84.Name = "Label84"
        '
        'NumericUpDown_reservoir_filling_time
        '
        resources.ApplyResources(Me.NumericUpDown_reservoir_filling_time, "NumericUpDown_reservoir_filling_time")
        Me.NumericUpDown_reservoir_filling_time.Maximum = New Decimal(New Integer() {22, 0, 0, 0})
        Me.NumericUpDown_reservoir_filling_time.Minimum = New Decimal(New Integer() {2, 0, 0, 0})
        Me.NumericUpDown_reservoir_filling_time.Name = "NumericUpDown_reservoir_filling_time"
        Me.NumericUpDown_reservoir_filling_time.Value = New Decimal(New Integer() {5, 0, 0, 0})
        '
        'Label83
        '
        resources.ApplyResources(Me.Label83, "Label83")
        Me.Label83.Name = "Label83"
        '
        'NumericUpDown_numb_of_replicate
        '
        resources.ApplyResources(Me.NumericUpDown_numb_of_replicate, "NumericUpDown_numb_of_replicate")
        Me.NumericUpDown_numb_of_replicate.Maximum = New Decimal(New Integer() {8, 0, 0, 0})
        Me.NumericUpDown_numb_of_replicate.Minimum = New Decimal(New Integer() {2, 0, 0, 0})
        Me.NumericUpDown_numb_of_replicate.Name = "NumericUpDown_numb_of_replicate"
        Me.NumericUpDown_numb_of_replicate.Value = New Decimal(New Integer() {3, 0, 0, 0})
        '
        'CheckBox4
        '
        resources.ApplyResources(Me.CheckBox4, "CheckBox4")
        Me.CheckBox4.Name = "CheckBox4"
        Me.CheckBox4.UseVisualStyleBackColor = True
        '
        'CheckBox_display_AUTOMODE
        '
        resources.ApplyResources(Me.CheckBox_display_AUTOMODE, "CheckBox_display_AUTOMODE")
        Me.CheckBox_display_AUTOMODE.Checked = True
        Me.CheckBox_display_AUTOMODE.CheckState = System.Windows.Forms.CheckState.Checked
        Me.CheckBox_display_AUTOMODE.Name = "CheckBox_display_AUTOMODE"
        Me.CheckBox_display_AUTOMODE.UseVisualStyleBackColor = True
        '
        'GroupBox_ROI
        '
        Me.GroupBox_ROI.Controls.Add(Me.Label92)
        Me.GroupBox_ROI.Controls.Add(Me.Label73)
        Me.GroupBox_ROI.Controls.Add(Me.Label91)
        Me.GroupBox_ROI.Controls.Add(Me.Button_upstream_ROI)
        Me.GroupBox_ROI.Controls.Add(Me.Label85)
        Me.GroupBox_ROI.Controls.Add(Me.NumericUpDown_measuredline_pixel)
        Me.GroupBox_ROI.Controls.Add(Me.Button_automatic_ROI)
        Me.GroupBox_ROI.Controls.Add(Me.Label71)
        Me.GroupBox_ROI.Controls.Add(Me.NumericUpDown_downstream_ROI_width_pixel)
        Me.GroupBox_ROI.Controls.Add(Me.NumericUpDown_upstream_ROI_width_pixel)
        Me.GroupBox_ROI.Controls.Add(Me.Label72)
        Me.GroupBox_ROI.Controls.Add(Me.Label74)
        Me.GroupBox_ROI.Controls.Add(Me.TextBox_downstream_ROI_pixel_origin_value)
        Me.GroupBox_ROI.Controls.Add(Me.TextBox_upstream_ROI_pixel_origin_value)
        Me.GroupBox_ROI.Controls.Add(Me.Button_downstream_ROI)
        resources.ApplyResources(Me.GroupBox_ROI, "GroupBox_ROI")
        Me.GroupBox_ROI.Name = "GroupBox_ROI"
        Me.GroupBox_ROI.TabStop = False
        '
        'Label92
        '
        resources.ApplyResources(Me.Label92, "Label92")
        Me.Label92.Name = "Label92"
        '
        'Label73
        '
        resources.ApplyResources(Me.Label73, "Label73")
        Me.Label73.Name = "Label73"
        '
        'Label91
        '
        resources.ApplyResources(Me.Label91, "Label91")
        Me.Label91.Name = "Label91"
        '
        'Button_upstream_ROI
        '
        Me.Button_upstream_ROI.BackColor = System.Drawing.Color.PaleTurquoise
        resources.ApplyResources(Me.Button_upstream_ROI, "Button_upstream_ROI")
        Me.Button_upstream_ROI.Name = "Button_upstream_ROI"
        Me.Button_upstream_ROI.UseVisualStyleBackColor = False
        '
        'Label85
        '
        resources.ApplyResources(Me.Label85, "Label85")
        Me.Label85.Name = "Label85"
        '
        'NumericUpDown_measuredline_pixel
        '
        resources.ApplyResources(Me.NumericUpDown_measuredline_pixel, "NumericUpDown_measuredline_pixel")
        Me.NumericUpDown_measuredline_pixel.Maximum = New Decimal(New Integer() {490, 0, 0, 0})
        Me.NumericUpDown_measuredline_pixel.Minimum = New Decimal(New Integer() {2, 0, 0, 0})
        Me.NumericUpDown_measuredline_pixel.Name = "NumericUpDown_measuredline_pixel"
        Me.NumericUpDown_measuredline_pixel.Value = New Decimal(New Integer() {250, 0, 0, 0})
        '
        'Button_automatic_ROI
        '
        Me.Button_automatic_ROI.BackColor = System.Drawing.Color.Silver
        resources.ApplyResources(Me.Button_automatic_ROI, "Button_automatic_ROI")
        Me.Button_automatic_ROI.Name = "Button_automatic_ROI"
        Me.Button_automatic_ROI.UseVisualStyleBackColor = False
        '
        'Label71
        '
        resources.ApplyResources(Me.Label71, "Label71")
        Me.Label71.Name = "Label71"
        '
        'NumericUpDown_downstream_ROI_width_pixel
        '
        Me.NumericUpDown_downstream_ROI_width_pixel.Cursor = System.Windows.Forms.Cursors.Default
        resources.ApplyResources(Me.NumericUpDown_downstream_ROI_width_pixel, "NumericUpDown_downstream_ROI_width_pixel")
        Me.NumericUpDown_downstream_ROI_width_pixel.Maximum = New Decimal(New Integer() {300, 0, 0, 0})
        Me.NumericUpDown_downstream_ROI_width_pixel.Name = "NumericUpDown_downstream_ROI_width_pixel"
        Me.NumericUpDown_downstream_ROI_width_pixel.Value = New Decimal(New Integer() {30, 0, 0, 0})
        '
        'NumericUpDown_upstream_ROI_width_pixel
        '
        resources.ApplyResources(Me.NumericUpDown_upstream_ROI_width_pixel, "NumericUpDown_upstream_ROI_width_pixel")
        Me.NumericUpDown_upstream_ROI_width_pixel.Maximum = New Decimal(New Integer() {655, 0, 0, 0})
        Me.NumericUpDown_upstream_ROI_width_pixel.Name = "NumericUpDown_upstream_ROI_width_pixel"
        Me.NumericUpDown_upstream_ROI_width_pixel.Value = New Decimal(New Integer() {300, 0, 0, 0})
        '
        'Label72
        '
        resources.ApplyResources(Me.Label72, "Label72")
        Me.Label72.Name = "Label72"
        '
        'Label74
        '
        resources.ApplyResources(Me.Label74, "Label74")
        Me.Label74.Name = "Label74"
        '
        'TextBox_downstream_ROI_pixel_origin_value
        '
        resources.ApplyResources(Me.TextBox_downstream_ROI_pixel_origin_value, "TextBox_downstream_ROI_pixel_origin_value")
        Me.TextBox_downstream_ROI_pixel_origin_value.Name = "TextBox_downstream_ROI_pixel_origin_value"
        '
        'TextBox_upstream_ROI_pixel_origin_value
        '
        resources.ApplyResources(Me.TextBox_upstream_ROI_pixel_origin_value, "TextBox_upstream_ROI_pixel_origin_value")
        Me.TextBox_upstream_ROI_pixel_origin_value.Name = "TextBox_upstream_ROI_pixel_origin_value"
        '
        'GroupBox_Integralmethod_AUTOMODE
        '
        Me.GroupBox_Integralmethod_AUTOMODE.Controls.Add(Me.Label70)
        Me.GroupBox_Integralmethod_AUTOMODE.Controls.Add(Me.NumericUpDown_AUTOMODE_integral_stabilisation_time)
        Me.GroupBox_Integralmethod_AUTOMODE.Controls.Add(Me.Label69)
        Me.GroupBox_Integralmethod_AUTOMODE.Controls.Add(Me.NumericUpDown_AUTOMODE_integral_integration_interval_ms)
        Me.GroupBox_Integralmethod_AUTOMODE.Controls.Add(Me.NumericUpDown_AUTOMODE_number_of_rep_integral)
        Me.GroupBox_Integralmethod_AUTOMODE.Controls.Add(Me.Label67)
        resources.ApplyResources(Me.GroupBox_Integralmethod_AUTOMODE, "GroupBox_Integralmethod_AUTOMODE")
        Me.GroupBox_Integralmethod_AUTOMODE.Name = "GroupBox_Integralmethod_AUTOMODE"
        Me.GroupBox_Integralmethod_AUTOMODE.TabStop = False
        '
        'Label70
        '
        resources.ApplyResources(Me.Label70, "Label70")
        Me.Label70.Name = "Label70"
        '
        'NumericUpDown_AUTOMODE_integral_stabilisation_time
        '
        resources.ApplyResources(Me.NumericUpDown_AUTOMODE_integral_stabilisation_time, "NumericUpDown_AUTOMODE_integral_stabilisation_time")
        Me.NumericUpDown_AUTOMODE_integral_stabilisation_time.Name = "NumericUpDown_AUTOMODE_integral_stabilisation_time"
        '
        'Label69
        '
        resources.ApplyResources(Me.Label69, "Label69")
        Me.Label69.Name = "Label69"
        '
        'NumericUpDown_AUTOMODE_integral_integration_interval_ms
        '
        resources.ApplyResources(Me.NumericUpDown_AUTOMODE_integral_integration_interval_ms, "NumericUpDown_AUTOMODE_integral_integration_interval_ms")
        Me.NumericUpDown_AUTOMODE_integral_integration_interval_ms.Increment = New Decimal(New Integer() {50, 0, 0, 0})
        Me.NumericUpDown_AUTOMODE_integral_integration_interval_ms.Maximum = New Decimal(New Integer() {10000, 0, 0, 0})
        Me.NumericUpDown_AUTOMODE_integral_integration_interval_ms.Minimum = New Decimal(New Integer() {50, 0, 0, 0})
        Me.NumericUpDown_AUTOMODE_integral_integration_interval_ms.Name = "NumericUpDown_AUTOMODE_integral_integration_interval_ms"
        Me.NumericUpDown_AUTOMODE_integral_integration_interval_ms.Value = New Decimal(New Integer() {1000, 0, 0, 0})
        '
        'NumericUpDown_AUTOMODE_number_of_rep_integral
        '
        resources.ApplyResources(Me.NumericUpDown_AUTOMODE_number_of_rep_integral, "NumericUpDown_AUTOMODE_number_of_rep_integral")
        Me.NumericUpDown_AUTOMODE_number_of_rep_integral.Maximum = New Decimal(New Integer() {10000, 0, 0, 0})
        Me.NumericUpDown_AUTOMODE_number_of_rep_integral.Minimum = New Decimal(New Integer() {5, 0, 0, 0})
        Me.NumericUpDown_AUTOMODE_number_of_rep_integral.Name = "NumericUpDown_AUTOMODE_number_of_rep_integral"
        Me.NumericUpDown_AUTOMODE_number_of_rep_integral.Value = New Decimal(New Integer() {10, 0, 0, 0})
        '
        'Label67
        '
        resources.ApplyResources(Me.Label67, "Label67")
        Me.Label67.Name = "Label67"
        '
        'Timer_getdistance
        '
        Me.Timer_getdistance.Enabled = True
        Me.Timer_getdistance.Interval = 1000
        '
        'ScatterGraph_intensityprofile
        '
        Me.ScatterGraph_intensityprofile.BackColor = System.Drawing.Color.Transparent
        Me.ScatterGraph_intensityprofile.Border = NationalInstruments.UI.Border.None
        resources.ApplyResources(Me.ScatterGraph_intensityprofile, "ScatterGraph_intensityprofile")
        Me.ScatterGraph_intensityprofile.Name = "ScatterGraph_intensityprofile"
        Me.ScatterGraph_intensityprofile.PlotAreaColor = System.Drawing.Color.White
        Me.ScatterGraph_intensityprofile.Plots.AddRange(New NationalInstruments.UI.ScatterPlot() {Me.ScatterPlot1, Me.ScatterPlot_background, Me.ScatterPlot_threshold})
        Me.ScatterGraph_intensityprofile.UseColorGenerator = True
        Me.ScatterGraph_intensityprofile.XAxes.AddRange(New NationalInstruments.UI.XAxis() {Me.XAxis2})
        Me.ScatterGraph_intensityprofile.YAxes.AddRange(New NationalInstruments.UI.YAxis() {Me.YAxis2})
        '
        'ScatterPlot1
        '
        Me.ScatterPlot1.XAxis = Me.XAxis2
        Me.ScatterPlot1.YAxis = Me.YAxis2
        '
        'XAxis2
        '
        Me.XAxis2.Mode = NationalInstruments.UI.AxisMode.Fixed
        Me.XAxis2.Range = New NationalInstruments.UI.Range(0.0R, 656.0R)
        '
        'YAxis2
        '
        Me.YAxis2.MajorDivisions.LabelForeColor = System.Drawing.Color.Black
        Me.YAxis2.MajorDivisions.LabelVisible = False
        '
        'ScatterPlot_background
        '
        Me.ScatterPlot_background.XAxis = Me.XAxis2
        Me.ScatterPlot_background.YAxis = Me.YAxis2
        '
        'ScatterPlot_threshold
        '
        Me.ScatterPlot_threshold.LineColor = System.Drawing.Color.Orange
        Me.ScatterPlot_threshold.LineColorPrecedence = NationalInstruments.UI.ColorPrecedence.UserDefinedColor
        Me.ScatterPlot_threshold.XAxis = Me.XAxis2
        Me.ScatterPlot_threshold.YAxis = Me.YAxis2
        '
        'Timer_intensityprofile
        '
        Me.Timer_intensityprofile.Interval = 500
        '
        'Label14
        '
        resources.ApplyResources(Me.Label14, "Label14")
        Me.Label14.Name = "Label14"
        '
        'GroupBox8
        '
        Me.GroupBox8.Controls.Add(Me.TextBox_temperature)
        Me.GroupBox8.Controls.Add(Me.Label93)
        resources.ApplyResources(Me.GroupBox8, "GroupBox8")
        Me.GroupBox8.Name = "GroupBox8"
        Me.GroupBox8.TabStop = False
        '
        'TextBox_temperature
        '
        Me.TextBox_temperature.BackColor = System.Drawing.Color.PaleGoldenrod
        resources.ApplyResources(Me.TextBox_temperature, "TextBox_temperature")
        Me.TextBox_temperature.Name = "TextBox_temperature"
        Me.TextBox_temperature.ReadOnly = True
        '
        'Label93
        '
        resources.ApplyResources(Me.Label93, "Label93")
        Me.Label93.Name = "Label93"
        '
        'NumericUpDown_grid_origin
        '
        resources.ApplyResources(Me.NumericUpDown_grid_origin, "NumericUpDown_grid_origin")
        Me.NumericUpDown_grid_origin.Maximum = New Decimal(New Integer() {600, 0, 0, 0})
        Me.NumericUpDown_grid_origin.Name = "NumericUpDown_grid_origin"
        Me.NumericUpDown_grid_origin.Value = New Decimal(New Integer() {100, 0, 0, 0})
        '
        'Column1
        '
        Me.Column1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.Column1.Frozen = True
        resources.ApplyResources(Me.Column1, "Column1")
        Me.Column1.Name = "Column1"
        '
        'Column11
        '
        Me.Column11.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader
        Me.Column11.Frozen = True
        resources.ApplyResources(Me.Column11, "Column11")
        Me.Column11.Name = "Column11"
        '
        'Column10
        '
        Me.Column10.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.Column10.Frozen = True
        resources.ApplyResources(Me.Column10, "Column10")
        Me.Column10.Name = "Column10"
        '
        'Column18
        '
        Me.Column18.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.Column18.Frozen = True
        resources.ApplyResources(Me.Column18, "Column18")
        Me.Column18.Name = "Column18"
        '
        'Column19
        '
        Me.Column19.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.Column19.Frozen = True
        resources.ApplyResources(Me.Column19, "Column19")
        Me.Column19.Name = "Column19"
        '
        'meas_cavispeed
        '
        Me.meas_cavispeed.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.meas_cavispeed.Frozen = True
        resources.ApplyResources(Me.meas_cavispeed, "meas_cavispeed")
        Me.meas_cavispeed.Name = "meas_cavispeed"
        '
        'max_cavispeed
        '
        Me.max_cavispeed.Frozen = True
        resources.ApplyResources(Me.max_cavispeed, "max_cavispeed")
        Me.max_cavispeed.Name = "max_cavispeed"
        '
        'Column_pressure
        '
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.692307!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Column_pressure.DefaultCellStyle = DataGridViewCellStyle3
        Me.Column_pressure.Frozen = True
        resources.ApplyResources(Me.Column_pressure, "Column_pressure")
        Me.Column_pressure.Name = "Column_pressure"
        '
        'raw_conductance_kgMPas
        '
        Me.raw_conductance_kgMPas.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.307693!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.raw_conductance_kgMPas.DefaultCellStyle = DataGridViewCellStyle4
        Me.raw_conductance_kgMPas.Frozen = True
        resources.ApplyResources(Me.raw_conductance_kgMPas, "raw_conductance_kgMPas")
        Me.raw_conductance_kgMPas.Name = "raw_conductance_kgMPas"
        '
        'Conductivity_SI_corrT
        '
        Me.Conductivity_SI_corrT.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.692307!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Conductivity_SI_corrT.DefaultCellStyle = DataGridViewCellStyle5
        Me.Conductivity_SI_corrT.Frozen = True
        resources.ApplyResources(Me.Conductivity_SI_corrT, "Conductivity_SI_corrT")
        Me.Conductivity_SI_corrT.Name = "Conductivity_SI_corrT"
        '
        'std_error_LP_integral
        '
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.692307!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.std_error_LP_integral.DefaultCellStyle = DataGridViewCellStyle6
        resources.ApplyResources(Me.std_error_LP_integral, "std_error_LP_integral")
        Me.std_error_LP_integral.Name = "std_error_LP_integral"
        '
        'Column_PLC_integral
        '
        Me.Column_PLC_integral.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        DataGridViewCellStyle7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.692307!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Column_PLC_integral.DefaultCellStyle = DataGridViewCellStyle7
        resources.ApplyResources(Me.Column_PLC_integral, "Column_PLC_integral")
        Me.Column_PLC_integral.Name = "Column_PLC_integral"
        '
        'Note
        '
        Me.Note.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        resources.ApplyResources(Me.Note, "Note")
        Me.Note.Name = "Note"
        '
        'speedclass
        '
        resources.ApplyResources(Me.speedclass, "speedclass")
        Me.speedclass.Name = "speedclass"
        '
        'Column8
        '
        Me.Column8.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        resources.ApplyResources(Me.Column8, "Column8")
        Me.Column8.Name = "Column8"
        '
        'roomtemperature2
        '
        resources.ApplyResources(Me.roomtemperature2, "roomtemperature2")
        Me.roomtemperature2.Name = "roomtemperature2"
        '
        'correctiontemp
        '
        Me.correctiontemp.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader
        resources.ApplyResources(Me.correctiontemp, "correctiontemp")
        Me.correctiontemp.Name = "correctiontemp"
        '
        'Column_temp_factor
        '
        resources.ApplyResources(Me.Column_temp_factor, "Column_temp_factor")
        Me.Column_temp_factor.Name = "Column_temp_factor"
        '
        'Column16
        '
        resources.ApplyResources(Me.Column16, "Column16")
        Me.Column16.Name = "Column16"
        '
        'column9
        '
        resources.ApplyResources(Me.column9, "column9")
        Me.column9.Name = "column9"
        '
        'Column2
        '
        resources.ApplyResources(Me.Column2, "Column2")
        Me.Column2.Name = "Column2"
        '
        'Column3
        '
        resources.ApplyResources(Me.Column3, "Column3")
        Me.Column3.Name = "Column3"
        '
        'Column4
        '
        resources.ApplyResources(Me.Column4, "Column4")
        Me.Column4.Name = "Column4"
        '
        'Column14
        '
        resources.ApplyResources(Me.Column14, "Column14")
        Me.Column14.Name = "Column14"
        '
        'column_deltaP
        '
        resources.ApplyResources(Me.column_deltaP, "column_deltaP")
        Me.column_deltaP.Name = "column_deltaP"
        '
        'Column_calib_optique
        '
        resources.ApplyResources(Me.Column_calib_optique, "Column_calib_optique")
        Me.Column_calib_optique.Name = "Column_calib_optique"
        '
        'Column_meas_MODE
        '
        resources.ApplyResources(Me.Column_meas_MODE, "Column_meas_MODE")
        Me.Column_meas_MODE.Name = "Column_meas_MODE"
        '
        'rotordiameter
        '
        Me.rotordiameter.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        resources.ApplyResources(Me.rotordiameter, "rotordiameter")
        Me.rotordiameter.Name = "rotordiameter"
        '
        'Lp_unit
        '
        resources.ApplyResources(Me.Lp_unit, "Lp_unit")
        Me.Lp_unit.Name = "Lp_unit"
        '
        'Equation_slope
        '
        resources.ApplyResources(Me.Equation_slope, "Equation_slope")
        Me.Equation_slope.Name = "Equation_slope"
        '
        'Equation_intercept
        '
        resources.ApplyResources(Me.Equation_intercept, "Equation_intercept")
        Me.Equation_intercept.Name = "Equation_intercept"
        '
        'Column6
        '
        resources.ApplyResources(Me.Column6, "Column6")
        Me.Column6.Name = "Column6"
        '
        'Column5
        '
        resources.ApplyResources(Me.Column5, "Column5")
        Me.Column5.Name = "Column5"
        '
        'Column_reservoir_crosssection
        '
        resources.ApplyResources(Me.Column_reservoir_crosssection, "Column_reservoir_crosssection")
        Me.Column_reservoir_crosssection.Name = "Column_reservoir_crosssection"
        '
        'branch_area_big_reservoir
        '
        Me.branch_area_big_reservoir.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        resources.ApplyResources(Me.branch_area_big_reservoir, "branch_area_big_reservoir")
        Me.branch_area_big_reservoir.Name = "branch_area_big_reservoir"
        '
        'Column_mean_diameter_big_res_mm
        '
        resources.ApplyResources(Me.Column_mean_diameter_big_res_mm, "Column_mean_diameter_big_res_mm")
        Me.Column_mean_diameter_big_res_mm.Name = "Column_mean_diameter_big_res_mm"
        '
        'Column_mean_diameter_small_res_mm
        '
        resources.ApplyResources(Me.Column_mean_diameter_small_res_mm, "Column_mean_diameter_small_res_mm")
        Me.Column_mean_diameter_small_res_mm.Name = "Column_mean_diameter_small_res_mm"
        '
        'number_of_stem
        '
        resources.ApplyResources(Me.number_of_stem, "number_of_stem")
        Me.number_of_stem.Name = "number_of_stem"
        '
        'AUTOMODE_rawdata_time
        '
        resources.ApplyResources(Me.AUTOMODE_rawdata_time, "AUTOMODE_rawdata_time")
        Me.AUTOMODE_rawdata_time.Name = "AUTOMODE_rawdata_time"
        '
        'AUTOMODE_rawdata_distance_pixel
        '
        resources.ApplyResources(Me.AUTOMODE_rawdata_distance_pixel, "AUTOMODE_rawdata_distance_pixel")
        Me.AUTOMODE_rawdata_distance_pixel.Name = "AUTOMODE_rawdata_distance_pixel"
        '
        'AUTOMODE_rawdata_distance_mm
        '
        resources.ApplyResources(Me.AUTOMODE_rawdata_distance_mm, "AUTOMODE_rawdata_distance_mm")
        Me.AUTOMODE_rawdata_distance_mm.Name = "AUTOMODE_rawdata_distance_mm"
        '
        'branch_diameter_rawdata_upstream
        '
        resources.ApplyResources(Me.branch_diameter_rawdata_upstream, "branch_diameter_rawdata_upstream")
        Me.branch_diameter_rawdata_upstream.Name = "branch_diameter_rawdata_upstream"
        '
        'branch_diameter_rawdata_downstream
        '
        resources.ApplyResources(Me.branch_diameter_rawdata_downstream, "branch_diameter_rawdata_downstream")
        Me.branch_diameter_rawdata_downstream.Name = "branch_diameter_rawdata_downstream"
        '
        'MAIN_Form
        '
        resources.ApplyResources(Me, "$this")
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.SteelBlue
        Me.Controls.Add(Me.NumericUpDown_grid_origin)
        Me.Controls.Add(Me.GroupBox8)
        Me.Controls.Add(Me.Label14)
        Me.Controls.Add(Me.Button_set_grid_origin)
        Me.Controls.Add(Me.TabControl1)
        Me.Controls.Add(Me.ScatterGraph_intensityprofile)
        Me.Controls.Add(Me.Button_AUTO_MANUAL)
        Me.Controls.Add(Me.Label_mouseposition)
        Me.Controls.Add(Me.GroupBox_centrifugecontrol)
        Me.Controls.Add(Me.Label_centri_running)
        Me.Controls.Add(Me.Panel6)
        Me.Controls.Add(Me.ToolStrip1)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Controls.Add(Me.ToolStripContainer1)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.PictureBox_main_image)
        Me.Controls.Add(Me.GroupBox_stopwatch)
        Me.Controls.Add(Me.PictureBox_RESERVOIR_Fill)
        Me.Controls.Add(Me.RichTextBox_MEASMODE_STATUS)
        Me.Controls.Add(Me.PictureBox2)
        Me.Controls.Add(Me.TextBox_time)
        Me.Controls.Add(Me.Label56)
        Me.Controls.Add(Me.Label_gridgraduation70)
        Me.Controls.Add(Me.Label_gridgraduation60)
        Me.Controls.Add(Me.Label_gridgraduation10)
        Me.Controls.Add(Me.Label_gridgraduation20)
        Me.Controls.Add(Me.Label_gridgraduation30)
        Me.Controls.Add(Me.Label_gridgraduation40)
        Me.Controls.Add(Me.Label_gridgraduation50)
        Me.Controls.Add(Me.NumericUpDown_exposureTimeAbs)
        Me.Controls.Add(Me.Label_grid_origin)
        Me.Controls.Add(Me.CheckBox_CAM_connected)
        Me.Controls.Add(Me.PictureBox_rec)
        Me.Controls.Add(Me.Button_save_camera_images)
        Me.Controls.Add(Me.CheckBox_save_camera_image)
        Me.Controls.Add(Me.Label_liveimage)
        Me.Controls.Add(Me.TabControl_main)
        Me.Controls.Add(Me.PictureBox_grid)
        Me.Controls.Add(Me.Panel5)
        Me.Controls.Add(Me.Label_gridgraduation150)
        Me.Controls.Add(Me.Label_gridgraduation120)
        Me.Controls.Add(Me.Label_gridgraduation110)
        Me.Controls.Add(Me.Label_gridgraduation90)
        Me.Controls.Add(Me.Label_gridgraduation80)
        Me.Controls.Add(Me.Label_gridgraduation130)
        Me.Controls.Add(Me.Label_gridgraduation100)
        Me.Controls.Add(Me.Label_gridgraduation140)
        Me.MinimizeBox = False
        Me.Name = "MAIN_Form"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.ToolStripContainer1.ResumeLayout(False)
        Me.ToolStripContainer1.PerformLayout()
        Me.TabControl_main.ResumeLayout(False)
        Me.TabPage3.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox9.ResumeLayout(False)
        Me.GroupBox9.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.Panel_MANUAL_markset.ResumeLayout(False)
        Me.Panel_MANUAL_markset.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.NumericUpDown_detect_speedclass_step, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage1.ResumeLayout(False)
        CType(Me.DataGridView_MAINDATA, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage2.PerformLayout()
        Me.GroupBox7.ResumeLayout(False)
        Me.GroupBox7.PerformLayout()
        CType(Me.NumericUpDown_outlier_LOW_threshold, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDown_outlier_threshold, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ScatterGraph1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox4.ResumeLayout(False)
        CType(Me.DataGridView_RAWDATA, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Chart3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.Panel5.ResumeLayout(False)
        Me.Panel5.PerformLayout()
        Me.GroupBox_pushwheel.ResumeLayout(False)
        Me.GroupBox_pushwheel.PerformLayout()
        CType(Me.LedArray_pushwheel_port1.ItemTemplate, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LedArray_pushwheel_port0.ItemTemplate, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Chart_PLC, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox_integral.ResumeLayout(False)
        Me.GroupBox_integral.PerformLayout()
        CType(Me.NumericUpDown_exposureTimeAbs, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox_stopwatch.ResumeLayout(False)
        Me.GroupBox_stopwatch.PerformLayout()
        CType(Me.NumericUpDown_countdown_sec, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDown_countdown_min, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel6.ResumeLayout(False)
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox_main_image, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox_RESERVOIR_Fill, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox_rec, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox_grid, System.ComponentModel.ISupportInitialize).EndInit()
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.ToolStrip1.ResumeLayout(False)
        Me.ToolStrip1.PerformLayout()
        Me.GroupBox_centrifugecontrol.ResumeLayout(False)
        Me.GroupBox_centrifugecontrol.PerformLayout()
        Me.Panel7.ResumeLayout(False)
        CType(Me.Led_brake, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Led_stop, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Led_start, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage_MANUAL.ResumeLayout(False)
        Me.TabPage_AUTO.ResumeLayout(False)
        Me.TabPage_AUTO.PerformLayout()
        Me.GroupBox6.ResumeLayout(False)
        Me.GroupBox6.PerformLayout()
        CType(Me.NumericUpDown_reservoir_filling_time, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDown_numb_of_replicate, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox_ROI.ResumeLayout(False)
        Me.GroupBox_ROI.PerformLayout()
        CType(Me.NumericUpDown_measuredline_pixel, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDown_downstream_ROI_width_pixel, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDown_upstream_ROI_width_pixel, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox_Integralmethod_AUTOMODE.ResumeLayout(False)
        Me.GroupBox_Integralmethod_AUTOMODE.PerformLayout()
        CType(Me.NumericUpDown_AUTOMODE_integral_stabilisation_time, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDown_AUTOMODE_integral_integration_interval_ms, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDown_AUTOMODE_number_of_rep_integral, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ScatterGraph_intensityprofile, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox8.ResumeLayout(False)
        Me.GroupBox8.PerformLayout()
        CType(Me.NumericUpDown_grid_origin, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents Timer_1sec As System.Windows.Forms.Timer
    Friend WithEvents RichTextBox_centri_tension As System.Windows.Forms.RichTextBox
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents RichTextBox3 As System.Windows.Forms.RichTextBox

    Friend WithEvents SaveFileDialog_main_data As System.Windows.Forms.SaveFileDialog

    Friend WithEvents toolStripSeparator5 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents AideToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TextBox_time As System.Windows.Forms.TextBox
    Friend WithEvents Button_set_LP0_Cochard As System.Windows.Forms.Button
    Friend WithEvents Label30 As System.Windows.Forms.Label
    Friend WithEvents RichTextBox_PLC_Cochard As System.Windows.Forms.RichTextBox
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents richtextbox_std_dev_LP_Cochard As System.Windows.Forms.RichTextBox
    Friend WithEvents Label29 As System.Windows.Forms.Label
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents RichTextBox_mean_inst_LP_Cochard As System.Windows.Forms.RichTextBox
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents TextBox_LP0_Cochard As System.Windows.Forms.TextBox
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label_interval_between_read As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents TextBox_location As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents TextBox_species As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents TextBox_interval_between_read As System.Windows.Forms.TextBox
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents TextBox_campaign_name As System.Windows.Forms.TextBox
    Friend WithEvents DataGridView_MAINDATA As System.Windows.Forms.DataGridView
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents TextBox_crosssectionarea_ofWATER As System.Windows.Forms.TextBox
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents TextBox_branch_crosssection As System.Windows.Forms.TextBox
    Friend WithEvents ComboBox_sampletype As System.Windows.Forms.ComboBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents TextBox_Max_meniscus_sep As System.Windows.Forms.TextBox
    Friend WithEvents Bt_setcavispeed As System.Windows.Forms.Button


    Friend WithEvents CheckBox_tachymeter_connected As System.Windows.Forms.CheckBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents RichTextBox_centri_rpm As System.Windows.Forms.RichTextBox

    Friend WithEvents RichTextBox_MEASMODE_STATUS As System.Windows.Forms.RichTextBox
    Friend WithEvents Label31 As System.Windows.Forms.Label
    Friend WithEvents TextBox_comment As System.Windows.Forms.TextBox

    Friend WithEvents OpenFileDialog_parameter As System.Windows.Forms.OpenFileDialog
    Friend WithEvents SaveFileDialog_parameter As System.Windows.Forms.SaveFileDialog
    Friend WithEvents ToolStripSeparator2 As System.Windows.Forms.ToolStripSeparator

    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage3 As System.Windows.Forms.TabPage
    Friend WithEvents Label33 As System.Windows.Forms.Label
    Friend WithEvents TextBox_sampleref3 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox_sampleref2 As System.Windows.Forms.TextBox
    Friend WithEvents Label34 As System.Windows.Forms.Label
    Friend WithEvents TextBox_sampleref1 As System.Windows.Forms.TextBox
    Public WithEvents TabControl_main As System.Windows.Forms.TabControl
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents TextBox_minmeniscusposition As System.Windows.Forms.TextBox
    Friend WithEvents ToolStripSeparator3 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents NumericUpDown_detect_speedclass_step As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents Label39 As System.Windows.Forms.Label
    Friend WithEvents ComboBox_correction_temp_source As System.Windows.Forms.ComboBox
    Friend WithEvents TextBox_temperature_correction As System.Windows.Forms.TextBox
    Friend WithEvents CheckBox_saverawcavispeed As System.Windows.Forms.CheckBox
    Friend WithEvents SaveFileDialog_cavispeed As System.Windows.Forms.SaveFileDialog
    Friend WithEvents CheckBox_APPEND_DATA_FILES As System.Windows.Forms.CheckBox
    Friend WithEvents OpenFileDialog_APPENDED As System.Windows.Forms.OpenFileDialog

    Friend WithEvents PictureBox_main_image As System.Windows.Forms.PictureBox
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents GroupBox_Cochard As System.Windows.Forms.GroupBox
    Friend WithEvents Label_liveimage As System.Windows.Forms.Label

    Friend WithEvents TextBox_Lp1_Cochard As System.Windows.Forms.TextBox
    Friend WithEvents TextBox_Lp2_Cochard As System.Windows.Forms.TextBox
    Friend WithEvents TextBox_Lp3_Cochard As System.Windows.Forms.TextBox
    Friend WithEvents TextBox_Lp4_Cochard As System.Windows.Forms.TextBox
    Friend WithEvents Label_LP1 As System.Windows.Forms.Label
    Friend WithEvents Label_LP2 As System.Windows.Forms.Label
    Friend WithEvents Label_LP3 As System.Windows.Forms.Label
    Friend WithEvents Label_LP4 As System.Windows.Forms.Label
    Friend WithEvents TextBox_Lp5_Cochard As System.Windows.Forms.TextBox
    Friend WithEvents TextBox_Lp6_Cochard As System.Windows.Forms.TextBox
    Friend WithEvents Label_LP5 As System.Windows.Forms.Label
    Friend WithEvents Label_LP6 As System.Windows.Forms.Label
    Friend WithEvents TextBox_LP7_Cochard As System.Windows.Forms.TextBox
    Friend WithEvents Label_LP7 As System.Windows.Forms.Label
    Friend WithEvents GroupBox_integral As System.Windows.Forms.GroupBox
    Friend WithEvents RichTextBox_PLC_integral As System.Windows.Forms.RichTextBox
    Friend WithEvents TextBox_LP0_integral As System.Windows.Forms.TextBox
    Friend WithEvents RichTextBox_mean_inst_LP_integral As System.Windows.Forms.RichTextBox
    Friend WithEvents Button_set_LP0_integral As System.Windows.Forms.Button
    Friend WithEvents richtextbox_std_dev_LP_integral As System.Windows.Forms.RichTextBox
    Friend WithEvents Label46 As System.Windows.Forms.Label
    Friend WithEvents Label48 As System.Windows.Forms.Label
    Friend WithEvents Label_int2 As System.Windows.Forms.Label
    Friend WithEvents Label50 As System.Windows.Forms.Label
    Friend WithEvents Label_int1 As System.Windows.Forms.Label
    Friend WithEvents Label52 As System.Windows.Forms.Label
    Friend WithEvents TextBox_Lp1_integral As System.Windows.Forms.TextBox
    Friend WithEvents TextBox_Lp2_integral As System.Windows.Forms.TextBox
    Friend WithEvents CheckBox_cochard_method As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox_integral_method As System.Windows.Forms.CheckBox
    Friend WithEvents Timer_camera As System.Windows.Forms.Timer
    Friend WithEvents CheckBox_save_camera_image As System.Windows.Forms.CheckBox
    Friend WithEvents Button_save_camera_images As System.Windows.Forms.Button
    Friend WithEvents PictureBox_rec As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox_grid As System.Windows.Forms.PictureBox
    Friend WithEvents OpenFileDialog_read_data As System.Windows.Forms.OpenFileDialog
    Friend WithEvents CheckBox_CAM_connected As System.Windows.Forms.CheckBox
    Friend WithEvents Button_set_grid_origin As System.Windows.Forms.Button
    Friend WithEvents Label_grid_origin As System.Windows.Forms.Label
    Friend WithEvents NumericUpDown_exposureTimeAbs As System.Windows.Forms.NumericUpDown
    Public WithEvents Label_gridgraduation50 As System.Windows.Forms.Label
    Public WithEvents Label_gridgraduation40 As System.Windows.Forms.Label
    Public WithEvents Label_gridgraduation30 As System.Windows.Forms.Label
    Public WithEvents Label_gridgraduation20 As System.Windows.Forms.Label
    Public WithEvents Label_gridgraduation10 As System.Windows.Forms.Label
    Public WithEvents Label_gridgraduation60 As System.Windows.Forms.Label
    Public WithEvents Label_gridgraduation70 As System.Windows.Forms.Label
    Friend WithEvents Label54 As System.Windows.Forms.Label
    Friend WithEvents Label56 As System.Windows.Forms.Label
    Friend WithEvents Label57 As System.Windows.Forms.Label
    Friend WithEvents FolderBrowserDialog_save_image As System.Windows.Forms.FolderBrowserDialog
    Friend WithEvents DefineSavedImageFolderToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PictureBox_RESERVOIR_Fill As System.Windows.Forms.PictureBox
    Friend WithEvents Label60 As System.Windows.Forms.Label
    Friend WithEvents Label59 As System.Windows.Forms.Label
    Friend WithEvents LinkLabel1 As System.Windows.Forms.LinkLabel
    Friend WithEvents InputSampleDiameterToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RECOMPUTEPLCToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator4 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents OpenDatafileToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Public WithEvents TextBox_max_cavispeed As System.Windows.Forms.TextBox
    Friend WithEvents Chart_PLC As System.Windows.Forms.DataVisualization.Charting.Chart
    Friend WithEvents NEWMEASUREMENTToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Button_update_datagrid As System.Windows.Forms.Button
    Friend WithEvents GroupBox_stopwatch As System.Windows.Forms.GroupBox
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents TextBox_countdown As System.Windows.Forms.TextBox
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents Panel6 As System.Windows.Forms.Panel
    Friend WithEvents RichTextBox1 As System.Windows.Forms.RichTextBox
    Friend WithEvents Label43 As System.Windows.Forms.Label
    Friend WithEvents Label42 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents NumericUpDown_countdown_sec As System.Windows.Forms.NumericUpDown
    Friend WithEvents NumericUpDown_countdown_min As System.Windows.Forms.NumericUpDown


    Friend WithEvents toolStripSeparator7 As System.Windows.Forms.ToolStripSeparator

    Friend WithEvents toolStripSeparator8 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents toolStripSeparator9 As System.Windows.Forms.ToolStripSeparator



    Friend WithEvents ToolStripSeparator6 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripButton_recomputePLC As System.Windows.Forms.ToolStripButton
    Public WithEvents ToolStripButton_activate_MEASUREMENT_mode As System.Windows.Forms.ToolStripButton
    Friend WithEvents TextBox_numberofstem As System.Windows.Forms.TextBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip

    Friend WithEvents PictureBox4 As System.Windows.Forms.PictureBox

    Friend WithEvents LinkLabel_minmeniscusposition As System.Windows.Forms.LinkLabel
    Public WithEvents Label_gridgraduation80 As System.Windows.Forms.Label
    Public WithEvents Label_gridgraduation90 As System.Windows.Forms.Label
    Public WithEvents Label_gridgraduation100 As System.Windows.Forms.Label
    Public WithEvents Label_gridgraduation110 As System.Windows.Forms.Label
    Public WithEvents Label_gridgraduation120 As System.Windows.Forms.Label
    Public WithEvents Label_gridgraduation130 As System.Windows.Forms.Label
    Public WithEvents Label_gridgraduation140 As System.Windows.Forms.Label
    Public WithEvents Label_gridgraduation150 As System.Windows.Forms.Label
    Friend WithEvents Button_recomputespeedclass As System.Windows.Forms.Button
    Friend WithEvents Button_setYaxisrange1 As System.Windows.Forms.Button
    Friend WithEvents Label37 As System.Windows.Forms.Label
    Friend WithEvents TextBox_chart_Yaxis_min As System.Windows.Forms.TextBox
    Friend WithEvents TextBox_chart_Yaxis_max As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Label44 As System.Windows.Forms.Label
    Friend WithEvents Button_default_axisrange As System.Windows.Forms.Button
    Friend WithEvents Button_set_Yaxisrange2 As System.Windows.Forms.Button
    Friend WithEvents Button_set_min_max_Yaxisrange As System.Windows.Forms.Button
    Friend WithEvents Label49 As System.Windows.Forms.Label
    Friend WithEvents Label47 As System.Windows.Forms.Label
    Friend WithEvents Button_centrifugation_pressure_simulation As System.Windows.Forms.Button
    Friend WithEvents TextBox_cavispeed_setvalue As System.Windows.Forms.TextBox
    Friend WithEvents Label_cavispeedcontrolsetvalue As System.Windows.Forms.Label
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents ToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem2 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem_sameparameter As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem_newparameter As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem_openfile As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator10 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripMenuItem_savedata As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem_savedata_as As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem_appenddata As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator11 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripMenuItem_imagefolder As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator13 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripMenuItem_exit As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItemexit_withoutsaving As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem15 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem_branchdiameter As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItemmin_meniscusposition As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem_reinit As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem_reinitchart As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem_reinitgrid As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem_reinitdatalogger As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem_reinitparameter As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem_reinitstddev As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem25 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem_shortcut As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem_about As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripContainer1 As System.Windows.Forms.ToolStripContainer
    Friend WithEvents ToolStrip1 As System.Windows.Forms.ToolStrip
    Friend WithEvents ToolStripButton_newmeas_sameparameter As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripButton_open As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripButton_save As System.Windows.Forms.ToolStripButton
    Friend WithEvents GroupBox_pushwheel As System.Windows.Forms.GroupBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label40 As System.Windows.Forms.Label
    Friend WithEvents TextBox_bin_pushwheel As System.Windows.Forms.TextBox
    Friend WithEvents Label51 As System.Windows.Forms.Label
    Friend WithEvents LedArray_pushwheel_port1 As NationalInstruments.UI.WindowsForms.LedArray
    Friend WithEvents LedArray_pushwheel_port0 As NationalInstruments.UI.WindowsForms.LedArray
    Friend WithEvents ShapeContainer1 As Microsoft.VisualBasic.PowerPacks.ShapeContainer
    Friend WithEvents RectangleShape2 As Microsoft.VisualBasic.PowerPacks.RectangleShape
    Friend WithEvents RectangleShape1 As Microsoft.VisualBasic.PowerPacks.RectangleShape
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents Label61 As System.Windows.Forms.Label
    Friend WithEvents Label55 As System.Windows.Forms.Label
    Friend WithEvents TextBox_set_pressure_fixed As System.Windows.Forms.TextBox
    Friend WithEvents TextBox_set_cavispeed_fixed As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox_centrifugecontrol As System.Windows.Forms.GroupBox
    Friend WithEvents Label_centri_running As System.Windows.Forms.Label
    Friend WithEvents Bt_centri_START_STOP As System.Windows.Forms.Button
    Friend WithEvents Bt_centri_BRAKE As System.Windows.Forms.Button
    Friend WithEvents CheckBox_autobrake As System.Windows.Forms.CheckBox
    Friend WithEvents Label63 As System.Windows.Forms.Label
    Friend WithEvents Label62 As System.Windows.Forms.Label
    Friend WithEvents Led_brake As NationalInstruments.UI.WindowsForms.Led
    Friend WithEvents Led_stop As NationalInstruments.UI.WindowsForms.Led
    Friend WithEvents Led_start As NationalInstruments.UI.WindowsForms.Led
    Friend WithEvents Panel7 As System.Windows.Forms.Panel
    Friend WithEvents Label66 As System.Windows.Forms.Label
    Friend WithEvents Label65 As System.Windows.Forms.Label
    Friend WithEvents Label64 As System.Windows.Forms.Label
    Friend WithEvents Label_mouseposition As System.Windows.Forms.Label
    Friend WithEvents Button_AUTO_MANUAL As System.Windows.Forms.Button
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents TabPage_MANUAL As System.Windows.Forms.TabPage
    Friend WithEvents TabPage_AUTO As System.Windows.Forms.TabPage
    Friend WithEvents Button_Startmeas_AUTOMODE As System.Windows.Forms.Button
    Friend WithEvents GroupBox_ROI As System.Windows.Forms.GroupBox
    Friend WithEvents Button_automatic_ROI As System.Windows.Forms.Button
    Friend WithEvents Label71 As System.Windows.Forms.Label
    Friend WithEvents NumericUpDown_downstream_ROI_width_pixel As System.Windows.Forms.NumericUpDown
    Friend WithEvents NumericUpDown_upstream_ROI_width_pixel As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label72 As System.Windows.Forms.Label
    Friend WithEvents Label74 As System.Windows.Forms.Label
    Friend WithEvents TextBox_downstream_ROI_pixel_origin_value As System.Windows.Forms.TextBox
    Friend WithEvents TextBox_upstream_ROI_pixel_origin_value As System.Windows.Forms.TextBox
    Friend WithEvents Button_downstream_ROI As System.Windows.Forms.Button
    Friend WithEvents GroupBox_Integralmethod_AUTOMODE As System.Windows.Forms.GroupBox
    Friend WithEvents Label70 As System.Windows.Forms.Label
    Friend WithEvents NumericUpDown_AUTOMODE_integral_stabilisation_time As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label69 As System.Windows.Forms.Label
    Friend WithEvents NumericUpDown_AUTOMODE_integral_integration_interval_ms As System.Windows.Forms.NumericUpDown
    Friend WithEvents RichTextBox_AUTOMODE_meanLp_integral As System.Windows.Forms.RichTextBox
    Friend WithEvents RichTextBox_AUTOMODE_stderror_Lp_integral As System.Windows.Forms.RichTextBox
    Friend WithEvents Label68 As System.Windows.Forms.Label
    Friend WithEvents NumericUpDown_AUTOMODE_number_of_rep_integral As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label67 As System.Windows.Forms.Label
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents CheckBox_validateboolean As System.Windows.Forms.CheckBox
    Friend WithEvents Button_validate_meas_AUTOMODE As System.Windows.Forms.Button
    Friend WithEvents CheckBox_display_AUTOMODE As System.Windows.Forms.CheckBox
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents Label82 As System.Windows.Forms.Label
    Friend WithEvents Label80 As System.Windows.Forms.Label
    Friend WithEvents RichTextBox4 As System.Windows.Forms.RichTextBox
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents Chart3 As System.Windows.Forms.DataVisualization.Charting.Chart
    Friend WithEvents GroupBox6 As System.Windows.Forms.GroupBox
    Friend WithEvents Label84 As System.Windows.Forms.Label
    Friend WithEvents NumericUpDown_reservoir_filling_time As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label83 As System.Windows.Forms.Label
    Friend WithEvents NumericUpDown_numb_of_replicate As System.Windows.Forms.NumericUpDown
    Friend WithEvents CheckBox4 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox5 As System.Windows.Forms.CheckBox
    Friend WithEvents DataGridView_RAWDATA As System.Windows.Forms.DataGridView
    Friend WithEvents RichTextBox_pos As System.Windows.Forms.RichTextBox
    Friend WithEvents Timer_getdistance As System.Windows.Forms.Timer
    Friend WithEvents Button_COMPUTE As System.Windows.Forms.Button
    Friend WithEvents Label85 As System.Windows.Forms.Label
    Friend WithEvents NumericUpDown_measuredline_pixel As System.Windows.Forms.NumericUpDown
    Friend WithEvents Column_time As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column_distance As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents distance_mm As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ScatterGraph1 As NationalInstruments.UI.WindowsForms.ScatterGraph
    Friend WithEvents dataplot1 As NationalInstruments.UI.ScatterPlot
    Friend WithEvents XAxis1 As NationalInstruments.UI.XAxis
    Friend WithEvents YAxis1 As NationalInstruments.UI.YAxis
    Friend WithEvents fittedplot1 As NationalInstruments.UI.ScatterPlot
    Friend WithEvents dataplot2 As NationalInstruments.UI.ScatterPlot
    Friend WithEvents fittedplot2 As NationalInstruments.UI.ScatterPlot
    Friend WithEvents dataplot3 As NationalInstruments.UI.ScatterPlot
    Friend WithEvents fittedplot3 As NationalInstruments.UI.ScatterPlot
    Friend WithEvents dataplot4 As NationalInstruments.UI.ScatterPlot
    Friend WithEvents fittedplot4 As NationalInstruments.UI.ScatterPlot
    Friend WithEvents dataplot5 As NationalInstruments.UI.ScatterPlot
    Friend WithEvents fittedplot5 As NationalInstruments.UI.ScatterPlot
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents dataplot6 As NationalInstruments.UI.ScatterPlot
    Friend WithEvents fittedplot6 As NationalInstruments.UI.ScatterPlot
    Friend WithEvents dataplot7 As NationalInstruments.UI.ScatterPlot
    Friend WithEvents fittedplot7 As NationalInstruments.UI.ScatterPlot
    Friend WithEvents dataplot8 As NationalInstruments.UI.ScatterPlot
    Friend WithEvents fittedplot8 As NationalInstruments.UI.ScatterPlot
    Friend WithEvents Button_intensity_profile As System.Windows.Forms.Button
    Friend WithEvents ScatterGraph_intensityprofile As NationalInstruments.UI.WindowsForms.ScatterGraph
    Friend WithEvents ScatterPlot1 As NationalInstruments.UI.ScatterPlot
    Friend WithEvents XAxis2 As NationalInstruments.UI.XAxis
    Friend WithEvents YAxis2 As NationalInstruments.UI.YAxis
    Friend WithEvents Timer_intensityprofile As System.Windows.Forms.Timer
    Friend WithEvents ScatterPlot_background As NationalInstruments.UI.ScatterPlot
    Friend WithEvents ScatterPlot_threshold As NationalInstruments.UI.ScatterPlot
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents Label_graphtype As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents RichTextBox_PLC_automode As System.Windows.Forms.RichTextBox
    Friend WithEvents Label81 As System.Windows.Forms.Label
    Friend WithEvents Label86 As System.Windows.Forms.Label
    Friend WithEvents Label75 As System.Windows.Forms.Label
    Friend WithEvents RichTextBox_Lp0_automode As System.Windows.Forms.RichTextBox
    Friend WithEvents Label87 As System.Windows.Forms.Label
    Friend WithEvents Button8 As System.Windows.Forms.Button
    Friend WithEvents Label88 As System.Windows.Forms.Label
    Friend WithEvents NumericUpDown_outlier_threshold As System.Windows.Forms.NumericUpDown
    Friend WithEvents CheckBox_outlier_reject As System.Windows.Forms.CheckBox
    Friend WithEvents Button_upstream_ROI As System.Windows.Forms.Button
    Friend WithEvents NumericUpDown_outlier_LOW_threshold As System.Windows.Forms.NumericUpDown
    Friend WithEvents GroupBox7 As System.Windows.Forms.GroupBox
    Friend WithEvents Label90 As System.Windows.Forms.Label
    Friend WithEvents Label89 As System.Windows.Forms.Label
    Friend WithEvents Label92 As System.Windows.Forms.Label
    Friend WithEvents Label73 As System.Windows.Forms.Label
    Friend WithEvents Label91 As System.Windows.Forms.Label
    Friend WithEvents Label94 As System.Windows.Forms.Label
    Friend WithEvents GroupBox8 As System.Windows.Forms.GroupBox
    Friend WithEvents TextBox_temperature As System.Windows.Forms.TextBox
    Friend WithEvents Label93 As System.Windows.Forms.Label
    Friend WithEvents Button_RAWDATA_graph_axis_default As System.Windows.Forms.Button
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents ComboBox_operator As System.Windows.Forms.ComboBox
    Friend WithEvents Label32 As System.Windows.Forms.Label
    Friend WithEvents NumericUpDown_grid_origin As System.Windows.Forms.NumericUpDown
    Friend WithEvents GroupBox9 As System.Windows.Forms.GroupBox
    Friend WithEvents Panel_MANUAL_markset As System.Windows.Forms.Panel
    Friend WithEvents Label58 As System.Windows.Forms.Label
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label_meas_voltage As System.Windows.Forms.Label
    Friend WithEvents RichTextBox_measured_voltage As System.Windows.Forms.RichTextBox
    Friend WithEvents Button_EDIT_ADVANCED_PARAMETERS As System.Windows.Forms.Button
    Friend WithEvents ToolStripButton_recompute_PLC As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripMenuItem_appendfile As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Label_SIunit As System.Windows.Forms.Label
    Friend WithEvents Label_cavispeed_acq As System.Windows.Forms.Label
    Friend WithEvents Label_optical_calibration As System.Windows.Forms.Label
    Friend WithEvents Label_optical_source As System.Windows.Forms.Label
    Friend WithEvents Label_rotor_diameter As System.Windows.Forms.Label
    Friend WithEvents Label_rotor_number As System.Windows.Forms.Label
    Friend WithEvents Label_cavitron_name As System.Windows.Forms.Label
    Friend WithEvents Label_temperature_source As System.Windows.Forms.Label
    Friend WithEvents Label_reservoir_area As System.Windows.Forms.Label
    Friend WithEvents ComboBox_cavi1000_reservoirsize As System.Windows.Forms.ComboBox
    Friend WithEvents Label35 As System.Windows.Forms.Label
    Friend WithEvents TextBox_meanupstreamdiameter As System.Windows.Forms.TextBox
    Friend WithEvents Label36 As System.Windows.Forms.Label
    Friend WithEvents Label38 As System.Windows.Forms.Label
    Friend WithEvents Label41 As System.Windows.Forms.Label
    Friend WithEvents Label45 As System.Windows.Forms.Label
    Friend WithEvents TextBox_meandownstreamdiameter As System.Windows.Forms.TextBox
    Friend WithEvents Label53 As System.Windows.Forms.Label
    Friend WithEvents Column1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column11 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column10 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column18 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column19 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents meas_cavispeed As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents max_cavispeed As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column_pressure As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents raw_conductance_kgMPas As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Conductivity_SI_corrT As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents std_error_LP_integral As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column_PLC_integral As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Note As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents speedclass As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents roomtemperature2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents correctiontemp As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column_temp_factor As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column16 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents column9 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column14 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents column_deltaP As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column_calib_optique As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column_meas_MODE As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents rotordiameter As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Lp_unit As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Equation_slope As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Equation_intercept As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column_reservoir_crosssection As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents branch_area_big_reservoir As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column_mean_diameter_big_res_mm As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column_mean_diameter_small_res_mm As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents number_of_stem As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents AUTOMODE_rawdata_time As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents AUTOMODE_rawdata_distance_pixel As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents AUTOMODE_rawdata_distance_mm As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents branch_diameter_rawdata_upstream As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents branch_diameter_rawdata_downstream As System.Windows.Forms.DataGridViewTextBoxColumn


End Class
