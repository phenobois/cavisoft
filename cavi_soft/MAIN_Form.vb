
'###############################################################################################
'CAVISOFT v 5.2.1
' July 2018

' (c)R�gis Burlett (regis.burlett@u-bordeaux.fr)
' Universit� Bordeaux , UMR BIOGECO

'################################################################################################

Option Explicit On


Imports System.Net.Mail
Imports NationalInstruments.DAQmx

Imports System.Runtime.InteropServices

Imports PylonC
Imports PylonC.NET 'PylonC_MD_VC80.dll
Imports System.Drawing
Imports System.Drawing.Imaging
Imports System.Drawing.Drawing2D
Imports System.Windows.Forms.DataVisualization.Charting
Imports System.Globalization
Imports System.Reflection
'Imports Microsoft.VisualBasic.PowerPacks
Imports System.IO
Imports System.Windows.Forms




Public Class MAIN_Form

    Private Property AUTOMODE_interval_integration_line_boolean As Boolean


    '''
    <DllImport("KERNEL32.DLL", EntryPoint:="Beep", SetLastError:=True, _
    CharSet:=CharSet.Unicode, ExactSpelling:=True, _
    CallingConvention:=CallingConvention.StdCall)> _
    Public Shared Function _
       aBeep(ByVal dwFreq As Integer, ByVal dwDuration As Integer) _
         As Boolean
        ' Leave the body of the function empty.
        ' This is the configuration for the system sound 
    End Function


    '--------------------------------------------------------------------------------
    Dim cavisoft_version As String = "version 5.2.1"

    Private Meas_mode_activated As Boolean = False
    Public is_MAINFORM_INIT As Boolean = False

    '--------------------------------------------------------------------------------
    ' CAVISPEED 
    Public set_Cavispeed_rpm As Integer
    Public read_Cavispeed_rpm As Integer
    Dim cavispeed_rad_s_1 As Double
    Public centri_tension As Double      'Tension related to centrifugation force in MPa
    Dim set_centri_tension As Double

    Dim max_cavispeed_rad_s_1 As Double
    Public max_Cavispeed_rpm As Double
    Dim max_centri_tension As Double

    Dim buffered_cavispeed_rpm As Double
    Dim stored_buffered_cavispeed_rpm As Double
    Dim crosssectionarea_OF_WATER As Double
    Public centri_Start As Boolean
    Public centri_Stop As Boolean
    Public centri_Brake As Boolean
    '--------------------------------------------------------------------------------
    'AUTOMODE VARIABLES
    Dim integral_equation_result As Double

    Dim MAX_downstream = 0
    Dim MAX_upstream = 0
    Dim interval_integration_line As Integer = 100
    Dim downstream_ROI_is_define As Boolean
    Dim upstream_ROI_is_define As Boolean
    Dim StdDev_AUTOMODE As Double
    Dim std_err_lp_automode As Double
    Dim PLC_automod As Double
    Dim is_Lp0set_AUTOMODE As Boolean
    Dim measured_line_pixel As Integer = 250

    Dim arrData_dist As Double()
    Dim arrData_time As Double()
    Dim lock_intensityprofile As Boolean = False
    Dim equation_array_time As Double()
    Dim equation_array_ditance_mm As Double()
    Public buffered_ComboBox_set_cavispeed As String

    Dim AUTOMODE_DATA_ARRAY_string As String
    Dim AUTOMODE_DATA_ARRAY As Double()

    Public rep As Integer = 0
    Dim rawdata_time_string As String
    Dim rawdata_distance_pixel_string As String
    Dim rawdata_distance_mm_string As String

    Public mean_diameter_big_res As Double
    Public mean_diameter_small_res As Double

    Dim avg_background_image_intensity As Integer
    Dim AUTOMODE_meanLp_integral As Double
    '--------------------------------------------------------------------------------
    ' COCHARD COMPUTATION variables
    'Equation 3 from Cochard et all 2002 PCE
    Dim F_star As Double ' water volume flowrate estimate
    Public cross_section_area As Double = 0 'cross section area

    Dim delta_P_star As Double ' mean pressure difference between r1 & r2 for computation with F_star
    Dim delta_P As Double  ' mean pressure difference between max r & min r 
    Dim r1, r2 As Double
    Public rho As Double = 1000 'density of water (1000 kg.m-3)


    

    Dim LP_SIunit As Double

    Dim PLC As Double
    Dim raw_LP_integral As Double
    Dim raw_AUTOMODE_meanLp_integral As Double
    Dim raw_AUTOMODE_meanLp_corrT As Double

    Public ComboBox_set_cavispeed_boolean As Boolean = False

    '--------------------------------------------------------------------------------
    ' INTEGRAL COMPUTATION variables
    'Equation from Launay and Inchauspe all 2010 '

    Dim LP_integral As Double
    Dim LP_integral_SIunit As Double
    Dim LP_corr_integral As Double
    Dim time_integral As Double
    Dim r1_integral As Double
    Dim r2_integral As Double
    Dim t1_integral As Double
    Dim t2_integral As Double

    Dim stopwatch4 As New Stopwatch
    Dim stopwatch5 As New Stopwatch

    Dim stopwatch_AUTOMODE_MEAS As New Stopwatch

    Dim start_timer_index_2 As Boolean
    Dim sum_mean_integral As Boolean
    Dim is_LP0_set_integral As Boolean
    Dim graph_PLC_serie_cochard As Boolean

    Dim m As Long
    Dim test_parite As Long
    Dim mean_Lp_integral As Double
    Dim variance_integral As Double
    Dim standard_deviation_integral As Double
    Dim std_err_lp_integral As Double
    Dim sum_variance_new As Double
    Dim PLC_integral As Double
    Public slope, intercept, residue, tolerance, equ_pearsonRsquare, a, b As Double

    Dim Lp2_LP_mean_integral As Double
    Dim Lp3_LP_mean_integral As Double
    Dim Lp4_LP_mean_integral As Double
    Dim Lp5_LP_mean_integral As Double
    Dim Lp6_LP_mean_integral As Double
    Dim Lp7_LP_mean_integral As Double

    Dim stopwatch_cochard As New Stopwatch
    Dim stopwatch_integral As New Stopwatch
    '--------------------------------------------------------------------------------
    'Ethernet Camera GIGE BASLER scA640

    Dim Num_devices As Long
    Dim hdev As PYLON_DEVICE_HANDLE

    Dim Grab_buffer As PylonBuffer(Of Byte)
    Dim Grab_result As PylonC.NET.PylonGrabResult_t

    Dim filename_save_image As String
    Dim start_count As Integer
    Dim save_camera_images As Boolean
    Dim button_saving_images_is_on_start As Boolean

    Dim bmap_width As Integer = 656
    Dim bmap_height As Integer = 494

    Dim is_cam_initialazed As Boolean

    Public optical_source As String

    Public max_pos_downstream = 0
    '--------------------------------------------------------------------------------
    ' TEMPERATURE variables

    Dim cavi_Temp1 As Double
    Dim cavi_Temp2 As Double
    Dim plate_IR_temp As Double
    Dim correction_Temp As Double ' select which temp is used for correstion of Lp in parameter tab
    Dim temperature_correction_factor As Double
    '---------------------------------------------------------------------
    ' Measure Mode variables

    Dim AUTO_mode_boolean As Boolean


    '--------------------------------------------------------------------------------
    'FILE MANAGEMENT VARIABLE
    Dim header As String
    Dim SAVED_data_list As String
    Dim read_data_filename As String
    Dim save_data_filename As String
    Dim parameter_filename As String
    Dim folder_save_image As String
    Dim parameter_values As String
    Dim parameter_header As String
    Dim read_parameter_string As String
    Dim param_array() As String
    Dim cavispeed_raw_data_filename As String
    Dim APPEND_filename As String

    Dim none As String

    Dim same_parameter_boolean As Boolean

    Dim data_to_save As String
    Dim data_to_read As String


    Dim datafile_extension As String


    ' ''''"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
    'GENERAL PARAMETERS STORED IN MY.SETTINGS
    Public cavitron_name As String
    Public rotor_name As String
    Public rotor_diameter As Double              'in cm

    Public DAQ_offset_ai0 As Double
    Public DAQ_offset_ai1 As Double
    Public DAQ_gain_ai0 As Double
    Public DAQ_gain_ai1 As Double
    Public RESERVOIR_FILLING_lowlevel_mV As Single
    Public RESERVOIR_FILLING_highlevel_mV As Single

    Public bt_START_lowlevel_mV As Single
    Public bt_START_highlevel_mV As Single

    Public Sorvallfrequencyoutput_gain_counter As Double
    Public Sorvallfrequencyoutput_offset_counter As Double

    Public centrifuge_model As String

    ' ''''"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
    '---------------------------------------------------------------------
    ' PARAMETERS & MISC

    Dim meas_MODE As String

    Dim operator_array() As String

    Dim species As String
    Dim lieu_prelevement As String
    Dim sample_ref1 As String
    Dim sample_ref2 As String
    Dim sample_ref3 As String
    Dim sample_type As String
    Dim treeage As String

    Dim operator_name As String
    Dim measurement_date As DateTime
    Dim campaign_name As String
    Dim rawdata As Type

    

    Public speedclass_detect_step As Double
    Public min_meniscus_position_pixel As Double  ' in pixel

    Public max_meniscus_interval As Double ' in graduation
    Public interval_between_read As Double ' in graduation
    Public cavi_grad_offset As Double ' offset of the graduation ruler (depend on the cavitron)

    Public reservoir_area As Double


   
    '  Public branch_diameter_bigreservoir As Double
    '  Public branch_diameter_smallreservoir As Double

    Public max__possible_cavispeed_rpm As Double
    Public max__possible_tension_MPa As Double


    Public displayformat As String

    Dim IS_stopwatch_launched As Boolean
    Dim add_sumLp As Boolean
    Dim Button_manual As Boolean
    Dim is_LP0_set As Boolean
    Dim ctrl As Boolean = False

    Dim speed_class As String
    Dim speed_class_ASCII_index As Double

    Dim calib_optic_grid As Double 'number of mm for 50 graduations on the grid = 350 pixel
    Public PIXEL_SIZE_mmperpixel As Double

    Dim start_timer_index As Boolean

    Dim i As Long  'loop
    Dim k As Long   'loop
    Dim j As Long 'loop
    Dim l As Long 'loop
    Dim auto_meas_loop As Integer = -999

    Public dataArray_port0() As Boolean = New Boolean(7) {"False", "False", "False", "False", "False", "False", "False", "False"}
    Public dataArray_port1() As Boolean = New Boolean(7) {"False", "False", "False", "False", "False", "False", "False", "False"}

    Dim stopwatch1 As New Stopwatch
    Dim stopwatch2 As New Stopwatch
    Dim stopwatch3 As New Stopwatch

    Dim countdown_set_min As Int16
    Dim countdown_set_sec As Int16
    Dim countdown_settime As Int32
    Dim live_countdown As Int32

    '---------------------------------------------------------------------
    'grid variables

    Dim num_ligne As Integer
    Dim num_colonne As Integer
    Dim graduation As Integer
    Dim graduation_size As Double
    Dim grande_graduation As Integer
    Dim grande_graduation_size As Integer
    Dim hauteur_camera As Double
    Dim offset_grid_line As Integer
    Dim image_regle As New Bitmap(656, 494, PixelFormat.Format24bppRgb)
    Dim image_regle_location As Point

    Dim coord1 As Point
    Dim coord2 As Point
    Dim coord_grid_origin As Point
    Dim dist_cal_pixel As Integer
    Dim picturebox3_doubleclick_result_select As String

    Dim pixel_grid_start As Long = 100
    Public grid_Color As Color

    '---------------------------------------------------------------------
    'KEY DEFINITION

    Dim START_MEAS_key As System.Windows.Forms.Keys
    Dim MEASUREMENT_key As System.Windows.Forms.Keys
    Dim VALIDATE_MEASUREMENT_key As System.Windows.Forms.Keys
    Dim SET_CAVISPEED_key As System.Windows.Forms.Keys
    Dim SET_LP0_key As System.Windows.Forms.Keys
    Dim SELECT_DATA_TAB_key As System.Windows.Forms.Keys
    Dim SELECT_GRAPH_TAB_key As System.Windows.Forms.Keys
    Dim SELECT_AUTOMODE_TAB_key As System.Windows.Forms.Keys
    Dim SELECT_PARAMETER_TAB_key As System.Windows.Forms.Keys
    Dim RECOMPUTE_STD_DEV_key As System.Windows.Forms.Keys
    Dim call_min_meniscus_dialogbox_key As System.Windows.Forms.Keys
    Dim copy_key As System.Windows.Forms.Keys
    Dim call_recompute_PLC_menu As System.Windows.Forms.Keys
    Dim FILL_RESERVOIR_key As System.Windows.Forms.Keys


    '---------------------------------------------------------------------
    ' PUSHWHEELL VARIABLE
    Dim DIGITAL_OUTPUT_cavispeed_relay_controlport0_task As Task = Nothing
    Dim DIGITAL_OUTPUT_cavispeed_relay_controlport1_task As Task = Nothing
   

    '-------------------------------------------------------------------------------------
    'definition des variables necessaires pour le fonctionnement du DAQ NI_USB6008
    Dim DAQ_task As Task
    Dim count_task As Task
    Dim RESERVOIR_FILLING_task As Task
    Dim is_counter_init As Boolean = False
    Dim Daq_reader As AnalogMultiChannelReader
    Dim DO_writer As DAQmx.DigitalSingleChannelWriter
    Dim DAQ_data() As Double
    Dim FILL_RESERVOIR_BOOLEAN As Boolean

   
    Public DAQ_offset_ai2 As Double = 0
    Public DAQ_offset_ai3 As Double = 0
    Public DAQ_offset_ai4 As Double = 0
    Public DAQ_offset_ai5 As Double = 0
    Public DAQ_offset_ai6 As Double = 0
    Public DAQ_offset_ai7 As Double = 0

   
    Public DAQ_gain_ai2 As Double = 1
    Public DAQ_gain_ai3 As Double = 1
    Public DAQ_gain_ai4 As Double = 1
    Public DAQ_gain_ai5 As Double = 1
    Public DAQ_gain_ai6 As Double = 1
    Public DAQ_gain_ai7 As Double = 1

   

    Private myCICounterReader As CounterReader
    Dim pulse_count As Int32
    Dim cavispeed_DAQ_channel As Double = 0

    Public ai0_maximumValue_volt As Double = 1
    Public ai0_minimumValue_volt As Double = 0

    Dim is_NIUSB6008_init As Boolean = False

    Dim is_NIUSB_DO_init As Boolean = False

    Public graphetype_string As String = "PLC_vs_pres"

    '-------------------------------------------------------------------------------------
    'definition des variables necessaires pour le fonctionnement du tachymetre RS232
    Dim RS232_tachymeter_HEX_value As String
    Dim buffer_RS232_tachymeter As Byte
    '-------------------------------------------------------------------------------------
    ' Variable used for Temperature DAQ with ethernet NI 
    Dim NumericUpDown_temp_number_value As Double

    Public thermocoupleType As AIThermocoupleType
    Dim temp1_Task As New Task
    Dim temp2_Task As New Task
    Dim temp3_Task As New Task
    Dim temp4_Task As New Task

    Dim runningTask As Task

    Dim temp1_reader As AnalogMultiChannelReader
    Dim temp2_reader As AnalogMultiChannelReader
    Dim temp3_reader As AnalogMultiChannelReader
    Dim temp4_reader As AnalogMultiChannelReader

    Dim channel0 As AIChannel
    Dim channel1 As AIChannel
    Dim channel2 As AIChannel
    Dim channel3 As AIChannel

    Public minimum_temp_value As Double
    Public maximum_temp_value As Double


   

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        '    SplashScreen1.Show()

        init()

        ' TextBox_campaign_name.Text = My.Settings.operator_index.ToString
        '  Me.ComboBox_plate_number.SelectedIndex = My.Settings.plate_index

        'Me.ComboBox_cavitron_numb.SelectedIndex = My.Settings.cavitron_index

        '  Me.ComboBox_cavispeed.SelectedIndex = My.Settings.cavispeed_index
        ' Me.ComboBox_operator.SelectedIndex = My.Settings.operator_index
        '  End Sub
        ''Declaration
        'Public Overridable Function GetSatelliteAssembly(ByVal culture As CultureInfo) As Assembly

    End Sub

    'End Function
    Private Sub init()

        CAVITRON_SETUP.LOAD_MY_SETTINGS()
        CAVITRON_SETUP.SYNC_MY_SETTINGS()


        CheckBox_save_camera_image.Hide()
        Button_save_camera_images.Hide()


        stopwatch1.Reset()
        stopwatch2.Reset()
        IS_stopwatch_launched = False
        is_LP0_set = False
        Button_manual = True


        '  CheckBox_integral_method.Checked = True
        GroupBox_integral.Enabled = True
        '   disable_integral()

        '     speed_class_ASCII_index = 96 'correspond to the ASCII code of the letter before "a" - NB on first test it will turn to "a"

        read_constant()

        save_data_filename = none
        read_data_filename = none

        parameter_filename = none
        filename_save_image = none
        folder_save_image = none

        APPEND_filename = none

        set_shortcut_keys()

        '   init_datalogger()
        'Debug.Print(AboutBox1.LabelVersion.ToString)
        '      refresh_graph()


      
        '     define_interval_label()

        j = l = 0

        PictureBox_RESERVOIR_Fill.Visible = False


        fill_combobox_operator()

        'Dialog_DAQ.set_to_default()

        '  ComboBox_optic.SelectedIndex = 1


        temperature_textbox_mgmt()

        RichTextBox_MEASMODE_STATUS.Select()
        displayformat = "0.#####"

        pixel_grid_start = NumericUpDown_grid_origin.Value
        dist_cal_pixel = 350
        ' draw_grid()
        speedclass_determination()


        'RESERVOIR_FILLING_lowlevel_mV = 0
        'RESERVOIR_FILLING_highlevel_mV = 5000

        'bt_START_lowlevel_mV = 0
        'bt_START_highlevel_mV = 5000

        'ai0_maximumValue_volt = 10
        'ai0_minimumValue_volt = 0



        '  Me.TextBox_campaign_name.Text = My.Settings.cavitron_index
        '''''''''''''''''
        Try

            'NI6008 (Dev 1) START/STOP/BRAKE = 0 
            Dim DIGITAL_OUTPUT_cavispeed_relay_channel_start_task As Task = Nothing
            DIGITAL_OUTPUT_cavispeed_relay_channel_start_task = New Task()
            DIGITAL_OUTPUT_cavispeed_relay_channel_start_task.DOChannels.CreateChannel("Dev1/Port0/line0:7", "port0", _
                  ChannelLineGrouping.OneChannelForAllLines)
            Dim writerchannel_start As New DigitalSingleChannelWriter(DIGITAL_OUTPUT_cavispeed_relay_channel_start_task.Stream)
            Dim dataArray_dev1port1() As Boolean = New Boolean(7) {"false", "false", "false", "false", "false", "false", "false", "false"}
            writerchannel_start.WriteSingleSampleMultiLine(True, dataArray_dev1port1)
            DIGITAL_OUTPUT_cavispeed_relay_channel_start_task.Dispose()
        Catch e As Exception
            MsgBox(e.Message)
        End Try
        '''''''''''''''''''''''''

        Digital_output_cavispeed_pushwheel_control()
        pushwheel_value_ZERO_VALUE()

        define_fileextension()

        '  ComboBox_set_cavispeed.SelectedIndex = 1 ' default is pressure set mode



        '    ComboBox_temperature_measurement.SelectedIndex = 1
        automode_SET()
        Chart3.ChartAreas(0).AxisY.LabelStyle.Format = "0"
        is_MAINFORM_INIT = True
        ' MsgBox("INIT = OK")
    End Sub
    Sub read_constant()

        ' LECTURE DES CONSTANTES
        species = TextBox_species.Text
        lieu_prelevement = TextBox_location.Text
        sample_ref1 = TextBox_sampleref1.Text
        sample_ref2 = TextBox_sampleref2.Text
        sample_ref3 = TextBox_sampleref3.Text
        sample_type = ComboBox_sampletype.Text
        treeage = TextBox_comment.Text
        operator_name = ComboBox_operator.Text



        If TextBox_Max_meniscus_sep.Text <> "" Then
            max_meniscus_interval = TextBox_Max_meniscus_sep.Text
        End If

        If TextBox_interval_between_read.Text <> "" Then
            interval_between_read = TextBox_interval_between_read.Text
        End If

        reservoir_area = CAVITRON_SETUP.ComboBox_reservoir_innersurface.Text



        If TextBox_campaign_name.Text <> "" Then
            campaign_name = TextBox_campaign_name.Text
        End If
        measurement_date = System.DateTime.Now

        '----
        speedclass_detect_step = NumericUpDown_detect_speedclass_step.Value
        'calib_optic_grid = TextBox7.Text

        '-----------------
        ' Define meas MODE
        If AUTO_mode_boolean = True Then
            If CheckBox_CAM_connected.Checked = True Then
                meas_MODE = "AUTO_CAMERA"
                '     ComboBox_optic.SelectedIndex = 1
            Else
                meas_MODE = "AUTO_BINO"
                '   ComboBox_optic.SelectedIndex = 2
            End If
        Else

            If CheckBox_CAM_connected.Checked = True Then
                meas_MODE = "MANUAL_CAMERA"
                '     ComboBox_optic.SelectedIndex = 1
            Else
                meas_MODE = "MANUAL_BINO"
                '   ComboBox_optic.SelectedIndex = 2
            End If
        End If
        countdown_set_min = NumericUpDown_countdown_min.Value
        countdown_set_sec = NumericUpDown_countdown_sec.Value

        crosssectionarea_OF_WATER = reservoir_area - cross_section_area
        TextBox_crosssectionarea_ofWATER.Text = crosssectionarea_OF_WATER
        temperature_correction_factor = (1.781018648 - 0.05871294 * correction_Temp + 0.00130376457 * correction_Temp ^ 2 - 0.00001801864802 * correction_Temp ^ 3 + 0.00000011235431235 * correction_Temp ^ 4)


        max__possible_cavispeed_rpm = Convert.ToDouble(CAVITRON_SETUP.TextBox_maxcentri_rpm.Text)

        max__possible_tension_MPa = -0.25 * 1000 * (2 * System.Math.PI * max__possible_cavispeed_rpm / 60) ^ 2 * ((rotor_diameter / 2 / 100) ^ 2 + ((rotor_diameter / 2 / 100) - (min_meniscus_position_pixel * PIXEL_SIZE_mmperpixel / 1000)) ^ 2) / 1000000

        If CAVITRON_SETUP.ComboBox_centrifugemodel.Text = "CAVI 1000" Then
            ComboBox_cavi1000_reservoirsize.Visible = True
        Else
            ComboBox_cavi1000_reservoirsize.Visible = False
        End If

    End Sub

    Sub refresh_graph()

        Chart_PLC.Update()

    End Sub

    Sub change_chart_range()

        Chart_PLC.ChartAreas(0).AxisY.Maximum = TextBox_chart_Yaxis_max.Text
        Chart_PLC.ChartAreas(0).AxisY.Minimum = TextBox_chart_Yaxis_min.Text
        '    Chart_PLC.ChartAreas(1).AxisY.Maximum = 120
        '   Chart_PLC.ChartAreas(1).AxisY.Minimum = -40

    End Sub

    Sub init_datalogger()

        ' INIT DATALOGGER NI USB6008-----------------------------------------------------

        '  If ComboBox_cavispeed.Text <> "" Then

        '   If ComboBox_cavispeed.Text = "NI_USB_6008 channel ai0" Then

        Try

            'correspond au DAQ NI-USB6008

            Dim IN_Channels() As String = {"Dev1/ai0", "Dev1/ai1", "Dev1/ai2", "Dev1/ai3", "Dev1/ai4", "Dev1/ai5", "Dev1/ai6", "Dev1/ai7"}
            Dim OUT_Channels() As String = {"Dev1/ao0", "Dev1/ao1"}


            '---------------------------------------
            'Cr�ation des canaux de mesure pour le DAQ
            DAQ_task = New Task

            'config DAQ channel for tachymeter
            DAQ_task.AIChannels.CreateVoltageChannel( _
                physicalChannelName:=IN_Channels(0), _
                nameToAssignChannel:="", _
                terminalConfiguration:=AITerminalConfiguration.Differential, _
                minimumValue:=ai0_minimumValue_volt, _
                maximumValue:=ai0_maximumValue_volt, _
                units:=AIVoltageUnits.Volts _
            )
            '  config DAQ channel for EE08
            DAQ_task.AIChannels.CreateVoltageChannel( _
                physicalChannelName:=IN_Channels(1), _
                nameToAssignChannel:="", _
                terminalConfiguration:=AITerminalConfiguration.Differential, _
                minimumValue:=0, _
                maximumValue:=10, _
                units:=AIVoltageUnits.Volts _
            )
            '---------------------------------------


        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)

        End Try

        'Cr�ation du Reader associ� � la l'objet Task d�fini ci dessus
        Daq_reader = New AnalogMultiChannelReader(DAQ_task.Stream)

        is_NIUSB6008_init = True
        '  End If
        'DAQ_offset_ai0 = Dialog5.TextBox_offset_ai0.Text
        'DAQ_offset_ai1 = Dialog5.TextBox_offset_ai1.Text
        'DAQ_offset_ai2 = Dialog5.TextBox_offset_ai2.Text
        'DAQ_offset_ai3 = Dialog5.TextBox_offset_ai3.Text
        'DAQ_offset_ai4 = Dialog5.TextBox_offset_ai4.Text
        'DAQ_offset_ai5 = Dialog5.TextBox_offset_ai5.Text
        'DAQ_offset_ai6 = Dialog5.TextBox_offset_ai6.Text
        'DAQ_offset_ai7 = Dialog5.TextBox_offset_ai6.Text

        'DAQ_gain_ai0 = Dialog5.TextBox_gain_ai0.Text
        'DAQ_gain_ai1 = Dialog5.TextBox_gain_ai1.Text
        'DAQ_gain_ai2 = Dialog5.TextBox_gain_ai2.Text
        'DAQ_gain_ai3 = Dialog5.TextBox_gain_ai3.Text
        'DAQ_gain_ai4 = Dialog5.TextBox_gain_ai4.Text
        'DAQ_gain_ai5 = Dialog5.TextBox_gain_ai5.Text
        'DAQ_gain_ai6 = Dialog5.TextBox_gain_ai6.Text
        'DAQ_gain_ai7 = Dialog5.TextBox_gain_ai7.Text







        '  End If

    End Sub

    Public Sub Digital_output_cavispeed_pushwheel_control()

        If centrifuge_model = "Sorvall RC serie" And CAVITRON_SETUP.ComboBox_cavispeed_setup.SelectedIndex <> 2 Then
            'if NOT CAVI1000 or cavimedium

            Try
                DIGITAL_OUTPUT_cavispeed_relay_controlport0_task = New Task()
                DIGITAL_OUTPUT_cavispeed_relay_controlport1_task = New Task()

                DIGITAL_OUTPUT_cavispeed_relay_controlport0_task.DOChannels.CreateChannel("Dev2/port0", "port0", _
                       ChannelLineGrouping.OneChannelForAllLines)
                DIGITAL_OUTPUT_cavispeed_relay_controlport1_task.DOChannels.CreateChannel("Dev2/port1", "port1", _
                      ChannelLineGrouping.OneChannelForAllLines)
                is_NIUSB_DO_init = True

            Catch exception As DaqException
                MessageBox.Show(exception.Message)
                is_NIUSB_DO_init = False
            End Try

        End If

    End Sub

    Public Sub init_cam()


        If optical_source = "BASLER sca640" Then
            Try

                Environment.SetEnvironmentVariable("PYLON_GIGE_HEARTBEAT", "300000")
                Pylon.Initialize()

                Num_devices = Pylon.EnumerateDevices
                hdev = Pylon.CreateDeviceByIndex(0)

                start_count = 0
                Timer_camera.Enabled = False
                is_cam_initialazed = True
                Label_liveimage.Visible = True

            Catch e As Exception

                MsgBox(e.ToString, MsgBoxStyle.Exclamation)
                CheckBox_CAM_connected.Checked = False
            End Try


        End If



    End Sub

    Public Sub fill_combobox_operator()

        Try
            ' read_operator_string = My.Computer.FileSystem.ReadAllText(My.Resources.operator_name_list, System.Text.Encoding.Default).ToString
            ComboBox_operator.Items.Clear()
            operator_array = CAVITRON_SETUP.RichTextBox_operator_list.Text.Split(",")
            For i = 0 To operator_array.Length - 1
                ComboBox_operator.Items.Add(operator_array(i))

            Next
            ComboBox_operator.Text = operator_array(0)

            ' TextBox1.Text = operator_array(2)

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try

        '   ComboBox_operator.Items.AddRange(operator_array)
    End Sub

    Public Sub ethernet_camera_acquisition()
        ' Acquisition of the image
        start_count = start_count + 1

        If Num_devices = 0 Then

            Throw New Exception("No devices found")

        End If

        If optical_source = "BASLER sca640" Then

            Try

                Grab_result = Nothing

                Grab_buffer = Nothing

                If Pylon.DeviceGrabSingleFrame(hdev, 0, Grab_buffer, Grab_result, 500) = True Then

                    Grab_result.PayloadType = NET.EPylonPayloadType.PayloadType_Image
                    '     TextBox_branch_diam.Text = Grab_buffer.Array(150).ToString

                End If

                'Check to see if the image was grabbed successfully.

                If Grab_result.Status = NET.EPylonGrabStatus.Grabbed Then


                    If folder_save_image = "none" Then

                        FolderBrowserDialog_save_image.ShowDialog()
                        folder_save_image = FolderBrowserDialog_save_image.SelectedPath

                    Else

                        filename_save_image = folder_save_image & "\" & sample_ref1 & "_" & sample_ref2 & "_" & sample_ref3 & "_" & max_Cavispeed_rpm & "_" & start_count & ".png"
                        ' "C:\Cavisoft_image\" & campaign_name & "\" & start_count & ".png"
                    End If
                    BitmapFast()

                ElseIf Grab_result.Status = NET.EPylonGrabStatus.Failed Then

                    '  Debug.Print("Frame {0} wasn't grabbed successfully.  Error code = {1}" & i + 1, Grab_result.ErrorCode)

                End If

            Catch ex As Exception

                MsgBox(ex.ToString, MsgBoxStyle.Exclamation)

                ' Debug.Print("EXCEPTION")

            End Try
        End If

    End Sub
    Public Sub BitmapFast()
        ' Display the image of the camera

        If optical_source = "BASLER sca640" Then


            Dim bmpData As New BitmapData
            Dim bmap As New Bitmap(bmap_width, bmap_height, PixelFormat.Format8bppIndexed)

            Dim rect As New Rectangle(0, 0, bmap_width, bmap_height)


            bmpData = bmap.LockBits(rect, System.Drawing.Imaging.ImageLockMode.ReadWrite, bmap.PixelFormat)

            bmap.Palette = GetGrayScalePalette()

            Marshal.Copy(Grab_buffer.Array, 0, bmpData.Scan0, Grab_buffer.Array.Length)


            bmap.UnlockBits(bmpData)

            If save_camera_images = True Then

                bmap.Save(filename_save_image, ImageFormat.Png)

            End If

            PictureBox_main_image.Image = bmap
        End If
    End Sub
    Public Function GetGrayScalePalette()

        Dim bmp As Bitmap = New Bitmap(1, 1, Imaging.PixelFormat.Format8bppIndexed)

        Dim monoPalette As ColorPalette = bmp.Palette

        Dim entries() As Color = monoPalette.Entries

        Dim i As Integer
        For i = 0 To 256 - 1 Step i + 1
            entries(i) = Color.FromArgb(i, i, i)
        Next

        Return monoPalette

    End Function

    Public Sub Start_camera_acquisition()
        If is_cam_initialazed = True Then

            If optical_source = "BASLER sca640" Then

                If Pylon.DeviceIsOpen(hdev) = False Then



                    Pylon.DeviceOpen(hdev, Pylon.cPylonAccessModeControl Or Pylon.cPylonAccessModeStream)
                    'size must be a multiple of 4 
                    Pylon.DeviceSetIntegerFeature(hdev, "Width", 656)
                    Pylon.DeviceSetFloatFeature(hdev, "ExposureTimeAbs", NumericUpDown_exposureTimeAbs.Value)


                    ethernet_camera_acquisition()

                    Timer_camera.Enabled = True

                Else

                    ethernet_camera_acquisition()

                    Timer_camera.Enabled = True

                End If

            End If

            CheckBox_save_camera_image.Show()
            PictureBox_rec.Hide()
            If AUTO_mode_boolean = False Then
                draw_grid()
            Else
                automatic_determination_of_ROI()
            End If
        End If

    End Sub
    Public Sub Stop_camera_acquisition()
        If is_cam_initialazed = True Then

            If optical_source = "BASLER sca640" Then

                Try
                    If Pylon.DeviceIsOpen(hdev) = True Then

                        Pylon.DeviceClose(hdev)

                        Pylon.DestroyDevice(hdev)

                        Pylon.Terminate()

                        Timer_camera.Enabled = False

                        start_count = 0

                        CheckBox_save_camera_image.Hide()
                        Button_save_camera_images.Hide()


                    End If
                Catch e As Exception
                End Try
            End If
        End If
        Label_liveimage.Visible = False

    End Sub
    Public Sub Save_camera_acquisitions()

        If CheckBox_save_camera_image.Checked = True Then

            If m = 1 Or i = 1 Then

                button_saving_images_is_on_start = True

            ElseIf m = 7 Or i = 7 Then

                button_saving_images_is_on_start = False

            End If

            If button_saving_images_is_on_start = True Then

                save_camera_images = True
                Button_save_camera_images.Text = "Stop saving images"

            Else

                save_camera_images = False
                Button_save_camera_images.Text = "Start saving images"

            End If

        End If

    End Sub

    Public Sub draw_grid()
        pixel_grid_start = NumericUpDown_grid_origin.Value
        'Dim offset As Long

        image_regle_location.X = 0
        image_regle_location.Y = 0
        offset_grid_line = 0

        graduation = 0
        '  grande_graduation = 0
        '   grande_graduation_size = 10

        dist_cal_pixel = 350
        graduation_size = Int(dist_cal_pixel / 50)

        calib_optic_grid = PIXEL_SIZE_mmperpixel * 350

        '------------------------------------------------------------------------
        'generate a blue  background
        For num_colonne = 0 To bmap_width - 1

            ' graduation = graduation + 1

            For num_ligne = 0 To bmap_height - 1

                image_regle.SetPixel(num_colonne, num_ligne, Color.Blue)

            Next
        Next
        '------------------------------------------------------------------------
        'Draw a big vertical line at the origin of the grid

        For num_colonne = pixel_grid_start - 1 To pixel_grid_start
            For num_ligne = offset_grid_line + 170 To offset_grid_line + 280

                image_regle.SetPixel(num_colonne, num_ligne, Color.Red)


            Next
        Next
        '------------------------------------------------------------------------
        Dim grad_type_index As Long
        Dim grad_number As Long

        Label_gridgraduation10.Visible = False
        Label_gridgraduation20.Visible = False
        Label_gridgraduation30.Visible = False
        Label_gridgraduation40.Visible = False
        Label_gridgraduation50.Visible = False
        Label_gridgraduation60.Visible = False
        Label_gridgraduation70.Visible = False
        Label_gridgraduation80.Visible = False
        Label_gridgraduation90.Visible = False
        Label_gridgraduation100.Visible = False
        Label_gridgraduation110.Visible = False
        Label_gridgraduation120.Visible = False
        Label_gridgraduation130.Visible = False
        Label_gridgraduation140.Visible = False
        Label_gridgraduation150.Visible = False


        For num_colonne = pixel_grid_start To bmap_width - 1

            image_regle.SetPixel(num_colonne, offset_grid_line + 225, Color.Red)

            'If graduation = graduation_size Then


            '    If num_colonne > pixel_grid_start Then

            graduation = graduation + 1

            '        If grande_graduation = 0 Then
            '            ' offset = num_colonne - 100
            If (graduation = graduation_size) Then ' Or (num_colonne = pixel_grid_start) Then
                grad_type_index = grad_type_index + 1
                grad_number = grad_number + 1



                If (grad_type_index Mod 10 = 0) Then  'Or (num_colonne = pixel_grid_start) Then

                    For num_ligne = offset_grid_line + 190 To offset_grid_line + 260

                        image_regle.SetPixel(num_colonne, num_ligne, Color.Red)
                        '          If num_colonne < PictureBox3.Width Then

                        Select Case grad_number
                            Case "10"
                                Label_gridgraduation10.Parent = PictureBox_grid
                                Label_gridgraduation10.Location = New Point(num_colonne - 13, 190 + offset_grid_line - 20)
                                Label_gridgraduation10.Visible = True
                            Case "20"
                                Label_gridgraduation20.Parent = PictureBox_grid
                                Label_gridgraduation20.Location = New Point(num_colonne - 13, 190 + offset_grid_line - 20)
                                Label_gridgraduation20.Visible = True
                            Case "30"
                                Label_gridgraduation30.Parent = PictureBox_grid
                                Label_gridgraduation30.Location = New Point(num_colonne - 13, 190 + offset_grid_line - 20)
                                Label_gridgraduation30.Visible = True
                            Case "40"
                                Label_gridgraduation40.Parent = PictureBox_grid
                                Label_gridgraduation40.Location = New Point(num_colonne - 13, 190 + offset_grid_line - 20)
                                Label_gridgraduation40.Visible = True
                            Case ("50")
                                Label_gridgraduation50.Parent = PictureBox_grid
                                Label_gridgraduation50.Location = New Point(num_colonne - 13, 190 + offset_grid_line - 20)
                                Label_gridgraduation50.Visible = True
                            Case ("60")
                                Label_gridgraduation60.Parent = PictureBox_grid
                                Label_gridgraduation60.Location = New Point(num_colonne - 13, 190 + offset_grid_line - 20)
                                Label_gridgraduation60.Visible = True
                            Case ("70")
                                Label_gridgraduation70.Parent = PictureBox_grid
                                Label_gridgraduation70.Visible = True
                                Label_gridgraduation70.Location = New Point(num_colonne - 13, 190 + offset_grid_line - 20)
                            Case ("80")
                                Label_gridgraduation80.Parent = PictureBox_grid
                                Label_gridgraduation80.Visible = True
                                Label_gridgraduation80.Location = New Point(num_colonne - 13, 190 + offset_grid_line - 20)
                            Case ("90")
                                Label_gridgraduation90.Parent = PictureBox_grid
                                Label_gridgraduation90.Visible = True
                                Label_gridgraduation90.Location = New Point(num_colonne - 13, 190 + offset_grid_line - 20)
                            Case ("100")
                                Label_gridgraduation100.Parent = PictureBox_grid
                                Label_gridgraduation100.Visible = True
                                Label_gridgraduation100.Location = New Point(num_colonne - 15, 190 + offset_grid_line - 20)
                            Case ("110")
                                Label_gridgraduation110.Parent = PictureBox_grid
                                Label_gridgraduation110.Visible = True
                                Label_gridgraduation110.Location = New Point(num_colonne - 15, 190 + offset_grid_line - 20)
                            Case ("120")
                                Label_gridgraduation120.Parent = PictureBox_grid
                                Label_gridgraduation120.Visible = True
                                Label_gridgraduation120.Location = New Point(num_colonne - 15, 190 + offset_grid_line - 20)
                            Case ("130")
                                Label_gridgraduation130.Parent = PictureBox_grid
                                Label_gridgraduation130.Visible = True
                                Label_gridgraduation130.Location = New Point(num_colonne - 15, 190 + offset_grid_line - 20)
                            Case ("140")
                                Label_gridgraduation140.Parent = PictureBox_grid
                                Label_gridgraduation140.Visible = True
                                Label_gridgraduation140.Location = New Point(num_colonne - 15, 190 + offset_grid_line - 20)
                            Case ("150")
                                Label_gridgraduation150.Parent = PictureBox_grid
                                Label_gridgraduation150.Visible = True
                                Label_gridgraduation150.Location = New Point(num_colonne - 15, 190 + offset_grid_line - 20)
                        End Select


                    Next

                Else
                    'Else
                    '  offset = num_colonne - 100

                    If (grad_type_index Mod 5 = 0) And (grad_type_index Mod 10 <> 0) Then
                        For num_ligne = offset_grid_line + 200 To offset_grid_line + 250
                            image_regle.SetPixel(num_colonne, num_ligne, Color.Red)
                        Next

                    Else
                        For num_ligne = offset_grid_line + 215 To offset_grid_line + 245
                            image_regle.SetPixel(num_colonne, num_ligne, Color.Red)
                        Next

                    End If

                End If
                graduation = 0


                For i = 0 To 6

                    If grad_type_index = (max_meniscus_interval - i * interval_between_read) Then
                        For num_ligne = offset_grid_line + 200 To offset_grid_line + 300
                            image_regle.SetPixel(num_colonne, num_ligne, Color.Orange)
                        Next
                    End If

                Next

            End If

            ' graduation = 0
            'End If

            'grande_graduation = grande_graduation + 1

            'If grande_graduation = grande_graduation_size Then

            '    grande_graduation = 0

            'End If

            '    End If



            'End If



        Next
        'ASSIGN the grid image to Picture box 3 (superpose to camera picture box)
        PictureBox_grid.BackColor = Color.Transparent
        image_regle.MakeTransparent(Color.Blue)
        PictureBox_grid.Parent = PictureBox_main_image
        PictureBox_grid.Location = image_regle_location
        PictureBox_grid.Image = image_regle


        grad_type_index = 0
    End Sub

    Sub erase_grid()

        ' For i = 0 To bmap_width - 1
        'For j = 0 To bmap_height - 1
        PictureBox_grid.BackColor = Color.Transparent
        image_regle.MakeTransparent(Color.Red)
        image_regle.MakeTransparent(Color.Green)
        image_regle.MakeTransparent(Color.Orange)
        Label_gridgraduation10.Visible = False
        Label_gridgraduation20.Visible = False
        Label_gridgraduation30.Visible = False
        Label_gridgraduation40.Visible = False
        Label_gridgraduation50.Visible = False
        Label_gridgraduation60.Visible = False
        Label_gridgraduation70.Visible = False



        PictureBox_grid.Parent = PictureBox_main_image
        PictureBox_grid.Location = image_regle_location
        PictureBox_grid.Image = image_regle
        Debug.Print("erase grid")
        '    Next
        '   Next


    End Sub






    Public Sub speedclass_determination()
        '----------------------------------------------------------------------------------
        'DETERMINATION OF SPEED CLASS
        ' Test if change in cavispeed is bigger than parameter-defined step. Increase speedclass value if required
        Dim i_speedclass As Int16

        If DataGridView_MAINDATA.RowCount > 1 Then

            DataGridView_MAINDATA.Rows((0)).Cells("speedclass").Value = "a"

            For i_speedclass = 1 To DataGridView_MAINDATA.RowCount - 2

                If (System.Math.Abs(DataGridView_MAINDATA.Rows(i_speedclass).Cells("max_cavispeed").Value - DataGridView_MAINDATA.Rows(i_speedclass - 1).Cells("max_cavispeed").Value) < speedclass_detect_step) Then
                    DataGridView_MAINDATA.Rows((i_speedclass)).Cells("speedclass").Value = DataGridView_MAINDATA.Rows((i_speedclass - 1)).Cells("speedclass").Value

                Else
                    speed_class_ASCII_index = Asc(DataGridView_MAINDATA.Rows(i_speedclass - 1).Cells("speedclass").Value) + 1
                    speed_class = Chr(speed_class_ASCII_index)
                    DataGridView_MAINDATA.Rows((i_speedclass)).Cells("speedclass").Value = speed_class
                End If
            Next

        Else
            speed_class = Chr(97) ' assign letter 'a' to first speed class
        End If

        'If DataGridView1.RowCount > 1 Then

        '    If CheckBox_tachymeter_connected.Checked = False Then

        '        If (Math.Abs(buffered_cavispeed_rpm - Cavispeed_rpm) < speedclass_detect_step) Then
        '            speed_class = DataGridView1.Rows((DataGridView1.Rows.Count - 2)).Cells("speedclass").Value

        '        Else
        '            speed_class_ASCII_index = Asc(DataGridView1.Rows((DataGridView1.Rows.Count - 2)).Cells("speedclass").Value) + 1
        '            speed_class = Chr(speed_class_ASCII_index)

        '        End If
        '    Else

        '        If (Math.Abs(stored_buffered_cavispeed_rpm - max_Cavispeed_rpm) < speedclass_detect_step) Then
        '            speed_class = DataGridView1.Rows((DataGridView1.Rows.Count - 2)).Cells("speedclass").Value
        '        Else
        '            speed_class_ASCII_index = Asc(DataGridView1.Rows((DataGridView1.Rows.Count - 2)).Cells("speedclass").Value) + 1
        '            speed_class = Chr(speed_class_ASCII_index)
        '        End If

        '    End If

        'Else
        '    speed_class = Chr(97) ' assign letter 'a' to first speed class
        'End If
        ''----------------------------------------------------------------------------------


    End Sub



    Sub set_shortcut_keys()

        START_MEAS_key = Keys.R


        MEASUREMENT_key = Keys.Space

        SET_CAVISPEED_key = Keys.S
        SET_LP0_key = Keys.NumPad0

        SELECT_DATA_TAB_key = Keys.NumPad8
        SELECT_AUTOMODE_TAB_key = Keys.NumPad9
        SELECT_PARAMETER_TAB_key = Keys.NumPad7

        FILL_RESERVOIR_key = Keys.Down

        ' Nested in a + CTRL loop ...................................
        call_min_meniscus_dialogbox_key = Keys.P
        VALIDATE_MEASUREMENT_key = Keys.V
        copy_key = Keys.C
        call_recompute_PLC_menu = Keys.W
        RECOMPUTE_STD_DEV_key = Keys.D
        '............................................................
    End Sub



    Private Sub fill_reservoir()

        '  If ComboBox_cavispeed.Text = "NI_USB_6008" Then   'correspond au DAQ NI-USB6008
        'verification de la validit� des d�finitions des canaux associ�s � la Task
        If is_NIUSB6008_init = False Then
            init_datalogger()
            MsgBox("No Input output were found... Please check your connexions")

        Else



            Try

                RESERVOIR_FILLING_task = New Task()
                RESERVOIR_FILLING_task.AOChannels.CreateVoltageChannel( _
                "Dev1/ao0", _
                    "aoChannel", _
                    0, _
                    5, _
                    AOVoltageUnits.Volts)





                If FILL_RESERVOIR_BOOLEAN = True Then

                    Dim writer As AnalogSingleChannelWriter = New AnalogSingleChannelWriter(RESERVOIR_FILLING_task.Stream)
                    writer.WriteSingleSample(True, 5)

                Else
                    Dim writer As AnalogSingleChannelWriter = New AnalogSingleChannelWriter(RESERVOIR_FILLING_task.Stream)
                    writer.WriteSingleSample(True, Convert.ToDouble(RESERVOIR_FILLING_lowlevel_mV) / 1000)
                End If


            Catch ex As Exception

                MsgBox(ex.Message, MsgBoxStyle.Exclamation)
                CheckBox_tachymeter_connected.Checked = False
                CAVITRON_SETUP.ComboBox_cavispeed_setup.Text = ""
                PictureBox_RESERVOIR_Fill.Visible = False
            End Try

        End If

    End Sub

    Private Sub get_plate_temperature()


        If CAVITRON_SETUP.ComboBox_temperature_measurement.SelectedIndex = 1 Then   'correspond to dev1/ai1
            '0V=> -40�C
            '10V=> 80�C 
            'verification de la validit� des d�finitions des canaux associ�s � la Task
            If is_NIUSB6008_init = False Then
                init_datalogger()
            End If
            Try
                DAQ_task.Control(TaskAction.Verify)

                DAQ_data = Daq_reader.ReadSingleSample

                cavi_Temp1 = Format((DAQ_gain_ai0 * DAQ_data(0)) + DAQ_offset_ai0, "0.##")
                TextBox_temperature.Text = cavi_Temp1
                ' plate_IR_temp = Format((0.1112 * 1000 * DAQ_data(1)) - 18, "0.#")

            Catch ex As Exception

                MsgBox(ex.Message, MsgBoxStyle.Exclamation)
                '  CheckBox_tachymeter_connected.Checked = False
                '  ComboBox_cavispeed.Text = ""

            End Try
        End If

        If CAVITRON_SETUP.ComboBox_temperature_measurement.SelectedIndex = 2 Then   'correspond to dev1/ai1
            '0V=> -40�C
            '10V=> 80�C 
            'verification de la validit� des d�finitions des canaux associ�s � la Task
            If is_NIUSB6008_init = False Then
                init_datalogger()
            End If
            Try
                DAQ_task.Control(TaskAction.Verify)

                DAQ_data = Daq_reader.ReadSingleSample

                cavi_Temp1 = Format((DAQ_gain_ai1 * DAQ_data(1)) + DAQ_offset_ai1, "0.##")
                TextBox_temperature.Text = cavi_Temp1
                ' plate_IR_temp = Format((0.1112 * 1000 * DAQ_data(1)) - 18, "0.#")

            Catch ex As Exception

                MsgBox(ex.Message, MsgBoxStyle.Exclamation)
                '  CheckBox_tachymeter_connected.Checked = False
                '  ComboBox_cavispeed.Text = ""

            End Try
        End If

        If CAVITRON_SETUP.ComboBox_temperature_measurement.SelectedIndex = 3 Then   'correspond to air sensor cavi 1000
            '0V=> -40�C
            '10V=> 80�C 
            'verification de la validit� des d�finitions des canaux associ�s � la Task
            If is_NIUSB6008_init = False Then
                init_datalogger()
            End If
            Try
                DAQ_task.Control(TaskAction.Verify)

                DAQ_data = Daq_reader.ReadSingleSample

                cavi_Temp1 = Format((DAQ_gain_ai1 * DAQ_data(1)) + DAQ_offset_ai1, "0.##")
                TextBox_temperature.Text = cavi_Temp1
                ' plate_IR_temp = Format((0.1112 * 1000 * DAQ_data(1)) - 18, "0.#")

            Catch ex As Exception
                CAVITRON_SETUP.ComboBox_temperature_measurement.SelectedIndex = 0
                MsgBox(ex.Message, MsgBoxStyle.Exclamation)
                '  CheckBox_tachymeter_connected.Checked = False
                '  ComboBox_cavispeed.Text = ""

            End Try
        End If
    End Sub

    Private Sub get_cavispeed_rpm()

        'Si oui, lecture du datalogger, si non, lecture de la valeur manuelle dans text box
        'test sur la Check box "tachym�tre connect�"
        If CheckBox_tachymeter_connected.Checked Then

            Select Case CAVITRON_SETUP.ComboBox_cavispeed_acquisiton.Text
                '---------------------------------------------------------------------------------------
                Case "NI_USB_6008 channel ai0"
                    'correspond au DAQ NI-USB6008 
                    'verification de la validit� des d�finitions des canaux associ�s � la Task
                    Try
                        DAQ_task.Control(TaskAction.Verify)
                        If is_NIUSB6008_init = False Then
                            init_datalogger()
                        End If

                        DAQ_data = Daq_reader.ReadSingleSample()

                        read_Cavispeed_rpm = Format(DAQ_gain_ai0 * DAQ_data(cavispeed_DAQ_channel) + DAQ_offset_ai0, "0")

                        RichTextBox_centri_rpm.Text = read_Cavispeed_rpm.ToString

                    Catch ex As Exception
                        centri_Start = False
                        RichTextBox_MEASMODE_STATUS.Select()

                        CheckBox_tachymeter_connected.Checked = False
                        CAVITRON_SETUP.ComboBox_cavispeed_acquisiton.Text = ""

                        MsgBox(ex.Message, MsgBoxStyle.Exclamation)
                    End Try

                    '---------------------------------------------------------------------------------------

                    '---------------------------------------------------------------------------------------
                Case "Sorvall RC5C+ frequency read" ' use sorvall direct frequency output. 
                    ' !!!!! The output is not direct !!!!!! USE CALIBRATION 
                    Try
                        If is_counter_init = True Then


                            Dim pulse_val As Int32
                            Dim time_count As Int32


                            pulse_count = myCICounterReader.ReadSingleSampleInt32

                            count_task.Stop()
                            stopwatch3.Stop()

                            pulse_val = pulse_count
                            time_count = stopwatch3.ElapsedMilliseconds
                            read_Cavispeed_rpm = Format(((((pulse_val / (time_count / 1000)) * Sorvallfrequencyoutput_gain_counter) - Sorvallfrequencyoutput_offset_counter) / 10), "#0") * 10   'ratio pulse/time arrondi � 10rpm pr�s

                            '  If Math.Abs(Cavispeed_rpm - RichTextBox_centri_rpm.Text) > 100 Then
                            RichTextBox_centri_rpm.Text = read_Cavispeed_rpm.ToString
                            'End If


                            stopwatch3.Reset()

                            count_task.Dispose()

                        End If

                        count_task = New Task()

                        count_task.CIChannels.CreateCountEdgesChannel("Dev1/ctr0", "Count Edges", _
                        CICountEdgesActiveEdge.Falling, 0, CICountEdgesCountDirection.Up)

                        myCICounterReader = New CounterReader(count_task.Stream)


                        count_task.Start()
                        stopwatch3.Start()


                        is_counter_init = True

                        '---------------------------------------------------------------------------------------
                    Catch e As Exception

                        CAVITRON_SETUP.ComboBox_cavispeed_acquisiton.Text = ""
                        centri_Start = False
                        RichTextBox_MEASMODE_STATUS.Select()
                        MsgBox(e.Message)
                    End Try


                Case ""

                    read_Cavispeed_rpm = RichTextBox_centri_rpm.Text
                Case "CAVI 1000"

                    DAQ_data = Daq_reader.ReadSingleSample
                    RichTextBox_measured_voltage.Text = DAQ_data(3)
            End Select

            '------------------------------------------------------------------------------------ ---
        Else

            read_Cavispeed_rpm = RichTextBox_centri_rpm.Text
        End If
        If CheckBox_saverawcavispeed.Checked = True Then
            If cavispeed_raw_data_filename <> "" Then
                My.Computer.FileSystem.WriteAllText(cavispeed_raw_data_filename, measurement_date & "," & read_Cavispeed_rpm.ToString & "," & cavi_Temp1.ToString & "," & AUTOMODE_meanLp_integral & Chr(13), True)
                ' TextBox_Lp1.Text = stopwatch3.ElapsedMilliseconds.ToString
            End If
        End If
        '---------------------------------------------------------------------------------------

        compute_cavispeed()
        If CAVITRON_SETUP.ComboBox_cavispeed_setup.SelectedIndex = 1 And CheckBox_tachymeter_connected.Checked = False Then
            compute_rotation_speed()
        Else
            compute_pressure()
        End If
        '---------------------------------------------------------------------------------------

        If read_Cavispeed_rpm < max__possible_cavispeed_rpm Then
            If max_Cavispeed_rpm < read_Cavispeed_rpm Then
                max_Cavispeed_rpm = read_Cavispeed_rpm
            End If
        End If

        TextBox_max_cavispeed.Text = max_Cavispeed_rpm.ToString

    End Sub

    Private Sub compute_cavispeed()

        cavispeed_rad_s_1 = 2 * System.Math.PI * read_Cavispeed_rpm / 60
        max_cavispeed_rad_s_1 = 2 * System.Math.PI * max_Cavispeed_rpm / 60
        delta_P = 0.5 * rho * cavispeed_rad_s_1 ^ 2 * ((rotor_diameter / 200) ^ 2 - (rotor_diameter / 200 - (TextBox_Max_meniscus_sep.Text / 10000)) ^ 2) / 1000000

    End Sub

    Private Sub compute_pressure()

        'Cochard 2002, �quation (3)
        ' P en MPa
        centri_tension = -0.25 * 1000 * cavispeed_rad_s_1 ^ 2 * ((rotor_diameter / 2 / 100) ^ 2 + ((rotor_diameter / 2 / 100) - (min_meniscus_position_pixel * PIXEL_SIZE_mmperpixel / 1000)) ^ 2) / 1000000
        RichTextBox_centri_tension.Text = Format(centri_tension, "##0.000")

        max_centri_tension = -0.25 * 1000 * max_cavispeed_rad_s_1 ^ 2 * ((rotor_diameter / 2 / 100) ^ 2 + ((rotor_diameter / 2 / 100) - (min_meniscus_position_pixel * PIXEL_SIZE_mmperpixel / 1000)) ^ 2) / 1000000



    End Sub

    Private Sub update_datagrid()

        read_constant()
        For i = 0 To DataGridView_MAINDATA.RowCount - 2
            DataGridView_MAINDATA.Rows(i).Cells("Column_pressure").Value = Format(-0.25 * 1000 * (2 * System.Math.PI * DataGridView_MAINDATA.Rows(i).Cells("max_cavispeed").Value / 60) ^ 2 * ((rotor_diameter / 2 / 100) ^ 2 + ((rotor_diameter / 2 / 100) - (min_meniscus_position_pixel * PIXEL_SIZE_mmperpixel / 1000)) ^ 2) / 1000000, "##0.000")
            DataGridView_MAINDATA.Rows(i).Cells("Column11").Value = campaign_name
            DataGridView_MAINDATA.Rows(i).Cells("Column10").Value = sample_ref1
            DataGridView_MAINDATA.Rows(i).Cells("Column18").Value = sample_ref2
            DataGridView_MAINDATA.Rows(i).Cells("Column19").Value = sample_ref3
            DataGridView_MAINDATA.Rows(i).Cells("Column16").Value = operator_name
            DataGridView_MAINDATA.Rows(i).Cells("Column2").Value = species
            DataGridView_MAINDATA.Rows(i).Cells("Column3").Value = TextBox_location.Text
            DataGridView_MAINDATA.Rows(i).Cells("Column4").Value = sample_type
            DataGridView_MAINDATA.Rows(i).Cells("Column14").Value = treeage
            DataGridView_MAINDATA.Rows(i).Cells("number_of_stem").Value = TextBox_numberofstem.Text

        Next

        speedclass_determination()
        Dialog_compute_PLC.ShowDialog()

    End Sub

    Private Sub compute_rotation_speed()


        If ComboBox_set_cavispeed_boolean = True Then

            centri_tension = RichTextBox_centri_tension.Text

            'Cochard 2002, �quation (3)
            ' P en MPa
            ' centri_tension = -0.25 * 1000 * cavispeed_rad_s_1 ^ 2 * ((rotor_diameter / 2 / 100) ^ 2 + ((rotor_diameter / 2 / 100) - (min_meniscus_position_grad * (calib_optic_grid / 50) / 1000)) ^ 2) / 1000000


            ' If TextBox_minmeniscusposition.Text = "" Then
            min_meniscus_position_pixel = 0
            '  Else
            '  min_meniscus_position = TextBox_minmeniscusposition.Text
            ' End If


            cavispeed_rad_s_1 = ((centri_tension) / (-0.25 * 1000 * ((rotor_diameter / 2 / 100) ^ 2 + ((rotor_diameter / 2 / 100) - (min_meniscus_position_pixel * PIXEL_SIZE_mmperpixel / 1000)) ^ 2) / 1000000)) ^ 0.5

            read_Cavispeed_rpm = cavispeed_rad_s_1 * 60 / 2 / System.Math.PI



            RichTextBox_centri_rpm.Text = Format(read_Cavispeed_rpm, "##0")

            '   max_centri_tension = -0.25 * 1000 * max_cavispeed_rad_s_1 ^ 2 * ((rotor_diameter / 2 / 100) ^ 2 + ((rotor_diameter / 2 / 100) - (min_meniscus_position * (calib_optic_grid / 50) / 10000)) ^ 2) / 1000000

        End If

    End Sub



    Private Sub compute_Lp_integral()

        r1_integral = max_meniscus_interval * (calib_optic_grid / 50) / 1000
        r2_integral = (max_meniscus_interval - ((m - 1) * interval_between_read)) * (calib_optic_grid / 50) / 1000

        time_integral = t1_integral

        'CALCUL DE LA CONDUCTANCE (EN mm.MPa-1.s-1)

        LP_integral = (System.Math.Log(((2 * (rotor_diameter / 2 / 100) / r2_integral) - 1) / ((2 * (rotor_diameter / 2 / 100) / r1_integral) - 1)) / ((rotor_diameter / 2 / 100) * rho * cavispeed_rad_s_1 ^ 2 * (time_integral))) * 10 ^ 9

        'CORRECTION EN TEMPERATURE

        If CAVITRON_SETUP.CheckBox_useSIunit.Checked = False Then
            LP_integral_SIunit = LP_integral
        Else
            Select Case CAVITRON_SETUP.ComboBox_Lp_SIunit_select.Text
                Case "kg.m-1.Mpa-1.s-1"
                    LP_integral_SIunit = Format(rho * (rotor_diameter / 100) * LP_integral / cross_section_area * (crosssectionarea_OF_WATER) / 1000, "0.#####")
                Case "mol.Mpa-1.s-1"
                    LP_integral_SIunit = Format(rho * (rotor_diameter / 100) * LP_integral / cross_section_area * (crosssectionarea_OF_WATER) / 1000 / 0.01801528, "0.#####")
                Case "m�.Mpa-1.s-1"

                    LP_integral_SIunit = Format((rotor_diameter / 100) * LP_integral / cross_section_area * (crosssectionarea_OF_WATER) / 1000, "0.00 E-00").ToString
            End Select


        End If

        LP_corr_integral = LP_integral_SIunit * (1.781018648 - 0.05871294 * correction_Temp + 0.00130376457 * correction_Temp ^ 2 - 0.00001801864802 * correction_Temp ^ 3 + 0.00000011235431235 * correction_Temp ^ 4)

        'AFFICHAGE LP_CORR_NEW

        display_LP_integral()

    End Sub
    Private Sub compute_std_dev_integral()

        'Lp2_LP_mean_new = (Textbox_LP_new2.Text) ^ 2
        'Lp3_LP_mean_new = (Textbox_LP_new3.Text) ^ 2
        'Lp4_LP_mean_new = (Textbox_LP_new4.Text) ^ 2
        'Lp5_LP_mean_new = (Textbox_LP_new5.Text) ^ 2
        'Lp6_LP_mean_new = (Textbox_LP_new6.Text) ^ 2
        'Lp7_LP_mean_new = (Textbox_LP_new7.Text) ^ 2

        sum_variance_new = sum_variance_new + (LP_corr_integral) ^ 2

        variance_integral = sum_variance_new * (1 / (m - 1)) - (mean_Lp_integral) ^ 2

        standard_deviation_integral = variance_integral ^ 0.5
        std_err_lp_integral = standard_deviation_integral / (m - 1) ^ 0.5
        '  Debug.Print(LP_integral)

        richtextbox_std_dev_LP_integral.Text = Format(std_err_lp_integral, displayformat)

    End Sub
    Private Sub display_LP_integral()


        If m = 1 Then

            TextBox_Lp1_integral.Text = "Start"

        End If

        If m = 2 Then

            TextBox_Lp2_integral.Text = Format(LP_corr_integral, displayformat)

        End If

        If m = 3 Then

            TextBox_Lp2_integral.Text = Format(LP_corr_integral, displayformat)


        End If

        If m = 4 Then

            TextBox_Lp2_integral.Text = Format(LP_corr_integral, displayformat)

            If sum_mean_integral = True Then

                Lp4_LP_mean_integral = LP_corr_integral

                m = 2

                meanLp_integral()

                compute_std_dev_integral()

                sum_mean_integral = False

                m = 4

            End If
            If is_LP0_set_integral = False Then

                TextBox_LP0_integral.Text = TextBox_Lp2_integral.Text
                is_LP0_set_integral = True
            End If
        End If

        If m = 5 Then

            TextBox_Lp2_integral.Text = Format(LP_corr_integral, displayformat)

            If sum_mean_integral = True Then

                Lp5_LP_mean_integral = LP_corr_integral

                m = 3

                meanLp_integral()

                compute_std_dev_integral()

                sum_mean_integral = False

                m = 5

            End If

        End If

        If m = 6 Then

            TextBox_Lp2_integral.Text = Format(LP_corr_integral, displayformat)

            If sum_mean_integral = True Then

                Lp6_LP_mean_integral = LP_corr_integral

                meanLp_integral()

                compute_std_dev_integral()

                sum_mean_integral = False

            End If

        End If

        If m = 7 Then

            TextBox_Lp2_integral.Text = Format(LP_corr_integral, displayformat)

            If sum_mean_integral = True Then

                Lp7_LP_mean_integral = LP_corr_integral

                meanLp_integral()

                compute_std_dev_integral()

                sum_mean_integral = False

            End If

        End If

        RichTextBox_mean_inst_LP_integral.Text = Format(mean_Lp_integral, displayformat)

    End Sub
    Private Sub meanLp_integral()

        mean_Lp_integral = (mean_Lp_integral * (m - 2) + LP_corr_integral) / (m - 1)


    End Sub
    Sub compute_PLC_integral()

        If (m > 1) And (ToolStripButton_activate_MEASUREMENT_mode.Checked = True) Then

            If is_LP0_set_integral = True Then

                PLC_integral = 100 * (1 - (RichTextBox_mean_inst_LP_integral.Text / TextBox_LP0_integral.Text))
                RichTextBox_PLC_integral.Text = Format(PLC_integral, "0.##")

            Else

                PLC_integral = 0.99999
                RichTextBox_PLC_integral.Text = "not set"

            End If

        End If

    End Sub

    Sub setLP0_integral()

        TextBox_LP0_integral.Text = RichTextBox_mean_inst_LP_integral.Text
        is_LP0_set_integral = True
        RichTextBox_MEASMODE_STATUS.Select()
        compute_PLC_integral()
    End Sub


    Sub compute_PLC_automode()

        If (ToolStripButton_activate_MEASUREMENT_mode.Checked = True) Then

            '  If is_LP0_set_integral = True Then

            PLC_automod = 100 * (1 - (RichTextBox_AUTOMODE_meanLp_integral.Text / RichTextBox_Lp0_automode.Text))
            RichTextBox_PLC_automode.Text = Format(PLC_automod, "0.##")

            If CheckBox_validateboolean.Checked = False Then
                send_data_to_datagridview()
            End If
            'Else

            '     PLC_automode = 0.99999
            '   RichTextBox_PLC_automode.Text = "not set"

            '   End If

        End If
    End Sub

    Public Sub pushwheel_value_ZERO_VALUE()

        If centrifuge_model = "Sorvall RC serie" And CAVITRON_SETUP.ComboBox_cavispeed_setup.SelectedIndex <> 2 Then
            Try
                'NI DO card Dev2
                Dim writerport0 As New DigitalSingleChannelWriter(DIGITAL_OUTPUT_cavispeed_relay_controlport0_task.Stream)
                Dim writerport1 As New DigitalSingleChannelWriter(DIGITAL_OUTPUT_cavispeed_relay_controlport1_task.Stream)


                dataArray_port0 = {False, False, False, False, False, False, False, False}
                dataArray_port1 = {False, False, False, False, False, False, False, False}

                CAVITRON_SETUP.LedArray_pushwheel_port0.SetValues(dataArray_port0)
                LedArray_pushwheel_port0.SetValues(dataArray_port0)
                writerport0.WriteSingleSampleMultiLine(True, dataArray_port0)
                CAVITRON_SETUP.LedArray_pushwheel_port1.SetValues(dataArray_port1)
                LedArray_pushwheel_port1.SetValues(dataArray_port1)
                writerport1.WriteSingleSampleMultiLine(True, dataArray_port1)



                If CAVITRON_SETUP.ComboBox_cavispeed_setup.SelectedIndex = 0 Then

                    TextBox_cavispeed_setvalue.Text = 0
                    If CheckBox_tachymeter_connected.Checked = False Then
                        RichTextBox_centri_rpm.Text = 0

                    End If
                End If
                If CAVITRON_SETUP.ComboBox_cavispeed_setup.SelectedIndex = 1 Then

                    TextBox_cavispeed_setvalue.Text = 0
                    If CheckBox_tachymeter_connected.Checked = False Then
                        RichTextBox_centri_tension.Text = 0

                    End If
                End If
                If CAVITRON_SETUP.ComboBox_cavispeed_setup.SelectedIndex = 2 Then
                    read_Cavispeed_rpm = 0
                    set_cavispeed_value()
                    get_cavispeed_rpm()
                End If
            Catch e As Exception

                MsgBox(e.Message)

            End Try

        End If

        set_Cavispeed_rpm = 0
        TextBox_set_cavispeed_fixed.Text = set_Cavispeed_rpm
        set_centri_tension = 0
        TextBox_set_pressure_fixed.Text = set_centri_tension

        CAVITRON_SETUP.TextBox_bin_pushwheel.Text = "0000000000000000"
        TextBox_bin_pushwheel.Text = "0000000000000000"

    End Sub

    Public Sub set_cavispeed_value()
        Try
            Select Case CAVITRON_SETUP.ComboBox_cavispeed_setup.SelectedIndex
                Case 0
                    set_Cavispeed_rpm = TextBox_cavispeed_setvalue.Text
                Case 1
                    set_centri_tension = -1 * TextBox_cavispeed_setvalue.Text
                    cavispeed_rad_s_1 = ((set_centri_tension) / (-0.25 * 1000 * ((rotor_diameter / 2 / 100) ^ 2 + ((rotor_diameter / 2 / 100)) ^ 2) / 1000000)) ^ 0.5
                    set_Cavispeed_rpm = cavispeed_rad_s_1 * 60 / 2 / System.Math.PI

                Case 2
                    set_Cavispeed_rpm = 0
            End Select

        Catch e As Exception
            MsgBox(e.Message)

        End Try

        If centrifuge_model = "Sorvall RC serie" Then


            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            'CAVITRON 3 AND 4
            Try

                Dim writerport0 As New DigitalSingleChannelWriter(DIGITAL_OUTPUT_cavispeed_relay_controlport0_task.Stream)
                Dim writerport1 As New DigitalSingleChannelWriter(DIGITAL_OUTPUT_cavispeed_relay_controlport1_task.Stream)

                Dim pushwheel_string_boolean_values As String




                pushwheel_string_boolean_values = assign_pushwheel_value(set_Cavispeed_rpm)
                CAVITRON_SETUP.TextBox_bin_pushwheel.Text = pushwheel_string_boolean_values
                TextBox_bin_pushwheel.Text = pushwheel_string_boolean_values
                '  Dim dataArray_port0() As Boolean = New Boolean(7) {}
                For i = 0 To 7 'pushwheel_string_boolean_values.Length - 1

                    If pushwheel_string_boolean_values.Chars(i) = "1" Then
                        dataArray_port0(i) = True
                    Else
                        dataArray_port0(i) = False
                    End If
                Next
                'Dim dataled = New Boolean() {True, True, True, True, True, True, True, True, True, True, True, True, True}
                ' ReDim data(14)

                '  Dim dataArray_port1() As Boolean = New Boolean(7) {}

                For i = 0 To 5 'pushwheel_string_boolean_values.Length - 1

                    If pushwheel_string_boolean_values.Chars(i + 8) = "1" Then
                        dataArray_port1(i) = True
                    Else
                        dataArray_port1(i) = False
                    End If
                Next

                If set_Cavispeed_rpm <= max__possible_cavispeed_rpm And set_Cavispeed_rpm >= 0 Then

                    CAVITRON_SETUP.LedArray_pushwheel_port0.SetValues(dataArray_port0)
                    LedArray_pushwheel_port0.SetValues(dataArray_port0)
                    writerport0.WriteSingleSampleMultiLine(True, dataArray_port0)

                    CAVITRON_SETUP.LedArray_pushwheel_port1.SetValues(dataArray_port1)
                    LedArray_pushwheel_port1.SetValues(dataArray_port1)
                    writerport1.WriteSingleSampleMultiLine(True, dataArray_port1)
                Else
                    MsgBox("You ask too much! please select a value below 13000 rpm", MsgBoxStyle.Exclamation)
                End If

                ' Dim writer As DigitalSingleChannelWriter = New DigitalSingleChannelWriter(DIGITAL_OUTPUT_cavispeed_relay_control_task.Stream)
                '   DO_writer.WriteSingleSampleSingleLine(False, dataArray)
                '  DO_writer.WriteSingleSamplePort(True, dataArray_port0())

            Catch e As Exception
                MsgBox(e.Message)
            End Try

            TextBox_set_cavispeed_fixed.Text = set_Cavispeed_rpm
            TextBox_set_pressure_fixed.Text = set_centri_tension

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            'CAVI 1000
        Else
            If centri_Start = True Then
                Try
                    ' Write START channel on Dev1/a01 ---- ONLY IF CAVITRON 3 or 4  !! 
                    Dim set_CAVI1000_speed_task As Task = Nothing
                    set_CAVI1000_speed_task = New Task()
                    set_CAVI1000_speed_task.AOChannels.CreateVoltageChannel( _
                    "Dev1/ao1", _
                        "aoChannel", _
                        0, _
                        5, _
                        AOVoltageUnits.Volts)



                    Dim writer As AnalogSingleChannelWriter = New AnalogSingleChannelWriter(set_CAVI1000_speed_task.Stream)
                    writer.WriteSingleSample(True, set_Cavispeed_rpm * 5 / 3000)

                Catch e As Exception
                    MsgBox(e.Message)

                End Try
            End If
        End If

    End Sub

    Public Sub Start_Stop_centri()


        If centri_Start = False Then
            '''''''''''CONDITION IF CENTRIFUGE RUN
            centri_Start = True
            centri_Stop = False
            Bt_centri_START_STOP.Text = "STOP"
            Bt_centri_START_STOP.BackColor = Color.DarkRed
            Label_centri_running.Visible = True
            centri_Brake = False


        Else
            '''''''''''CONDITION IF CENTRIFUGE STOP
            centri_Start = False
            centri_Stop = True

            Bt_centri_START_STOP.Text = "START"
            Bt_centri_START_STOP.BackColor = Color.Silver
            Label_centri_running.Visible = False

            If CheckBox_autobrake.Checked = True Then
                centri_Brake = True
                Bt_centri_BRAKE.BackColor = Color.Orange
            Else
                centri_Brake = False
                Bt_centri_BRAKE.BackColor = Color.Silver
            End If


        End If


        write_digital_START_STOP_BRAKE()


    End Sub




    Public Sub write_digital_START_STOP_BRAKE()
        Try

            Dim DIGITAL_OUTPUT_cavispeed_relay_channel_start_task As Task = Nothing

            DIGITAL_OUTPUT_cavispeed_relay_channel_start_task = New Task()

            DIGITAL_OUTPUT_cavispeed_relay_channel_start_task.DOChannels.CreateChannel("Dev1/Port0/line0:7", "port0", _
                  ChannelLineGrouping.OneChannelForAllLines)
            Dim writerchannel_start As New DigitalSingleChannelWriter(DIGITAL_OUTPUT_cavispeed_relay_channel_start_task.Stream)


            Dim dataArray_dev1port1() As Boolean = New Boolean(7) {"false", "false", "false", "false", "false", "false", "false", "false"}

            dataArray_dev1port1 = New Boolean(7) {centri_Start, centri_Stop, centri_Brake, "false", "false", "false", "false", "false"}
            writerchannel_start.WriteSingleSampleMultiLine(True, dataArray_dev1port1)

            DIGITAL_OUTPUT_cavispeed_relay_channel_start_task.Dispose()
        Catch e As Exception
            centri_Start = False
            RichTextBox_MEASMODE_STATUS.Select()
            MsgBox(e.Message, MsgBoxStyle.Exclamation)

        End Try



        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        '' START BUTTON
        If is_NIUSB6008_init = False Then
            init_datalogger()
        End If
        Try
            Dim START_CENTRI_task As Task = Nothing
            START_CENTRI_task = New Task()
            START_CENTRI_task.AOChannels.CreateVoltageChannel( _
            "Dev1/ao1", _
                "aoChannel", _
                0, _
                5, _
                AOVoltageUnits.Volts)





            If centri_Start = True Then

                Dim writer As AnalogSingleChannelWriter = New AnalogSingleChannelWriter(START_CENTRI_task.Stream)
                writer.WriteSingleSample(True, bt_START_highlevel_mV / 1000)

            Else
                Dim writer As AnalogSingleChannelWriter = New AnalogSingleChannelWriter(START_CENTRI_task.Stream)
                writer.WriteSingleSample(True, bt_START_lowlevel_mV / 1000)
            End If

            START_CENTRI_task.Dispose()
        Catch ex As Exception
            centri_Start = False
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)

        End Try

    End Sub


    '###############################################################################################################################################
    ' COUNTDOWN #########################

    Sub start_countdown() Handles Timer_1sec.Tick

        read_constant()

        'Dim stopwatch_countdown As Stopwatch
        If live_countdown > 0 Then
            live_countdown = live_countdown - 1
            TextBox_countdown.BackColor = Color.DarkRed
        Else
            TextBox_countdown.BackColor = Color.PaleGoldenrod
        End If
        TextBox_countdown.Text = live_countdown
    End Sub
    Sub reset_countdown()
        read_constant()
        live_countdown = NumericUpDown_countdown_min.Value * 60 + NumericUpDown_countdown_sec.Value
        TextBox_countdown.Text = live_countdown '& " sec"
    End Sub

    '~###############################################

    Sub temperature_textbox_mgmt()




        ' READ SOURCE FOR CORRECTION TEMP
        Select Case ComboBox_correction_temp_source.Text

            Case "fixed temperature correction"
                TextBox_temperature_correction.Visible = True
                correction_Temp = TextBox_temperature_correction.Text

            Case "  external sensor"
                TextBox_temperature_correction.Visible = False
                correction_Temp = cavi_Temp1

        End Select

    End Sub





    Private Sub send_data_to_datagridview()
        'enregitre la moyenne des valeurs instantan�es quand on presse "valider la donn�e"
        buffered_cavispeed_rpm = read_Cavispeed_rpm
        stored_buffered_cavispeed_rpm = max_Cavispeed_rpm
        Dim mean_inst_LP_integral As String

        Dim PLC_int As Double


        If ToolStripButton_activate_MEASUREMENT_mode.Checked = True Then

            read_constant()    'Lecture des constantes  avant d'enregistrer.
            compute_pressure()
            speedclass_determination()
            If AUTO_mode_boolean = True Then
                raw_LP_integral = Format(raw_AUTOMODE_meanLp_corrT, "0.#######")
                mean_inst_LP_integral = Format(AUTOMODE_meanLp_integral, "0.#######")
                std_err_lp_integral = Format(std_err_lp_automode, "0.######")
                PLC_int = Format(PLC_automod, "0.##")

            Else
                raw_LP_integral = 0
                mean_inst_LP_integral = Format(mean_Lp_integral, "0.#######")
                std_err_lp_integral = Format(std_err_lp_integral, "0.###")
                PLC_int = Format(PLC_integral, "0.##")

            End If

            'SAVED_data_list = measurement_date, TextBox_campaign_name.Text, sample_ref1, sample_ref2, sample_ref3, Cavispeed_rpm, max_Cavispeed_rpm, Format(max_centri_tension," & Chr(34) & "0.###" & Chr(34) & "), RichTextBox_mean_inst_LP_Cochard.Text, std_dev_lp_cochard, Format(PLC," & Chr(34) & "0.##" & Chr(34) & "), RichTextBox_mean_inst_LP_integral.Text, standard_deviation_integral, Format(PLC_integral," & Chr(34) & "0.##" & Chr(34) & "), "", speed_class, plate_IR_temp, cavi_Temp1, cavi_Temp2, correction_Temp, operator_name, ComboBox_plate_number.Text, species, lieu_prelevement, sample_type, treeage, TextBox_branch_diam.Text, delta_P, calib_optic_grid, meas_MODE
            DataGridView_MAINDATA.Rows.Add(measurement_date, campaign_name, sample_ref1, sample_ref2, sample_ref3, _
                                           read_Cavispeed_rpm, max_Cavispeed_rpm, Format(max_centri_tension, "0.###"), raw_LP_integral, _
                                           mean_inst_LP_integral, std_err_lp_integral, PLC_int, "", speed_class, _
                                            cavi_Temp1, cavi_Temp2, correction_Temp, Format(temperature_correction_factor, "0.####"), _
                                           operator_name, rotor_name, species, lieu_prelevement, sample_type, treeage, Format(delta_P, "0.####"), _
                                           Format(PIXEL_SIZE_mmperpixel, "0.#####"), meas_MODE, rotor_diameter, CAVITRON_SETUP.ComboBox_Lp_SIunit_select.Text, slope, intercept, _
                                           residue, equ_pearsonRsquare, reservoir_area, cross_section_area, mean_diameter_big_res, mean_diameter_small_res, TextBox_numberofstem.Text, rawdata_time_string, _
                                           rawdata_distance_pixel_string, rawdata_distance_mm_string, dialog_branchdiameter.branch_diameter_rawdata_upstream, _
                                           dialog_branchdiameter.branch_diameter_rawdata_downstream)
            '  DataGridView1.Rows.Add(SAVED_data_list)
            save_data_to_file()
        Else
            '  MsgBox("The MEASURE mode is not activated", ghg)
            MEASMODE_not_activated()
        End If

        ctrl = False



        RichTextBox_MEASMODE_STATUS.Select()

        DataGridView_MAINDATA.FirstDisplayedCell = DataGridView_MAINDATA(0, DataGridView_MAINDATA.Rows.Count - 1)
        '     init_graph()
        update_graph()
    End Sub
    Sub MEASMODE_not_activated()
        Dim r As MsgBoxResult

        r = MsgBox("The MEASURE mode is not activated. Click OK to activate?", MsgBoxStyle.OkCancel, "Activate MEASUREMENT MODE")

        If r = MsgBoxResult.Ok Then
            ToolStripButton_activate_MEASUREMENT_mode.Checked = True
            If AUTO_mode_boolean = False Then
                draw_grid()
            Else
                draw_ROI()
                '  automatic_determination_of_ROI()
            End If
        End If
    End Sub


    Private Sub save_data_to_file()

        'chr(13) = ASCII codfe for line feed
        'chr(9) = ASCII code for tabulation
        Dim row As DataGridViewRow = Me.DataGridView_MAINDATA.Rows(0)



        '     header = DataGridView_MAINDATA.Columns[Index].HeaderText;

        If save_data_filename = none Then
            select_filename_save_data()
        Else

            header = "CAVISOFT datafile for " & cavitron_name & " and " & rotor_name & ". Cavisoft " & cavisoft_version & " has been used" & Chr(13)

            For Each col As DataGridViewColumn In row.DataGridView.Columns
                header = header & col.HeaderText & ","
            Next
            header = header & Environment.NewLine

            '  RichTextBox4.Text = header
            ''  "measurement_date" & "," & "campaign_name" & "," & "sample_ref1" & "," & "sample_ref2" & "," & "sample_ref3" & "," & "meas_cavispeed_rpm" & "," & "max_Cavispeed_rpm" & "," & "pressure" & "," & "raw_conductance_mmMPas_corrT" & "," & "conductivity_SI_corrT" & "," & "std_dev_conductivity" & "," & "PLC_percent" & "," & "note" & "," & "speed_class" & "," & "plate_temp" & "," & "cavi_Temp1" & "," & "cavi_Temp2" & "," & "temperature_used_for_correction" & "," & "operator" & "," & "plate_number" & "," & "branch_diam_bigreservoir_mm" & "," & "branch_diam_smallreservoir_mm" & "," & "species" & "," & "sampling_location" & "," & "sample_type" & "," & "tree_age" & "," & "delta_P_MPa" & "," & "calib_optic_mm_per50grad" & "," & "meas_MODE" & "," & "Rotor_diameter" & "," & "conductance unit" & "," & "Equation_slope" & "," & "Equation_intercept" & "," & "RAW DATA time" & "," & "RAW DATA distance pixel" & "," & "RAW DATA distance mm" & "," & "number of stems" & "," & "rawdata branch diameter UPSTREAM (mm)" & "," & "rawdata branch diameter DOWNSTREAM (mm)" & Chr(13)


            My.Computer.FileSystem.WriteAllText(save_data_filename, header, False)

            data_to_save = ""

            For Each r As DataGridViewRow In Me.DataGridView_MAINDATA.Rows
                For Each c As DataGridViewCell In r.Cells
                    data_to_save = data_to_save & c.Value & ","
                Next
                data_to_save = data_to_save & Environment.NewLine
            Next


            My.Computer.FileSystem.WriteAllText(save_data_filename, data_to_save, True)
        End If
        RichTextBox_MEASMODE_STATUS.Select()
    End Sub


    Public Sub define_fileextension()

        Select Case CAVITRON_SETUP.ComboBox_fileextension.Text
            Case ".dat"
                datafile_extension = ".dat"
            Case ".txt"
                datafile_extension = ".txt"
            Case ".csv"
                datafile_extension = ".csv"
            Case ".xls"
                datafile_extension = ".xls"
        End Select

    End Sub

    Public Sub load_data_to_cavisoft()

        If read_data_filename = none Then
            select_filename_read_data()
            load_data_to_cavisoft()
        Else
            Try
                ' header = "CAVISOFT datafile for " & ComboBox_cavitron_numb.Text & " and " & ComboBox_plate_number.Text & ". Cavisoft " & cavisoft_version & " has been used" & Chr(13) & "measurement_date,campaign_name,sample_ref1,sample_ref2,sample_ref3,meas_cavispeed_rpm,max_Cavispeed_rpm,pressure,mean_Lp_corr,std_dev_LP,PLC,note,speed_class,plate_temp,cavi_Temp1,cavi_Temp2,temperature_used_for_correction,operator,plate_number,species,sampling_location,sample_type,tree_age,branch_diam" & Chr(13)

                data_to_read = My.Computer.FileSystem.ReadAllText(read_data_filename, System.Text.Encoding.UTF8)
                '    Debug.Print(read_data_filename)
                ' Debug.Print(data_to_read)


                Dim TextLine As String = ""
                Dim SplitLine() As String

                If System.IO.File.Exists(read_data_filename) = True Then

                    Dim objReader As New System.IO.StreamReader(read_data_filename)

                    Do While objReader.Peek() <> -1
                        TextLine = objReader.ReadLine()

                        SplitLine = Split(TextLine, ",")
                        ' Discard the line statring with 
                        If Char.IsLetter(SplitLine(0)) Then '''''''TextLine.StartsWith("C") Or TextLine.StartsWith("m") Or TextLine.StartsWith(" ") Then
                        Else
                            Me.DataGridView_MAINDATA.Rows.Add(SplitLine)
                        End If
                    Loop

                    Dim one_before_the_last_row = Me.DataGridView_MAINDATA.Rows.Count - 2

                    Me.DataGridView_MAINDATA.Rows.RemoveAt(one_before_the_last_row)
                    objReader.Close()
                Else

                    MsgBox("File Does Not Exist")

                End If


                'DataGridView1.Rows.Add(0measurement_date, 1TextBox_campaign_name.Text, 2sample_ref1, 3sample_ref2,4 sample_ref3,5 Cavispeed_rpm, 6max_Cavispeed_rpm, 7Format(max_centri_tension, "0.###"), RichTextBox_mean_inst_LP_Cochard.Text, 9.std_dev_lp_cochard, 10Format(PLC, "0.##"), 11.RichTextBox_mean_inst_LP_integral.Text, 12.standard_deviation_integral, 13Format(PLC_integral, "0.##"),14 "", 15.speed_class, 16plate_IR_temp, 17cavi_Temp1, 18cavi_Temp2, 19.correction_Temp, 20 operator_name, 21ComboBox_plate_number.Text, 22species, 23lieu_prelevement, 24sample_type,25 treeage, 26TextBox_branch_diam.Text, 27delta_P, 28calib_optic_grid, 29meas_MODE)
                ' LECTURE DES CONSTANTES
                '  AMODIFIER()
                TextBox_campaign_name.Text = DataGridView_MAINDATA(1, 1).Value
                TextBox_sampleref1.Text = DataGridView_MAINDATA(2, 1).Value
                TextBox_sampleref2.Text = DataGridView_MAINDATA(3, 1).Value
                TextBox_sampleref3.Text = DataGridView_MAINDATA(4, 1).Value
                max_Cavispeed_rpm = DataGridView_MAINDATA(6, DataGridView_MAINDATA.Rows.Count - 2).Value
                TextBox_max_cavispeed.Text = max_Cavispeed_rpm
                TextBox_temperature_correction.Text = DataGridView_MAINDATA(16, 1).Value
                ComboBox_operator.Text = DataGridView_MAINDATA(18, 1).Value
                'ComboBox_plate_number.Text = DataGridView_MAINDATA(21, 1).Value
                ''    Dialog_branch_diameter.TextBox_bigreservoirdiameter.Text = DataGridView_MAINDATA(22, 1).Value
                ''   Dialog_branch_diameter.TextBox_smallreservoirdiameter.Text = DataGridView_MAINDATA(23, 1).Value
                TextBox_branch_crosssection.Text = DataGridView_MAINDATA(32, 1).Value
                cross_section_area = TextBox_branch_crosssection.Text
                TextBox_crosssectionarea_ofWATER.Text = DataGridView_MAINDATA(31, 1).Value
                crosssectionarea_OF_WATER = TextBox_crosssectionarea_ofWATER.Text

                TextBox_numberofstem.Text = DataGridView_MAINDATA(36, 1).Value

                TextBox_species.Text = DataGridView_MAINDATA(20, 1).Value

                TextBox_location.Text = DataGridView_MAINDATA(21, 1).Value

                ComboBox_sampletype.Text = DataGridView_MAINDATA(22, 1).Value
                TextBox_comment.Text = DataGridView_MAINDATA(23, 1).Value


                speed_class_ASCII_index = 97


                Dialog_compute_PLC.ShowDialog()
                update_graph()
                read_data_filename = save_data_filename

            Catch ex As Exception
                MsgBox(ex.Message, MsgBoxStyle.Exclamation)
                read_data_filename = none
            End Try
        End If


        '        My.Computer.FileSystem.WriteAllText(read_data_filename, data_to_save, True)
        '  TextBox_max_cavispeed.Text = DataGridView1(6, DataGridView1.Rows.Count - 2).Value
        RichTextBox_MEASMODE_STATUS.Select()


    End Sub

    Sub append_file()


        If APPEND_filename = none Then

            OpenFileDialog_APPENDED.ShowDialog()
            APPEND_filename = OpenFileDialog_APPENDED.FileName
            save_data_to_file()
            ' header = "CAVISOFT datafile for " & ComboBox_cavitron_numb.Text & " and " & ComboBox_plate_number.Text & ". Cavisoft " & cavisoft_version & " has been used" & Chr(13) & "measurement_date,campaign_name,sample_ref1,sample_ref2,sample_ref3,meas_cavispeed_rpm,max_Cavispeed_rpm,pressure,mean_Lp_corr,std_dev_LP,PLC,note,speed_class,plate_temp,cavi_Temp1,cavi_Temp2,temperature_used_for_correction,operator,plate_number,species,sampling_location,sample_type,tree_age,branch_diam" & Chr(13)
            If OpenFileDialog_APPENDED.CheckFileExists = False Then
                My.Computer.FileSystem.WriteAllText(APPEND_filename, header & Environment.NewLine, True)
            End If
            My.Computer.FileSystem.WriteAllText(APPEND_filename, data_to_save, True)
        Else
            OpenFileDialog_APPENDED.ShowDialog()
            APPEND_filename = OpenFileDialog_APPENDED.FileName
            save_data_to_file()
            My.Computer.FileSystem.WriteAllText(APPEND_filename, data_to_save, True)
        End If



    End Sub

    Private Sub append_complete_file()

        select_filename_read_data()
        data_to_read = My.Computer.FileSystem.ReadAllText(read_data_filename, System.Text.Encoding.UTF8)

        OpenFileDialog_APPENDED.ShowDialog()
        APPEND_filename = OpenFileDialog_APPENDED.FileName
        'save_data_to_file()
        ' header = "CAVISOFT datafile for " & ComboBox_cavitron_numb.Text & " and " & ComboBox_plate_number.Text & ". Cavisoft " & cavisoft_version & " has been used" & Chr(13) & "measurement_date,campaign_name,sample_ref1,sample_ref2,sample_ref3,meas_cavispeed_rpm,max_Cavispeed_rpm,pressure,mean_Lp_corr,std_dev_LP,PLC,note,speed_class,plate_temp,cavi_Temp1,cavi_Temp2,temperature_used_for_correction,operator,plate_number,species,sampling_location,sample_type,tree_age,branch_diam" & Chr(13)

        My.Computer.FileSystem.WriteAllText(APPEND_filename, data_to_read, True)

    End Sub

    Private Sub select_filename_save_data()
        '       MsgBox("S�lectionnez un nom de fichier pour l'enregistrement des donn�es", MsgBoxStyle.OkOnly)
        read_constant()
        SaveFileDialog_main_data.DefaultExt = datafile_extension

        SaveFileDialog_main_data.FileName = sample_ref1

        SaveFileDialog_main_data.ShowDialog()

        'If SaveFileDialog_main_data.ShowDialog = Windows.Forms.DialogResult.Cancel Then
        '    SaveFileDialog_main_data.FileName = "none"
        '    save_data_filename = "none"
        'Else
        '    save_data_filename = SaveFileDialog_main_data.FileName
        'End If
        If SaveFileDialog_main_data.FileName <> datafile_extension Then
            save_data_filename = SaveFileDialog_main_data.FileName
        Else
            save_data_filename = "none"
        End If

        '  MsgBox(data_filename)

    End Sub

    Private Sub select_filename_read_data()
        ' OpenFileDialog_read_data.DefaultExt = ".dat"
        OpenFileDialog_read_data.ShowDialog()

        read_data_filename = OpenFileDialog_read_data.FileName
        '  MsgBox(data_filename)

    End Sub



    Sub erase_instant_Lp_integral()

        TextBox_Lp1_integral.Text = 0
        TextBox_Lp2_integral.Text = 0


        m = 0
        LP_corr_integral = 0
        mean_Lp_integral = 0
        sum_variance_new = 0
        variance_integral = 0
        standard_deviation_integral = 0

        RichTextBox_PLC_integral.Text = 0
        RichTextBox_mean_inst_LP_integral.Text = 0
        richtextbox_std_dev_LP_integral.Text = 0

    End Sub


    Sub disable_integral()

        TextBox_Lp1_integral.Text = -9999
        TextBox_Lp2_integral.Text = -9999

        LP_corr_integral = -9999
        standard_deviation_integral = -9999
        PLC_integral = -9999

        RichTextBox_PLC_integral.Text = -9999
        RichTextBox_mean_inst_LP_integral.Text = -9999
        richtextbox_std_dev_LP_integral.Text = -9999

    End Sub


    Sub update_graph()



        Dim i As Integer
        Dim j As Integer
        Dim arrData_pressure As Double()

        Dim arrData_integral As Double()
        Dim arrData_time As DateTime()
        Dim arrData_conductance As Double()
        Dim max_pressure As Double = 0
        Dim max_cond As Double = 0

        Try

            Select Case graphetype_string
                '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                Case "PLC_vs_pres"

                    Chart_PLC.Series("Integral method").XValueType = ChartValueType.Double

                    If DataGridView_MAINDATA.RowCount > 1 Then
                        i = DataGridView_MAINDATA.RowCount - 1
                        '   Debug.Print(DataGridView_MAINDATA.RowCount)
                        ReDim arrData_pressure(0 To i - 1)
                        ReDim arrData_integral(0 To i - 1)


                        For j = 0 To (i - 1)

                            If (DataGridView_MAINDATA.Rows(j).Cells("column_PLC_integral").Value < -999) Or (RichTextBox_PLC_integral.Text = "-Infini") Then


                                DataGridView_MAINDATA.Rows(j).Cells("column_PLC_integral").Value = 0

                            End If

                            arrData_pressure(j) = DataGridView_MAINDATA.Rows(j).Cells("column_pressure").Value

                            arrData_integral(j) = DataGridView_MAINDATA.Rows(j).Cells("column_PLC_integral").Value

                            If DataGridView_MAINDATA.Rows(j).Cells("column_pressure").Value < max_pressure Then
                                max_pressure = arrData_pressure(j)
                            End If
                            '
                            '      Debug.Print(DataGridView1.Rows(j).Cells("column_PLC_integral").Value)

                            'Next
                            ' Chart_PLC.DataBindTa()


                            Chart_PLC.Series("Integral method").Enabled = True

                            TextBox_chart_Yaxis_max.Text = 120
                            TextBox_chart_Yaxis_min.Text = -20

                            change_chart_range()



                            Chart_PLC.Series("Integral method").Points.DataBindXY(arrData_pressure, arrData_integral)


                        Next

                        '  Chart_PLC.Series("").ChartArea.
                        Chart_PLC.ChartAreas(0).AxisX.Minimum = (System.Math.Truncate(max_pressure) - 1)

                        Chart_PLC.ChartAreas(0).AxisX.MajorGrid.Interval = 1
                        Chart_PLC.Update()
                        '  PLC_graph.ChartData = arrData

                        ' PLC_graph.Refresh()

                    Else

                        Chart_PLC.Series("Integral method").Enabled = False
                    End If
                    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                Case "cond_vs_pres"

                    Chart_PLC.Series("Integral method").XValueType = ChartValueType.Double
                    If DataGridView_MAINDATA.RowCount > 1 Then
                        i = DataGridView_MAINDATA.RowCount - 1
                        '   Debug.Print(DataGridView_MAINDATA.RowCount)
                        ReDim arrData_pressure(0 To i - 1)
                        ReDim arrData_conductance(0 To i - 1)
                        '   ReDim arrData_integral(0 To i - 1)


                        For j = 0 To (i - 1)

                            If (DataGridView_MAINDATA.Rows(j).Cells("column_PLC_integral").Value < -999) Or (RichTextBox_PLC_integral.Text = "-Infini") Then

                                DataGridView_MAINDATA.Rows(j).Cells("column_PLC_integral").Value = 0

                            End If

                            arrData_pressure(j) = DataGridView_MAINDATA.Rows(j).Cells("column_pressure").Value
                            arrData_conductance(j) = DataGridView_MAINDATA.Rows(j).Cells("Conductivity_SI_corrT").Value
                            '    arrData_integral(j) = DataGridView_MAINDATA.Rows(j).Cells("column_PLC_integral").Value

                            If DataGridView_MAINDATA.Rows(j).Cells("column_pressure").Value < max_pressure Then
                                max_pressure = arrData_pressure(j)
                            End If
                            If DataGridView_MAINDATA.Rows(j).Cells("Conductivity_SI_corrT").Value > max_cond Then
                                max_cond = arrData_conductance(j)
                            End If

                            '
                            '      Debug.Print(DataGridView1.Rows(j).Cells("column_PLC_integral").Value)

                            'Next
                            ' Chart_PLC.DataBindTa()

                            Chart_PLC.Series("Integral method").Enabled = True



                            change_chart_range()



                            Chart_PLC.Series("Integral method").Points.DataBindXY(arrData_pressure, arrData_conductance)


                        Next

                        '  Chart_PLC.Series("").ChartArea.
                        Chart_PLC.ChartAreas(0).AxisX.Minimum = (System.Math.Truncate(max_pressure) - 1)

                        Chart_PLC.ChartAreas(0).AxisX.MajorGrid.Interval = 1
                        Chart_PLC.ChartAreas(0).AxisY.Maximum = (System.Math.Truncate(max_cond) + 1)
                        TextBox_chart_Yaxis_max.Text = (System.Math.Truncate(max_cond) + 1)
                        Chart_PLC.ChartAreas(0).AxisY.Minimum = 0
                        TextBox_chart_Yaxis_min.Text = 0
                        Chart_PLC.Update()
                        '  PLC_graph.ChartData = arrData

                        ' PLC_graph.Refresh()

                    Else

                        Chart_PLC.Series("Integral method").Enabled = False
                    End If

                    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                    'Case "cond_vs_time"

                    '    Chart_PLC.Series("Integral method").XValueType = ChartValueType.DateTime

                    '    If DataGridView_MAINDATA.RowCount > 1 Then
                    '        i = DataGridView_MAINDATA.RowCount - 1
                    '        '   Debug.Print(DataGridView_MAINDATA.RowCount)
                    '        ReDim arrData_time(0 To i - 1)
                    '        ReDim arrData_conductance(0 To i - 1)
                    '        '   ReDim arrData_integral(0 To i - 1)


                    '        For j = 0 To (i - 1)



                    '            arrData_time(j) = Convert.ToDouble(Date.FromOADate(DataGridView_MAINDATA.Rows(j).Cells("Column1").Value))
                    '            arrData_conductance(j) = DataGridView_MAINDATA.Rows(j).Cells("Conductivity_SI_corrT").Value
                    '            '    arrData_integral(j) = DataGridView_MAINDATA.Rows(j).Cells("column_PLC_integral").Value


                    '            '    max_datetime = DataGridView_MAINDATA.Rows(DataGridView_MAINDATA.RowCount - 2).Cells("column1").Value
                    '            'min_datetime = DataGridView_MAINDATA.Rows(1).Cells("column1").Value

                    '            If DataGridView_MAINDATA.Rows(j).Cells("Conductivity_SI_corrT").Value > max_cond Then
                    '                max_cond = arrData_conductance(j)
                    '            End If

                    '            '
                    '            '      Debug.Print(DataGridView1.Rows(j).Cells("column_PLC_integral").Value)

                    '            'Next
                    '            ' Chart_PLC.DataBindTa()
                    '            '  Chart_PLC.ChartAreas(0).AxisY.Maximum = 


                    '            Chart_PLC.Series("Integral method").Enabled = True

                    '            change_chart_range()

                    '            Chart_PLC.Series("Integral method").Points.DataBindXY(arrData_time, arrData_conductance)


                    '        Next

                    '        '  Chart_PLC.Series("").ChartArea.
                    '        '  Chart_PLC.ChartAreas(0).AxisX.LabelStyle.Format = "HH:mm"
                    '        '  Chart_PLC.ChartAreas(0).AxisX.Maximum = System.DateTime.Now.ToOADate
                    '        ' Chart_PLC.ChartAreas(0).AxisX.Minimum = min_datetime.Date.ToOADate
                    '        '  Chart_PLC.ChartAreas(0).AxisX.MajorGrid.Interval = 1
                    '        Chart_PLC.ChartAreas(0).AxisY.Maximum = (System.Math.Truncate(max_cond) + 1)
                    '        TextBox_chart_Yaxis_max.Text = (System.Math.Truncate(max_cond) + 1)
                    '        Chart_PLC.ChartAreas(0).AxisY.Minimum = 0
                    '        TextBox_chart_Yaxis_min.Text = 0
                    '        Chart_PLC.Update()
                    '        '  PLC_graph.ChartData = arrData

                    '        ' PLC_graph.Refresh()

                    '    Else

                    '        Chart_PLC.Series("Integral method").Enabled = False
                    '    End If

            End Select


        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try

    End Sub









    '######################
    'This set of instruction, used to allow only numeric keypress in the user-accessible textboxes
    'it needs the numberonly function !!



    Private Sub TextBox_interval_between_read_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox_interval_between_read.KeyPress
        e.Handled = NumbersOnly(e.KeyChar, TextBox_interval_between_read)
    End Sub
    Private Sub TextBox_max_meniscus_sep_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox_Max_meniscus_sep.KeyPress
        e.Handled = NumbersOnly(e.KeyChar, TextBox_Max_meniscus_sep)
    End Sub
    Private Sub TextBox_LP0_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        e.Handled = NumbersOnly(e.KeyChar, TextBox_LP0_Cochard)
    End Sub
    Private Sub TextBox_branch_diam_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox_branch_crosssection.KeyPress
        e.Handled = NumbersOnly(e.KeyChar, TextBox_branch_crosssection)
    End Sub

    Private Sub TextBox_temperature_correction_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox_temperature_correction.KeyPress
        e.Handled = NumbersOnly(e.KeyChar, TextBox_temperature_correction)
    End Sub
    Private Sub TextBox_sample_number_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox_sampleref1.KeyPress
        e.Handled = NumbersOnly(e.KeyChar, TextBox_sampleref1)
    End Sub
    Private Sub TextBox_sample_repetition_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox_sampleref2.KeyPress
        e.Handled = NumbersOnly(e.KeyChar, TextBox_sampleref2)
    End Sub

    Public Function NumbersOnly(ByVal pstrChar As Char, ByVal oTextBox As TextBox) As Boolean
        'validate the entry for a textbox limiting it to only numeric values and the decimal point
        If (Convert.ToString(pstrChar) = "." And InStr(oTextBox.Text, ".")) Then Return True 'accept only one instance of the decimal point
        If Convert.ToString(pstrChar) <> "." And pstrChar <> vbBack Then
            Return IIf(IsNumeric(pstrChar), False, True) 'check if numeric is returned
        End If
        Return False 'for backspace
    End Function
    Public Function RT_NumbersOnly(ByVal pstrChar As Char, ByVal orichTextBox As RichTextBox) As Boolean
        'validate the entry for a textbox limiting it to only numeric values and the decimal point
        If (Convert.ToString(pstrChar) = "." And InStr(orichTextBox.Text, ".")) Then Return True 'accept only one instance of the decimal point
        If Convert.ToString(pstrChar) <> "." And pstrChar <> vbBack Then
            Return IIf(IsNumeric(pstrChar), False, True) 'check if numeric is returned
        End If
        Return False 'for backspace
    End Function

    '######################
    'disables close button
    Protected Overrides ReadOnly Property CreateParams() As CreateParams
        Get
            Dim cp As CreateParams = MyBase.CreateParams
            Const CS_NOCLOSE As Integer = &H200
            cp.ClassStyle = cp.ClassStyle Or CS_NOCLOSE
            Return cp
        End Get
    End Property

    '######################

    ''########################################################################################################################
    ''########################################################################################################################
    ''########################################################################################################################
    'GESTION EVENEMENTS

    '----KEYBOARD MANAGEMENT  -----------------------
    Public Sub form1_KeyDown1(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles RichTextBox_MEASMODE_STATUS.KeyDown

        '##########################################################################################################################################
        ' Event when START_MEAS key is pressed
        'RichTextBox_PLC_Cochard

        If (e.KeyCode = START_MEAS_key) Then
            start_timer_index = True
            stopwatch1.Reset()
            stopwatch2.Reset()

            i = 0

            read_constant()
            update_graph()
            speedclass_determination()

            erase_instant_Lp_integral()

            start_timer_index_2 = True

            stopwatch4.Stop()
            stopwatch5.Stop()

            stopwatch4.Reset()
            stopwatch5.Reset()

        End If



        '##########################################################################################################################################

        ' Event when MEASUREMENT key is pressed

        If (e.KeyCode = MEASUREMENT_key) Then
            If ToolStripButton_activate_MEASUREMENT_mode.Checked = True Then

                If AUTO_mode_boolean = True Then

                    start_AUTO_meas()
                    ' Automode action are triggered by getditance timer... 

                Else '  MANUAL MODE 

                    If IS_stopwatch_launched = False Then

                        stopwatch2.Stop()

                        stopwatch1.Reset()
                        stopwatch1.Start()

                        IS_stopwatch_launched = True
                        i = i + 1
                        add_sumLp = True

                        If CAVITRON_SETUP.CheckBox_play_a_sound.Checked = True Then
                            aBeep(CAVITRON_SETUP.NumericUpDown_beep_freq.Text + i * 20, CAVITRON_SETUP.TextBox_beep_duration.Text)
                        End If

                    Else

                        stopwatch1.Stop()

                        stopwatch2.Reset()
                        stopwatch2.Start()

                        IS_stopwatch_launched = False
                        i = i + 1
                        add_sumLp = True

                        If CAVITRON_SETUP.CheckBox_play_a_sound.Checked = True Then
                            aBeep(CAVITRON_SETUP.NumericUpDown_beep_freq.Text + i * 20, CAVITRON_SETUP.TextBox_beep_duration.Text)
                        End If

                    End If

                    'INTEGRAL METHOD MANUAL

                    '   If CheckBox_integral_method.Checked = True Then

                    If start_timer_index_2 = True Then
                        If (e.KeyCode = MEASUREMENT_key) Then
                            m = m + 1
                            sum_mean_integral = True
                            If m = 1 Then
                                stopwatch4.Start()
                                stopwatch5.Start()
                            Else
                                t1_integral = stopwatch4.ElapsedMilliseconds / 1000
                                t2_integral = stopwatch5.ElapsedMilliseconds / 1000

                                stopwatch5.Stop()
                                stopwatch5.Reset()
                                stopwatch5.Start()
                            End If
                        End If

                        compute_Lp_integral()
                        compute_PLC_integral()
                        Save_camera_acquisitions()
                    End If

                End If
            Else

                aBeep(130, 100)
                MEASMODE_not_activated()
                ' MsgBox("Measurement mode is not activated", MsgBoxStyle.Information)
            End If
        End If
        '##########################################################################################################################################

        ' Event when VALIDATE_MEASUREMENT key is pressed
        If e.KeyCode = Keys.ControlKey Then
            ctrl = True
        Else
            '     ctrl = False
        End If

        If ctrl = True Then
            If e.KeyCode = VALIDATE_MEASUREMENT_key Then

                send_data_to_datagridview()
                speedclass_determination()
                ctrl = False
            End If
        End If
        '##########################################################################################################################################

        ' Event when "call_min_meniscus_dialogbox_key" key is pressed
        If ctrl = True Then
            If e.KeyCode = call_min_meniscus_dialogbox_key Then

                Dialog_meniscus.Show()
                ctrl = False
            End If
        End If

        '##########################################################################################################################################

        ' Event when "call_recompute_PLC_menu_key" key is pressed
        If ctrl = True Then
            If e.KeyCode = call_recompute_PLC_menu Then

                Dialog_compute_PLC.Show()
                ctrl = False
            End If
        End If


        '##########################################################################################################################################

        ' Event when SET_CAVISPEED key is pressed
        If CheckBox_tachymeter_connected.Checked Then

            If (e.KeyCode = SET_CAVISPEED_key) Then
                max_Cavispeed_rpm = 0
            End If
        Else

            If (e.KeyCode = SET_CAVISPEED_key) Then
                dialog_cavispeed.Show()
            End If
        End If


        If ctrl = True Then
            If (e.KeyCode = Keys.F1) Then
                pushwheel_value_ZERO_VALUE()

                ctrl = False
            End If
        End If

        '##########################################################################################################################################



        '##########################################################################################################################################

        ' Event when SET_LP0 key is pressed
        If (e.KeyCode = SET_LP0_key) Then

            setLP0_integral()
        End If
        '##########################################################################################################################################

        ' Event when FILL_RESERVOIR_key is pressed
        If (e.KeyCode = FILL_RESERVOIR_key) Then

            PictureBox_RESERVOIR_Fill.Visible = True
            FILL_RESERVOIR_BOOLEAN = True
            fill_reservoir()


        End If


        '##########################################################################################################################################

        ' Event when TAB keys are pressed

        ' DATA
        If (e.KeyCode = SELECT_DATA_TAB_key) Then
            TabControl_main.SelectedTab = TabPage1
            RichTextBox_MEASMODE_STATUS.Select()
        End If

        If (e.KeyCode = SELECT_AUTOMODE_TAB_key) Then
            '   update_graph()
            TabControl_main.SelectedTab = TabPage2
            RichTextBox_MEASMODE_STATUS.Select()
        End If

        If (e.KeyCode = SELECT_PARAMETER_TAB_key) Then
            TabControl_main.SelectedTab = TabPage3
            RichTextBox_MEASMODE_STATUS.Select()
        End If

        '######################################################################################



    End Sub

    Public Sub form1_KeyDown2(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles RichTextBox_PLC_integral.KeyDown
        ' if textbox PLC_integral is selected, it jumps to PLC_cochard...
        RichTextBox_MEASMODE_STATUS.Select()

    End Sub


    Public Sub form1_KeyDown3(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles RichTextBox_MEASMODE_STATUS.KeyUp

        '##########################################################################################################################################

        ' Event when FILL reservoir key is RELEASED
        If (e.KeyCode = FILL_RESERVOIR_key) Then



            FILL_RESERVOIR_BOOLEAN = False
            fill_reservoir()

            PictureBox_RESERVOIR_Fill.Visible = False

        End If
        ctrl = False
    End Sub
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    '----BUTTON MANAGEMENT -----------------------

   



    Private Sub Bt_setcavispeed_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Bt_setcavispeed.Click


        Select Case CAVITRON_SETUP.ComboBox_cavispeed_setup.SelectedIndex
            Case 0
                dialog_cavispeed.Show()
            Case 1
                dialog_cavispeed.Show()
            Case 2
                If CheckBox_tachymeter_connected.Checked Then
                    MsgBox("The tachymeter is connected. It is not possible to input manually the cavispeed")
                Else
                    dialog_cavispeed.Show()
                End If

        End Select

    End Sub






    Private Sub Button_set_LP0_integral_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_set_LP0_integral.Click
        '     init()
        setLP0_integral()

    End Sub

    Private Sub Button_save_camera_images_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_save_camera_images.Click


        If button_saving_images_is_on_start = False Then

            button_saving_images_is_on_start = True
            Button_save_camera_images.Text = "Stop saving images"
            save_camera_images = True
            PictureBox_rec.Show()

        Else

            button_saving_images_is_on_start = False
            Button_save_camera_images.Text = "Start saving images"
            save_camera_images = False
            PictureBox_rec.Hide()

        End If

        'Save_camera_acquisitions()

    End Sub



    Private Sub NumericUpDown_temp_number_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        read_constant()

        temperature_textbox_mgmt()
    End Sub

    Private Sub ComboBox_correction_temp_source_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox_correction_temp_source.SelectedIndexChanged
        temperature_textbox_mgmt()

    End Sub



    Public Sub new_measurement()

        If CheckBox_APPEND_DATA_FILES.Checked = True Then
            append_file()
        End If

        ToolStripButton_activate_MEASUREMENT_mode.Checked = False

        TextBox_branch_crosssection.Text = ""
        dialog_branchdiameter.DataGridView1.DataSource = Nothing
        dialog_branchdiameter.DataGridView1.Refresh()

        cross_section_area = 0
        TextBox_numberofstem.Text = ""
        TextBox_crosssectionarea_ofWATER.Text = ""
        crosssectionarea_OF_WATER = 0
        TextBox_sampleref1.Text = ""
        TextBox_sampleref2.Text = ""
        TextBox_sampleref3.Text = ""

        '     TextBox_LP0_Cochard.Text = ""
        TextBox_LP0_integral.Text = ""

        DataGridView_MAINDATA.Rows.Clear()
        dialog_branchdiameter.NumericUpDown_numberofstem.Value = 0
        dialog_branchdiameter.DataGridView1.Rows.Clear()



        start_timer_index = True
        stopwatch1.Reset()
        stopwatch2.Reset()

        stopwatch4.Reset()
        stopwatch5.Reset()

        erase_instant_Lp_integral()
        TextBox_branch_crosssection.Text = "0"
        'TextBox_optic_calibration.Text = "50"
        TextBox_Max_meniscus_sep.Text = "50"
        TextBox_interval_between_read.Text = "5"
        If same_parameter_boolean = True Then

        Else
            parameter_filename = "none"
            TextBox_campaign_name.Text = ""
            TextBox_species.Text = ""
            TextBox_location.Text = ""
            ComboBox_sampletype.SelectedIndex = 0
            TextBox_comment.Text = ""
            init()

        End If


        TextBox_max_cavispeed.Text = 0
        RichTextBox_centri_rpm.Text = 0
        RichTextBox_centri_tension.Text = 0
        max_cavispeed_rad_s_1 = 0
        max_Cavispeed_rpm = 0
        max_centri_tension = 0
        ' branch_diameter_bigreservoir = 0
        '   branch_diameter_smallreservoir = 0




        save_data_filename = none
        read_data_filename = none
        is_LP0_set = False
        is_LP0_set_integral = False

        filename_save_image = none
        folder_save_image = none

        read_constant()
        speedclass_determination()

        update_graph()



        CheckBox_APPEND_DATA_FILES.CheckState = CheckState.Checked

        pushwheel_value_ZERO_VALUE()
        RichTextBox_MEASMODE_STATUS.Select()
    End Sub




    Private Sub ToolStripButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton_activate_MEASUREMENT_mode.CheckedChanged

        If ToolStripButton_activate_MEASUREMENT_mode.Checked = True Then
            RichTextBox_MEASMODE_STATUS.Text = "MEASURE MODE ACTIVATED"
            RichTextBox_MEASMODE_STATUS.BackColor = Color.SkyBlue
            ToolStripButton_activate_MEASUREMENT_mode.BackColor = Color.Red
            '---------------------------------------------------------------------------
            'numeric parameters are set to readonly to avoid bugs in case the text is erased
            '  TextBox_max__rotor_diam_cm.ReadOnly = True
            '  TextBox_optic_calibration.ReadOnly = True
            TextBox_Max_meniscus_sep.ReadOnly = True
            TextBox_Max_meniscus_sep.BackColor = Color.LightGray

            TextBox_interval_between_read.ReadOnly = True
            TextBox_interval_between_read.BackColor = Color.LightGray

            TextBox_temperature_correction.ReadOnly = True
            TextBox_temperature_correction.BackColor = Color.LightGray
            '---------------------------------------------------------------------------

            If RichTextBox_MEASMODE_STATUS.Focus = True Then
                RichTextBox_MEASMODE_STATUS.BackColor = Color.DarkSeaGreen

            End If
            If AUTO_mode_boolean = True Then
                Timer_intensityprofile.Stop()
                lock_intensityprofile = False
                ScatterGraph_intensityprofile.Visible = False
                TabControl_main.SelectTab(2)
            Else
                TabControl_main.SelectTab(0)
            End If



            If TextBox_branch_crosssection.Text = "" Or TextBox_branch_crosssection.Text = "0" Then
                MsgBox("FORGOT TO INPUT THE BRANCH DIAMETER ?! It is still time to do so ! ", MsgBoxStyle.Critical)
                ToolStripButton_activate_MEASUREMENT_mode.Checked = False
                dialog_branchdiameter.ShowDialog()

            End If

            If cross_section_area > reservoir_area Then
                MsgBox("PLEASE  CHECK THE BRANCH DIAMETER OR THE RESERVOIR TYPE ! ", MsgBoxStyle.Critical)
                ToolStripButton_activate_MEASUREMENT_mode.Checked = False
            End If

            If cavispeed_rad_s_1 > 0 Then
            Else

                MsgBox("Cavitron is not spinning... or your forgot to enter the value manually", MsgBoxStyle.Critical)
                ToolStripButton_activate_MEASUREMENT_mode.Checked = False
                dialog_cavispeed.ShowDialog()
            End If


        Else
            RichTextBox_MEASMODE_STATUS.Text = "MEASURE MODE NON ACTIVATED"
            RichTextBox_MEASMODE_STATUS.BackColor = Color.DarkRed
            ' RichTextBox_PLC_Cochard.BackColor = Color.DarkRed ' disconnected when MEAS MODE NOT ACTIVE
            RichTextBox_PLC_integral.BackColor = Color.DarkRed
            '---------------------------------------------------------------------------
            'numeric parameters are set to readonly to avoid bugs in case the text is erased

            '  TextBox_max__rotor_diam_cm.ReadOnly = False

            TextBox_Max_meniscus_sep.ReadOnly = False
            TextBox_Max_meniscus_sep.BackColor = Color.White
            TextBox_interval_between_read.ReadOnly = False
            TextBox_interval_between_read.BackColor = Color.White
            TextBox_temperature_correction.ReadOnly = False
            TextBox_temperature_correction.BackColor = Color.White


            '---------------------------------------------------------------------------
            TabControl_main.SelectTab(0)
            TextBox_Max_meniscus_sep.Select()

            If AUTO_mode_boolean = True Then
                Timer_intensityprofile.Start()
                lock_intensityprofile = True
                display_intensity_profile()
            End If
        End If

        '  define_interval_label()
        If AUTO_mode_boolean = False Then
            draw_grid()
        Else
            automatic_determination_of_ROI()
        End If



    End Sub

    Private Sub CheckBox_CAM_connected_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox_CAM_connected.CheckedChanged

        If CheckBox_CAM_connected.Checked = True Then
            init_cam()
            Start_camera_acquisition()
            Timer_intensityprofile.Start()
            lock_intensityprofile = False
        Else
            Timer_intensityprofile.Stop()
            lock_intensityprofile = True
            Stop_camera_acquisition()
        End If


    End Sub


    '#####################################################


    '------ TEXTBOX & COMBOBOX & CHECKBOX TEXTCHANGE MANAGEMENT -------------------


    Private Sub CheckBox1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox_tachymeter_connected.CheckedChanged

        If CAVITRON_SETUP.ComboBox_cavispeed_acquisiton.Text <> "Sorvall RC5C+ frequency read" Then
            get_cavispeed_rpm()
        End If
        RichTextBox_centri_rpm.Text = 0
    End Sub

    Private Sub CheckBox3_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox_saverawcavispeed.CheckedChanged


        If CheckBox_saverawcavispeed.Checked = True Then

            SaveFileDialog_cavispeed.ShowDialog()
            cavispeed_raw_data_filename = SaveFileDialog_cavispeed.FileName
        End If
    End Sub




    Private Sub CheckBox_save_camera_image_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox_save_camera_image.CheckedChanged

        If CheckBox_save_camera_image.Checked = True Then

            Button_save_camera_images.Show()
            button_saving_images_is_on_start = False

        Else

            Button_save_camera_images.Hide()

        End If

    End Sub


    Private Sub RichTextBoxMEASSTATUS_gotfocus(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RichTextBox_MEASMODE_STATUS.GotFocus

        If ToolStripButton_activate_MEASUREMENT_mode.Checked = True Then
            RichTextBox_MEASMODE_STATUS.BackColor = Color.DarkSeaGreen
        Else
            RichTextBox_MEASMODE_STATUS.BackColor = Color.DarkRed
        End If

    End Sub
    Private Sub RichTextBox_MEASSTATUS_lostfocus(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RichTextBox_MEASMODE_STATUS.LostFocus

        If RichTextBox_MEASMODE_STATUS.Focused = False Then
            RichTextBox_MEASMODE_STATUS.BackColor = Color.DarkRed

        End If

    End Sub




    '#############################################################################################################################

    'TIMERS EVENT

    Private Sub Timer_1sec_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer_1sec.Tick



        '''''''''''''''''''''''''''''''''''''''
        '''''''''''''''''''''''''''''''''''''''
        ' SECURITY if 
        If CAVITRON_SETUP.ComboBox_cavispeed_setup.SelectedIndex <> 2 Then
            If centri_Stop = False Then

                If read_Cavispeed_rpm > max__possible_cavispeed_rpm Then
                    centri_Stop = True
                    Start_Stop_centri()
                    MsgBox("The maximum possible cavispeed was exceeded !!! Centrifuge is stopping !!", MsgBoxStyle.Critical)
                End If

                If (read_Cavispeed_rpm - set_Cavispeed_rpm) > 1500 Then

                    centri_Stop = True
                    Start_Stop_centri()

                    MsgBox(" The reading of the cavispeed value exceed the set value by more than 1500 rpm !!! For safety reason, Centrifuge is stopping !!", MsgBoxStyle.Critical)
                End If
            End If
        Else
            centri_Stop = False
            centri_Start = False
            centri_Brake = False
        End If

        '''''''''''''''''''''''''''''''''''''''
        '''''''''''''''''''''''''''''''''''''''
        If is_NIUSB_DO_init = True Then
            write_digital_START_STOP_BRAKE()
        End If

        Led_start.Value = centri_Start
        Led_stop.Value = centri_Stop
        Led_brake.Value = centri_Brake


        '''''''''''''''''''''''''''''''''''''''
        If is_MAINFORM_INIT Then
            get_cavispeed_rpm()
        End If

        ' update of the displayed clock
        TextBox_time.Text = Format(Now(), "dd MMM yy   HH:mm:ss")
        If is_NIUSB6008_init = True Then
            get_plate_temperature()
        End If



    End Sub
    Public Sub Timer_camera_tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer_camera.Tick

        ethernet_camera_acquisition()

    End Sub

    ' ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' START OF PICTURE BOX CLICK OPTION """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
    Private LocalMousePosition As Point




    Private Sub PictureBox1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox_grid.MouseDoubleClick
        ' EVENT WHEN PICTURE BOX IS CLICKED WILL DEPEND ON THE VARAIBLE : "picturebox3_doubleclick_result_select" - string


        LocalMousePosition = PictureBox_main_image.PointToClient(System.Windows.Forms.Cursor.Position)
        'Debug.Print("X=" & LocalMousePosition.X & "," & "Y= " & LocalMousePosition.Y)
        ' TextBox_LP0_Cochard.Text = ("X=" & LocalMousePosition.X & "," & "Y= " & LocalMousePosition.Y)

        If picturebox3_doubleclick_result_select = "set grid origin" Then
            coord_grid_origin.X = LocalMousePosition.X
            coord_grid_origin.Y = LocalMousePosition.Y
            pixel_grid_start = coord_grid_origin.X
            NumericUpDown_grid_origin.Value = coord_grid_origin.X

            For i = 0 To 1
                For j = 0 To 25
                    image_regle.SetPixel(LocalMousePosition.X + i, 250 + j, Color.Yellow)
                Next
            Next
            draw_grid()
        End If

        If picturebox3_doubleclick_result_select = "define downstream ROI" Then
            TextBox_downstream_ROI_pixel_origin_value.Text = LocalMousePosition.X
            downstream_ROI_is_define = True
            upstream_ROI_is_define = True
            draw_ROI()
            ' picturebox3_doubleclick_result_select = ""
        End If

        If picturebox3_doubleclick_result_select = "define upstream ROI" Then
            TextBox_upstream_ROI_pixel_origin_value.Text = LocalMousePosition.X
            ' downstream_ROI_is_define = True
            upstream_ROI_is_define = True
            draw_ROI()
            ' picturebox3_doubleclick_result_select = ""
        End If

        PictureBox_grid.Image = image_regle
    End Sub


    Private Sub Button_grid_origin_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_set_grid_origin.Click
        picturebox3_doubleclick_result_select = "set grid origin"
    End Sub

    Private Sub Button_downstream_ROI_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_downstream_ROI.Click
        picturebox3_doubleclick_result_select = "define downstream ROI"

    End Sub

    Private Sub Button9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_upstream_ROI.Click
        picturebox3_doubleclick_result_select = "define upstream ROI"
    End Sub

    Private Sub PictureBox3_mousemove(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles PictureBox_grid.MouseMove
        Label_mouseposition.Visible = True
        LocalMousePosition = PictureBox_grid.PointToClient(System.Windows.Forms.Cursor.Position)
        Label_mouseposition.Text = "X: " & LocalMousePosition.X & "  --  Y: " & LocalMousePosition.Y

    End Sub

    ' END OF PICTURE BOX CLICK OPTION """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
    ' ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''*
    ' ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''


    Private Sub Bt_stopcentri_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Bt_centri_START_STOP.Click

        If is_NIUSB6008_init = True Then
            Start_Stop_centri()
        Else
            MsgBox("USB 6008 NOT INITIALISED")
        End If

    End Sub

    Private Sub Button_brake_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Bt_centri_BRAKE.Click
        My.Settings.rotor_name = 2

        ' My.Settings.Save()

        If is_NIUSB6008_init = True Then
            If centri_Brake = True Then
                centri_Brake = False
                Bt_centri_BRAKE.BackColor = Color.Silver

            Else
                centri_Brake = True
                Bt_centri_BRAKE.BackColor = Color.Orange
            End If

            write_digital_START_STOP_BRAKE()
        Else
            MsgBox("USB 6008 NOT INITIALISED")
        End If
    End Sub
    Private Sub NumericUpDown_grid_origin_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NumericUpDown_grid_origin.ValueChanged
        pixel_grid_start = NumericUpDown_grid_origin.Value
        draw_grid()
    End Sub


    Private Sub NumericUpDown_exposureTimeAbs_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NumericUpDown_exposureTimeAbs.ValueChanged
        If is_cam_initialazed = True Then
            Pylon.DeviceSetFloatFeature(hdev, "ExposureTimeAbs", NumericUpDown_exposureTimeAbs.Value)
        End If
    End Sub

    

    Private Sub LinkLabel1_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles LinkLabel1.LinkClicked
        dialog_branchdiameter.ShowDialog()

    End Sub


    Private Sub Button_recompute_pressure_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_update_datagrid.Click
        Update_datagrid()
    End Sub

    

    Private Sub NumericUpDown2_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NumericUpDown_countdown_min.ValueChanged
        read_constant()
    End Sub

    Private Sub NumericUpDown3_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NumericUpDown_countdown_sec.ValueChanged
        read_constant()
    End Sub

    Private Sub Button2_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        reset_countdown()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        start_countdown()
    End Sub



  

    'TOOLSTRIP



    Private Sub SAMEPARAMETERToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripMenuItem_sameparameter.Click

        same_parameter_boolean = True
        new_measurement()
    End Sub

    Private Sub NouveauToolStripButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton_newmeas_sameparameter.Click
        Dim r As MsgBoxResult

        r = MsgBox("Are you sure ?This will refresh all the data, but keep your parameter.", MsgBoxStyle.OkCancel, "New measurement")

        If r = MsgBoxResult.Ok Then
            same_parameter_boolean = True
            new_measurement()
        End If

    End Sub

    Private Sub NEWPARAMETERToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripMenuItem_newparameter.Click
        same_parameter_boolean = False
        new_measurement()
    End Sub

    Private Sub OuvrirToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripMenuItem_openfile.Click
        load_data_to_cavisoft()
    End Sub
    Private Sub OuvrirToolStripButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton_open.Click
        load_data_to_cavisoft()
    End Sub
    Private Sub AppendDataToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripMenuItem_appenddata.Click
        append_file()
        CheckBox_APPEND_DATA_FILES.CheckState = CheckState.Unchecked
    End Sub
    Private Sub ToolStripMenuItem_appendfile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        append_complete_file()
    End Sub
    Private Sub EnregistrerToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripMenuItem_savedata.Click
        save_data_to_file()
    End Sub
    Private Sub EnregistrerToolStripButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton_save.Click
        save_data_to_file()
    End Sub
    Private Sub SaveDataAsToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripMenuItem_savedata_as.Click
        select_filename_save_data()
        save_data_to_file()
    End Sub
 

   

    Private Sub DefineIMAGESaveFolderToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripMenuItem_imagefolder.Click
        FolderBrowserDialog_save_image.ShowDialog()
        folder_save_image = FolderBrowserDialog_save_image.SelectedPath
    End Sub



    Private Sub RecomputePLCToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RECOMPUTEPLCToolStripMenuItem.Click
        Dialog_compute_PLC.Show()
    End Sub

    Private Sub InputBranchDiameterToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripMenuItem_branchdiameter.Click
        dialog_branchdiameter.ShowDialog()
    End Sub

    Private Sub MinimumMensicusPositionToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripMenuItemmin_meniscusposition.Click
        Dialog_meniscus.Show()
    End Sub

    Private Sub LinkLabel_minmeniscusposition_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles LinkLabel_minmeniscusposition.LinkClicked
        Dialog_meniscus.Show()
    End Sub

    Private Sub DAQToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        CAVITRON_SETUP.Show()
    End Sub



    Private Sub ShortcutKeysToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripMenuItem_shortcut.Click
        Dialog_shortcut.Show()
    End Sub

    Private Sub AideToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripMenuItem_about.Click
        AboutBox1.Show()
    End Sub

    Private Sub ToolStripButton_recomputePLC_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton_recompute_PLC.Click
        Dialog_compute_PLC.Show()
    End Sub



    Private Sub ReinitializeChartToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripMenuItem_reinitchart.Click
        Chart_PLC.ResetAutoValues()
        update_graph()
    End Sub
    Private Sub ReinitialiseGridToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripMenuItem_reinitgrid.Click
        pixel_grid_start = 100
        dist_cal_pixel = 350
        draw_grid()

    End Sub

    Private Sub ReinitialiseDataloggerToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripMenuItem_reinitdatalogger.Click
        init_datalogger()
    End Sub

    Private Sub ReinitialiseParameterToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripMenuItem_reinitparameter.Click
        init()
    End Sub


    Private Sub ToolStripMenuItem_Quitter_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripMenuItem_exit.Click

        Stop_camera_acquisition()
        pushwheel_value_ZERO_VALUE()
        Timer_intensityprofile.Stop()

        If CheckBox_APPEND_DATA_FILES.Checked = True Then
            append_file()

        End If


        Dim r As MsgBoxResult

        r = MsgBox("Are your data  saved?! Are you sure you want to exit this program?", MsgBoxStyle.YesNo, "Close")
        If r = MsgBoxResult.Yes Then
            Close()
        End If

        If r = MsgBoxResult.No Then
            save_data_to_file()
        End If

    End Sub
    Private Sub ToolStripMenuItem_ExitWithoutSaving_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripMenuItemexit_withoutsaving.Click

        Timer_intensityprofile.Stop()

        Stop_camera_acquisition()
        pushwheel_value_ZERO_VALUE()
        Close()
    End Sub


    Private Sub RecomputeStandardDeviationToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripMenuItem_reinitstddev.Click
        compute_std_dev_integral()
    End Sub

    

    Private Sub Button_recomputespeedclass_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_recomputespeedclass.Click
        speedclass_determination()
    End Sub


    Private Sub Button3_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_setYaxisrange1.Click
        TabControl_main.SelectedTab = TabPage3
        TextBox_chart_Yaxis_max.Select()
        change_chart_range()
    End Sub

    Private Sub Button_set_Yaxisrange2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_set_Yaxisrange2.Click
        change_chart_range()
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_default_axisrange.Click
        TextBox_chart_Yaxis_max.Text = 100
        TextBox_chart_Yaxis_min.Text = -20

        change_chart_range()
    End Sub



    Private Sub Button_set_min_max_Yaxisrange_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_set_min_max_Yaxisrange.Click

        Dim max_PLC = 0
        Dim min_PLC = 0

        If DataGridView_MAINDATA.RowCount > 2 Then

            For j = 0 To DataGridView_MAINDATA.RowCount - 2
               

                If max_PLC < DataGridView_MAINDATA.Rows(j).Cells("Column_PLC_integral").Value Then
                    max_PLC = DataGridView_MAINDATA.Rows(j).Cells("Column_PLC_integral").Value
                End If

             

                If min_PLC > DataGridView_MAINDATA.Rows(j).Cells("Column_PLC_integral").Value Then
                    min_PLC = DataGridView_MAINDATA.Rows(j).Cells("Column_PLC_integral").Value
                End If

            Next

            TextBox_chart_Yaxis_max.Text = Format((max_PLC + 10) / 10, "#0") * 10
            TextBox_chart_Yaxis_min.Text = Format((min_PLC - 10) / 10, "#0") * 10

        Else

            TextBox_chart_Yaxis_max.Text = 100
            TextBox_chart_Yaxis_min.Text = -20

        End If
        change_chart_range()
    End Sub

    Private Sub Chart_PLC_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Chart_PLC.Click
        TabControl_main.SelectedTab = TabPage3
        TextBox_chart_Yaxis_max.Select()
    End Sub

    


    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_centrifugation_pressure_simulation.Click
        centrifugation_pressure_simulation.ShowDialog()
    End Sub




    '*************FUNCTION*******************************
    '****************************************************
    '****************************************************
    '**************************************************** 
    '****************************************************
    '****************************************************
    Public Shared Function assign_pushwheel_value(ByVal Cavispeed_rpm As Integer)
        Dim i As Int16
        Dim cavispeed_text As String = Cavispeed_rpm.ToString.PadLeft(5, "0")
        Dim cavispeed_pushwheel_bin_string As String = ""

        Dim cavispeed_charlist_dec As New List(Of String)(New String() {"", "", "", "", ""})
        Dim cavispeed_charlist_bin As New List(Of String)(New String() {"", "", "", "", ""})
        For i = 0 To cavispeed_text.Length - 2

            cavispeed_charlist_dec(i) = cavispeed_text.Chars(i)

            If i = 0 Then
                cavispeed_charlist_bin(i) = Convert_IntToBin(cavispeed_charlist_dec(i)).PadLeft(2, "0")
            Else
                cavispeed_charlist_bin(i) = Convert_IntToBin(cavispeed_charlist_dec(i)).PadLeft(4, "0")
            End If
            cavispeed_pushwheel_bin_string = cavispeed_pushwheel_bin_string + cavispeed_charlist_bin(i)

        Next

        Return cavispeed_pushwheel_bin_string

    End Function



    Public Shared Function Convert_IntToBin(ByVal intValue As Integer) As String
        Dim sb As New System.Text.StringBuilder
        While intValue <> 0
            sb.Insert(0, (intValue Mod 2).ToString())
            intValue = intValue \ 2
        End While
        Return sb.ToString()
    End Function


    '****************************************************
    '****************************************************

    

    Private Sub TextBox_temperature_correction_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox_temperature_correction.TextChanged
        temperature_textbox_mgmt()
    End Sub

    



    Private Sub TextBox_cavispeed_setvalue_mouseenter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox_cavispeed_setvalue.MouseEnter
        GroupBox_pushwheel.Visible = True
    End Sub

    Private Sub TextBox_cavispeed_setvalue_mouseleave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox_cavispeed_setvalue.MouseLeave
        GroupBox_pushwheel.Visible = False
    End Sub


    Private Sub PictureBox1_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox_grid.MouseLeave
        Label_mouseposition.Visible = False



    End Sub



    Private Sub Button_AUTO_MANUAL_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_AUTO_MANUAL.Click
        If AUTO_mode_boolean = False Then
            automode_SET()
        Else
            manualmode_set()
        End If
    End Sub

    Sub automode_SET()
        Button_AUTO_MANUAL.BackgroundImage = My.Resources.auto_mode_image
        Button_AUTO_MANUAL.Text = "                                         AUTO   MODE"
        AUTO_mode_boolean = True
        '    GroupBox_Cochard_MANUALMODE.Visible = False
        '  GroupBox_integral_MANUALMODE.Visible = False
        TabControl1.SelectedTab = TabPage_AUTO
        AUTOMODE_interval_integration_line_boolean = True
        erase_grid()
        draw_ROI()
        '  draw_grid()
        '   CheckBox_display_grid.Checked = False
        Label_grid_origin.Visible = False
        NumericUpDown_grid_origin.Visible = False
        Button_set_grid_origin.Visible = False
        ' CheckBox_cochard_method.Checked = False
        '  CheckBox_cochard_method.Enabled = False
        TabControl_main.SelectTab(0)
        Panel_MANUAL_markset.Visible = False
    End Sub
    Sub manualmode_set()
        Button_AUTO_MANUAL.BackgroundImage = My.Resources.manual_mode_image
        Button_AUTO_MANUAL.Text = "                                         MANUAL MODE"
        AUTO_mode_boolean = False
        '   GroupBox_Cochard_MANUALMODE.Visible = True
        '  GroupBox_integral_MANUALMODE.Visible = True
        TabControl1.SelectedTab = TabPage_MANUAL

        draw_grid()
        '   CheckBox_display_grid.Checked = True
        Label_grid_origin.Visible = True
        NumericUpDown_grid_origin.Visible = True
        Button_set_grid_origin.Visible = True

        '    CheckBox_cochard_method.Checked = True
        '   CheckBox_cochard_method.Enabled = True
        TabControl_main.SelectTab(0)
        Panel_MANUAL_markset.Visible = True
    End Sub

    Private Sub CheckBox4_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox_validateboolean.CheckedChanged

        If CheckBox_validateboolean.Checked = True Then
            Button_validate_meas_AUTOMODE.Visible = True
        Else
            Button_validate_meas_AUTOMODE.Visible = False

        End If

    End Sub



    Private Sub Button_Startmeas_AUTOMODE_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_Startmeas_AUTOMODE.Click
        ' ethernet_camera_acquisition()
        ' BitmapFast()
        ' stopwatch_AUTOMODE_MEAS.Reset()

        start_AUTO_meas()

    End Sub

    Public Sub start_AUTO_meas()

        If ToolStripButton_activate_MEASUREMENT_mode.Checked = True Then

            If AUTO_mode_boolean = True Then

                If downstream_ROI_is_define = False Then
                    automatic_determination_of_ROI()
                    draw_ROI()
                    erase_grid()
                End If

                DataGridView_RAWDATA.Rows.Clear()
                Dim nbmeas = ((1000 * NumericUpDown_AUTOMODE_number_of_rep_integral.Value) - (1000 * NumericUpDown_AUTOMODE_integral_stabilisation_time.Value)) / NumericUpDown_AUTOMODE_integral_integration_interval_ms.Value

                Timer_getdistance.Start()
                'NOTE :: The rest of the operation (computation & such occur on timer_getdistance tick ... ) This Sub only manage basic automode Measurement operation.

                'Timer_getdistance.Interval = NumericUpDown_AUTOMODE_integral_integration_interval_ms.Value

                stopwatch_AUTOMODE_MEAS.Reset()
                stopwatch_AUTOMODE_MEAS.Start()
                auto_meas_loop = 0
                Timer_camera.Stop()
                rep = rep + 1
                '  stopwatch_AUTOMODE_MEAS.Stop()
            Else
                MsgBox("AUTOMODE is not activated. Impossible to measure automatically !!", MsgBoxStyle.Exclamation)

            End If
        Else

            MEASMODE_not_activated()
        End If

    End Sub


    Sub automatic_determination_of_ROI()

        If AUTO_mode_boolean = True Then
            Dim pixel_intensity As Integer = 0
            Dim max_intensity_value_downstream As Integer
            Dim next_index As Integer


            ' AVERAGE BACKGROUND DETERMINATION

            Dim sum_background_image_intensity As Integer

            downstream_ROI_is_define = False
            upstream_ROI_is_define = False
            If CheckBox_CAM_connected.Checked = True Then


                num_ligne = NumericUpDown_measuredline_pixel.Value

                For j = ((num_ligne - 1) * 656) To ((num_ligne - 1) * 656) + bmap_width
                    pixel_intensity = Grab_buffer.Array(j)
                    sum_background_image_intensity = sum_background_image_intensity + pixel_intensity
                    '     Debug.Print(pixel_intensity)
                    '  Debug.Print(sum_background_image_intensity)
                Next
                avg_background_image_intensity = sum_background_image_intensity / bmap_width



                'DOWNSTREAM ROI determination
                'For j = ((num_ligne - 1) * 656) To ((num_ligne - 1) * 656) + bmap_width

                '    If downstream_ROI_is_define = False Then
                '        pixel_intensity = Grab_buffer.Array(j)
                '        If pixel_intensity > max_downstreamROI Then
                '            max_downstreamROI = pixel_intensity
                '            TextBox_downstream_ROI_pixel_origin_value.Text = j - ((num_ligne - 1) * 656)
                '            Debug.Print("max pixel intensity downstream = " & pixel_intensity)
                '            downstream_ROI_is_define = True
                '            draw_ROI()
                '        Else
                '            next_index = next_index + 1
                '        End If

                '    End If
                'Next


                'UPSTREAM ROI determination
                'For j = (((num_ligne - 1) * 656) + TextBox_downstream_ROI_pixel_origin_value.Text + (NumericUpDown_downstream_ROI_width_pixel.Value * 2)) To ((num_ligne - 1) * 656) + bmap_width

                '    If upstream_ROI_is_define = False Then
                '        pixel_intensity = Grab_buffer.Array(j)
                '        If pixel_intensity > max_upstreamROI Then
                '            max_upstreamROI = pixel_intensity
                '            TextBox_upstream_ROI_pixel_origin_value.Text = j - ((num_ligne - 1) * 656)
                '            '  Debug.Print("max pixel intensity upstream= " & pixel_intensity)
                '            upstream_ROI_is_define = True
                '            draw_ROI()
                '        Else
                '            next_index = next_index + 1
                '        End If

                '    End If
                'Next

                'DOWNSTREAM ROI determination
                For j = ((num_ligne - 1) * 656) To ((num_ligne - 1) * 656) + bmap_width

                    If downstream_ROI_is_define = False Then
                        pixel_intensity = Grab_buffer.Array(j)
                        If pixel_intensity > avg_background_image_intensity + CAVITRON_SETUP.NumericUpDown_autodetect_threshold.Value Then
                            TextBox_downstream_ROI_pixel_origin_value.Text = j - ((num_ligne - 1) * 656)


                        Else
                            next_index = next_index + 1

                        End If

                    End If
                Next

            
                ''UPSTREAM ROI determination
                'For j = (((num_ligne - 1) * 656) + TextBox_downstream_ROI_pixel_origin_value.Text + (NumericUpDown_downstream_ROI_width_pixel.Value)) To ((num_ligne - 1) * 656) + bmap_width - TextBox_downstream_ROI_pixel_origin_value.Text - (NumericUpDown_downstream_ROI_width_pixel.Value)

                '    If upstream_ROI_is_define = False Then
                '        pixel_intensity = Grab_buffer.Array(j)
                '        If pixel_intensity > avg_background_image_intensity + NumericUpDown_autodetect_threshold.Value Then
                '            TextBox_upstream_ROI_pixel_origin_value.Text = j - ((num_ligne - 1) * 656) ' + TextBox_downstream_ROI_pixel_origin_value.Text + (NumericUpDown_downstream_ROI_width_pixel.Value)
                '            TextBox_AUTO_LP2_cochard.Text = "max pixel intensity upstream = " & pixel_intensity
                '            upstream_ROI_is_define = True
                '            draw_ROI()
                '        Else
                '            next_index = next_index + 1
                '        End If

                '    End If
                'Next


            Else
                MsgBox("Camera not connected !", MsgBoxStyle.Exclamation)

            End If

            downstream_ROI_is_define = True
            upstream_ROI_is_define = True

            TextBox_upstream_ROI_pixel_origin_value.Text = 550 'Int((bmap_width - TextBox_downstream_ROI_pixel_origin_value.Text - NumericUpDown_downstream_ROI_width_pixel.Value * 2) / 2) + TextBox_downstream_ROI_pixel_origin_value.Text + NumericUpDown_downstream_ROI_width_pixel.Value * 2
            NumericUpDown_upstream_ROI_width_pixel.Value = 300 '(bmap_width - TextBox_downstream_ROI_pixel_origin_value.Text - (NumericUpDown_downstream_ROI_width_pixel.Value * 2))
            draw_ROI()


        End If

    End Sub

    Public Sub draw_ROI()


        If downstream_ROI_is_define = True Then
            If AUTO_mode_boolean = True Then

                Try
                    image_regle.MakeTransparent(Color.PaleGreen)
                    For num_colonne = TextBox_downstream_ROI_pixel_origin_value.Text - NumericUpDown_downstream_ROI_width_pixel.Value - 1 To TextBox_downstream_ROI_pixel_origin_value.Text + NumericUpDown_downstream_ROI_width_pixel.Value + 1
                        '        ' For num_ligne = 0 To bmap_height - 1
                        image_regle.SetPixel(num_colonne, interval_integration_line - 25, Color.PaleGreen)
                        image_regle.SetPixel(num_colonne, bmap_height - interval_integration_line + 25, Color.PaleGreen)
                        '        'Next
                    Next

                    For num_ligne = interval_integration_line - 25 To bmap_height - interval_integration_line + 25
                        '        ' For num_ligne = 0 To bmap_height - 1
                        image_regle.SetPixel(TextBox_downstream_ROI_pixel_origin_value.Text - NumericUpDown_downstream_ROI_width_pixel.Value - 1, num_ligne, Color.PaleGreen)
                        image_regle.SetPixel(TextBox_downstream_ROI_pixel_origin_value.Text + NumericUpDown_downstream_ROI_width_pixel.Value + 1, num_ligne, Color.PaleGreen)
                        '        'Next
                    Next

                    PictureBox_grid.BackColor = Color.Transparent
                    image_regle.MakeTransparent(Color.Blue)
                    PictureBox_grid.Parent = PictureBox_main_image
                    PictureBox_grid.Location = image_regle_location
                    PictureBox_grid.Image = image_regle
                    '  Debug.Print("downstream_ROI")
                    AUTOMODE_interval_integration_line_boolean = True
                Catch e As Exception
                    MsgBox(e.ToString, MsgBoxStyle.Exclamation)
                End Try
            End If
        End If


        '------------------------------------------------------------------------
        If upstream_ROI_is_define = True Then
            If AUTO_mode_boolean = True Then
                Try
                    image_regle.MakeTransparent(Color.PaleTurquoise)
                    For num_colonne = TextBox_upstream_ROI_pixel_origin_value.Text - (NumericUpDown_upstream_ROI_width_pixel.Value) To TextBox_upstream_ROI_pixel_origin_value.Text   'NumericUpDown_upstream_ROI_width_pixel.Value
                        '        ' For num_ligne = 0 To bmap_height - 1
                        image_regle.SetPixel(num_colonne, interval_integration_line - 25, Color.PaleTurquoise)
                        image_regle.SetPixel(num_colonne, bmap_height - interval_integration_line + 25, Color.PaleTurquoise)
                        '        'Next
                    Next

                    For num_ligne = interval_integration_line - 25 To bmap_height - interval_integration_line + 25
                        '        ' For num_ligne = 0 To bmap_height - 1
                        image_regle.SetPixel(TextBox_upstream_ROI_pixel_origin_value.Text - (NumericUpDown_upstream_ROI_width_pixel.Value), num_ligne, Color.PaleTurquoise)
                        image_regle.SetPixel(TextBox_upstream_ROI_pixel_origin_value.Text, num_ligne, Color.PaleTurquoise)
                        '        'Next
                    Next


                    PictureBox_grid.BackColor = Color.Transparent
                    image_regle.MakeTransparent(Color.Blue)
                    PictureBox_grid.Parent = PictureBox_main_image
                    PictureBox_grid.Location = image_regle_location
                    PictureBox_grid.Image = image_regle
                    Debug.Print("upstream_ROI")
                    AUTOMODE_interval_integration_line_boolean = True




                Catch e As Exception
                    MsgBox(e.ToString, MsgBoxStyle.Exclamation)
                End Try
            End If
        End If

        Try
            image_regle.MakeTransparent(Color.PaleGoldenrod)
            For num_ligne = 0 To bmap_width - 1
                image_regle.SetPixel(num_ligne, NumericUpDown_measuredline_pixel.Value, Color.PaleGoldenrod)

            Next
        Catch e As Exception
            MsgBox(e.ToString, MsgBoxStyle.Exclamation)
        End Try


    End Sub

    Private Sub Button_automatic_ROI_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_automatic_ROI.Click
        automatic_determination_of_ROI()
    End Sub

    Public Sub get_distance()

        ' distance in pixel !!
        ' To compute mm  -- CAV3  =>   350 pixels = 47.29mm
        '                   CAV4  =>   350 pixels = 50.87mm


        DataGridView_RAWDATA.Rows.Add(1)

        Dim array_list As New List(Of Double())

        ReDim AUTOMODE_DATA_ARRAY(657)

        ' stopwatch_AUTOMODE_MEAS.Reset()
        '  stopwatch_AUTOMODE_MEAS.Start()
        Try
            If is_cam_initialazed Then
                If Pylon.DeviceGrabSingleFrame(hdev, 0, Grab_buffer, Grab_result, 500) = True Then

                    Grab_result.PayloadType = NET.EPylonPayloadType.PayloadType_Image
                    '     TextBox_branch_diam.Text = Grab_buffer.Array(150).ToString

                End If
            End If

        Catch e As Exception
            MsgBox(e.Message, MsgBoxStyle.Exclamation)
        End Try

        '   DataGridView2.Rows.Add(659)
        Dim i As Integer
        j = measured_line_pixel
        For i = 0 To 655

            AUTOMODE_DATA_ARRAY(i + 1) = Grab_buffer.Array((j * 656) + i)
            '  DataGridView2(1, i).Value = Grab_buffer.Array((j * 656) + i)
            ' DataGridView2(0, i).Value = i


            AUTOMODE_DATA_ARRAY(0) = stopwatch_AUTOMODE_MEAS.ElapsedMilliseconds


            '   AUTOMODE_DATA_ARRAY_string = AUTOMODE_DATA_ARRAY_string & "," & AUTOMODE_DATA_ARRAY(i)


        Next
        '  Dim downstreamROI_size = NumericUpDown_downstream_ROI_width_pixel.Value


        max_pos_downstream = 0

        MAX_downstream = 0
        For i = TextBox_downstream_ROI_pixel_origin_value.Text - Int(NumericUpDown_downstream_ROI_width_pixel.Value / 2) To TextBox_downstream_ROI_pixel_origin_value.Text + Int(NumericUpDown_downstream_ROI_width_pixel.Value / 2) ' To (NumericUpDown_downstream_ROI_width_pixel.Value)



            If AUTOMODE_DATA_ARRAY(i) > MAX_downstream Then
                MAX_downstream = AUTOMODE_DATA_ARRAY(i)
                max_pos_downstream = i '- TextBox_downstream_ROI_pixel_origin_value.Text
            Else
                'MAX_downstream = MAX_downstream

            End If
        Next
        Dim distance_mm As Double
        Dim distance = 0
        Dim max_pos_upstream = 0
        MAX_upstream = 0
        For i = TextBox_upstream_ROI_pixel_origin_value.Text - NumericUpDown_upstream_ROI_width_pixel.Value To TextBox_upstream_ROI_pixel_origin_value.Text
            If AUTOMODE_DATA_ARRAY(i) > MAX_upstream Then
                MAX_upstream = AUTOMODE_DATA_ARRAY(i)
                max_pos_upstream = i
            Else
                'MAX_downstream = MAX_downstream

            End If
        Next
        RichTextBox_pos.Text = " downstream pos =  " & max_pos_downstream & " upstream pos =  " & max_pos_upstream

        distance = (max_pos_upstream - max_pos_downstream)

        distance_mm = (max_pos_upstream - max_pos_downstream) * PIXEL_SIZE_mmperpixel 'TextBox_optic_calibration.Text / dist_cal_pixel
     

        DataGridView_RAWDATA(0, auto_meas_loop).Value = stopwatch_AUTOMODE_MEAS.ElapsedMilliseconds
        DataGridView_RAWDATA(1, auto_meas_loop).Value = distance
        DataGridView_RAWDATA(2, auto_meas_loop).Value = distance_mm
        If DataGridView_MAINDATA.Rows.Count > 1 Then
            DataGridView_RAWDATA.FirstDisplayedCell = DataGridView_RAWDATA(0, DataGridView_RAWDATA.Rows.Count - 1)
        End If
        '  TextBox_minmeniscusposition.Text = MAX_downstream & "----" & (max_pos + TextBox_downstream_ROI_pixel_origin_value.Text)

        '  array_list.Add(AUTOMODE_DATA_ARRAY)
        'For i = 0 To array_list.Count
        ' AUTOMODE_DATA_ARRAY_string. = array_list.
        '       RichTextBox2.Text = AUTOMODE_DATA_ARRAY_string
        ' RichTextBox2.Text = AUTOMODE_DATA_ARRAY(0) & "," & AUTOMODE_DATA_ARRAY(80) & "," & AUTOMODE_DATA_ARRAY(100) & "," & AUTOMODE_DATA_ARRAY(180)

        auto_meas_loop = auto_meas_loop + 1
        '    Next

        ' draw_chart_distvsTime()

        ' compute_equation_for_lp_graph(26, 0.0038533435, 0.0033588239)

    End Sub

    Public Sub compute_equation_ALL_array()

        ReDim equation_array_time(DataGridView_RAWDATA.RowCount - 1)
        ReDim equation_array_ditance_mm(DataGridView_RAWDATA.RowCount - 1)

        For i = 1 To DataGridView_RAWDATA.RowCount - 1
            If CheckBox_outlier_reject.Checked = True Then

                If DataGridView_RAWDATA.Rows(i).Cells(1).Value < NumericUpDown_outlier_threshold.Value And DataGridView_RAWDATA.Rows(i).Cells(1).Value > NumericUpDown_outlier_LOW_threshold.Value Then
                    equation_array_time(i) = (Convert.ToDouble(DataGridView_RAWDATA.Rows(i).Cells("Column_time").Value / 1000)) - (Convert.ToDouble(DataGridView_RAWDATA.Rows(0).Cells("Column_time").Value / 1000))
                    equation_array_ditance_mm(i) = Convert.ToDouble(compute_equation_for_lp_graph(rotor_diameter, DataGridView_RAWDATA.Rows(0).Cells("distance_mm").Value / 1000, DataGridView_RAWDATA.Rows(i).Cells("distance_mm").Value / 1000))

                End If

            Else
                equation_array_time(i) = (Convert.ToDouble(DataGridView_RAWDATA.Rows(i).Cells("Column_time").Value / 1000)) - (Convert.ToDouble(DataGridView_RAWDATA.Rows(0).Cells("Column_time").Value / 1000))
                equation_array_ditance_mm(i) = Convert.ToDouble(compute_equation_for_lp_graph(rotor_diameter, DataGridView_RAWDATA.Rows(0).Cells("distance_mm").Value / 1000, DataGridView_RAWDATA.Rows(i).Cells("distance_mm").Value / 1000))

            End If


        Next

    End Sub


    Public Sub draw_chart_distvsTime()

        compute_equation_ALL_array()

        Dim i As Integer
        Dim j As Integer


        If DataGridView_RAWDATA.RowCount > 1 Then
            i = DataGridView_RAWDATA.RowCount - 1
            '   Debug.Print(DataGridView2.RowCount)
            ReDim arrData_dist(0 To i - 1)
            ReDim arrData_time(0 To i - 1)

            For j = 0 To (i - 1)
                arrData_dist(j) = Convert.ToDouble(DataGridView_RAWDATA.Rows(j).Cells("Column_distance").Value)
                arrData_time(j) = Convert.ToDouble(DataGridView_RAWDATA.Rows(j).Cells("Column_time").Value)
                'RichTextBox2.Text = arrData_dist(j)
                '  RichTextBox2.Text = arrData_time(j)
                '  Clipboard.SetText(arrData_dist(j) & arrData_time(j))
            Next

            Select Case rep
                Case 1
                    Chart3.Series("Series1").Enabled = True
                    Chart3.Series("Series1").Points.DataBindXY(arrData_time, arrData_dist)
                Case 2
                    Chart3.Series("Series2").Enabled = True
                    Chart3.Series("Series2").Points.DataBindXY(arrData_time, arrData_dist)
                Case 3
                    Chart3.Series("Series3").Enabled = True
                    Chart3.Series("Series3").Points.DataBindXY(arrData_time, arrData_dist)
                Case 4
                    Chart3.Series("Series4").Enabled = True
                    Chart3.Series("Series4").Points.DataBindXY(arrData_time, arrData_dist)
                Case 5
                    Chart3.Series("Series5").Enabled = True
                    Chart3.Series("Series5").Points.DataBindXY(arrData_time, arrData_dist)
                Case 6
                    Chart3.Series("Series6").Enabled = True
                    Chart3.Series("Series6").Points.DataBindXY(arrData_time, arrData_dist)
                Case 7
                    Chart3.Series("Series7").Enabled = True
                    Chart3.Series("Series7").Points.DataBindXY(arrData_time, arrData_dist)
                Case 8
                    Chart3.Series("Series8").Enabled = True
                    Chart3.Series("Series8").Points.DataBindXY(arrData_time, arrData_dist)
                    rep = 0

            End Select

        End If
        image_regle.MakeTransparent(Color.Orange)
        For i = 100 To 102
            For j = 0 To DataGridView_RAWDATA.RowCount - 1
                image_regle.SetPixel(max_pos_downstream + DataGridView_RAWDATA.Rows(j).Cells("Column_distance").Value, i + (j * 3), Color.Orange)
                ' image_regle.SetPixel(bmap_width - 20, num_ligne, Color.PaleTurquoise)
                '        'Next
                draw_ROI()
            Next
        Next
        fill_rawdatastring()
    End Sub

    Public Sub fill_rawdatastring()
        rawdata_time_string = ""
        rawdata_distance_pixel_string = ""
        rawdata_distance_mm_string = ""


        For i = 0 To DataGridView_RAWDATA.RowCount - 1
            rawdata_time_string = rawdata_time_string & Convert.ToString(DataGridView_RAWDATA.Rows(i).Cells("Column_time").Value) & ";"
            rawdata_distance_pixel_string = rawdata_distance_pixel_string & Convert.ToString(DataGridView_RAWDATA.Rows(i).Cells("Column_distance").Value) & ";"
            rawdata_distance_mm_string = rawdata_distance_mm_string & Convert.ToString(DataGridView_RAWDATA.Rows(i).Cells("distance_mm").Value) & ";"
        Next

        ' send_data_to_datagridview()
    End Sub
    Public Sub reset_graph_distvstime()

        Chart3.Series(0).Points.Clear()
        Chart3.Series(1).Points.Clear()
        Chart3.Series(2).Points.Clear()
        Chart3.Series(3).Points.Clear()
        Chart3.Series(4).Points.Clear()
        Chart3.Series(5).Points.Clear()
        Chart3.Series(6).Points.Clear()
        Chart3.Series(7).Points.Clear()
        '  Chart3.Series(8).Points.Clear()

    End Sub
    Public Sub reset_graph_functionvstime()
        ScatterGraph1.ClearData()
    End Sub

    Public Sub curve_fit()


        '   RichTextBox_AUTOMODE_stderror_Lp_integral.Text = rep
    


        Dim xData(DataGridView_RAWDATA.RowCount - 1) As Double
        Dim yData(DataGridView_RAWDATA.RowCount - 1) As Double
        Dim xData1(DataGridView_RAWDATA.RowCount - 1) As Double
        Dim yData1(DataGridView_RAWDATA.RowCount - 1) As Double
        Dim xData2(DataGridView_RAWDATA.RowCount - 1) As Double
        Dim yData2(DataGridView_RAWDATA.RowCount - 1) As Double
        Dim xData3(DataGridView_RAWDATA.RowCount - 1) As Double
        Dim yData3(DataGridView_RAWDATA.RowCount - 1) As Double
        Dim xData4(DataGridView_RAWDATA.RowCount - 1) As Double
        Dim yData4(DataGridView_RAWDATA.RowCount - 1) As Double
        Dim xData5(DataGridView_RAWDATA.RowCount - 1) As Double
        Dim yData5(DataGridView_RAWDATA.RowCount - 1) As Double
        Dim xData6(DataGridView_RAWDATA.RowCount - 1) As Double
        Dim yData6(DataGridView_RAWDATA.RowCount - 1) As Double
        Dim xData7(DataGridView_RAWDATA.RowCount - 1) As Double
        Dim yData7(DataGridView_RAWDATA.RowCount - 1) As Double
        Dim xData8(DataGridView_RAWDATA.RowCount - 1) As Double
        Dim yData8(DataGridView_RAWDATA.RowCount - 1) As Double


        Dim weight(DataGridView_RAWDATA.RowCount - 1) As Double
        '   Dim noise(), fittedData() As Double





        Try
            ' Initialization.

            Dim coeffArray() As Double = {}
            '   Dim samples As Integer = samplesNumericEdit.Value
            '   Dim xArray() As Double = New Double(DataGridView2.RowCount - 1) {}
            '  Dim dataArray() As Double = New Double(DataGridView2.RowCount - 1) {}
            Dim fittedArray() As Double = New Double(DataGridView_RAWDATA.RowCount - 1) {}
            Dim fittedArray1() As Double = New Double(DataGridView_RAWDATA.RowCount - 1) {}
            Dim fittedArray2() As Double = New Double(DataGridView_RAWDATA.RowCount - 1) {}
            Dim fittedArray3() As Double = New Double(DataGridView_RAWDATA.RowCount - 1) {}
            Dim fittedArray4() As Double = New Double(DataGridView_RAWDATA.RowCount - 1) {}
            Dim fittedArray5() As Double = New Double(DataGridView_RAWDATA.RowCount - 1) {}
            Dim fittedArray6() As Double = New Double(DataGridView_RAWDATA.RowCount - 1) {}
            Dim fittedArray7() As Double = New Double(DataGridView_RAWDATA.RowCount - 1) {}
            Dim fittedArray8() As Double = New Double(DataGridView_RAWDATA.RowCount - 1) {}


         
            ''    Dim functionGen As NationalInstruments.Analysis.SignalGeneration.BasicFunctionGenerator = New BasicFunctionGenerator(BasicFunctionGeneratorSignal.Sine, 2.0 / samples, BasicFunctionGenerator.DefaultAmplitude, BasicFunctionGenerator.DefaultPhase, BasicFunctionGenerator.DefaultOffset, 1.0, samples)
            'For i As Integer = 0 To weight.Length - 1
            '    weight(i) = 1.0
            'Next
            '          tolerance = 0.0001
            ' Generate the fitted plot.
            xData = equation_array_time
            yData = equation_array_ditance_mm

            '  MsgBox(xData(0).ToString)
            '  MsgBox(yData(0).ToString)
            '  fittedArray = NationalInstruments.Analysis.Math.CurveFit.PolynomialFit(xArray, dataArray, 2, Analysis.Math.PolynomialFitAlgorithm.Svd, coeffArray, mean)

            '        fittedArray = NationalInstruments.Analysis.Math.CurveFit.LinearFit(xData, yData, NationalInstruments.Analysis.Math.FitMethod.LeastSquare, weight, tolerance, slope, intercept, residue)
            '   StdDev_AUTOMODE = NationalInstruments.Analysis.Math.Statistics.StandardDeviation(fittedArray)

            Dim regressiondata
            regressiondata = Stats.Regression(xData(0), xData, yData)

            slope = regressiondata.Regslope
            intercept = regressiondata.regYInt
            residue = regressiondata.ResidualErr
            equ_pearsonRsquare = regressiondata.PearsonsR * regressiondata.PearsonsR
            std_err_lp_automode = 2 * residue
            '' fit array determination
            For i = 0 To DataGridView_RAWDATA.RowCount - 1
                fittedArray(i) = Stats.Regression(xData(i), xData, yData).regValue
                ' MsgBox(fittedArray(i).ToString)
            Next

            Select Case rep
                Case 1
                    ' RichTextBox_AUTOMODE_stderror_Lp_integral.BackColor = Color.Blue
                    xData1 = xData
                    yData1 = yData
                    fittedArray1 = fittedArray
                    dataplot1.PlotXY(xData1, yData1)

                    fittedplot1.PlotXY(xData1, fittedArray1)

                Case 2
                    '  RichTextBox_AUTOMODE_stderror_Lp_integral.BackColor = Color.White
                    xData2 = xData
                    yData2 = yData
                    fittedArray2 = fittedArray
                    dataplot2.PlotXY(xData2, yData2)
                    fittedplot2.PlotXY(xData2, fittedArray2)
                Case 3
                    '      RichTextBox_AUTOMODE_stderror_Lp_integral.BackColor = Color.Red
                    xData3 = xData
                    yData3 = yData
                    fittedArray3 = fittedArray
                    dataplot3.PlotXY(xData3, yData3)
                    fittedplot3.PlotXY(xData3, fittedArray3)
                Case 4
                    xData4 = xData
                    yData4 = yData
                    fittedArray4 = fittedArray
                    dataplot4.PlotXY(xData4, yData4)
                    fittedplot4.PlotXY(xData4, fittedArray4)
                Case 5
                    xData5 = xData
                    yData5 = yData
                    fittedArray5 = fittedArray
                    dataplot5.PlotXY(xData5, yData5)
                    fittedplot5.PlotXY(xData5, fittedArray5)
                Case 6
                    xData6 = xData
                    yData6 = yData
                    fittedArray6 = fittedArray
                    dataplot6.PlotXY(xData6, yData6)
                    fittedplot6.PlotXY(xData6, fittedArray6)
                Case 7
                    xData7 = xData
                    yData7 = yData
                    fittedArray7 = fittedArray
                    dataplot7.PlotXY(xData7, yData7)
                    fittedplot7.PlotXY(xData7, fittedArray7)
                Case 8
                    xData8 = xData
                    yData8 = yData
                    fittedArray8 = fittedArray
                    dataplot8.PlotXY(xData8, yData8)
                    fittedplot8.PlotXY(xData8, fittedArray8)
                    rep = 0


            End Select

            'dataplot1.PlotXY(xData, yData)
            'fittedplot1.PlotXY(xData, fittedArray)
            'dataplot2.PlotXY(xData, yData)
            'fittedplot2.PlotXY(xData, fittedArray)

            'see WANG et al JPH 2014 equation 4
            '     ScatterGraph1.Plots.Add(dataplot2)
            dialog_branchdiameter.compute_crosssectionarea()
            ' Display the mean and coefficient data.
            '   RichTextBox4.Text = "y= " & slope & "X + " & intercept & "  residue=  " & residue & "  R�= " & equ_pearsonR

            RichTextBox4.Text = regressiondata.ToString

            raw_AUTOMODE_meanLp_integral = -1 * slope * crosssectionarea_OF_WATER / ((rotor_diameter / 200) * cavispeed_rad_s_1 * cavispeed_rad_s_1) * 10 ^ 6
            std_err_lp_automode = std_err_lp_automode * crosssectionarea_OF_WATER / ((rotor_diameter / 200) * cavispeed_rad_s_1 * cavispeed_rad_s_1) * 10 ^ 6
            Select Case CAVITRON_SETUP.ComboBox_Lp_SIunit_select.Text
                Case "kg.m-1.Mpa-1.s-1"
                    AUTOMODE_meanLp_integral = raw_AUTOMODE_meanLp_integral / cross_section_area * (rotor_diameter / 100)
                    std_err_lp_automode = std_err_lp_automode / cross_section_area * (rotor_diameter / 100)
                Case "mol.Mpa-1.s-1"
                    AUTOMODE_meanLp_integral = raw_AUTOMODE_meanLp_integral / cross_section_area * (rotor_diameter / 100) / 0.01801528
                    std_err_lp_automode = std_err_lp_automode / cross_section_area * (rotor_diameter / 100) / 0.01801528
                Case "m�.Mpa-1.s-1"

                    AUTOMODE_meanLp_integral = raw_AUTOMODE_meanLp_integral / cross_section_area * (rotor_diameter / 100) / rho
                    std_err_lp_automode = std_err_lp_automode / cross_section_area * (rotor_diameter / 100) / rho
            End Select

            raw_AUTOMODE_meanLp_corrT = raw_AUTOMODE_meanLp_integral * (1.781018648 - 0.05871294 * correction_Temp + 0.00130376457 * correction_Temp ^ 2 - 0.00001801864802 * correction_Temp ^ 3 + 0.00000011235431235 * correction_Temp ^ 4)

            AUTOMODE_meanLp_integral = AUTOMODE_meanLp_integral * (1.781018648 - 0.05871294 * correction_Temp + 0.00130376457 * correction_Temp ^ 2 - 0.00001801864802 * correction_Temp ^ 3 + 0.00000011235431235 * correction_Temp ^ 4)

            RichTextBox_AUTOMODE_meanLp_integral.Text = Format(AUTOMODE_meanLp_integral, "0.#####")
            'STANDARD ERROR = Std DEV / squareroot(n-1)
            'std_err_lp_automode = StdDev_AUTOMODE / (xData.Length) ^ 0.5
            RichTextBox_AUTOMODE_stderror_Lp_integral.Text = std_err_lp_automode


            If is_Lp0set_AUTOMODE = False Then
                RichTextBox_Lp0_automode.Text = AUTOMODE_meanLp_integral
                compute_PLC_automode()
                is_Lp0set_AUTOMODE = True
            Else
                compute_PLC_automode()
            End If

            '     richAUTOMODE_meanLp_integral()

            'mean & "  -  " & coeffArray(0) & "  -  " & coeffArray(1) & "  -  " & coeffArray(2)
            'mseNumericEdit.Value = mean
            'coeff1NumericEdit.Value = coeffArray(0)
            'coeff2NumericEdit.Value = coeffArray(1)
            'coeff3NumericEdit.Value = coeffArray(2)

            ' Plot the data on the graph.
            'dataPlot.PlotXY(xArray, dataArray)
            'fittedPlot.PlotXY(xArray, fittedArray)

        Catch exception As Exception
            MessageBox.Show(exception.Message)

            '   Array.Clear(xData, 0, DataGridView_RAWDATA.RowCount - 1)
            '   Array.Clear(yData, 0, DataGridView_RAWDATA.RowCount - 1)
            ' Array.Clear(fittedArray, 0, DataGridView2.RowCount - 1)

        End Try
    End Sub


    Private Sub Timer_getdistance_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer_getdistance.Tick

        If (stopwatch_AUTOMODE_MEAS.ElapsedMilliseconds / 1000 > NumericUpDown_AUTOMODE_integral_stabilisation_time.Value) Then
            If (stopwatch_AUTOMODE_MEAS.ElapsedMilliseconds / 1000 < NumericUpDown_AUTOMODE_number_of_rep_integral.Value) Then
                '      If auto_meas_loop > -1 And auto_meas_loop < NumericUpDown_AUTOMODE_number_of_rep_integral.Value Then
                If is_cam_initialazed Then
                    get_distance()
                Else
                    MsgBox("YOU NEED TO CONNECT THE CAMERA FIRST!")
                    Timer_getdistance.Stop()
                End If
            Else
                Timer_getdistance.Stop()
                draw_chart_distvsTime()
                Timer_camera.Start()

                curve_fit()
                'If CheckBox_validateboolean.Checked = False Then
                '    send_data_to_datagridview()
                'End If
            End If
        End If


    End Sub

    Private Sub Button7_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_COMPUTE.Click

        '  fill_datagrid_pos()
        Dim i As Int16
        Try
            draw_chart_distvsTime()
            If is_cam_initialazed = True Then
                Timer_camera.Start()
            End If
            Timer_getdistance.Stop()
            curve_fit()
        Catch
        End Try
        '     send_data_to_datagridview()
        If CheckBox_validateboolean.Checked = False And DataGridView_MAINDATA.Rows.Count > 2 Then
            For i = 0 To DataGridView_MAINDATA.ColumnCount - 1
                Me.DataGridView_MAINDATA.Rows(DataGridView_MAINDATA.Rows.Count - 2).Cells(i).Style.ForeColor = Color.Blue
            Next


        End If







        ''  fill_datagrid_pos()
        'draw_chart_distvsTime()
        'Timer_camera.Start()
        '' Timer_getdistance.Stop()
        'curve_fit()
        'If CheckBox_validateboolean.Checked = False Then
        '    send_data_to_datagridview()
        'End If
    End Sub



    Private Sub NumericUpDown3_ValueChanged_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NumericUpDown_measuredline_pixel.ValueChanged
        measured_line_pixel = NumericUpDown_measuredline_pixel.Value
        draw_ROI()
    End Sub

    Function compute_equation_for_lp_graph(ByVal rotor_diameter As Double, ByVal r1 As Double, ByVal r2 As Double)

        integral_equation_result = System.Math.Log(r2 / r1) + System.Math.Log(((2 * rotor_diameter / 200) - r1) / ((2 * rotor_diameter / 200) - r2))
        '   integral_equation_result = Math.Log(((2 * (rotor_diameter / 2 / 100) / r2) - 1) / ((2 * (rotor_diameter / 2 / 100) / r1) - 1))

        Return integral_equation_result
    End Function






    Private Sub Button_validate_meas_AUTOMODE_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_validate_meas_AUTOMODE.Click
        send_data_to_datagridview()
    End Sub



    Public Sub display_intensity_profile()
        Dim xData(656) As Double
        Dim yData(656) As Double
        Dim backgroung_array(656) As Double
        Dim threshold_array(656) As Double

        Array.Clear(xData, 0, 656)
        Array.Clear(yData, 0, 656)
        Array.Clear(backgroung_array, 0, 656)
        Array.Clear(threshold_array, 0, 656)

        Try
            If is_cam_initialazed Then
                If Pylon.DeviceGrabSingleFrame(hdev, 0, Grab_buffer, Grab_result, 500) = True Then

                    Grab_result.PayloadType = NET.EPylonPayloadType.PayloadType_Image
                    '     TextBox_branch_diam.Text = Grab_buffer.Array(150).ToString

                End If

                Dim i As Integer
                j = measured_line_pixel
                For i = 1 To 656

                    yData(i) = Grab_buffer.Array((j * 656) + i - 1)
                    If downstream_ROI_is_define = True Then
                        backgroung_array(i) = avg_background_image_intensity
                        threshold_array(i) = avg_background_image_intensity + CAVITRON_SETUP.NumericUpDown_autodetect_threshold.Value
                    Else
                        backgroung_array(i) = 1
                        threshold_array(i) = 2

                    End If
                    xData(i) = i
                Next

                ScatterPlot1.PlotXY(xData, yData)
                ScatterPlot_background.PlotXY(xData, backgroung_array)
                ScatterPlot_threshold.PlotXY(xData, threshold_array)


            Else
                MsgBox("YOU NEED TO CONNECT THE CAMERA FIRST!")
                Timer_camera.Stop()

            End If
            ScatterGraph_intensityprofile.Visible = True
        Catch e As Exception
            MsgBox(e.Message, MsgBoxStyle.Exclamation)
        End Try

    End Sub


    Private Sub Button_intensity_profile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_intensity_profile.Click


        If lock_intensityprofile = True Then
            lock_intensityprofile = False
            Button_intensity_profile.Text = "display intensity profile"
            Button_intensity_profile.BackColor = Color.Silver

        Else
            lock_intensityprofile = True
            Button_intensity_profile.Text = "intensity profile -LOCKED -    click to unlock"
            Button_intensity_profile.BackColor = Color.Salmon
        End If




    End Sub
    Private Sub Button_intensity_profile_mouseenter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_intensity_profile.MouseEnter
        Timer_intensityprofile.Start()

    End Sub

    Private Sub Button_intensity_profile_mouseleave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_intensity_profile.MouseLeave
        If lock_intensityprofile = False Then
            Timer_intensityprofile.Stop()
            ScatterGraph_intensityprofile.Visible = False
        Else
        End If

    End Sub

    Private Sub Timer_intensityprofile_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer_intensityprofile.Tick
        If is_cam_initialazed Then
            display_intensity_profile()
        End If
    End Sub




    Private Sub Button6_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        Form_graphtype.ShowDialog()
    End Sub


    Private Sub NumericUpDown_AUTOMODE_integral_integration_interval_ms_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NumericUpDown_AUTOMODE_integral_integration_interval_ms.ValueChanged
        Timer_getdistance.Interval = NumericUpDown_AUTOMODE_integral_integration_interval_ms.Value
    End Sub

    Private Sub RichTextBox_PLC_Cochard_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        RichTextBox_MEASMODE_STATUS.Select()
    End Sub

    Private Sub RichTextBox_PLC_integral_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RichTextBox_PLC_integral.GotFocus
        RichTextBox_MEASMODE_STATUS.Select()
    End Sub

    Private Sub Button8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button8.Click
        rep = 0
        reset_graph_distvstime()
        reset_graph_functionvstime()
    End Sub



    Private Sub NumericUpDown_upstream_ROI_width_pixel_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NumericUpDown_upstream_ROI_width_pixel.ValueChanged
        draw_ROI()
    End Sub

    Private Sub NumericUpDown_downstream_ROI_width_pixel_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NumericUpDown_downstream_ROI_width_pixel.ValueChanged
        draw_ROI()
    End Sub

    Private Sub TextBox_upstream_ROI_pixel_origin_value_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox_upstream_ROI_pixel_origin_value.Validated
        draw_ROI()
    End Sub

    Private Sub TextBox_downstream_ROI_pixel_origin_value_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox_downstream_ROI_pixel_origin_value.Validated
        draw_ROI()
    End Sub


    Private Sub Button_RAWDATA_graph_axis_default_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_RAWDATA_graph_axis_default.Click
        If Button_RAWDATA_graph_axis_default.Text = "set axis to default" Then

            Chart3.ChartAreas(0).AxisY.Maximum = 650
            Chart3.ChartAreas(0).AxisY.Minimum = 0
            Button_RAWDATA_graph_axis_default.Text = "ZOOM axis"
        Else
            Chart3.ChartAreas(0).AxisY.Maximum = NumericUpDown_outlier_threshold.Value
            Chart3.ChartAreas(0).AxisY.Minimum = NumericUpDown_outlier_LOW_threshold.Value
            Button_RAWDATA_graph_axis_default.Text = "set axis to default"
        End If

    End Sub





    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_EDIT_ADVANCED_PARAMETERS.Click
        CAVITRON_SETUP.ShowDialog()

    End Sub

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub





    Private Sub ComboBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox_cavi1000_reservoirsize.SelectedIndexChanged
        If ComboBox_cavi1000_reservoirsize.Text = "big" Then
            CAVITRON_SETUP.ComboBox_reservoir_innersurface.SelectedIndex = 2
            CAVITRON_SETUP.SYNC_MY_SETTINGS()
            CAVITRON_SETUP.SAVE_MY_SETTINGS()
        Else
            CAVITRON_SETUP.ComboBox_reservoir_innersurface.SelectedIndex = 1
            CAVITRON_SETUP.SYNC_MY_SETTINGS()
            CAVITRON_SETUP.SAVE_MY_SETTINGS()
        End If
    End Sub


End Class


