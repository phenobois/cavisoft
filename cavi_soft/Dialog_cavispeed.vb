﻿Imports System.Windows.Forms

Public Class dialog_cavispeed
    ' Public cavispeed_rpm
    Public previous_setcavispeed As Double
    Public previous_settension As Double


    Private Sub OK_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK_Button.Click

        If RichTextBox1.Text = "" Then
            RichTextBox1.Text = "0"
        End If


        Select Case CAVITRON_SETUP.ComboBox_cavispeed_setup.SelectedIndex
            Case 0   ' set rpm
                label_dialog_set_cavispeed.Text = "Please enter desired cavitron rotation speed (in rpm) "
                If Convert.ToDouble(RichTextBox1.Text) > Convert.ToDouble(CAVITRON_SETUP.TextBox_maxcentri_rpm.Text) Then
                    MsgBox("this is too high for this rotor !! Please try again or change the maximum value in the advanced parameters")
                    RichTextBox1.Text = ""
                    RichTextBox1.Select()
                Else

                    If RichTextBox1.Text > previous_setcavispeed + 4000 Then
                        MsgBox("this is a big step (more than 4000 rpm ) !! Please try again. If you really want to do that, please enter an intermediate value before")
                        RichTextBox1.Text = ""
                        RichTextBox1.Select()

                    Else

                        Dim r As MsgBoxResult
                        r = MsgBox("you asked a cavispeed of " & RichTextBox1.Text & " rpm. Are you sure??", MsgBoxStyle.YesNo + MessageBoxDefaultButton.Button2, "Apply cavispeed setup")
                        If r = MsgBoxResult.Yes Then

                            MAIN_Form.TextBox_cavispeed_setvalue.Text = RichTextBox1.Text
                            If MAIN_Form.CheckBox_tachymeter_connected.Checked = False Then
                                MAIN_Form.read_Cavispeed_rpm = RichTextBox1.Text
                                MAIN_Form.RichTextBox_centri_rpm.Text = RichTextBox1.Text

                            Else
                                MAIN_Form.RichTextBox_centri_rpm.Text = 0

                            End If

                            MAIN_Form.set_cavispeed_value()


                        End If
                    End If
                End If
            Case 1   'set pressure
                label_dialog_set_cavispeed.Text = "Please enter desired cavitron centrifugation pressure (in MPa) "

                If RichTextBox1.Text > Math.Abs(MAIN_Form.max__possible_tension_MPa) Then
                    MsgBox("this is too high for this rotor !! Please try again or change the maximum value in the advanced parameters")
                    RichTextBox1.Text = ""
                    RichTextBox1.Select()
                Else

                    If RichTextBox1.Text > previous_settension + 3 Then
                        MsgBox("this is a big step (more than 3 MPa) !! Please try again. If you really want to do that, please enter an intermediate value before")
                        RichTextBox1.Text = ""
                        RichTextBox1.Select()

                    Else
                        Dim r As MsgBoxResult

                        r = MsgBox("you asked a pressure of " & RichTextBox1.Text & " MPa. Are you sure??", MsgBoxStyle.YesNo + MessageBoxDefaultButton.Button2, "Apply cavispeed setup")

                        If r = MsgBoxResult.Yes Then
                            MAIN_Form.TextBox_cavispeed_setvalue.Text = RichTextBox1.Text

                            If MAIN_Form.CheckBox_tachymeter_connected.Checked = False Then

                                MAIN_Form.RichTextBox_centri_tension.Text = -RichTextBox1.Text
                            Else
                                MAIN_Form.RichTextBox_centri_tension.Text = 0
                            End If

                            MAIN_Form.set_cavispeed_value()

                        End If
                    End If
                End If

            Case 2   ' manual set
                label_dialog_set_cavispeed.Text = "Please enter the cavitron rotation speed and set the centrifuge value (in rpm) "
                'cavispeed_rpm = RichTextBox1.Text
                If RichTextBox1.Text = "" Then
                    RichTextBox1.Text = "0"
                End If
                MAIN_Form.read_Cavispeed_rpm = RichTextBox1.Text
                MAIN_Form.RichTextBox_centri_rpm.Text = RichTextBox1.Text

        End Select

            'My.Computer.Clipboard.SetText(TextBox1.Text)
            'Form1.RichTextBox1.Text = My.Computer.Clipboard.GetText()

        
        Me.DialogResult = System.Windows.Forms.DialogResult.OK
        MAIN_Form.reset_countdown()
        MAIN_Form.start_countdown()
        MAIN_Form.rep = 0
        MAIN_Form.reset_graph_distvstime()
        MAIN_Form.reset_graph_functionvstime()
      
       

            Me.Close()


       

    End Sub

    Private Sub Cancel_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel_Button.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub
    Private Sub TextBox7_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles RichTextBox1.KeyPress
        e.Handled = MAIN_Form.RT_NumbersOnly(e.KeyChar, RichTextBox1)
    End Sub

    Private Sub dialog_cavispeed_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        RichTextBox1.Select()
        previous_setcavispeed = MAIN_Form.set_Cavispeed_rpm
        previous_settension = MAIN_Form.TextBox_cavispeed_setvalue.Text

        Select Case CAVITRON_SETUP.ComboBox_cavispeed_setup.SelectedIndex
            Case 0

                label_dialog_set_cavispeed.Text = "Please enter desired cavitron rotation speed (in rpm) "
                Label1.Visible = False
                Label2.Visible = False
            Case 1
                label_dialog_set_cavispeed.Text = "Please enter desired cavitron centrifugation pressure (in MPa) "
                Label1.Visible = True
                Label2.Visible = True
            Case 2
                Label1.Visible = False
                Label2.Visible = False
                label_dialog_set_cavispeed.Text = "Please enter the cavitron rotation speed and set the centrifuge value (in rpm) "

        End Select


      

    End Sub

  
End Class
